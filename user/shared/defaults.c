/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 */
/*
 * Router default NVRAM values
 *
 * Copyright 2001-2003, ASUSTek Inc.
 * All Rights Reserved.
 * 
 * THIS SOFTWARE IS OFFERED "AS IS", AND BROADCOM GRANTS NO WARRANTIES OF ANY
 * KIND, EXPRESS OR IMPLIED, BY STATUTE, COMMUNICATION OR OTHERWISE. BROADCOM
 * SPECIFICALLY DISCLAIMS ANY IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A SPECIFIC PURPOSE OR NONINFRINGEMENT CONCERNING THIS SOFTWARE.
 *
 * $Id: defaults.c,v 1.2 2007/06/21 04:55:53 jordan_hsiao Exp $
 */

//#include <epivers.h>
#include <string.h>
#include <nvram/bcmnvram.h>
//#include <nvram/typedefs.h>
//#include <wlioctl.h>

//#define XSTR(s) STR(s)
//#define STR(s) #s

#include "../../mydef.h"

struct nvram_tuple router_defaults[] = {
	/* Restore defaults */
	{ "restore_defaults", "0", 0 },		/* Set to 0 to not restore defaults on boot */

#if 0
	{"ModuleName", "RT2860", 0},
	{"SystemName", "RalinkAP", 0},
	{"WebInit", "1", 0},
	{"HostName", "10.10.10.254", 0},
	{"Login", "admin", 0},
	{"Password", "admin", 0},
#ifdef CONFIG_LAN_WAN_SUPPORT
	{"OperationMode", "1", 0},              //0:Bridge, 1:Gateway, 2:Wireless-ISP
	{"Platform", "ICPLUS", 0},
	{"wanConnectionMode", "DHCP", 0},       //STATIC,DHCP,PPPOE,L2TP,PPTP
	{"wan_ipaddr", "192.168.1.1", 0},
	{"wan_netmask", "255.255.255.0", 0},
	{"wan_gateway", "192.168.1.254", 0},
	{"wan_primary_dns", "192.168.1.5", 0},
	{"wan_secondary_dns", "168.95.1.1", 0},
#else
	{"OperationMode", "0", 0},              //0:Bridge, 1:Gateway, 2:Wireless-ISP
	{"Platform", "MARVELL", 0},
	{"wanConnectionMode", "STATIC", 0},     //STATIC,DHCP,PPPOE,L2TP,PPTP
	{"wan_ipaddr", "10.10.10.254", 0},
	{"wan_netmask", "255.255.255.0", 0},
	{"wan_gateway", "10.10.10.253", 0},
	{"wan_primary_dns", "10.10.10.251", 0},
	{"wan_secondary_dns", "168.95.1.1", 0},
#endif
	{"wan_pppoe_user", "pppoe_user", 0},
	{"wan_pppoe_pass", "pppoe_passwd", 0},
	{"wan_l2tp_ip", "l2tp_server", 0},
	{"wan_l2tp_user", "l2tp_user", 0},
	{"wan_l2tp_pass", "l2tp_passwd", 0},
	{"wan_pptp_ip", "pptp_server", 0},
	{"wan_pptp_user", "pptp_user", 0},
	{"wan_pptp_pass", "pptp_passwd", 0},
	{"lan_ipaddr", "10.10.10.254", 0},
	{"lan_netmask", "255.255.255.0", 0},
	{"dhcpEnabled", "1", 0},
	{"dhcpStart", "10.10.10.100", 0},
	{"dhcpEnd", "10.10.10.200", 0},
	{"dhcpMask", "255.255.255.0", 0},
	{"dhcpPriDns", "10.10.10.251", 0},
	{"dhcpSecDns", "168.95.1.1", 0},
	{"dhcpGateway", "10.10.10.254", 0},
	{"dhcpLease", "86400", 0},
	{"stpEnabled", "0", 0},
	{"lltdEnabled", "0", 0},
	{"natEnabled", "1", 0},
	{"IPPortFilterEnable", "0", 0},
	{"IPPortFilterRules", "", 0},
	{"PortForwardEnable", "0", 0},
	{"PortForwardRules", "", 0},
	{"MacFilterEnable", "0", 0},
	{"MacFilterRules", "", 0},
	{"DefaultFirewallPolicy", "1", 0},
	{"DMZEnable", "0", 0},
	{"DMZIPAddress", "", 0},
	{"TZ", "", 0},
	{"NTPServerIP", "", 0},
	{"NTPSync", "", 0},
	{"DDNSProvider", "", 0},
	{"DDNS", "", 0},
	{"DDNSAccount", "", 0},
	{"DDNSPassword", "", 0},
#endif
	
	{"CountryRegion", "5", 0},
	{"CountryRegionABand", "7", 0},
	{"NetworkType", "Infra", 0}, // Infra or Adhoc
//	{"CountryCode", "", 0},
	{"BssidNum", "1", 0},
//	{"SSID", "RT2880_AP", 0},
//	{"WirelessMode", "9", 0},
//	{"FixedTxMode", "1", 0},
//	{"TxRate", "0", 0},
		{"Channel", "8", 0}, //Paul add 2012/3/16
//	{"BasicRate", "15", 0},
//	{"BeaconPeriod", "100", 0},
//	{"DtimPeriod", "1", 0},
	{"TxPower", "100", 0},
//	{"BGProtection", "0", 0},
	{"DisableOLBC", "0", 0},
//	{"TxAntenna", "", 0},
//	{"RxAntenna", "", 0},
	{"TxPreamble", "0", 0},
//	{"RTSThreshold", "2347", 0},
//	{"FragThreshold", "2346", 0},
	{"TxBurst", "1", 0},
	{"PktAggregate", "1", 0},
//	{"NoForwarding", "0", 0},
//	{"NoForwardingBTNBSSID", "0", 0},
//	{"HideSSID", "0", 0},
	{"ShortSlot", "1", 0},
	{"AutoChannelSelect", "0", 0}, //Paul add 2012/3/16
	{"WiFiTest", "0", 0},
	{"TGnWifiTest", "0", 0},
//	{"AccessPolicy0", "0", 0},
//	{"AccessControlList0", "", 0},
	{"AccessPolicy1", "0", 0},
	{"AccessControlList1", "", 0},
	{"AccessPolicy2", "0", 0},
	{"AccessControlList2", "", 0},
	{"AccessPolicy3", "0", 0},
	{"AccessControlList3", "", 0},

	{"HT_AutoBA", "1", 0},
	{"HT_HTC", "1", 0},
	{"HT_RDG", "1", 0},
	{"HT_LinkAdapt", "0", 0},
	{"HT_BW", "0", 0},
	{"HT_EXTCHA", "1", 0},
	{"HT_OpMode", "0", 0},
	{"HT_MpduDensity", "5", 0},
	{"HT_AMSDU", "0", 0},
	{"HT_GI", "1", 0},
	{"HT_BAWinSize", "64", 0},
	{"HT_MCS", "33", 0},
	{"HT_BADecline", "0", 0},
	{"HT_TxStream", "2", 0},
	{"HT_RxStream", "2", 0},
	{"HT_STBC", "1", 0},
	{"HT_PROTECT", "1", 0},
	{"HT_MIMOPSMode", "3", 0},
//	{"HT_40MHZ_INTOLERANT", "0", 0},
	{"HT_DisallowTKIP", "1", 0},

	{"McastPhyMode", "0", 0},
	{"McastMcs", "0", 0},

	{"GreenAP", "1", 0},

/*
	{"WscConfMode", "0", 0},
	{"WscConfStatus", "2", 0},
	{"WscAKMP", "1", 0},
	{"WscConfigured", "0", 0},
	{"WscModeOption", "0", 0},
	{"WscActionIndex", "9", 0},
	{"WscPinCode", "", 0},
	{"WscRegResult", "1", 0},
	{"WscUseUPnP", "1", 0},
	{"WscUseUFD", "0", 0},
	{"WscSSID", "RalinkInitialAP", 0},
	{"WscKeyMGMT", "WPA-EAP", 0},
	{"WscConfigMethod", "138", 0},
	{"WscAuthType", "1", 0},
	{"WscEncrypType", "1", 0},
	{"WscNewKey", "scaptest", 0},
*/

//	{"WmmCapable", "1", 0},
	{"APAifsn", "3;7;1;1", 0},
	{"APCwmin", "4;4;3;2", 0},
	{"APCwmax", "6;10;4;3", 0},
	{"APTxop", "0;0;94;47", 0},
	{"APACM", "0;0;0;0", 0},
	{"BSSAifsn", "3;7;2;2", 0},
	{"BSSCwmin", "4;4;3;2", 0},
	{"BSSCwmax", "10;10;4;3", 0},
	{"BSSTxop", "0;0;94;47", 0},
	{"BSSACM", "0;0;0;0", 0},
//	{"AckPolicy", "0;0;0;0", 0},
	{"APSDCapable", "0", 0},
	{"DLSCapable", "0", 0},

	{"IEEE80211H", "0", 0},
	{"CSPeriod", "6", 0},
	{"RDRegion", "0", 0},
	{"WirelessEvent", "0", 0},

//	{"AuthMode", "OPEN", 0},
//	{"EncrypType", "NONE", 0},
//	{"DefaultKeyID", "1", 0},
//	{"Key1Str", "", 0},
//	{"Key2Str", "", 0},
//	{"Key3Str", "", 0},
//	{"Key4Str", "", 0},
//	{"Key1Type", "0", 0},	
//	{"Key2Type", "0", 0},
//	{"Key3Type", "0", 0},
//	{"Key4Type", "0", 0},
//	{"WPAPSK", "12345678", 0},
	{"RekeyMethod", "TIME", 0},
//	{"RekeyInterval", "86400", 0},
	{"PMKCachePeriod", "10", 0},
	{"PreAuth", "0", 0},

//	{"WdsEnable", "0", 0},
	{"WdsPhyMode", "0"},		//0:CCK, 1:OFDM, 2:HTMIX, 3:GREENFIELD
//	{"WdsEncrypType", "NONE", 0},	//NONE, WEP, TKIP, AES
//	{"WdsList", "", 0},
//	{"WdsKey", "", 0},

//	{"IEEE8021X", "0", 0},
//	{"RADIUS_Server", "", 0},
//	{"RADIUS_Port", "1812", 0},
//	{"RADIUS_Key", "", 0},
	{"RADIUS_Acct_Server", "", 0},
	{"RADIUS_Acct_Port", "1813", 0},
	{"RADIUS_Acct_Key", "", 0},
	{"EAPifname", "br0", 0},
	{"PreAuthifname", "br0", 0},
	{"session_timeout_interval", "0", 0},
	{"idle_timeout_interval", "0", 0},

	{"IgmpSnEnable", "1", 0},

	/* OS parameters */
	{ "os_name", "", 0 },			/* OS name string */
//	{ "os_version", EPI_VERSION_STR, 0 },	/* OS revision */
//	{ "os_date", __DATE__, 0 },		/* OS date */

	/* Miscellaneous parameters */
	{ "timer_interval", "3600", 0 },	/* Timer interval in seconds */
	{ "ntp_server", "192.5.41.40 192.5.41.41 133.100.9.2", 0 },		/* NTP server */
	{ "time_zone", "PST8PDT", 0 },		/* Time zone (GNU TZ format) */
	{ "log_level", "0", 0 },		/* Bitmask 0:off 1:denied 2:accepted */
	{ "upnp_enable", "0", 0 },		/* Start UPnP */
	{ "ezc_enable", "1", 0 },		/* Enable EZConfig updates */
//	{ "ezc_version", EZC_VERSION_STR, 0 },	/* EZConfig version */
	{ "is_default", "1", 0 },		/* is it default setting: 1:yes 0:no */
	{ "os_server", "", 0 },			/* URL for getting upgrades */
	{ "stats_server", "", 0 },		/* URL for posting stats */
	{ "console_loglevel", "1", 0 },		/* Kernel panics only */

	/* Big switches */
	{ "router_disable", "0", 0 },		/* lan_proto=static lan_stp=0 wan_proto=disabled */
	{ "ure_disable", "1", 0 },		/* sets APSTA for radio and puts wirelesss interfaces in correct lan */
	{ "fw_disable", "0", 0 },		/* Disable firewall (allow new connections from the WAN) */
	//{ "fw_disable", "1", 0 },		/* Disable firewall (allow new connections from the WAN) */	// win7 logo
	{ "log_ipaddr", "", 0 },		/* syslog recipient */

	/* LAN H/W parameters */
	{ "lan_ifname", "br0", 0 },		/* LAN interface name */
	{ "lan_ifnames", "br0", 0 },		/* Enslaved LAN interfaces */
	{ "lan_hwnames", "", 0 },		/* LAN driver names (e.g. et0) */
	{ "lan_hwaddr", "", 0 },		/* LAN interface MAC address */

	/* LAN TCP/IP parameters */
	{ "lan_dhcp", "0", 0 },			/* DHCP client [static|dhcp] */
	{ "lan_ipaddr", "192.168.1.1", 0 },	/* LAN IP address */
	//{ "lan_ipaddr", "192.168.0.1", 0 },	/* LAN IP address */		// for win7 logo test
	{ "lan_netmask", "255.255.255.0", 0 },	/* LAN netmask */
	{ "lan_gateway", "192.168.1.1", 0 },	/* LAN gateway */
	//{ "lan_gateway", "192.168.0.1", 0 },	/* LAN gateway */		// for win7 logo test
	{ "lan_proto", "dhcp", 0 },		/* DHCP server [static|dhcp] */
	{ "lan_wins", "", 0 },			/* x.x.x.x x.x.x.x ... */
	{ "lan_domain", "", 0 },		/* LAN domain name */
	{ "lan_lease", "86400", 0 },		/* LAN lease time in seconds */
	{ "lan_stp", "1", 0 },			/* LAN spanning tree protocol */
	{ "lan_route", "", 0 },			/* Static routes (ipaddr:netmask:gateway:metric:ifname ...) */

//#ifdef __CONFIG_NAT__
	/* WAN H/W parameters */
	{ "wan_ifname", "eth2.2", 0 },		/* WAN interface name */
	{ "wan_ifnames", "eth2.2", 0 },		/* WAN interface names */
	{ "wan_hwname", "", 0 },		/* WAN driver name (e.g. et1) */
	{ "wan_hwaddr", "", 0 },		/* WAN interface MAC address */
	{ "cur_hwaddr", "", 0 },		/* eth MAC address */

	/* WAN TCP/IP parameters */
	{ "wan_proto", "dhcp", 0 },		/* [static|dhcp|pppoe|disabled] */
	//{ "wan_proto", "static", 0 },		/* [static|dhcp|pppoe|disabled] */	// win7 logo
	{ "wan_ipaddr", "0.0.0.0", 0 },	/* WAN IP address */
	{ "wan_netmask", "0.0.0.0", 0 },	/* WAN netmask */
	{ "wan_gateway", "0.0.0.0", 0 },	/* WAN gateway */
	//{ "wan_ipaddr", "17.1.1.1", 0 },	/* WAN IP address */	// win7 logo
	//{ "wan_netmask", "255.255.255.0", 0 },	/* WAN netmask */	// win7 logo
	//{ "wan_gateway", "17.1.1.1", 0 },	/* WAN gateway */	// win7 logo
	{ "wan_dns", "", 0 },			/* x.x.x.x x.x.x.x ... */
	{ "wan_wins", "", 0 },			/* x.x.x.x x.x.x.x ... */
	{ "wan_hostname", "", 0 },		/* WAN hostname */
	{ "wan_domain", "", 0 },		/* WAN domain name */
	{ "wan_lease", "86400", 0 },		/* WAN lease time in seconds */

	/* PPPoE parameters */
	{ "wan_pppoe_ifname", "ppp0", 0 },		/* PPPoE enslaved interface */
	{ "wan_pppoe_username", "", 0 },	/* PPP username */
	{ "wan_pppoe_passwd", "", 0 },		/* PPP password */
	//{ "wan_pppoe_idletime", "60", 0 },	/* Dial on demand max idle time (seconds) */
	{ "wan_pppoe_idletime", "0", 0 },	// oleg patch
	{ "wan_pppoe_keepalive", "0", 0 },	/* Restore link automatically */
	{ "wan_pppoe_demand", "0", 0 },		/* Dial on demand */
	{ "wan_pppoe_mru", "1492", 0 },		/* Negotiate MRU to this value */
	{ "wan_pppoe_mtu", "1492", 0 },		/* Negotiate MTU to the smaller of this value or the peer MRU */
	{ "wan_pppoe_service", "", 0 },		/* PPPoE service name */
	{ "wan_pppoe_ac", "", 0 },		/* PPPoE access concentrator name */

	/* Misc WAN parameters */
	{ "wan_desc", "", 0 },			/* WAN connection description */
	{ "wan_route", "", 0 },			/* Static routes (ipaddr:netmask:gateway:metric:ifname ...) */
	{ "wan_primary", "0", 0 },		/* Primary wan connection */
	{ "wan_unit", "0", 0 },			/* Last configured connection */

	/* Filters */
	{ "filter_maclist", "", 0 },		/* xx:xx:xx:xx:xx:xx ... */
	{ "filter_macmode", "deny", 0 },	/* "allow" only, "deny" only, or "disabled" (allow all) */
	{ "filter_client0", "", 0 },		/* [lan_ipaddr0-lan_ipaddr1|*]:lan_port0-lan_port1,
						 * proto,enable,day_start-day_end,sec_start-sec_end,desc */

	/* Port forwards */
	{ "dmz_ipaddr", "", 0 },		/* x.x.x.x (equivalent to 0-60999>dmz_ipaddr: 0-60999) */
	{ "forward_port0", "", 0 },		/* wan_port0-wan_port1>lan_ipaddr: lan_port0-lan_port1[:,]proto[:,]enable[:,]desc */
	{ "autofw_port0", "", 0 },		/* out_proto:out_port,in_proto:in_port0-in_port1>to_port0-to_port1,enable,desc */

	/* DHCP server parameters */
	{ "dhcp_start", "192.168.1.2", 0 },	/* First assignable DHCP address */
	//{ "dhcp_start", "192.168.0.2", 0 },	/* First assignable DHCP address */	// for win7 logo test
	{ "dhcp_end", "192.168.1.254", 0 },	/* Last assignable DHCP address */	
	//{ "dhcp_end", "192.168.0.254", 0 },	/* Last assignable DHCP address */	// for win7 logo test
	{ "dhcp_domain", "wan", 0 },		/* Use WAN domain name first if available (wan|lan) */
	{ "dhcp_wins", "wan", 0 },		/* Use WAN WINS first if available (wan|lan) */
//#endif	/* __CONFIG_NAT__ */

	/* Web server parameters */
	{ "http_username", "", 0 },		/* Username */
	{ "http_passwd", "admin", 0 },		/* Password */
	{ "http_wanport", "", 0 },		/* WAN port to listen on */
	{ "http_lanport", "80", 0 },		/* LAN port to listen on */

	/* Wireless parameters */
#if 0	
	{ "wl_ifname", "", 0 },			/* Interface name */
	{ "wl_hwaddr", "", 0 },			/* MAC address */
	{ "wl_phytype", "b", 0 },		/* Current wireless band ("a" (5 GHz), "b" (2.4 GHz), or "g" (2.4 GHz)) */
	{ "wl_phytypes", "", 0 },		/* List of supported wireless bands (e.g. "ga") */
	{ "wl_corerev", "", 0 },		/* Current core revision */
	{ "wl_radioids", "", 0 },		/* List of radio IDs */
	{ "wl_ssid", "Broadcom", 0 },		/* Service set ID (network name) */
	{ "wl_antdiv", "-1", 0 },		/* Antenna Diversity (-1|0|1|3) */
	{ "wl_infra", "1", 0 },			/* Network Type (BSS/IBSS) */
	{ "wl_nbw", "20", 0},			/* N-BW */
	{ "wl_nctrlsb", "none", 0},		/* N-CTRL SB */
	{ "wl_nband", "2", 0},			/* N-BAND */
	{ "wl_nmcsidx", "-1", 0},		/* MCS Index for N - rate */
	{ "wl_nmode", "-1", 0},			/* N-mode */
	{ "wl_nreqd", "0", 0},			/* Require 802.11n support */
	{ "wl_vlan_prio_mode", "off", 0},	/* VLAN Priority support */
	{ "wl_leddc", "0x640000", 0},		/* 100% duty cycle for LED on router */
	{ "wl_afterburner", "off", 0 },		/* AfterBurner */
	{ "wl_frameburst", "off", 0 },		/* BRCM Frambursting mode (off|on) */
	{ "wl_maxassoc", "128", 0},		/* Max associations driver could support */
	{ "wl_unit", "0", 0 },			/* Last configured interface */
	{ "wl_sta_retry_time", "5", 0 },	/* Seconds between association attempts */	
#endif

	{ "wl_country_code", "", 0 },		/* Country Code (default obtained from driver) */
	{ "wl_ssid", "ASUS", 0},
	{ "wl_gmode", "2", 0 },			/* 54g mode */
	{ "wl_channel", "8", 0 },		/* Channel number */  //Paul change to default 8, 2012/3/16
	{ "wl_rateset", "default", 0 },		/* "default" or "all" or "12" */
	{ "wl_bcn", "100", 0 },			/* Beacon interval */
	{ "wl_dtim", "1", 0 },			/* DTIM period */
	{ "wl_gmode_protection", "auto", 0 },	/* 802.11g RTS/CTS protection (off|auto) */
	{ "wl_rts", "2347", 0 },		/* RTS threshold */
	{ "wl_frag", "2346", 0 },		/* Fragmentation threshold */
	{ "wl_ap_isolate", "0", 0 },            /* AP isolate mode */
	{ "wl_closed", "0", 0 },		/* Closed (hidden) network */
	{ "wl_macmode", "disabled", 0 },	/* "allow" only, "deny" only, or "disabled"(allow all) */
	{ "wl_maclist", "", 0 },		/* xx:xx:xx:xx:xx:xx ... */
	{ "wl_wme", "1", 0 },			/* WME mode (off|on) */
	{ "wl_wme_no_ack", "off", 0},           /* WME No-Acknowledgment mode */
	{ "wl_auth_mode", "open", 0 },		/* Network authentication mode Open System */
	{ "wl_key", "1", 0 },			/* Current WEP key */
	{ "wl_key1", "", 0 },			/* 5/13 char ASCII or 10/26 char hex */
	{ "wl_key2", "", 0 },			/* 5/13 char ASCII or 10/26 char hex */
	{ "wl_key3", "", 0 },			/* 5/13 char ASCII or 10/26 char hex */
	{ "wl_key4", "", 0 },			/* 5/13 char ASCII or 10/26 char hex */
	{ "wl_key_type", "0", 0 } ,		/* WEP key format (HEX/ASCII)*/
	{ "wl_mrate", "0", 0 },			/* Mcast Rate (bps, 0 for auto) */

	/* WPA parameters */
	{ "wl_crypto", "tkip", 0 },		/* WPA data encryption */
	{ "wl_wpa_psk", "12345678", 0 },	/* WPA pre-shared key */
	{ "wl_wpa_gtk_rekey", "0", 0 },		/* GTK rotation interval */

	{ "wl_radius_ipaddr", "", 0 },		/* RADIUS server IP address */
	{ "wl_radius_port", "1812", 0 },	/* RADIUS server UDP port */
	{ "wl_radius_key", "", 0 },		/* RADIUS shared secret */

	{ "wl_lazywds", "0", 0 },		/* Enable "lazy" WDS mode (0|1) */
        { "ap_scanning", "0", 0 },
        { "updating", "0", 0 },
        { "ap_selecting", "0", 0 },
        { "httpd_check_ddns", "0", 0 },
        { "uploading", "0", 0 },
        { "pppd_way", "2", 0 },
        { "btn_rst", "0", 0 },          	// ate test     
        { "btn_ez", "0", 0 },           	// ate test

	/* Paul modify 2012/3/12, dsl-n1x iptv port disabled by default*/
        { "wan_stb_x", "0", 0 },           	// oleg patch
        { "wan_pppoe_options_x", "", 0 },	// oleg patch
        { "wan_pptp_options_x", "", 0},		// oleg patch
        { "fw_dos_x", "0", 0 },			// oleg patch
        { "dr_enable_x", "1", 0 },		// oleg patch
        { "mr_enable_x", "0", 0 },		// oleg patch
#if 0
	{ "wl_mode", "ap", 0 },			/* AP mode (ap|sta|wds) */
	{ "wl_auth", "0", 0 },			/* Shared key authentication optional (0) or required (1) */
	{ "wl_net_reauth", "36000", 0 },	/* Network Re-auth/PMK caching duration */
	{ "wl_akm", "", 0 },			/* WPA akm list */
	{ "wl_reg_mode", "off", 0 },		/* Regulatory: 802.11H(h)/802.11D(d)/off(off) */
	{ "wl_dfs_preism", "60", 0 },		/* 802.11H pre network CAC time */
	{ "wl_dfs_postism", "60", 0 },		/* 802.11H In Service Monitoring CAC time */
	{ "wl_wep", "disabled", 0 },		/* WEP data encryption (enabled|disabled) */
	{ "wl_rate", "0", 0 },			/* Rate (bps, 0 for auto) */
	{ "wl_plcphdr", "long", 0 },		/* 802.11b PLCP preamble type */
	{ "wl_radio", "1", 0 },			/* Enable (1) or disable (0) radio */

	/* WME parameters (cwmin cwmax aifsn txop_b txop_ag adm_control oldest_first) */

	/* EDCA parameters for STA */
	{ "wl_wme_sta_be", "15 1023 3 0 0 off off", 0 },	/* WME STA AC_BE parameters */
	{ "wl_wme_sta_bk", "15 1023 7 0 0 off off", 0 },	/* WME STA AC_BK parameters */
	{ "wl_wme_sta_vi", "7 15 2 6016 3008 off off", 0 },	/* WME STA AC_VI parameters */
	{ "wl_wme_sta_vo", "3 7 2 3264 1504 off off", 0 },	/* WME STA AC_VO parameters */

	/* EDCA parameters for AP */
	{ "wl_wme_ap_be", "15 63 3 0 0 off off", 0 },		/* WME AP AC_BE parameters */
	{ "wl_wme_ap_bk", "15 1023 7 0 0 off off", 0 },		/* WME AP AC_BK parameters */
	{ "wl_wme_ap_vi", "7 15 1 6016 3008 off off", 0 },	/* WME AP AC_VI parameters */
	{ "wl_wme_ap_vo", "3 7 1 3264 1504 off off", 0 },	/* WME AP AC_VO parameters */

	{ "wl_wme_apsd", "on", 0},		/* WME APSD mode */
#endif




/// from flash.default
// for the nvrams of Group type {
// Wireless
// RBRList
{"wl_wdslist_x", "", 0},

// ACLList
{"wl_maclist_x", "", 0},

// LAN
// ManualDHCPList
{"dhcp_staticmac_x", "", 0},
{"dhcp_staticip_x", "", 0},

// GWStatic
{"sr_ipaddr_x", "", 0},
{"sr_netmask_x", "", 0},
{"sr_gateway_x", "", 0},
{"sr_matric_x", "", 0},
{"sr_if_x", "", 0},

// WAN
// x_USRRuleList
{"qos_service_name_x", "", 0},
{"qos_ip_x", "", 0},
{"qos_port_x", "", 0},
{"qos_prio_x", "", 0},

// TriggerList
{"autofw_outport_x", "", 0},
{"autofw_outproto_x", "", 0},
{"autofw_inport_x", "", 0},
{"autofw_inproto_x", "", 0},
{"autofw_desc_x", "", 0},

// VSList
{"vts_port_x", "", 0},
{"vts_ipaddr_x", "", 0},
{"vts_lport_x", "", 0},
{"vts_proto_x", "", 0},
{"vts_protono_x", "", 0},
{"vts_desc_x", "", 0},

// USB Application
// Storage_UserList
{"acc_username", "", 0},
{"acc_password", "", 0},

// Firewall
// UrlList
{"url_keyword_x", "", 0},

// MFList
{"macfilter_list_x", "", 0},

// LWFilterList
{"filter_lw_srcip_x", "", 0},
{"filter_lw_srcport_x", "", 0},
{"filter_lw_dstip_x", "", 0},
{"filter_lw_dstport_x", "", 0},
{"filter_lw_proto_x", "", 0},
{"filter_lw_desc_x", "", 0},
// for the nvrams of Group type }

{"http_username", "admin", 0},

{"http_passwd", "admin", 0},

{"wan_proto", "", 0},

{"x_DHCPClient", "1", 0},	// 2008.03 James.

{"wan_mode_x", "2", 0},

{"wan_etherspeed_x", "auto", 0},

{"wan_route_x", "IP_Routed", 0},

{"wan_nat_x", "1", 0},

{"wan_ipaddr", "", 0},

{"wan_netmask", "", 0},

{"wan_gateway", "", 0},

{"wan_dnsenable_x", "1", 0},

{"wan_dns1_x", "", 0},

{"wan_dns2_x", "", 0},

{"dmz_ip", "", 0},

{"sp_battle_ips", "1", 0},

{"fw_pt_rtsp", "0", 0},

{"vts_enable_x", "0", 0},

{"vts_num_x", "0", 0},

{"autofw_enable_x", "0", 0},

{"autofw_num_x", "0", 0},

{"wan_pppoe_username", "", 0},

{"wan_pppoe_passwd", "", 0},

{"wan_pppoe_idletime", "0", 0},

{"wan_pppoe_txonly_x", "0", 0},

{"wan_pppoe_mtu", "1492", 0},

{"wan_pppoe_mru", "1492", 0},

{"wan_pppoe_service", "", 0},

{"wan_pppoe_ac", "", 0},

{"wan_pppoe_relay_x", "0", 0},

{"wan_pppoe_options_x", "", 0},

{"wan_pptp_options_x", "", 0},

{"wan_hostname", "", 0},

{"wan_hwaddr_x", "", 0},

{"def_hwaddr_x", "", 0}, //2008.08 magic

{"wan_heartbeat_x", "", 0},

{"wan_proto_t", "", 0},

{"wan_ipaddr_t", "", 0},

{"wan_netmask_t", "", 0},

{"wan_gateway_t", "", 0},

{"wan_dns_t", "", 0},

{"wan_status_t", "", 0},

{"ddns_status_t", "", 0},

{"fw_enable_x", "1", 0},
//{"fw_enable_x", "0", 0},	// win7 logo

{"fw_log_x", "none", 0},

{"fw_dos_x", "0", 0},

{"misc_natlog_x", "0", 0},

{"misc_http_x", "0", 0},

{"misc_httpport_x", "8080", 0},

{"misc_lpr_x", "0", 0},

{"misc_ping_x", "0", 0},

{"misc_conntrack_x", "4096", 0},

{"fw_wl_enable_x", "0", 0},

{"filter_wl_date_x", "1111111", 0},

{"filter_wl_time_x", "00002359", 0},

{"filter_wl_default_x", "ACCEPT", 0},

{"filter_wl_icmp_x", "", 0},

{"fw_lw_enable_x", "0", 0},

{"filter_lw_date_x", "1111111", 0},

{"filter_lw_time_x", "00002359", 0},

{"filter_lw_default_x", "ACCEPT", 0},

{"filter_lw_icmp_x", "", 0},

{"url_enable_x", "0", 0},

{"url_date_x", "1111111", 0},

{"url_time_x", "00002359", 0},

{"url_num_x", "0", 0},

{"filter_wl_num_x", "0", 0},

{"filter_lw_num_x", "0", 0},

//{"macfilter_enable_x", "disabled", 0},
{"macfilter_enable_x", "0", 0},	// 2008.01 James.

{"macfilter_num_x", "0", 0},

{"filter_wl_srcip_x", "", 0},

{"filter_wl_srcport_x", "", 0},

{"filter_wl_dstip_x", "", 0},

{"filter_wl_dstport_x", "", 0},

{"filter_wl_proto_x", "", 0},

{"sr_enable_x", "0", 0},

{"sr_rip_x", "0", 0},

{"sr_num_x", "0", 0},

{"dr_static_rip_x", "0", 0},

{"dr_static_matric_x", "1", 0},

{"dr_default_x", "1", 0},

//{"dr_static_rip_x", "0", 0},

{"dr_staticnum_x", "0", 0},

{"dr_staticipaddr_x", "", 0},

{"dr_staticnetmask_x", "0", 0},

{"dr_staticgateway_x", "", 0},

{"lan_proto_x", "1", 0},

//{"lan_ipaddr", "192.168.1.1", 0}, //2008.08 magic

{"lan_netmask", "255.255.255.0", 0},

{"lan_ipaddr_t", "", 0},

{"lan_netmask_t", "", 0},

{"lan_gateway_t", "", 0},

{"lan_hostname", "", 0},

{"lan_gateway", "", 0},

{"dhcp_enable_x", "1", 0},

{"lan_domain", "", 0},

{"dhcp_start", "192.168.1.2", 0}, //2008.08 magic
//{"dhcp_start", "192.168.0.2", 0}, // for win7 logo test

{"dhcp_end", "192.168.1.254", 0}, //2008.08 magic
//{"dhcp_end", "192.168.0.254", 0}, // for win7 logo test

{"dhcp_lease", "86400", 0},

{"dhcp_gateway_x", "", 0},

{"dhcp_dns1_x", "", 0},

{"dhcp_wins_x", "", 0},

{"dhcp_static_x", "0", 0},

{"dhcp_staticnum_x", "0", 0},

{"log_ipaddr", "", 0},

{"upnp_enable", "1", 0}, //2008.08 magic

{"time_zone", "UCT12", 0}, //GMT0

{"time_interval", "20", 0},

{"ntp_server1", "time.nist.gov", 0},

{"ntp_server0", "pool.ntp.org", 0}, //2008.08 magic

//{"ntp_server1", "", 0},	// oleg patch //2008.08 magic

//{"wl_gmode", "2", 0}, //2008.08 magic

{"wl_gmode_protection_x", "1", 0}, //2008.08 magic

{"ddns_enable_x", "0", 0},

{"ddns_server_x", "", 0},

{"ddns_username_x", "", 0},

{"ddns_passwd_x", "", 0},

{"ddns_hostname_x", "", 0},

{"ddns_wildcard_x", "0", 0},

{"wl_macmode", "disabled", 0},

{"wl_macapply_x", "Both", 0},

{"wl_macnum_x", "0", 0},

{"dr_enable_x", "1", 0},

{"mr_enable_x", "0", 0},

{"wl_radius_ipaddr", "", 0},

{"wl_radius_port", "1812", 0},

{"wl_radius_key", "", 0},

{"wl_country_code", "", 0}, //2008.08 magic

{"wl_ssid", "ASUS", 0},

{"wl_ssid2", "ASUS", 0},

{"wl_mode_x", "0", 0},

{"wl_channel", "8", 0}, //Paul change to default 8, 2012/3/16

{"wl_wdsapply_x", "0", 0},

{"wl_lazywds", "0", 0},

{"wl_wdsnum_x", "0", 0},

{"wl_auth_mode", "open", 0},

//{"wl_crypto", "0", 0},
{"wl_crypto", "tkip", 0},	// 2008.06 James.
{"wl0.1_crypto", "tkip", 0},	// 2008.06 James.
{"wl0.2_crypto", "tkip", 0},	// 2008.06 James.
{"wl0.3_crypto", "tkip", 0},	// 2008.06 James.

{"wl_wpa_psk", "", 0},

{"wl_wep_x", "0", 0},

{"wl_phrase_x", "", 0},

{"wl_key1", "", 0},

{"wl_key2", "", 0},

{"wl_key3", "", 0},

{"wl_key4", "", 0},

{"wl_key", "1", 0},

{"wl_wpa_gtk_rekey", "0", 0},

{"wl_afterburner", "off", 0},

{"wl_closed", "0", 0},

{"wl_ap_isolate", "0", 0},

{"wl_rate", "0", 0},

{"wl_rateset", "default", 0},

{"wl_frag", "2346", 0},

{"wl_rts", "2347", 0},

{"wl_dtim", "1", 0},

{"wl_bcn", "100", 0},

//{"wl_frameburst", "0", 0},
{"wl_frameburst", "off", 0},	// 2008.01 James

{"wl_mode_ex", "ap", 0},

{"wl_radio_x", "1", 0},

{"wl_radio_date_x", "1111111", 0},

{"wl_radio_time_x", "00002359", 0},

{"wl_radio_power_x", "17", 0},

//{"wl_wme", "off", 0},

//{"wl_wme_no_ack", "off", 0},

//{"wl_preauth", "1", 0},
{"wl_preauth", "disabled", 0},	// 2008.07 James.

{"wl_net_reauth", "36000", 0},

{"wl_macmode", "disabled", 0},

{"wl_macapply_x", "Both", 0},

{"wl_macnum_x", "0", 0},

{"wl_radius_ipaddr", "", 0},

{"wl_radius_port", "1812", 0},

{"wl_radius_key", "", 0},

#ifdef DLM
{"usb_webenable_x", "1", 0},

{"usb_webmode_x", "1", 0},

{"usb_webdriver_x", "0", 0},

{"usb_webimage_x", "1", 0},

{"usb_websense_x", "1", 0},

{"usb_webrectime_x", "0", 0},

{"usb_webfresh_x", "1", 0},

{"usb_webcaption_x", "Web Camera Live Demo!!!", 0},

{"usb_webhttpport_x", "7776", 0},

{"usb_webhttpcheck_x", "0", 0},

{"usb_webactivex_x", "7777", 0},

{"usb_websecurity_x", "0", 0},

{"usb_websecurity_date_x", "1111111", 0},

{"usb_websecurity_time_x", "00002359", 0},

{"usb_websendto_x", "", 0},

{"usb_webmserver_x", "", 0},

{"usb_websubject_x", "Motion detection alert!!!", 0},

{"usb_webattach_x", "1", 0},

{"usb_webremote_x", "LAN Only", 0},

{"usb_webremote1_x", "", 0},

{"usb_webremote2_x", "", 0},

{"usb_webremote3_x", "", 0},

{"usb_webremote4_x", "", 0},

{"usb_webremote5_x", "", 0},

{"usb_webremote6_x", "", 0},

{"usb_ftpenable_x", "1", 0},

{"usb_ftpanonymous_x", "1", 0},

{"usb_ftpsuper_x", "0", 0},

{"usb_ftpport_x", "21", 0},

{"usb_ftpmax_x", "12", 0},

{"usb_ftptimeout_x", "120", 0},

{"usb_ftpstaytimeout_x", "240", 0},

{"usb_ftpscript_x", "", 0},

{"usb_ftpnum_x", "0", 0},

{"usb_bannum_x", "0", 0},

{"qos_rulenum_x", "0", 0},

{"usb_ftpusername_x", "", 0},

{"usb_ftppasswd_x", "", 0},

{"usb_ftpmaxuser_x", "", 0},

{"usb_ftprights_x", "", 0},

{"usb_ftpbanip_x", "", 0},
#else

{"usb_ftpenable_x", "1", 0},
                                                                                                                             
{"usb_ftpport_x", "21", 0},

{"usb_webhttpport_x", "7776", 0},
                                                                                                                             
{"usb_webactivex_x", "7777", 0},

#endif

//{"qos_uminbw_x", "", 0},

/*
{"wl_guest_enable", "0", 0},
{"wl_guest_ssid_1", "guest", 0},
{"wl_guest_auth_mode_1", "open", 0},
{"wl_guest_crypto_1", "0", 0},
{"wl_guest_wpa_psk_1", "", 0},
{"wl_guest_wep_x_1", "0", 0},
{"wl_guest_phrase_x_1", "", 0},
{"wl_guest_key1_1", "", 0},
{"wl_guest_key2_1", "", 0},
{"wl_guest_key3_1", "", 0},
{"wl_guest_key4_1", "", 0},
{"wl_guest_key_1", "1", 0},
{"wl_guest_wpa_gtk_rekey_1", "0", 0},
{"lan1_ipaddr", "192.168.2.1", 0},
{"lan1_netmask", "255.255.255.0", 0},
{"dhcp1_enable_x", "1", 0},
{"dhcp1_start", "192.168.2.2", 0},
{"dhcp1_end", "192.168.2.254", 0},
{"lan1_lease", "86400", 0},
*/
// following are added by Jiahao for WL500gP
#ifdef DLM
{"acc_num", "0", 0},
{"st_samba_mode", "1", 0},
{"st_ftp_mode", "1", 0},
{"enable_ftp", "1", 0},
{"enable_samba", "1", 0},
{"st_max_user", "5", 0},
#if DSL_N12U == 1
{"computer_name", "DSL-N12U", 0},
{"computer_nameb", "DSL-N12U", 0},
#else
{"computer_name", "DSL-N10", 0},
{"computer_nameb", "DSL-N10", 0}, // 2008.03 James.
#endif
{"st_samba_workgroup", "WORKGROUP", 0},
{"st_samba_workgroupb", "WORKGROUP", 0}, // 2008.03 James.

{"apps_dl", "1", 0},
{"apps_dl_share", "0", 0},
{"apps_dl_seed", "24", 0},
{"apps_dms", "1", 0},
{"apps_caps", "0", 0},
{"apps_comp", "0", 0},
{"apps_pool", "harddisk/part0", 0},
{"apps_share", "share", 0},
{"apps_ver", "", 0},
{"apps_seed", "0", 0},
{"apps_upload_max", "0", 0},
#if DSL_N12U == 1
{"machine_name", "DSL-N12U", 0},
#else
{"machine_name", "DSL-N10", 0},
#endif
#endif

{"temp_lang", "", 0},

#ifdef DLM
{"apps_dms_usb_port", "1", 0},
{"apps_running", "0", 0},
{"apps_dl_share_port_from", "6880", 0},
{"apps_dl_share_port_to", "6999", 0},
#endif

{"httpd_die_reboot", "", 0},

#ifdef DLM
{"sh_num", "0", 0},
//{"sh_", "", 0},	// 2007.12 James.
//{"acc_", "", 0},	// 2007.12 James.
#endif

//{"qos_", "", 0},	// 2007.12 James.

{"console_loglevel", "7", 0},
{"qos_enable","0", 0},
{"qos_global_enable","0", 0},
{"qos_userdef_enable","0", 0},
{"qos_shortpkt_prio","0", 0}, //Internet
{"qos_pshack_prio","0", 0},   //Game
{"qos_tos_prio","0", 0},
{"qos_userspec_app","0", 0},
{"qos_service_enable","0", 0},
{"qos_service_ubw","0", 0},
{"qos_manual_ubw","0", 0},	// 2008.05 James.
{"qos_dfragment_enable","0", 0},
{"qos_dfragment_size","0", 0},
{"ftp_lang", "", 0},

 /* WSC parameters */
{ "wps_enable", "0", 0 },
//{ "wps_enable", "1", 0 },	// win7 logo
{ "wps_mode", "2", 0 },
{ "wsc_config_state", "0", 0 },         /* config state unconfiged */
//{ "wsc_manufacture", "ASUSTek Computer Inc.", 0 },
//{ "wsc_model_name", "Wireless Router", 0 },
//{ "wsc_device_name", "ASUS Wireless Router", 0 },
#if DSL_N12U == 1
{ "wsc_model_number", "DSL-N12U", 0 },
#else
{ "wsc_model_number", "DSL-N10", 0 },
#endif

{ "wsc_mode", "enabled", 0 },		/* disable wsc */
//{ "wl_wsc_mode", "enabled", 0 },	/* disable: disable wsc  enabled: enable wsc*/
//{ "wsc_device_pin", "12345670", 0 },            /* wsc default PIN*/
//{ "wsc_method", "1", 0}, //2008.09 magic
//{ "wsc_sta_pin", "0", 0 },		
//{ "wsc_uuid", "0x000102030405060708090A0B0C0D0EBB", 0 },
//{ "wsc_cli", "0", 0 },	
//{ "wsc_client_role", "", 0 },	

{ "wcn_enable", "1", 0},

{ "wl_wpa_mode", "0", 0},
{ "wl0.1_wpa_mode", "0", 0},
{ "wl0.2_wpa_mode", "0", 0},
{ "wl0.3_wpa_mode", "0", 0},

//{ "lltd_device_name", "ASUS_ROUTER", 0},/* Cherry added in 2007/2/27. */

{ "x_Setting", "0", 0 },    // 2007.10 James
{ "r_Setting", "0", 0 },

{ "asusddns_tos_agreement", "0", 0},	// 2007.12 James

{ "asus_mfg", "", 0 },	// 2008.03 James.
{ "asus_mfg_webcam", "", 0 },	// 2008.03 James.
{ "asus_mfg_flash", "", 0 },	// 2008.03 James.
{ "btn_rst", "", 0 },	// 2008.03 James.
{ "btn_ez", "", 0 },	// 2008.03 James.

 /* APCLI/STA parameters */
{ "sta_clonemac", "ff:ff:ff:ff:ff:ff", 0 },
{ "sta_ssid", "", 0 },
{ "sta_bssid", "", 0 },
{ "sta_auth_mode", "open", 0 },
{ "sta_wep_x", "0", 0 },
{ "sta_wpa_mode", "1", 0 },
{ "sta_crypto", "tkip", 0 },
{ "sta_wpa_psk", "12345678", 0 },
{ "sta_key", "1", 0 },
{ "sta_key_type", "0", 0 },
{ "sta_key1", "", 0 },
{ "sta_key2", "", 0 },
{ "sta_key3", "", 0 },
{ "sta_key4", "", 0 },
{ "sta_check_ha", "0", 0 },
{ "sta_authorized", "0", 0 },
{ "sta_connected", "0", 0 },

{ "record_lanaddr", "", 0 },
{ "telnetd", "0", 0 },

/* Multiple SSID */
{"mbss1_enabled", "0", 0},
{"mbss1_ssid", "ASUS_MSSID_1", 0},
{"mbss1_auth_mode", "open", 0},
{"mbss1_crypto", "tkip", 0},
{"mbss1_key", "1", 0},
{"mbss1_key_type", "0", 0},
{"mbss1_key1", "", 0},
{"mbss1_key2", "", 0},
{"mbss1_key3", "", 0},
{"mbss1_key4", "", 0},
{"mbss1_wep_x", "0", 0},
{"mbss1_wpa_psk", "", 0},
{"mbss1_wpa_mode", "0", 0},
{"mbss1_closed", "0", 0},
{"mbss1_ap_isolate", "0", 0},
{"mbss1_nolan", "0", 0},
{"mbss1_nowan", "0", 0},
{"mbss1_priority", "1", 0},

{"mbss2_enabled", "0", 0},
{"mbss2_ssid", "ASUS_MSSID_2", 0},
{"mbss2_auth_mode", "open", 0},
{"mbss2_crypto", "tkip", 0},
{"mbss2_key", "1", 0},
{"mbss2_key_type", "0", 0},
{"mbss2_key1", "", 0},
{"mbss2_key2", "", 0},
{"mbss2_key3", "", 0},
{"mbss2_key4", "", 0},
{"mbss2_wep_x", "0", 0},
{"mbss2_wpa_psk", "", 0},
{"mbss2_wpa_mode", "0", 0},
{"mbss2_closed", "0", 0},
{"mbss2_ap_isolate", "0", 0},
{"mbss2_nolan", "0", 0},
{"mbss2_nowan", "0", 0},
{"mbss2_priority", "1", 0},

{"mbss3_enabled", "0", 0},
{"mbss3_ssid", "ASUS_MSSID_3", 0},
{"mbss3_auth_mode", "open", 0},
{"mbss3_crypto", "tkip", 0},
{"mbss3_key", "1", 0},
{"mbss3_key_type", "0", 0},
{"mbss3_key1", "", 0},
{"mbss3_key2", "", 0},
{"mbss3_key3", "", 0},
{"mbss3_key4", "", 0},
{"mbss3_wep_x", "0", 0},
{"mbss3_wpa_psk", "", 0},
{"mbss3_wpa_mode", "0", 0},
{"mbss3_closed", "0", 0},
{"mbss3_ap_isolate", "0", 0},
{"mbss3_nolan", "0", 0},
{"mbss3_nowan", "0", 0},
{"mbss3_priority", "1", 0},
#if DSL_N12U == 1
{ "Dev3G", "AUTO", 0},
{ "hsdpa_apn", "Internet", 0},
{ "hsdpa_country", "", 0},
{ "hsdpa_isp", "", 0},
{ "hsdpa_dialnum", "*99#", 0},
{ "hsdpa_user", "", 0},
{ "hsdpa_pass", "", 0},
//{ "hsdpa_enable", "0", 0},
{ "wan_3g_pin", "", 0},
{ "wan_3g_puk", "", 0},
{ "EVDO_on", "", 0},
#endif
{ "dhcp_enable_x", "1", 0 },
{ "sw_mode", "1", 0}, //DSL-N10
{ "dsl_config_num", "0", 0},
{ "dsl_modulation", "5", 0},
{ "dsl_annex", "4", 0}, //Paul 2012/4/18, change Annex mode from Annex A(0) to Annex A/I/J/L/M(4)
{ "dsl_snrm_offset", "0", 0}, /* Paul add 2012/9/24, for SNR Margin tweaking. */
{ "dsl_sra", "1", 0}, /* Paul add 2012/10/15, for setting SRA. */
{ "preferred_lang", "EN", 0 },
{ "dsl_nat", "1", 0 },
// 0
{ "dsl_vpi0", "0", 0 },
{ "dsl_vci0", "0", 0 },
{ "dsl_encap0", "0", 0 },
{ "dsl_mode0", "0", 0 },
{ "dsl_proto0", "0", 0 },
{ "dsl_ifc_name0", "", 0 },
{ "dsl_pppoe_username0", "", 0 },
{ "dsl_pppoe_passwd0", "", 0 },
{ "dsl_pppoe_idletime0", "0", 0 },
{ "dsl_pppoe_options0", "", 0 },
{ "dsl_pppoe_mtu0", "576", 0 },
{ "dsl_pppoe_mru0", "576", 0 },
{ "dsl_pppoe_service0", "", 0 },
{ "dsl_pppoe_ac0", "", 0 },
{ "dsl_pppoe_options_x0", "", 0 },
{ "dsl_pppoe_dial_on_demand0", "", 0 },
{ "dsl_DHCPClient0", "0", 0 },
{ "dsl_ipaddr0", "10.0.0.1", 0 },
{ "dsl_netmask0", "10.0.0.2", 0 },
{ "dsl_gateway0", "10.0.0.3", 0 },
{ "dsl_dnsenable0", "0", 0 },
{ "dsl_dns10", "10.0.0.4", 0 },
{ "dsl_dns20", "10.0.0.5", 0 },
{ "dsl_svc_cat0", "0", 0 },
{ "dsl_pcr0", "0", 0 },
{ "dsl_scr0", "0", 0 },
{ "dsl_mbs0", "0", 0 },
// 1
{ "dsl_vpi1", "0", 0 },
{ "dsl_vci1", "0", 0 },
{ "dsl_encap1", "0", 0 },
{ "dsl_mode1", "0", 0 },
{ "dsl_proto1", "0", 0 },
{ "dsl_ifc_name1", "", 0 },
{ "dsl_pppoe_username1", "", 0 },
{ "dsl_pppoe_passwd1", "", 0 },
{ "dsl_pppoe_idletime1", "0", 0 },
{ "dsl_pppoe_options1", "", 0 },
{ "dsl_pppoe_mtu1", "576", 0 },
{ "dsl_pppoe_mru1", "576", 0 },
{ "dsl_pppoe_service1", "", 0 },
{ "dsl_pppoe_ac1", "", 0 },
{ "dsl_pppoe_options_x1", "", 0 },
{ "dsl_pppoe_dial_on_demand1", "", 0 },
{ "dsl_DHCPClient1", "0", 0 },
{ "dsl_ipaddr1", "10.0.0.1", 0 },
{ "dsl_netmask1", "10.0.0.2", 0 },
{ "dsl_gateway1", "10.0.0.3", 0 },
{ "dsl_dnsenable1", "0", 0 },
{ "dsl_dns11", "10.0.0.4", 0 },
{ "dsl_dns21", "10.0.0.5", 0 },
{ "dsl_svc_cat1", "0", 0 },
{ "dsl_pcr1", "0", 0 },
{ "dsl_scr1", "0", 0 },
{ "dsl_mbs1", "0", 0 },
// 2
{ "dsl_vpi2", "0", 0 },
{ "dsl_vci2", "0", 0 },
{ "dsl_encap2", "0", 0 },
{ "dsl_mode2", "0", 0 },
{ "dsl_proto2", "0", 0 },
{ "dsl_ifc_name2", "", 0 },
{ "dsl_pppoe_username2", "", 0 },
{ "dsl_pppoe_passwd2", "", 0 },
{ "dsl_pppoe_idletime2", "0", 0 },
{ "dsl_pppoe_options2", "", 0 },
{ "dsl_pppoe_mtu2", "576", 0 },
{ "dsl_pppoe_mru2", "576", 0 },
{ "dsl_pppoe_service2", "", 0 },
{ "dsl_pppoe_ac2", "", 0 },
{ "dsl_pppoe_options_x2", "", 0 },
{ "dsl_pppoe_dial_on_demand2", "", 0 },
{ "dsl_DHCPClient2", "0", 0 },
{ "dsl_ipaddr2", "10.0.0.1", 0 },
{ "dsl_netmask2", "10.0.0.2", 0 },
{ "dsl_gateway2", "10.0.0.3", 0 },
{ "dsl_dnsenable2", "0", 0 },
{ "dsl_dns12", "10.0.0.4", 0 },
{ "dsl_dns22", "10.0.0.5", 0 },
{ "dsl_svc_cat2", "0", 0 },
{ "dsl_pcr2", "0", 0 },
{ "dsl_scr2", "0", 0 },
{ "dsl_mbs2", "0", 0 },
// 3
{ "dsl_vpi3", "0", 0 },
{ "dsl_vci3", "0", 0 },
{ "dsl_encap3", "0", 0 },
{ "dsl_mode3", "0", 0 },
{ "dsl_proto3", "0", 0 },
{ "dsl_ifc_name3", "", 0 },
{ "dsl_pppoe_username3", "", 0 },
{ "dsl_pppoe_passwd3", "", 0 },
{ "dsl_pppoe_idletime3", "0", 0 },
{ "dsl_pppoe_options3", "", 0 },
{ "dsl_pppoe_mtu3", "576", 0 },
{ "dsl_pppoe_mru3", "576", 0 },
{ "dsl_pppoe_service3", "", 0 },
{ "dsl_pppoe_ac3", "", 0 },
{ "dsl_pppoe_options_x3", "", 0 },
{ "dsl_pppoe_dial_on_demand3", "", 0 },
{ "dsl_DHCPClient3", "0", 0 },
{ "dsl_ipaddr3", "10.0.0.1", 0 },
{ "dsl_netmask3", "10.0.0.2", 0 },
{ "dsl_gateway3", "10.0.0.3", 0 },
{ "dsl_dnsenable3", "0", 0 },
{ "dsl_dns13", "10.0.0.4", 0 },
{ "dsl_dns23", "10.0.0.5", 0 },
{ "dsl_svc_cat3", "0", 0 },
{ "dsl_pcr3", "0", 0 },
{ "dsl_scr3", "0", 0 },
{ "dsl_mbs3", "0", 0 },
// 4
{ "dsl_vpi4", "0", 0 },
{ "dsl_vci4", "0", 0 },
{ "dsl_encap4", "0", 0 },
{ "dsl_mode4", "0", 0 },
{ "dsl_proto4", "0", 0 },
{ "dsl_ifc_name4", "", 0 },
{ "dsl_pppoe_username4", "", 0 },
{ "dsl_pppoe_passwd4", "", 0 },
{ "dsl_pppoe_idletime4", "0", 0 },
{ "dsl_pppoe_options4", "", 0 },
{ "dsl_pppoe_mtu4", "576", 0 },
{ "dsl_pppoe_mru4", "576", 0 },
{ "dsl_pppoe_service4", "", 0 },
{ "dsl_pppoe_ac4", "", 0 },
{ "dsl_pppoe_options_x4", "", 0 },
{ "dsl_pppoe_dial_on_demand4", "", 0 },
{ "dsl_DHCPClient4", "0", 0 },
{ "dsl_ipaddr4", "10.0.0.1", 0 },
{ "dsl_netmask4", "10.0.0.2", 0 },
{ "dsl_gateway4", "10.0.0.3", 0 },
{ "dsl_dnsenable4", "0", 0 },
{ "dsl_dns14", "10.0.0.4", 0 },
{ "dsl_dns24", "10.0.0.5", 0 },
{ "dsl_svc_cat4", "0", 0 },
{ "dsl_pcr4", "0", 0 },
{ "dsl_scr4", "0", 0 },
{ "dsl_mbs4", "0", 0 },
// 5
{ "dsl_vpi5", "0", 0 },
{ "dsl_vci5", "0", 0 },
{ "dsl_encap5", "0", 0 },
{ "dsl_mode5", "0", 0 },
{ "dsl_proto5", "0", 0 },
{ "dsl_ifc_name5", "", 0 },
{ "dsl_pppoe_username5", "", 0 },
{ "dsl_pppoe_passwd5", "", 0 },
{ "dsl_pppoe_idletime5", "0", 0 },
{ "dsl_pppoe_options5", "", 0 },
{ "dsl_pppoe_mtu5", "576", 0 },
{ "dsl_pppoe_mru5", "576", 0 },
{ "dsl_pppoe_service5", "", 0 },
{ "dsl_pppoe_ac5", "", 0 },
{ "dsl_pppoe_options_x5", "", 0 },
{ "dsl_pppoe_dial_on_demand5", "", 0 },
{ "dsl_DHCPClient5", "0", 0 },
{ "dsl_ipaddr5", "10.0.0.1", 0 },
{ "dsl_netmask5", "10.0.0.2", 0 },
{ "dsl_gateway5", "10.0.0.3", 0 },
{ "dsl_dnsenable5", "0", 0 },
{ "dsl_dns15", "10.0.0.4", 0 },
{ "dsl_dns25", "10.0.0.5", 0 },
{ "dsl_svc_cat5", "0", 0 },
{ "dsl_pcr5", "0", 0 },
{ "dsl_scr5", "0", 0 },
{ "dsl_mbs5", "0", 0 },
// 6
{ "dsl_vpi6", "0", 0 },
{ "dsl_vci6", "0", 0 },
{ "dsl_encap6", "0", 0 },
{ "dsl_mode6", "0", 0 },
{ "dsl_proto6", "0", 0 },
{ "dsl_ifc_name6", "", 0 },
{ "dsl_pppoe_username6", "", 0 },
{ "dsl_pppoe_passwd6", "", 0 },
{ "dsl_pppoe_idletime6", "0", 0 },
{ "dsl_pppoe_options6", "", 0 },
{ "dsl_pppoe_mtu6", "576", 0 },
{ "dsl_pppoe_mru6", "576", 0 },
{ "dsl_pppoe_service6", "", 0 },
{ "dsl_pppoe_ac6", "", 0 },
{ "dsl_pppoe_options_x6", "", 0 },
{ "dsl_pppoe_dial_on_demand6", "", 0 },
{ "dsl_DHCPClient6", "0", 0 },
{ "dsl_ipaddr6", "10.0.0.1", 0 },
{ "dsl_netmask6", "10.0.0.2", 0 },
{ "dsl_gateway6", "10.0.0.3", 0 },
{ "dsl_dnsenable6", "0", 0 },
{ "dsl_dns16", "10.0.0.4", 0 },
{ "dsl_dns26", "10.0.0.5", 0 },
{ "dsl_svc_cat6", "0", 0 },
{ "dsl_pcr6", "0", 0 },
{ "dsl_scr6", "0", 0 },
{ "dsl_mbs6", "0", 0 },
// 7
{ "dsl_vpi7", "0", 0 },
{ "dsl_vci7", "0", 0 },
{ "dsl_encap7", "0", 0 },
{ "dsl_mode7", "0", 0 },
{ "dsl_proto7", "0", 0 },
{ "dsl_ifc_name7", "", 0 },
{ "dsl_pppoe_username7", "", 0 },
{ "dsl_pppoe_passwd7", "", 0 },
{ "dsl_pppoe_idletime7", "0", 0 },
{ "dsl_pppoe_options7", "", 0 },
{ "dsl_pppoe_mtu7", "576", 0 },
{ "dsl_pppoe_mru7", "576", 0 },
{ "dsl_pppoe_service7", "", 0 },
{ "dsl_pppoe_ac7", "", 0 },
{ "dsl_pppoe_options_x7", "", 0 },
{ "dsl_pppoe_dial_on_demand7", "", 0 },
{ "dsl_DHCPClient7", "0", 0 },
{ "dsl_ipaddr7", "10.0.0.1", 0 },
{ "dsl_netmask7", "10.0.0.2", 0 },
{ "dsl_gateway7", "10.0.0.3", 0 },
{ "dsl_dnsenable7", "0", 0 },
{ "dsl_dns17", "10.0.0.4", 0 },
{ "dsl_dns27", "10.0.0.5", 0 },
{ "dsl_svc_cat7", "0", 0 },
{ "dsl_pcr7", "0", 0 },
{ "dsl_scr7", "0", 0 },
{ "dsl_mbs7", "0", 0 },

//Paul add 2011/10/12
// change to another name
//{ "failover_enable", "0", 0 }, 
//{ "primary_wan", "dslwan", 0},

{ "use_eth_wan", "0", 0 },
{ "ethwan_proto", "dhcp", 0},
{ "ethwan_DHCPClient", "1", 0},
{ "ethwan_ipaddr", "", 0 },
{ "ethwan_netmask", "", 0 },
{ "ethwan_gateway", "", 0 },
{ "ethwan_dnsenable_x", "1", 0 },
{ "ethwan_dns1_x", "", 0},
{ "ethwan_dns2_x", "", 0},
{ "ethwan_pppoe_username", "", 0 },
{ "ethwan_pppoe_passwd", "", 0 },
{ "ethwan_pppoe_dial_on_demand", "0", 0 },
{ "ethwan_pppoe_idletime", "0", 0 },
{ "ethwan_pppoe_txonly_x", "0", 0 },
{ "ethwan_pppoe_mtu", "1492", 0 },
{ "ethwan_pppoe_mru", "1492", 0 },
{ "ethwan_pppoe_service", "", 0 },
{ "ethwan_pppoe_ac", "", 0 },
{ "ethwan_pppoe_options_x", "", 0 },
//{ "ethwan_pptp_options_x", "", 0 },
{ "ethwan_pppoe_relay_x", "0", 0 },
{ "ethwan_heartbeat_x", "", 0 },
{ "ethwan_hostname", "", 0 },
{ "ethwan_hwaddr_x", "", 0 },
{ "ethwan_upnp_enable", "1", 0 },

{ "udpxy_enable_x", "0", 0},

//------------------------end of N10 , 1.0.0.4 ----


{ "use_failover_eth_wan", "0", 0},
{ "failover_3g_enable", "0", 0 }, 
{ "hsdpa_enable", "0", 0 }, 



	{ 0, 0, 0 }
};

