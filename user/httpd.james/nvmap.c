/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 */
#ifndef NOUSB
#include <stdio.h>
#include <stdlib.h>
#include <nvram/bcmnvram.h>


#define XSTR(s) STR(s)
#define STR(s) #s

typedef struct nvmap
{
	char *sid;
	char *fid;
	char *name;
} nvmap;

struct nvmap maps[] = {
{ "BRIPAddress", "IPRouters", "currentip"},
{ "general.log", "ProductID", "productid" },
{ "General", "x_Username", "username" },
{ "General", "x_Password", "password" },
{ "LANHostConfig", "x_LANDHCPClient", "lan_proto"},
{ "LANHostConfig", "IPRouters", "lan_ipaddr"},
{ "LANHostConfig", "SubnetMask", "lan_netmask"},
{ "LANHostConfig", "x_Gateway", "lan_gateway"},
{ "LANHostConfig", "DHCPServerConfigurable", "dhcp_proto"},
{ "LANHostConfig", "DomainName", "dhcp_domain"},
{ "LANHostConfig", "MinAddress", "dhcp_start"},
{ "LANHostConfig", "MaxAddress", "dhcp_end"},
{ "LANHostConfig", "LeaseTime", "dhcp_leases"},
{ "WLANConfig11b", "SSID", "wl_ssid" },
{ "WLANConfig11b", "x_APMode", "wl_wdsmode_x"},
{ "WLANConfig11b", "Channel", "wl_channel"},
{ "WLANConfig11b", "x_BRApply", "wl_wdsapply_x"},
{ "WLANConfig11b", "x_BRestrict", "wl_lazywds"},
{ "WLANConfig11b", "RBRListCount", "wl_wds_num_x"},
{ "WLANConfig11b", "RBRList", "wl_wdslist_x"},
{ "WLANConfig11b", "DataRateAll", "wl_rate"},
{ "WLANConfig11b", "x_Mode11g", "wl_gmode_x"},
{ "WLANConfig11b", "x_GProtection", "wl_gmode_protection_x"},
{ "WLANConfig11b", "AuthenticationMethod", "wl_authmode_x"},
{ "WLANConfig11b", "WEPType", "wl_weptype_x"},
{ "WLANConfig11b", "x_Phrase", "wl_wpapsk"},
{ "WLANConfig11b", "WEPKey1", "wl_wepkey1"},
{ "WLANConfig11b", "WEPKey2", "wl_wepkey2"},
{ "WLANConfig11b", "WEPKey3", "wl_wepkey3"},
{ "WLANConfig11b", "WEPKey4", "wl_wepkey4"},
{ "WLANConfig11b", "WEPDefaultKey", "wl_wepkey"},
{ "WLANConfig11b", "x_BlockBCSSID", "wl_closed"},
{ "WLANConfig11b", "DataRate", "wl_rateset_x"},
{ "WLANConfig11b", "x_Rekey", "wl_rekey"},
{ "WLANConfig11b", "x_Frag", "wl_frag"},
{ "WLANConfig11b", "x_RTS", "wl_rts"},
{ "WLANConfig11b", "x_DTIM", "wl_dtim"},
{ "WLANConfig11b", "x_Beacon", "wl_beacon"},
{ "WLANConfig11b", "x_FrameBurst", "wl_framburst_x"},
{ "WLANConfig11b", "x_BRhwaddr", "wl_wds"},

{ "DeviceSecurity11a", "ACLMethod", "wl_macmode_x"},
{ "DeviceSecurity11a", "ACLListCount", "wl_maclist_num_x"},
{ "DeviceSecurity11a", "MacAddr", "wl_maclist_x"},
{ "WLANAuthentication11a", "ExAuthDBIPAddr", "wl_radius_ipaddr"},
{ "WLANAuthentication11a", "ExAuthDBPortNumber", "wl_radius_port"},
{ "WLANAuthentication11a", "ExAuthDBPassword", "wl_radius_key"},


{ "Storage", "StorageApps", "st_apps"},
{ "Storage", "SharedCount", "sh_num"},
{ "Storage", "SharedPath", "sh_path"},
{ "Storage", "SharedName", "sh_name"},
{ "Storage", "SharedEnable", "sh_rright_x"},
{ "Storage", "WriteEnable", "sh_wright_x"},
{ "Storage", "UserCount", "acc_num"},
{ "Storage", "UserName", "acc_username"},
{ "Storage", "UserPassword", "acc_password"},
{ "Storage", "x_SharedPath", "sh_path_x"},
{ "Storage", "x_SharedName", "sh_name_x"},
{ "Storage", "x_SharedComment", "sh_comment_x"},
{ "Storage", "x_SharedMaxUser", "sh_max_user_x"},
{ "Storage", "x_AccAny", "sh_accuser_x0"},
{ "Storage", "x_AccAny_Share", "sh_accuser_share_x0"},
{ "Storage", "x_AccAny_Write", "sh_accuser_write_x0"},
{ "Storage", "x_AccUser1", "sh_accuser_x1"},
{ "Storage", "x_AccUser1_Share", "sh_accuser_share_x1"},
{ "Storage", "x_AccUser1_Write", "sh_accuser_write_x1"},
{ "Storage", "x_AccUser2", "sh_accuser_x2"},
{ "Storage", "x_AccUser2_Share", "sh_accuser_share_x2"},
{ "Storage", "x_AccUser2_Write", "sh_accuser_write_x2"},
{ "Storage", "x_AccUser3", "sh_accuser_x3"},
{ "Storage", "x_AccUser3_Share", "sh_accuser_share_x3"},
{ "Storage", "x_AccUser3_Write", "sh_accuser_write_x3"},
{ "Storage", "x_AccUser4", "sh_accuser_x4"},
{ "Storage", "x_AccUser4_Share", "sh_accuser_share_x4"},
{ "Storage", "x_AccUser4_Write", "sh_accuser_write_x4"},
{ "Storage", "x_AccUser5", "sh_accuser_x5"},
{ "Storage", "x_AccUser5_Share", "sh_accuser_share_x5"},
{ "Storage", "x_AccUser5_Write", "sh_accuser_write_x5"},
{ "Storage", "x_AccUser6", "sh_accuser_x6"},
{ "Storage", "x_AccUser6_Share", "sh_accuser_share_x6"},
{ "Storage", "x_AccUser6_Write", "sh_accuser_write_x6"},
{ NULL, NULL, NULL }};

/* This function is used to map nvram value from asus to Broadcom */
void readFromNvram()
{	
	char tmpstr[32], tmpstr1[32], macbuf[18];
	char list[2048];
	int i, num;

	printf("read from nvram\n");

	/* Wireless Section */
	/* GMODE */
	strcpy(tmpstr, nvram_get("wl_gmode_x"));

	if (strcmp(tmpstr, "1")==0) 
	{
		nvram_set("wl0_gmode", XSTR(GMODE_PERFORMANCE));
	}
	else if (strcmp(tmpstr, "2")==0) 
	{
		nvram_set("wl0_gmode", XSTR(GMODE_LRS));
	}
	else if (strcmp(tmpstr, "3")==0) 	
	{
		nvram_set("wl0_gmode", XSTR(GMODE_B_DEFERRED));
	}
	else
	{
		nvram_set("wl0_gmode", XSTR(GMODE_AUTO));
	}

	if (nvram_match("wl_gmode_protection_x", "0")==0)
		nvram_set("wl0_gmode_protection", "off");
	else nvram_set("wl0_gmode_protection", "auto");

	/* Authentication and Encryption */
	strcpy(tmpstr, nvram_get("wl_authmode_x"));
	strcpy(tmpstr1, nvram_get("wl_weptype_x"));

	if (strcmp(tmpstr, "1")==0) /* Shared */
	{
		nvram_set("wl0_auth", "1");
		nvram_set("wl0_auth_mode", "disabled");
		nvram_set("wl0_wep", "wep");
	}
	else if (strcmp(tmpstr, "2")==0) /* WPA-PSK */
	{	
		nvram_set("wl0_auth", "0");
		nvram_set("wl0_auth_mode", "psk");
		
		if(strcmp(tmpstr1, "1")==0) nvram_set("wl0_wep", "aes");
		else if (strcmp(tmpstr1, "2")==0) nvram_set("wl0_wep", "tkip+aes");
		else nvram_set("wl0_wep", "tkip");
	}
	else if (strcmp(tmpstr, "3")==0) /* WPA */
	{	
		nvram_set("wl0_auth", "0");
		nvram_set("wl0_auth_mode", "wpa");

		if(strcmp(tmpstr1, "1")==0) nvram_set("wl0_wep", "aes");
		else if (strcmp(tmpstr1, "2")==0) nvram_set("wl0_wep", "tkip+aes");
		else nvram_set("wl0_wep", "tkip");

	}
	else if (strcmp(tmpstr, "4")==0) /* Radius */
	{	
		nvram_set("wl0_auth", "0");
		nvram_set("wl0_auth_mode", "radius");


		if(strcmp(tmpstr1, "1")==0) nvram_set("wl0_wep", "wep");
		else if (strcmp(tmpstr1, "2")==0) nvram_set("wl0_wep", "wep");
		else nvram_set("wl0_wep", "");
	}
	else /* Open or Shared */
	{	
		nvram_set("wl0_auth", "0");
		nvram_set("wl0_auth_mode", "disabled");

		if(strcmp(tmpstr1, "1")==0) nvram_set("wl0_wep", "wep");
		else if (strcmp(tmpstr1, "2")==0) nvram_set("wl0_wep", "wep");
		else nvram_set("wl0_wep", "");
	}


	/* WDS control */
	if (nvram_match("wl_wds_x", "1")) nvram_set("wl0_mode", "wds");
	else nvram_set("wl0_mode", "ap");
	
	if (nvram_match("wl_lazywds", "1")) nvram_set("wl0_lazywds", "1");
	else nvram_set("wl0_lazywds", "0");

	if (nvram_match("wl_wdsapply_x", "1"))
	{
		num = atoi(nvram_get("wl_wdslist_num_x"));
		list[0]=0;

		for(i=0;i<num;i++)
		{
			sprintf(list, "%s %s", list, mac_conv("wl_wdslist_x", i, macbuf));
		}
		nvram_set("wl0_maclist", list);
	}

	/* Mac filter */
	strcpy(tmpstr, nvram_get("wl_macmode_x"));

	if (strcmp(tmpstr, "Accept")==0) /* allow */
	{
		nvram_set("wl0_macmode", "allow");
	}
	else if (strcmp(tmpstr, "Deny")==0) /* deny */
	{
		nvram_set("wl0_macmode", "deny");
	}

	if (!strcmp(tmpstr, "Disable"))
	{
		num = atoi(nvram_get("wl_maclist_num_x"));
		list[0]=0;

		for(i=0;i<num;i++)
		{
			sprintf(list, "%s;%s", list, mac_conv("wl_maclist_x", i, macbuf));
		}

		nvram_set("wl0_maclist", list);
	}

	/* Direct copy value */

	nvram_set("wl0_ssid", nvram_get("wl_ssid"));
	nvram_set("wl0_channel", nvram_get("wl_channel"));
	nvram_set("wl0_country", nvram_get("wl_country"));
	nvram_set("wl0_rate", nvram_get("wl_rate"));
	nvram_set("wl0_frag", nvram_get("wl_frag"));
	nvram_set("wl0_rts", nvram_get("wl_rts"));
	nvram_set("wl0_dtim", nvram_get("wl_dtim"));
	nvram_set("wl0_bcn", nvram_get("wl_bcn"));
	nvram_set("wl0_plcphdr", nvram_get("wl_plcphdr"));
	nvram_set("wl0_key", nvram_get("wl_key"));
	nvram_set("wl0_key1", nvram_get("wl_key1"));
	nvram_set("wl0_key2", nvram_get("wl_key2"));
	nvram_set("wl0_key3", nvram_get("wl_key3"));
	nvram_set("wl0_key4", nvram_get("wl_key4"));

	/* LAN Section */

	/* Storage Section */
}

void findNVRAMName(char *serviceId, char *field, char *name)
{    	
    int idx;
   
    idx = 0;
	
    //printf("find : %s %s\n", serviceId, field);	
    	
    while(maps[idx].sid!=NULL)
    {      
        if( strcmp(serviceId, maps[idx].sid) == 0)
	{	   	
           if(strcmp(field, maps[idx].fid) == 0) break;
	   //else if(strstr(field, maps[idx].fid)) break;	
	}
        idx ++;   
    } 
    
    if (maps[idx].sid==NULL)
    {	
        strcpy(name, field);
    }
    else
    {	 
        sprintf(name, maps[idx].name);

 	//printf("nvram name: %s\n", name);
    }
}

char *findpattern(char *target, char *pattern)
{
	char *find;
	int len;

	printf("find : %s %s\n", target, pattern);
	if ((find=strstr(target, pattern)))
	{
		len = strlen(pattern);
		if (find[len]==';' || find[len]==0)
		{
			return find;
		}
	}
	return NULL;
}

char *strcat_r(char *dst, char *src, char *tar)
{
	sprintf(tar, "%s%s", dst, src);
	return(tar);
}


char *nvram_get_i(char *name, int idx)
{
	char tmpstr1[64];

	if (idx!=-1)
	{
		sprintf(tmpstr1, "%s%d", name, idx);
	}
	else
	{
		sprintf(tmpstr1, "%s", name); 
	}
	//printf("get : %s %s %s\n", tmpstr2, tmpstr1, name1);

	return(nvram_get(tmpstr1));
}

char *mac_conv(char *mac_name, int idx, char *buf)
{
	char *mac;
	int i, j;
	
	mac = nvram_get_i(mac_name, idx);

	j=0;	
	for(i=0; i<12; i++)
	{		
		if (i!=0&&i%2==0) buf[j++] = ':';
		buf[j++] = mac[i];
	}
	buf[j] = 0;

	return(buf);
}
 

int nvram_match_i(char *name, int idx, char *match) {
	const char *value = nvram_get_i(name, idx);

	printf("match %s %s\n", value, match);
	return (value && !strcmp(value, match));
}


#ifndef NOUSB
void nvram_set_i(char *name, int idx, char *value)
{
	char tmpstr1[64];

	if (idx!=-1)
	{
		sprintf(tmpstr1, "%s%d", name, idx);
	}
	else
	{
		sprintf(tmpstr1, "%s", name); 
	}
	//printf("get : %s %s %s\n", tmpstr2, tmpstr1, name1);

	nvram_set(tmpstr1, value);
}

nvram_cpy(char *name1, char *name2, int idx)
{

	char tmpstr1[289];

	strcpy(tmpstr1, nvram_get_i(name2, idx));
	nvram_set(name1, tmpstr1);	
}

nvram_cpy2(char *name1, char *name2, int idx)
{

	char tmpstr1[289];

	strcpy(tmpstr1, nvram_get(name2));
	nvram_set_i(name1, idx, tmpstr1);	
}

void initSharedEntry(int index)
{
    int index=atoi(nvram_safe_get("sh_num"));
                                                                                                                                              
                                                                                                                                              
    // get index for latest shared entry
    index--;
                                                                                                                                              
    //nvram_set_i("sh_path", index, "");
    //nvram_set_i("sh_name", index, "");
    nvram_set_i("sh_comment", index, "");
    nvram_set_i("sh_commentb", index, "");
    nvram_set_i("sh_max_user", index, "12");
    nvram_set_i("sh_acc_user", index, "");
    nvram_set_i("sh_rright", index, "");
    nvram_set_i("sh_wright", index, "");
    nvram_set_i("sh_acc_onlist_num", index, "0");
                                                                                                                                              
    //if (nvram_match_i("sh_rright_x", index, "on"))
        //nvram_set_i("sh_rright", index, "Guest;");
    //else nvram_set_i("sh_rright", index, "");
                                                                                                                                              
                                                                                                                                              
    //if (nvram_match_i("sh_wright_x", index, "on"))
        //nvram_set_i("sh_wright", index, "Guest;");
    //else nvram_set_i("sh_wright", index, "");
}                                                                                                                                             
                                                                                                                                              
void delSharedEntry(int *delMap)
{
    int count;
    int i, del;
    char cstr[16];
                                                                                                                                              
    count = atoi(nvram_safe_get("sh_num"));
                                                                                                                                              
    nvram_del_list("sh_path", delMap);
    nvram_del_list("sh_name", delMap);
    nvram_del_list("sh_nameb", delMap);
    nvram_del_list("sh_comment", delMap);
    nvram_del_list("sh_commentb", delMap);
    nvram_del_list("sh_rright", delMap);
    nvram_del_list("sh_wright", delMap);
    nvram_del_list("sh_acc_user", delMap);
    nvram_del_list("sh_acc_onlist_num", delMap);
                                                                                                                                              
    i=0;
    while(delMap[i]!=-1) i++;
                                                                                                                                              
    count-=i;
    sprintf(cstr, "%d\n", count);
    nvram_set("sh_num", cstr);
}

nvram_del_list(char *names, int *delMap)
{
    char oname[32], nname[32], *oval, *nval;
    int oi, ni, di;
                                                                                                                                              
    oi=0;
    ni=0;
    di=0;
    while(1)
    {
        sprintf(oname, "%s%d", names, oi);
        sprintf(nname, "%s%d", names, ni);
                                                                                                                                              
        oval = nvram_get(oname);
        nval = nvram_get(nname);
                                                                                                                                              
        if (oval==NULL) break;
                                                                                                                                              
        printf("delSharedEntry->nvram_del_list: %d %d %d %d %s\n", oi, ni, di, delMap[di], oval);
                                                                                                                                              
        if (delMap[di]!=-1&&delMap[di]==oi)
        {
                oi++;
                di++;
        }
        else
        {
                nvram_set(nname, oval);
                ni++;
                oi++;
        }
    }
}
#endif

#ifndef NOUSB
void getSharedEntry(int index)
{
    char idxstr[10], idxstr1[10], tmpstr[32], tmpstr1[289], acc[32];
    char *path;
    char rright[128], wright[128], username[32];
    char *ruser, *wuser;
    int i, j, acc_num;

    sprintf(idxstr, "%d", index);
    strcpy(tmpstr1, nvram_get(strcat_r("sh_path", idxstr, tmpstr)));
//Yau
//    printf("path: %s  index=%d\n", tmpstr1,index);

    nvram_set("sh_path_x", idxstr);
    nvram_cpy("sh_name_x", "sh_name", index);
    nvram_cpy("sh_nameb_x", "sh_nameb", index);
    nvram_cpy("sh_comment_x", "sh_comment", index);
    nvram_cpy("sh_commentb_x", "sh_commentb", index);
    nvram_cpy("sh_max_user_x", "sh_max_user", index);

    nvram_cpy("sh_acc_onlist_num_x", "sh_acc_onlist_num", index);    
    nvram_cpy("sh_acc_user_x", "sh_acc_user", index);
    nvram_set("sh_edit_x", idxstr); 

}
#endif

#ifndef NOUSB
void setSharedEntry( char *value1, char *value2, char *value3, char *value4)
{
    int index;
                                                                                                               
    index = atoi(nvram_get("sh_edit_x"));
//Yau
printf("setSharedEntry:: index= %d, %s, %s\n",index,value1,value2);
    nvram_set_i("sh_name", index, value1);
    nvram_set_i("sh_comment",index, value2);
    nvram_set_i("sh_nameb", index, value3);
    nvram_set_i("sh_commentb",index, value4);
    //nvram_cpy("sh_max_user", "sh_max_user_x", index);
}

#endif
#endif
