/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 */
#ifndef DONGLES
#define DONGLES

#define UNKNOWNDEV				0x0000
#define SN_MU_Q101				0x0010
#define SN_OPTION_ICON225			0x0011
#define SN_Option_GlobeSurfer_Icon		0x0012
#define SN_Option_GlobeSurfer_Icon72		0x0013
#define SN_Option_GlobeTrotter_GT_MAX36		0x0014
#define SN_Option_GlobeTrotter_GT_MAX72 	0x0015
#define SN_Option_GlobeTrotter_EXPRESS72	0x0016
#define SN_Option_iCON210			0x0017
#define SN_Option_GlobeTrotter_HSUPA_Modem	0x0018
#define SN_Option_iCON401			0x0019
#define SN_Vodafone_K3760			0x0020
#define SN_ATT_USBConnect_Quicksilver		0x0021
#define SN_HUAWEI_E169				0x0030
#define SN_HUAWEI_E220				0x0031
#define SN_Huawei_E180				0x0032
#define SN_Huawei_E630				0x0033
#define SN_Huawei_E270				0x0034
#define SN_Huawei_E1550				0x0035
#define SN_Huawei_E1612				0x0036
#define SN_Huawei_E1690				0x0037
#define SN_Huawei_K3765				0x0038
#define SN_Huawei_K4505				0x0039
#define SN_ZTE_MF620				0x0040
#define SN_ZTE_MF622				0x0041
#define SN_ZTE_MF628				0x0042
#define SN_ZTE_MF626				0x0043
#define SN_ZTE_AC8710				0x0044
#define SN_ZTE_AC2710				0x0045
#define SN_ZTE6535_Z				0x0046
#define SN_ZTE_K3520_Z				0x0047
#define SN_ZTE_MF110				0x0048
#define SN_ZTE_K3565				0x0049
#define SN_ONDA_MT503HS				0x0055
#define SN_ONDA_MT505UP				0x0056
#define SN_Novatel_Wireless_Ovation_MC950D	0x0060
#define SN_Novatel_U727				0x0061
#define SN_Novatel_MC990D			0x0062
#define SN_Novatel_U760				0x0063
#define SN_Alcatel_X020				0x0070
#define SN_Alcatel_X200				0x0071
#define SN_AnyDATA_ADU_500A			0x0072
#define SN_BandLuxe_C120			0x0080
#define SN_BandLuxe_C270			0x0081
#define SN_Solomon_S3Gm660			0x0090
#define SN_C_motechD50				0x0095
#define SN_C_motech_CGU628			0x0096
#define SN_Toshiba_G450				0x0100
#define SN_UTStarcom_UM175			0x0105
#define SN_Hummer_DTM5731			0x0110
#define SN_A_Link_3GU				0x0115
#define SN_Sierra_Wireless_Compass597		0x0120
#define SN_Sierra881U				0x0121
#define SN_Sony_Ericsson_MD400			0x0125
#define SN_LG_LDU_1900D				0x0130
#define SN_Samsung_SGH_Z810			0x0135
#define SN_MobiData_MBD_200HU			0x0140
#define SN_ST_Mobile				0x0145
#define SN_MyWave_SW006				0x0150
#define SN_Cricket_A600				0x0155
#define SN_EpiValley_SEC7089			0x0160
#define SN_Samsung_U209				0x0161
#define SN_D_Link_DWM162_U5			0x0165
#define SN_Novatel_MC760			0x0170
#define SN_Philips_TalkTalk			0x0175
#define SN_HuaXing_E600				0x0176
#define SN_C_motech_CHU_629S			0x0180
#define SN_Sagem9520				0x0185
#define SN_Nokia_CS15				0x0190
#define SN_Vodafone_MD950			0x0195
#define SN_Siptune_LM75				0x0196

#endif
