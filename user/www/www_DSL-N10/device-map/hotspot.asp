﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta HTTP-EQUIV="Pragma" CONTENT="no-cache">
<meta HTTP-EQUIV="Expires" CONTENT="-1">
<link rel="shortcut icon" href="images/favicon.png" />
<link rel="icon" href="images/favicon.png">

<title></title>
<link rel="stylesheet" type="text/css" href="/NM_style.css">
<link rel="stylesheet" type="text/css" href="/form_style.css">

<script type="text/javascript" src="/general.js"></script>
<script type="text/javascript" src="/state.js"></script>
<script type="text/javascript" src="formcontrol.js"></script>
<script type="text/javascript" src="/ajax.js"></script>
<script type="text/javascript" src="/detectWAN.js"></script>
<script>
<% wanlink(); %>

function initial(){
	flash_button();
	detectWANstatus();
	showtext($("WANIP"), wanlink_ipaddr());
	
	var dnsArray = wanlink_dns().split(" ");
	if(dnsArray[0])
		showtext($("DNS1"), dnsArray[0]);
	if(dnsArray[1])
		showtext($("DNS2"), dnsArray[1]);

	showtext($("connectionType"), wanlink_type());
	showtext($("gateway"), wanlink_gateway());
	
	document.form.sta_ssid.value = decodeURIComponent(document.form.sta_ssid2.value);
	document.form.sta_wpa_psk.value = decodeURIComponent(document.form.sta_wpa_psk_org.value);
	document.form.sta_key1.value = decodeURIComponent(document.form.sta_key1_org.value);
	document.form.sta_key2.value = decodeURIComponent(document.form.sta_key2_org.value);
	document.form.sta_key3.value = decodeURIComponent(document.form.sta_key3_org.value);
	document.form.sta_key4.value = decodeURIComponent(document.form.sta_key4_org.value);
	
	if(document.form.sta_wpa_psk.value.length <= 0)
		document.form.sta_wpa_psk.value = "Please type Password";
	
	//wl_auth_mode_change(1);
	domore_create();

	$("connection_status").style.display = "none";
}

function getWANStatus(){
	if("<% detect_if_wan(); %>" == "1" && wanlink_statusstr() == "Connected")
		return 1;
	else
		return 0;
}

function submitWANAction(){
	var status = getWANStatus();
	
	switch(status){
		case 0:
			parent.showLoading();
			setTimeout('location.href = "/device-map/wan_action.asp?wanaction=Connect";', 1);
			break;
		case 1:
			parent.showLoading();
			setTimeout('location.href = "/device-map/wan_action.asp?wanaction=Disconnect";', 1);
			break;
		default:
			alert("No change!");
	}
}

function sbtnOver(o){
	o.style.color = "#FFFFFF";		
	o.style.background = "url(/images/sbtn.gif) #FFCC66";
	o.style.cursor = "pointer";
}

function sbtnOut(o){
	o.style.color = "#000000";
	o.style.background = "url(/images/sbtn0.gif) #FFCC66";
}

function domore_create(){
	var option_AP = new Array();
	option_AP[0] = document.createElement("option");
	option_AP[0].text = "<#menu5_7_4#>";
	option_AP[0].value = "../Main_WStatus_Content.asp"
	
	if(sw_mode == 2){
		$("Router_domore").remove(6);
		$("Router_domore").remove(5);
		$("Router_domore").remove(4);
		$("Router_domore").options[3].value="../Advanced_FirmwareUpgrade_Content.asp";
		$("Router_domore").options[3].text="<#menu5_6_3#>";
		$("Router_domore").remove(2);
		$("Router_domore").options[1].value="../Advanced_WAdvanced_Content.asp";
		$("Router_domore").options[1].text="<#menu5_1#> - <#menu5_1_6#>";
		try{
    	$("Router_domore").add(option_AP[0],null);
    }
  	catch(ex){
    	$("Router_domore").add(option_AP[0],3);
    }
	}
	else if(sw_mode == 4){
		$("show_dns").style.display = "none";
		$("show_wanip").style.display = "none";
		$("show_conn_type").style.display = "none";
		$("show_gateway").style.display = "none";
		$("Router_domore").remove(6);
		$("Router_domore").remove(5);
		$("Router_domore").remove(4);
		$("Router_domore").remove(2);
	}
	else if(sw_mode != 1 && sw_mode != 5){
		$("Router_domore").remove(6);
		$("Router_domore").remove(5);
		$("Router_domore").remove(4);
		$("Router_domore").options[3].value="../Advanced_APLAN_Content.asp";
		try{
    	$("Router_domore").add(option_AP[0],null);
    }
  	catch(ex){
    	$("Router_domore").add(option_AP[0],4);
    }
	}
}

function wl_auth_mode_change(isload){
	var wl_gmode = "<% nvram_get_x("WLANConfig11b", "wl_gmode"); %>";
	var mode = document.form.sta_auth_mode.value;
	var opts = document.form.sta_auth_mode.options;
	var new_array;
	var cur_crypto;
	var cur_key_index, cur_key_obj;
	
	//if(mode == "open" || mode == "shared" || mode == "radius"){ //2009.03 magic
	if(mode == "open" || mode == "shared"){ //2009.03 magic
		if(wl_gmode == 2){
			alert("<#WLANConfig11n_automode_limition_hint#>");
		}
		blocking("all_related_wep", 1);
		change_wep_type(mode);
	}
	else{
		blocking("all_related_wep", 0);
	}
	
	/* enable/disable crypto algorithm */
	if(mode == "wpa" || mode == "wpa2" || mode == "psk")
		blocking("sta_crypto", 1);
	else
		blocking("sta_crypto", 0);
	
	/* enable/disable psk passphrase */
	if(mode == "psk")
		blocking("sta_wpa_psk", 1);
	else
		blocking("sta_wpa_psk", 0);
	
	/* update sta_crypto */
	for(var i = 0; i < document.form.sta_crypto.length; ++i)
		if(document.form.sta_crypto[i].selected){
			cur_crypto = document.form.sta_crypto[i].value;
			break;
		}
	
	/* Reconstruct algorithm array from new crypto algorithms */
	if(mode == "psk"){
		/* Save current crypto algorithm */
		if(isModel() == "SnapAP" || isBand() == 'b')
			new_array = new Array("TKIP");
		else{
			if(opts[opts.selectedIndex].text == "WPA-Personal")
				new_array = new Array("TKIP");
			else if(opts[opts.selectedIndex].text == "WPA2-Personal")
				new_array = new Array("AES");
			else
				new_array = new Array("TKIP", "AES", "TKIP+AES");
		}
		
		free_options(document.form.sta_crypto);
		for(var i in new_array){
			document.form.sta_crypto[i] = new Option(new_array[i], new_array[i].toLowerCase());
			document.form.sta_crypto[i].value = new_array[i].toLowerCase();
			if(new_array[i].toLowerCase() == cur_crypto)
				document.form.sta_crypto[i].selected = true;
		}
	}
	else if(mode == "wpa"){
		if(opts[opts.selectedIndex].text == "WPA-Enterprise")
			new_array = new Array("TKIP");
		else
			new_array = new Array("TKIP", "AES", "TKIP+AES");
		
		free_options(document.form.sta_crypto);
		for(var i in new_array){
			document.form.sta_crypto[i] = new Option(new_array[i], new_array[i].toLowerCase());
			document.form.sta_crypto[i].value = new_array[i].toLowerCase();
			if(new_array[i].toLowerCase() == cur_crypto)
				document.form.sta_crypto[i].selected = true;
		}
	}
	else if(mode == "wpa2"){
		new_array = new Array("AES");
		
		free_options(document.form.sta_crypto);
		for(var i in new_array){
			document.form.sta_crypto[i] = new Option(new_array[i], new_array[i].toLowerCase());
			document.form.sta_crypto[i].value = new_array[i].toLowerCase();
			if(new_array[i].toLowerCase() == cur_crypto)
				document.form.sta_crypto[i].selected = true;
		}
	}
	
	/* Save current network key index */
	for(var i = 0; i < document.form.sta_key.length; ++i)
		if(document.form.sta_key[i].selected){
			cur_key_index = document.form.sta_key[i].value;
			break;
		}
	
	/* Define new network key indices */
	//if(mode == "psk" || mode == "wpa" || mode == "wpa2" || mode == "radius")
	if(mode == "psk" || mode == "wpa" || mode == "wpa2")
		new_array = new Array("2", "3");
	else{
		new_array = new Array("1", "2", "3", "4");
		
		if(!isload)
			cur_key_index = "1";
	}
	
	/* Reconstruct network key indices array from new network key indices */
	free_options(document.form.sta_key);
	for(var i in new_array){
		document.form.sta_key[i] = new Option(new_array[i], new_array[i]);
		document.form.sta_key[i].value = new_array[i];
		if(new_array[i] == cur_key_index)
			document.form.sta_key[i].selected = true;
	}
	
	wl_wep_change();
}

function change_wep_type(mode){
	var cur_wep = document.form.sta_wep_x.value;
	var wep_type_array;
	var value_array;
	
	free_options(document.form.sta_wep_x);
	
	//if(mode == "shared" || mode == "radius"){ //2009.03 magic
	if(mode == "shared"){ //2009.03 magic
		wep_type_array = new Array("WEP-64bits", "WEP-128bits");
		value_array = new Array("1", "2");
	}
	else{
		wep_type_array = new Array("None", "WEP-64bits", "WEP-128bits");
		value_array = new Array("0", "1", "2");
	}
	
	add_options_x2(document.form.sta_wep_x, wep_type_array, value_array, cur_wep);
	
	if(mode == "psk" || mode == "wpa" || mode == "wpa2") //2009.03 magic
	//if(mode == "psk" || mode == "wpa" || mode == "wpa2" || mode == "radius") //2009.03 magic
		document.form.sta_wep_x.value = "0";
	
	change_wlweptype(document.form.sta_wep_x);
}

function change_wlweptype(wep_type_obj){
	var mode = document.form.sta_auth_mode.value; //2009.03 magic
	
	//if(wep_type_obj.value == "0" || mode == "radius") //2009.03 magic
	if(wep_type_obj.value == "0")  //2009.03 magic
		blocking("all_wep_key", 0);
	else
		blocking("all_wep_key", 1);
	
	wl_wep_change();
}

function wl_wep_change(){
	var mode = document.form.sta_auth_mode.value;
	var wep = document.form.sta_wep_x.value;
	
	if(mode == "psk" || mode == "wpa" || mode == "wpa2"){
		if(mode == "psk"){
			blocking("sta_crypto", 1);
			blocking("sta_wpa_psk", 1);
		}
		
		blocking("all_related_wep", 0);
	}
	else{
		blocking("sta_crypto", 0);
		blocking("sta_wpa_psk", 0);
		
		//if(mode == "radius") //2009.03 magic
		//	blocking("all_related_wep", 0); //2009.03 magic
		//else //2009.03 magic
		//	blocking("all_related_wep", 1);
		
		if(wep == "0" || mode == "radius")
			blocking("all_wep_key", 0);
		else{
			blocking("all_wep_key", 1);
			
			show_key();
		}
	}
	
	change_key_des();
}

function change_key_des(){
	var objs = getElementsByName_iefix("span", "key_des");
	var wep_type = document.form.sta_wep_x.value;
	var str = "";
	
	if(wep_type == "1")
		str = " (<#WLANConfig11b_WEPKey_itemtype1#>)";
	else if(wep_type == "2")
		str = " (<#WLANConfig11b_WEPKey_itemtype2#>)";
	
	str += ":";
	
	for(var i = 0; i < objs.length; ++i)
		showtext(objs[i], str);
}

function change_auth_mode(auth_mode_obj){
	wl_auth_mode_change(0);
	
	if(auth_mode_obj.value == "psk" || auth_mode_obj.value == "wpa"){
		var opts = document.form.sta_auth_mode.options;
		
		if(opts[opts.selectedIndex].text == "WPA-Personal")
			document.form.sta_wpa_mode.value = "1";
		else if(opts[opts.selectedIndex].text == "WPA2-Personal")
			document.form.sta_wpa_mode.value="2";
		else if(opts[opts.selectedIndex].text == "WPA-Auto-Personal")
			document.form.sta_wpa_mode.value="0";
		else if(opts[opts.selectedIndex].text == "WPA-Enterprise")
			document.form.sta_wpa_mode.value="3";
		else if(opts[opts.selectedIndex].text == "WPA-Auto-Enterprise")
			document.form.sta_wpa_mode.value = "4";
		
		if(auth_mode_obj.value == "psk"){
			document.form.sta_wpa_psk.focus();
			document.form.sta_wpa_psk.select();
		}
	}
	else if(auth_mode_obj.value == "shared")
		show_key();	
	//else if(auth_mode_obj.value == "shared" || auth_mode_obj.value == "radius")
}

function show_key(){
	var wep_type = document.form.sta_wep_x.value;
	var keyindex = document.form.sta_key.value;
	var cur_key_obj = eval("document.form.sta_key"+keyindex);
	var cur_key_length = cur_key_obj.value.length;
	
	if(wep_type == 1){
		if(cur_key_length == 5 || cur_key_length == 10)
			document.form.sta_asuskey1.value = cur_key_obj.value;
		else
			document.form.sta_asuskey1.value = "0000000000";
	}
	else if(wep_type == 2){
		if(cur_key_length == 13 || cur_key_length == 26)
			document.form.sta_asuskey1.value = cur_key_obj.value;
		else
			document.form.sta_asuskey1.value = "00000000000000000000000000";
	}
	else
		document.form.sta_asuskey1.value = "";
	
	document.form.sta_asuskey1.focus();
	document.form.sta_asuskey1.select();
}

function show_wepkey_help(){
	if(document.form.sta_wep_x.value == 1)
		parent.showHelpofDrSurf(0, 12);
	else if(document.form.sta_wep_x.value == 2)
		parent.showHelpofDrSurf(0, 13);
}

function submitForm(){
	var auth_mode = document.form.sta_auth_mode.value;
		
	if(!validate_string_ssid(document.form.sta_ssid))
		return false;
	
	if(auth_mode == "psk" || auth_mode == "wpa" || auth_mode == "wpa2"){
		if(!validate_psk(document.form.sta_wpa_psk))
			return false;
	}
	else{
		if(!validate_wlkey(document.form.sta_asuskey1))
			return false;
	}
	
	stopFlag = 1;
	
	document.form.current_page.value = "";
	document.form.next_page.value = "/";
	document.form.action_mode.value = " Apply ";
	
	var wep11 = eval('document.form.sta_key'+document.form.sta_key.value);
	wep11.value = document.form.sta_asuskey1.value;
	
	if((auth_mode == "shared" || auth_mode == "wpa" || auth_mode == "wpa2" || auth_mode == "radius")
			&& document.form.wps_enable.value == "1"){
		document.form.wps_enable.value = "0";
		document.form.action_script.value = "WPS_apply";
	}
	document.form.wsc_config_state.value = "1";
	
	if(auth_mode == "wpa" || auth_mode == "wpa2" || auth_mode == "radius"){
		document.form.target = "";
		document.form.next_page.value = "/Advanced_WSecurity_Content.asp";
	}
	
	parent.showLoading();
	document.form.submit();
	
	return true;
}

function startPBCmethod(){
	stopFlag = 1;
	
	document.WPSForm.action_script.value = "WPS_push_button";
	document.WPSForm.current_page.value = "/";
	document.WPSForm.submit();
}
</script>
</head>

<body class="statusbody" onload="initial();">
<iframe name="hidden_frame" id="hidden_frame" width="0" height="0" frameborder="0"></iframe>
<form method="post" name="form" id="form" action="/start_apply2.htm">
<input type="hidden" name="current_page" value="">
<input type="hidden" name="next_page" value="">
<input type="hidden" name="sid_list" value="WLANConfig11b;">
<input type="hidden" name="group_id" value="">
<input type="hidden" name="action_mode" value="">
<input type="hidden" name="action_script" value="">
<input type="hidden" name="productid" value="<% nvram_get_x("",  "productid"); %>">

<input type="hidden" name="wps_enable" value="<% nvram_get_x("WLANConfig11b", "wps_enable"); %>">
<input type="hidden" name="wsc_config_state" value="<% nvram_get_x("WLANConfig11b", "wsc_config_state"); %>">
<input type="hidden" name="sta_wpa_mode" value="<% nvram_get_x("WLANConfig11b", "sta_wpa_mode"); %>">
<input type="hidden" name="sta_key1" value="">
<input type="hidden" name="sta_key2" value="">
<input type="hidden" name="sta_key3" value="">
<input type="hidden" name="sta_key4" value="">
<input type="hidden" name="wl_radio_x" value="<% nvram_get_x("WLANConfig11b", "wl_radio_x"); %>">

<input type="hidden" name="sta_ssid2" value="<% nvram_char_to_ascii("WLANConfig11b", "sta_ssid"); %>">
<input type="hidden" name="sta_wpa_psk_org" value="<% nvram_char_to_ascii("WLANConfig11b", "sta_wpa_psk"); %>">
<input type="hidden" name="sta_key_type" value="<% nvram_get_x("WLANConfig11b","sta_key_type"); %>"><!--Lock Add 1125 for ralink platform-->
<input type="hidden" name="sta_key1_org" value="<% nvram_char_to_ascii("WLANConfig11b", "sta_key1"); %>">
<input type="hidden" name="sta_key2_org" value="<% nvram_char_to_ascii("WLANConfig11b", "sta_key2"); %>">
<input type="hidden" name="sta_key3_org" value="<% nvram_char_to_ascii("WLANConfig11b", "sta_key3"); %>">
<input type="hidden" name="sta_key4_org" value="<% nvram_char_to_ascii("WLANConfig11b", "sta_key4"); %>">

<table id="routertable" width="270" border="1" align="center" cellpadding="4" cellspacing="0" bordercolor="#6b8fa3" class="table1px">
  <tr id="connection_status">
    <th width="95"><#ConnectionStatus#></th>
    <td>
	  <span id="connectstatus"></span>
    <input type="button" id="connectbutton" class="button" value="" onclick="submitWANAction();">
	</td>
  </tr>
  <tr id="sta_ssid">
    <th width="95"><#Wireless_name#>(SSID)</th>
    <td>
      <input type="text" name="sta_ssid" disabled="disabled" onfocus="parent.showHelpofDrSurf(0, 1);" value="<% nvram_get_x("WLANConfig11b", "sta_ssid"); %>" maxlength="32" size="22" class="input"/>
    </td>
  </tr>
  <tr id="sta_auth_mode">
    <th width="95"><#WLANConfig11b_AuthenticationMethod_itemname#></th>
    <td>
      <select name="sta_auth_mode" class="input" disabled="disabled" onfocus="parent.showHelpofDrSurf(0, 5);" onchange="change_auth_mode(this);">
		<option value="open" <% nvram_match_x("WLANConfig11b","sta_auth_mode", "open","selected"); %>>Open System</option>
		<option value="shared" <% nvram_match_x("WLANConfig11b","sta_auth_mode", "shared","selected"); %>>Shared Key</option>
		<option value="psk" <% nvram_double_match_x("WLANConfig11b", "sta_auth_mode", "psk", "WLANConfig11b", "sta_wpa_mode", "1", "selected"); %>>WPA-Personal</option>
		<option value="psk" <% nvram_double_match_x("WLANConfig11b", "sta_auth_mode", "psk", "WLANConfig11b", "sta_wpa_mode", "2", "selected"); %>>WPA2-Personal</option>
		<option value="psk" <% nvram_double_match_x("WLANConfig11b", "sta_auth_mode", "psk", "WLANConfig11b", "sta_wpa_mode", "0", "selected"); %>>WPA-Auto-Personal</option>
		<option value="wpa" <% nvram_double_match_x("WLANConfig11b", "sta_auth_mode", "wpa", "WLANConfig11b", "sta_wpa_mode", "3", "selected"); %>>WPA-Enterprise</option>
		<option value="wpa2" <% nvram_match_x("WLANConfig11b", "sta_auth_mode", "wpa2", "selected"); %>>WPA2-Enterprise</option>
		<option value="wpa" <% nvram_double_match_x("WLANConfig11b", "sta_auth_mode", "wpa", "WLANConfig11b", "sta_wpa_mode", "4", "selected"); %>>WPA-Auto-Enterprise</option>
		<option value="radius" <% nvram_match_x("WLANConfig11b","sta_auth_mode", "radius","selected"); %>>Radius with 802.1x</option>
	  </select>
    </td>
  </tr>
</table>

<div id='all_related_wep' style='display:none;'>
<table width="270" border="1" align="center" cellpadding="4" cellspacing="0" bordercolor="#6b8fa3" class="table1px">
  <tr>
	<th width="95"><#WLANConfig11b_WEPType_itemname#></th>
	<td>
	  <select name="sta_wep_x" id="sta_wep_x" disabled="disabled" class="input" onfocus="parent.showHelpofDrSurf(0, 9);" onchange="change_wlweptype(this);">
		<option value="0" <% nvram_match_x("WLANConfig11b", "sta_wep_x", "0", "selected"); %>>None</option>
		<option value="1" <% nvram_match_x("WLANConfig11b", "sta_wep_x", "1", "selected"); %>>WEP-64bits</option>
		<option value="2" <% nvram_match_x("WLANConfig11b", "sta_wep_x", "2", "selected"); %>>WEP-128bits</option>
	  </select>
	</td>
  </tr>
</table>

<div id='all_wep_key' style='display:none;'>
<table width="270" border="1" align="center" cellpadding="4" cellspacing="0" bordercolor="#6b8fa3" class="table1px">
  <tr>
    <th width="95"><#WLANConfig11b_WEPDefaultKey_itemname#></th>
    <td>
      <select name="sta_key" class="input"  disabled="disabled" onfocus="parent.showHelpofDrSurf(0, 10);" onchange="show_key();">
        <option value="1" <% nvram_match_x("WLANConfig11b", "sta_key", "1", "selected"); %>>Key1</option>
        <option value="2" <% nvram_match_x("WLANConfig11b", "sta_key", "2", "selected"); %>>Key2</option>
        <option value="3" <% nvram_match_x("WLANConfig11b", "sta_key", "3", "selected"); %>>Key3</option>
        <option value="4" <% nvram_match_x("WLANConfig11b", "sta_key", "4", "selected"); %>>Key4</option>
      </select>
    </td>
  </tr>
  <tr>
    <th width="95"><#WLANConfig11b_WEPKey_itemname#></th>
    <td>
      <input type="text" id="sta_asuskey1" name="sta_asuskey1" disabled="disabled" onfocus="show_wepkey_help();" onKeyUp="return change_wlkey(this, 'WLANConfig11b');" value="" size="22" class="input"/>
    </td>
  </tr>
</table>
</div>
</div>

<div id='sta_crypto' style='display:none;'>
<table width="270" border="1" align="center" cellpadding="4" cellspacing="0" bordercolor="#6b8fa3" class="table1px">
  <tr>
	<th width="95"><#WLANConfig11b_WPAType_itemname#></th>
	<td>
	  <select name="sta_crypto" class="input" disabled="disabled" onfocus="parent.showHelpofDrSurf(0, 6);" onchange="wl_auth_mode_change(0);">
		<option value="tkip" <% nvram_match_x("WLANConfig11b", "sta_crypto", "tkip", "selected"); %>>TKIP</option>
		<option value="aes" <% nvram_match_x("WLANConfig11b", "sta_crypto", "aes", "selected"); %>>AES</option>
		<option value="tkip+aes" <% nvram_match_x("WLANConfig11b", "sta_crypto", "tkip+aes", "selected"); %>>TKIP+AES</option>
	  </select>
	</td>
  </tr>
</table>
</div>

<div id='sta_wpa_psk' style='display:none'>
<table width="270" border="1" align="center" cellpadding="4" cellspacing="0" bordercolor="#6b8fa3" class="table1px">
  <tr>
    <th width="95"><#WPA-PSKKey#></th>
    <td>
      <input type="text" id="sta_wpa_psk" name="sta_wpa_psk" disabled="disabled" onfocus="parent.showHelpofDrSurf(0, 7);" value="" size="22" maxlength="63" class="input"/>
    </td>
  </tr>
</table>
</div>

<table width="270" border="1" align="center" cellpadding="4" cellspacing="0" bordercolor="#6b8fa3" class="table1px">
  <tr id="apply_tr">
    <td colspan="2"><input id="applySecurity" type="button" class="sbtn" value="<#AP_survey#>" onclick="javascript:parent.location.href='../survey.asp';"  onmouseover="sbtnOver(this);" onmouseout="sbtnOut(this);" style="margin-left:110px;"></td>
  </tr>
  <tr id="show_wanip" style="display='block'">
    <th width="95"><#WAN_IP#></th>
    <td><span id="WANIP"></span></td>
  </tr>
  <tr id="show_dns" style="display='block'">
    <th width="95">DNS</th>
    <td><span id="DNS1"></span><br><span id="DNS2"></span></td>
  </tr>
  <tr id="show_conn_type">
    <th width="95"><#Connectiontype#></th>
    <td><span id="connectionType"></span></td>
  </tr>
  <tr id="show_gateway" style="display='block'">
    <th width="95"><#Gateway#></th>
    <td><span id="gateway"></span></td>
  </tr> 
  
</table>
<select id="Router_domore" class="domore" onchange="domore_link(this);">
	<option><#MoreConfig#>...</option>
	<option value="../Advanced_WirelessSTA_Content.asp"><#menu5_1#> - <#Hot_Spot#></option>
	<option value="../Advanced_WirelessHS_Content.asp"><#menu5_1#> - <#menu5_1_1#></option>
	<option value="../Advanced_LAN_Content.asp"><#menu5_2_1#></option>
	<option value="../Advanced_DHCP_Content.asp"><#menu5_2_2#></option>
	<option value="../Advanced_GWStaticRoute_Content.asp"><#menu5_2_3#></option>
	<option value="../Main_LogStatus_Content.asp"><#menu5_7_2#></option>
</select>
</form>

<form method="post" name="WPSForm" id="WPSForm" action="/start_apply.htm">
<input type="hidden" name="current_page" value="">
<input type="hidden" name="sid_list" value="">
<input type="hidden" name="action_script" value="">
</form>

<form method="post" name="stopPINForm" id="stopPINForm" action="/start_apply.htm" target="hidden_frame">
<input type="hidden" name="current_page" value="">
<input type="hidden" name="sid_list" value="WLANConfig11b;">
<input type="hidden" name="group_id" value="">
<input type="hidden" name="action_mode" value="">
<input type="hidden" name="wsc_config_command" value="<% nvram_get_x("WLANConfig11b", "wsc_config_command"); %>">
</form>
</body>
</html>