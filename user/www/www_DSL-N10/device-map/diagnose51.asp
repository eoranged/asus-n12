﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<title></title>
<link href="../other.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="/state.js"></script>
<script>
function initial(){
	var html_code = '';
	var chk3g = "<% nvram_get_x("", "chk3g"); %>";
	
	if( chk3g != "0" ) {
		html_code += '<li>\n';
		if( chk3g == "1" )
			html_code += '<#web_redirect_reason51_1#>\n';
		else if( chk3g == "2" )
			html_code += '<#web_redirect_reason51_2#>\n';
		else if( chk3g == "3" )
			html_code += '<#web_redirect_reason51_3#>\n';
		else
			html_code += '<#web_redirect_reason51_99#>\n';
		html_code += '</li>\n';
	}
	
	$("reason").innerHTML = html_code;
}
</script>
</head>

<body onload="initial();" class="diagnosebody">

<span class="diagnose_title"><#web_redirect_fail_reason0#>:</span>
<div id="failReason" class="diagnose_suggest2" style="color:#CC0000">
<ul>
	<li>
		<span><#web_redirect_reason51#></span>
	</li>
	<span id="reason"/>
</ul>
</div>

<br/>

<span class="diagnose_title"><#web_redirect_suggestion0#>:</span>
<div id="sug_code" class="diagnose_suggest">
<ul>
	<li>
		<span><#web_redirect_suggestion51#></span>
	</li>
	<li>
		<span><#web_redirect_suggestion_final#></span>
		<a href="/Advanced_3G_Modem_Content.asp" target="_parent"><#web_redirect_suggestion_etc#></a>
		<span><#web_redirect_suggestion_etc_desc#></span>
	</li>
</ul>
</div>

</body>
</html>
