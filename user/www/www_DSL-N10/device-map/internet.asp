﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta HTTP-EQUIV="Pragma" CONTENT="no-cache">
<meta HTTP-EQUIV="Expires" CONTENT="-1">
<meta HTTP-EQUIV="refresh" CONTENT="5">
<title></title>
<link rel="stylesheet" type="text/css" href="../NM_style.css">
<link rel="stylesheet" type="text/css" href="../form_style.css">

<script type="text/javascript" src="formcontrol.js"></script>
<script type="text/javascript" src="/state.js"></script>
<script type="text/javascript" src="/detectWAN.js"></script>
<script>
<% wanlink(); %>

var ethwan = "<% nvram_get_x("Layer3Forwarding", "use_eth_wan"); %>";
var wan_proto = "<% nvram_get_x("Layer3Forwarding", "wan_proto"); %>";

DSLWANList = [<%get_DSL_WAN_list("DSLWANConfig", "DSLWANList");%>];

var wan_status_log = "<% get_wan_status_log(); %>";

function initial(){
	flash_button();
	//detectWANstatus();

	showtext($("WANIP"), wanlink_ipaddr());

	var dnsArray = wanlink_dns().split(" ");
	if(dnsArray[0])
		showtext($("DNS1"), dnsArray[0]);
	if(dnsArray[1])
		showtext($("DNS2"), dnsArray[1]);

	showtext($("connectionType"), wanlink_type());

	showtext($("gateway"), wanlink_gateway());

	if (getWANStatus() == 1){
		$("connectstatus").innerHTML="<#Connected#>";
		$("connectbutton").value="<#Disconnect#>";
		$("connectbtn_show").style.display = "";
	}
	else{
		$("connectstatus").innerHTML="<#Disconnected#>";

		if(DSLWANList.length > 0 || ethwan != "0")
		{
			$("connectbutton").value="<#Connect#>";
			$("connectbtn_show").style.display = "";
		}
		else if(wan_proto="3g") {
			$("connectbutton").value="<#Connect#>";
			$("connectbtn_show").style.display = "";
		}
		else
			$("connectbtn_show").style.display = "none";
	  }
	  
	  // when use bridge mode, connect button has no effect
	if(wan_proto == "bridge") $("connectbtn_show").style.display = "none";
}

function getWANStatus(){
	var current_wanlink_statusstr = "";
	if(wan_proto == "pptp" || wan_proto == "l2tp")
	{
		if(wan_status_log.indexOf("Failed to authenticate ourselves to peer") >= 0)
			current_wanlink_statusstr = "Disconnected";
		else if(wan_status_log.indexOf("No response from ISP") >= 0)
			current_wanlink_statusstr = "Disconnected";
		else
			current_wanlink_statusstr = wanlink_statusstr();
	}
	else
	{
		current_wanlink_statusstr = wanlink_statusstr();
	}
	
	if("<% detect_if_wan(); %>" == "1" && current_wanlink_statusstr == "Connected")
		return 1;
	else
		return 0;
}

function submitWANAction(){
	var status = getWANStatus();
	
	switch(status){
		case 0:
			parent.showLoading();
			setTimeout('location.href = "/device-map/wan_action.asp?wanaction=Connect";', 1);
			break;
		case 1:
			parent.showLoading();
			setTimeout('location.href = "/device-map/wan_action.asp?wanaction=Disconnect";', 1);
			break;
		default:
			alert("No change!");
	}
}

function goQIS(){
	parent.showLoading();
	parent.location.href = '/QIS_wizard.htm';
}

function sbtnOver(o){
	o.style.color = "#FFFFFF";
	o.style.background = "url(/images/sbtn.gif) #FFCC66";
	o.style.cursor = "pointer";
}

function sbtnOut(o){
	o.style.color = "#000000";
	o.style.background = "url(/images/sbtn0.gif) #FFCC66";
}
</script>
</head>

<body class="statusbody" onload="initial();" BGCOLOR="#A7D2E2">
<table width="95%" border="1" align="center" cellpadding="4" cellspacing="0" bordercolor="#6b8fa3" class="table1px">
  <tr>
    <th><#wan_mode#></th>
    <td>
<script>
	var use_failover_eth_wan = "<% nvram_get_x("Layer3Forwarding","use_failover_eth_wan"); %>";
	var failover_3g_enable = "<% nvram_get_x("Layer3Forwarding","failover_3g_enable"); %>";
	var DSLWANList = [<% get_DSL_WAN_list("DSLWANConfig", "DSLWANList"); %>];
	var eth_wan_failover_status = "<% nvram_get_x("Layer3Forwarding","eth_wan_failover_status"); %>";
	var failover_3g_status = "<% nvram_get_x("Layer3Forwarding","failover_3g_status"); %>";
	var enable3g = "<% nvram_get_x("General", "hsdpa_enable"); %>";
	//ethwan

	if(use_failover_eth_wan == "0" && failover_3g_enable == "0") //FailOver/FailBack disabled
	{
		if(enable3g != "0")
			document.writeln("<#menu5_3_3gwan#>");
		else if(ethwan != "0")
			document.writeln("<#menu5_eth_wan#>");
		else
			document.writeln("<#menu5_dsl_wan#>");

		document.writeln("</td></tr>");
	}
	else
	{
		if(DSLWANList.length > 0)
		{
			if(ethwan != "0")
			{
				if(eth_wan_failover_status == "done")
					document.writeln("<#menu5_eth_wan#>");
				else
					document.writeln("<#menu5_dsl_wan#>");

				document.writeln("</td></tr>");
				document.writeln('<tr><th width="120"><#nm_fail_over#></th><td width="150"><#btn_Enabled#></td></tr>');
				document.writeln('<tr><th width="120"><#primary_wan#></th><td width="150"><#menu5_dsl_wan#></td></tr>');
				document.writeln('<tr><th width="120"><#backup_wan#></th><td width="150"><#menu5_eth_wan#></td></tr>');
			}
			else if(enable3g != "0")
			{
				if(failover_3g_status == "done")
					document.writeln("<#menu5_3_3gwan#>");
				else
					document.writeln("<#menu5_dsl_wan#>");

				document.writeln("</td></tr>");
				document.writeln('<tr><th width="120"><#nm_fail_over#></th><td width="150"><#btn_Enabled#></td></tr>');
				document.writeln('<tr><th width="120"><#primary_wan#></th><td width="150"><#menu5_dsl_wan#></td></tr>');								document.writeln('<tr><th width="120"><#backup_wan#></th><td width="150"><#menu5_3_3gwan#></td></tr>');
			}
			else
			{
				document.writeln("<#menu5_dsl_wan#>");
				document.writeln("</td></tr>");
			}
		}
		else //user deleted DSL WAN PVC, so consider as Failover/FailBack disabled
		{
			if(enable3g != "0")
				document.writeln("<#menu5_3_3gwan#>");
			else if(ethwan != "0")
				document.writeln("<#menu5_eth_wan#>");
			else
				document.writeln("<#menu5_dsl_wan#>");

			document.writeln("</td></tr>");
		}
	}
</script>
  <tr>
    <th width="120"><#ConnectionStatus#></th>
    <td width="150">
	  <span id="connectstatus"></span><span id="connectbtn_show"><input type="button" id="connectbutton" class="button" value="" onclick="submitWANAction();"></span>
	</td>
  </tr>
  <tr>
    <th><#WAN_IP#></th>
    <td><span id="WANIP"></span></td>
  </tr>
  <tr>
    <th>DNS</th>
    <td><span id="DNS1"></span><br><span id="DNS2"></span></td>
  </tr>
  <tr>
    <th><#Connectiontype#></th>
    <td><span id="connectionType"></span></td>
  </tr>
  <tr>
    <th><#Gateway#></th>
    <td><span id="gateway"></span></td>
  </tr>
  <tr>
    <th><#QIS#></th>
    <td><input type="button" class="sbtn" value="<#btn_go#>" onclick="javascript:goQIS();" onmouseover="sbtnOver(this);" onmouseout="sbtnOut(this);"></td>
	</tr>
</table>
<select class="domore" onchange="domore_link(this);">
  <option value="../index.asp"><#MoreConfig#>...</option>
  <option value="../Advanced_WAN_Content.asp"><#menu5_3_1#> - <#menu5_dsl_wan#></option>
  <option value="../Advanced_Eth_WAN_Content.asp"><#menu5_3_1#> - <#menu5_eth_wan#></option>
	<script>
	if("<#Web_Title#>" == "DSL-N12U")
		document.writeln('<option value="../Advanced_3G_Modem_Content.asp"><#menu5_3_1#> - <#menu5_3_3gwan#></option>');
	</script>
	<option value="../Advanced_Port_Mapping.asp"><#menu5_3_3#></option>
  <option value="../Advanced_PortTrigger_Content.asp"><#menu5_3_4#></option>
  <option value="../Advanced_VirtualServer_Content.asp"><#menu5_3_5#></option>
  <option value="../Advanced_Exposed_Content.asp"><#menu5_3_6#></option>
  <option value="../Advanced_ASUSDDNS_Content.asp"><#menu5_3_7#></option>
  <option value="../Main_IPTStatus_Content.asp"><#menu5_7_5#></option>
</select>
</body>
</html>

