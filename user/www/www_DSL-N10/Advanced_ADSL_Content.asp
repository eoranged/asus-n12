﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<html xmlns:v>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta HTTP-EQUIV="Pragma" CONTENT="no-cache">
<meta HTTP-EQUIV="Expires" CONTENT="-1">
<title>ASUS <#Web_Title#> Wireless-N ADSL2/2+ Modem Router - <#menu5_3_8#></title>
<link rel="stylesheet" type="text/css" href="index_style.css">
<link rel="stylesheet" type="text/css" href="form_style.css">
<script language="JavaScript" type="text/javascript" src="/state.js"></script>
<script language="JavaScript" type="text/javascript" src="/general.js"></script>
<script language="JavaScript" type="text/javascript" src="/popup.js"></script>
<script type="text/javascript" language="JavaScript" src="/help.js"></script>
<script>
wan_route_x = '<% nvram_get_x("IPConnection", "wan_route_x"); %>';
wan_nat_x = '<% nvram_get_x("IPConnection", "wan_nat_x"); %>';
wan_proto = '<% nvram_get_x("Layer3Forwarding",  "wan_proto"); %>';

function initial(){
      var dsl_modulation_x = "<% nvram_get_x("DSLWANConfig", "dsl_modulation"); %>";
      var dsl_annex_x = "<% nvram_get_x("DSLWANConfig", "dsl_annex"); %>";
      show_banner(1);
      show_menu(5,6,4);
      if (dsl_modulation_x == "0")
         document.form.dsl_modulation[0].checked = true;
      else if (dsl_modulation_x == "1")
         document.form.dsl_modulation[1].checked = true;
      else if (dsl_modulation_x == "2")
         document.form.dsl_modulation[2].checked = true;
      else if (dsl_modulation_x == "3")
         document.form.dsl_modulation[3].checked = true;
      else if (dsl_modulation_x == "4")
         document.form.dsl_modulation[4].checked = true;
      else
         document.form.dsl_modulation[5].checked = true;

      if (dsl_annex_x == "0")
         document.form.dsl_annex[0].checked = true;
      else if (dsl_annex_x == "1")
         document.form.dsl_annex[1].checked = true;
      else if (dsl_annex_x == "2")
         document.form.dsl_annex[2].checked = true;
      else if (dsl_annex_x == "3")
         document.form.dsl_annex[3].checked = true;
      else if (dsl_annex_x == "4")
         document.form.dsl_annex[4].checked = true;

	show_footer();
}

function applyRule(){
	//showLoading(3);
	//location.href='as.asp';

	document.form.action_mode.value = " Apply ";
	document.form.submit();
}

</script>
</head>
<body onload="initial();">
<div id="TopBanner"></div>
<div id="Loading" class="popup_bg"></div>
<iframe name="hidden_frame" id="hidden_frame" src="" width="0" height="0" frameborder="0"></iframe>

<form method="post" name="form" id="ruleForm" action="start_apply.htm" target="hidden_frame">
<input type="hidden" name="productid" value="<% nvram_get_f("general.log","productid"); %>">
<input type="hidden" name="current_page" value="Advanced_ADSL_Content.asp">
<input type="hidden" name="next_page" value="Advanced_ADSL_Content.asp">
<input type="hidden" name="next_host" value="">
<input type="hidden" name="sid_list" value="IPConnection;DSLWANConfig;">
<input type="hidden" name="group_id" value="">
<input type="hidden" name="modified" value="0">
<input type="hidden" name="action_mode" value="">
<input type="hidden" name="first_time" value="">
<input type="hidden" name="action_script" value="">
<input type="hidden" name="preferred_lang" id="preferred_lang" value="<% nvram_get_x("LANGUAGE", "preferred_lang"); %>">
<input type="hidden" name="firmver" value="<% nvram_get_x("",  "firmver"); %>">
<input type="hidden" name="wl_ssid2" value="<% nvram_char_to_ascii("WLANConfig11b","wl_ssid"); %>">
<table class="content" align="center" cellpadding="0" cellspacing="0">
  <tr>
	<td width="23">&nbsp;</td>

	<!--=====Beginning of Main Menu=====-->
	<td valign="top" width="202">
	  <div id="mainMenu"></div>
	  <div id="subMenu"></div>
	</td>

    <td valign="top">
	<div id="tabMenu" class="submenuBlock"></div><br />

<!--===================================Beginning of Main Content===========================================-->
<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
	<td align="left" valign="top">
	  <table width="500" border="0" align="center" cellpadding="4" cellspacing="0" class="FormTitle" table>
		<thead>
		<tr>
		  <td><#menu5_3_8#></td>
		</tr>
		</thead>
		<tr>
                  <td bgcolor="#FFFFFF"><#dslsetting_disc0#></td>
                </tr>
		<tbody>
		<tr>
		  <td bgcolor="#FFFFFF">
		    <table width="100%" border="1" align="center" cellpadding="4" cellspacing="0" bordercolor="#6b8fa3" class="FormTable">
		      <thead>
		      <tr>
	  	  	<td colspan="7"><#dslsetting_disc1#></td>
		      </tr>
		      </thead>
		      <tr>
                  	<td><input name="dsl_modulation" type="radio" value="0"> T1.413 </td>
                      </tr>
                      <tr>
                  	<td><input name="dsl_modulation" type="radio" value="1"> G.lite </td>
                      </tr>
                      <tr>
                  	<td><input name="dsl_modulation" type="radio" value="2"> G.Dmt </td>
                      </tr>
                      <tr>
                  	<td><input name="dsl_modulation" type="radio" value="3"> ADSL2 </td>
                      </tr>
		      <tr>
                  	<td><input name="dsl_modulation" type="radio" value="4"> ADSL2+ </td>
		      </tr>
		      <tr>
                  	<td><input name="dsl_modulation" type="radio" value="5"> Multiple Mode </td>
		      </tr>
            	    </table><br/>
		    <table width="100%" border="1" align="center" cellpadding="4" cellspacing="0" bordercolor="#6b8fa3" class="FormTable">
		      <thead>
		      <tr>
	  	  	<td colspan="7"><#dslsetting_disc2#></td>
		      </tr>
		      </thead>
                      <tr>
                  	<td><input name="dsl_annex" type="radio" value="0"> Annex A </td>
                      </tr>
                      <tr>
                  	<td><input name="dsl_annex" type="radio" value="1"> Annex I </td>
                      </tr>
                      <tr>
                  	<td><input name="dsl_annex" type="radio" value="2"> Annex A/L </td>
                      </tr>
                      <tr>
                  	<td><input name="dsl_annex" type="radio" value="3"> Annex M </td>
                      </tr>
                      <tr>
                  	<td><input name="dsl_annex" type="radio" value="4"> Annex A/I/J/L/M </td>
                      </tr>
 		    </table><br/>
		    <table width="100%" border="1" align="center" cellpadding="4" cellspacing="0" bordercolor="#6b8fa3" class="FormTable">
		      <thead>
		      <tr>
	  	  	<td colspan="7">Stability Adjustment</td>
		      </tr>
		      </thead>
				 		<tr>
						<td>
							<select id="" class="input_option" name="dsl_snrm_offset">
								<option value="0" <% nvram_match_x("General", "dsl_snrm_offset", "0", "selected"); %>>Disabled</option>
								<option value="2560" <% nvram_match_x("General", "dsl_snrm_offset", "2560", "selected"); %>>5 dB</option>
								<option value="2048" <% nvram_match_x("General", "dsl_snrm_offset", "2048", "selected"); %>>4 dB</option>
								<option value="1536" <% nvram_match_x("General", "dsl_snrm_offset", "1536", "selected"); %>>3 dB</option>
								<option value="1024" <% nvram_match_x("General", "dsl_snrm_offset", "1024", "selected"); %>>2 dB</option>
								<option value="512" <% nvram_match_x("General", "dsl_snrm_offset", "512", "selected"); %>>1 dB</option>
								<option value="-512" <% nvram_match_x("General", "dsl_snrm_offset", "-512", "selected"); %>>-1 dB</option>
								<option value="-1024" <% nvram_match_x("General", "dsl_snrm_offset", "-1024", "selected"); %>>-2 dB</option>
								<option value="-1536" <% nvram_match_x("General", "dsl_snrm_offset", "-1536", "selected"); %>>-3 dB</option>
								<option value="-2048" <% nvram_match_x("General", "dsl_snrm_offset", "-2048", "selected"); %>>-4 dB</option>
								<option value="-2560" <% nvram_match_x("General", "dsl_snrm_offset", "-2560", "selected"); %>>-5 dB</option>
							</select>
						</td>
						</tr>
 		    </table> <br/>
		    <table width="100%" border="1" align="center" cellpadding="4" cellspacing="0" bordercolor="#6b8fa3" class="FormTable">
		      <thead>
		      <tr>
	  	  	<td colspan="7">SRA(Seamless Rate Adaptation)</td>
		      </tr>
		      </thead>
						<tr>
						<td>
							<select id="" class="input_option" name="dsl_sra">
								<option value="0" <% nvram_match_x("General", "dsl_sra", "0", "selected"); %>>Disabled</option>
								<option value="1" <% nvram_match_x("General", "dsl_sra", "1", "selected"); %>>Enabled</option>
							</select>
						</td>
						</tr>
 		    </table>
		  </td>
		</tr>
		<tr align="right">
          	  <td bgcolor="#FFFFFF">
              	    <input type="button" name="button" class="button" onclick="applyRule();" value="<#CTL_apply#>"/>
          	  </td>
           	</tr>
	    </tbody>
          </table>
        </td>
</form>

	<td id="help_td" style="width:15px;" valign="top">
<form name="hint_form"></form>
	  <div id="helpicon" onClick="openHint(0,0);" title="Click to open AiHelp">
	  	<img src="images/help.gif" />
	  </div>

	  <div id="hintofPM" style="display:none;">
		<table width="100%" cellpadding="0" cellspacing="1" class="Help" bgcolor="#999999">
		  <thead>
		  <tr>
			<td>
			  <div id="helpname" class="AiHintTitle"></div>
			  <a href="javascript:;" onclick="closeHint()" ><img src="images/button-close.gif" class="closebutton" /></a>
			</td>
		  </tr>
		  </thead>

		  <tr>
			<td valign="top" >
			  <div class="hint_body2" id="hint_body"></div>
			  <iframe id="statusframe" name="statusframe" class="statusframe" src="" frameborder="0"></iframe>
			</td>
		  </tr>
		</table>
	  </div>
	</td>

	<!--==============Ending of hint content=============-->
  </tr>
</table>
		<!--===================================Ending of Main Content===========================================-->
	</td>

    <td width="10" align="center" valign="top">&nbsp;</td>
  </tr>
</table>

<div id="footer"></div>
</body>
</html>
