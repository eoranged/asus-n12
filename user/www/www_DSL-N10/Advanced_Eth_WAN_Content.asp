﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<html xmlns:v>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7"/>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta HTTP-EQUIV="Pragma" CONTENT="no-cache">
<meta HTTP-EQUIV="Expires" CONTENT="-1">
<title>ASUS <#Web_Title#> Wireless-N ADSL2/2+ Modem Router - <#menu5_3_1#> - <#menu5_eth_wan#></title>
<link rel="stylesheet" type="text/css" href="./index_style.css">
<link rel="stylesheet" type="text/css" href="./form_style.css">
<script language="JavaScript" type="text/javascript" src="./state.js"></script>
<script language="JavaScript" type="text/javascript" src="./general.js"></script>
<script language="JavaScript" type="text/javascript" src="./popup.js"></script>
<script type="text/javascript" language="JavaScript" src="./help.js"></script>
<script type="text/javascript" language="JavaScript" src="./detect.js"></script>
<script>
wan_route_x = '<% nvram_get_x("IPConnection", "wan_route_x"); %>';
wan_nat_x = '<% nvram_get_x("IPConnection", "wan_nat_x"); %>';
wan_proto = '<% nvram_get_x("Layer3Forwarding",  "wan_proto"); %>';

var DSLWANList = [<% get_DSL_WAN_list("DSLWANConfig", "DSLWANList"); %>];
var all_bridge = 1;
for(var i = 0; i < DSLWANList.length; i++){
	switch(DSLWANList[i][2])
	{
		case "pppoe":
		case "pppoa":
		case "mer":
		case "ipoa":
			all_bridge = 0;
			break;
	}
	if(all_bridge == 0)
		break;
}


var wan_mode = '<% nvram_get_x("Layer3Forwarding",  "use_eth_wan"); %>';
var connection_type = '<% nvram_get_x("Layer3Forwarding",  "ethwan_proto"); %>';
var dsl_config = '<% nvram_get_x("", "dsl_config_num"); %>';
var use_failover2 = "<% nvram_get_x("Layer3Forwarding",  "use_failover_eth_wan"); %>";

function initial()
{
	show_banner(1);
	show_menu(5,3,2);
	show_footer();

	if(wan_mode == "0") //Ethernet WAN off
		hideEthernetWAN("none");
	else
		hideEthernetWAN("");
}

function applyRule()
{
	var wan_port = document.form.use_eth_wan.value;
/*
	if(document.form.use_failover_eth_wan[0].checked) {
		// no support for VPN and DHCP
		if (document.form.ethwan_proto.value == "pptp" || document.form.ethwan_proto.value == "l2tp") {
			if (document.form.ethwan_DHCPClient[0].checked == true) {
				alert("<#eth_failover_vpn_no_supp_msg#>");
				return;
			}
		}
	}	
*/	
	
	if(document.form.use_failover_eth_wan[0].checked) {
		// failover on
		if (dsl_config == '0') {
			alert("<#no_PVC_warning_msg#>");
			location.href = "/Advanced_WAN_Content.asp";
			return;
		}
	}
	
	if(document.form.use_failover_eth_wan[0].checked) {
		if (all_bridge == 1) {
			alert("<#failover_no_supp_bridge#>");
			return;
		}
	}	
	
	
	if(document.form.use_failover_eth_wan[0].checked) {
		// failover on
		document.form.wan_stb_x.value = "0";
	}
	
	if(!check_macaddr(document.form.ethwan_hwaddr_x, check_hwaddr_flag(document.form.ethwan_hwaddr_x))) {
		document.form.ethwan_hwaddr_x.select();
		document.form.ethwan_hwaddr_x.focus();
		return;
	}
	
	showLoading();

	if (wan_port != "0") {
		// disable stb port, if it's equal to ethernet wan port only
		if (document.form.wan_stb_x.value == wan_port)
			document.form.wan_stb_x.value = "0"
		document.form.wan_proto.value = "";	// close 3g wan
		document.form.hsdpa_enable.value = "0";	// close 3g wan
		document.form.failover_3g_enable.value = "0";	// close 3g failover
	}

	document.form.action_mode.value = " Apply ";
	document.form.current_page.value = "/Advanced_Eth_WAN_Content.asp";
	document.form.next_page.value = "";
	document.form.submit();
}

function check_macaddr(obj,flag){
	if(flag == 1){
		alert("<#JS_validmac#>");
		return false;
	}else if(flag == 2){
		alert("<#JS_validmac2#>");
		return false;		
	}else{
		return true;
	}	
}

function enable_idle_timeout(flag)
{
	document.form.ethwan_pppoe_dial_on_demand[0].checked = flag;
	document.form.ethwan_pppoe_dial_on_demand[1].checked = !flag;
	inputCtrl(document.form.ethwan_pppoe_idletime, flag);
	// fix button color insist with others
	// inputCtrl change the style , change them back
	if (navigator.userAgent.indexOf('Opera')!=-1)
	{
		document.form.ethwan_pppoe_dial_on_demand[0].style = "";
		document.form.ethwan_pppoe_dial_on_demand[1].style = "";
	}
}

function enable_ppp_account(flag)
{
	inputCtrl(document.form.ethwan_pppoe_username, flag);
	inputCtrl(document.form.ethwan_pppoe_passwd, flag);
	inputCtrl(document.form.ethwan_pppoe_mtu, flag);
	//inputCtrl(document.form.ethwan_pppoe_mru, flag);
}

function enable_dhcp_client(flag)
{
	document.form.ethwan_DHCPClient[0].checked = flag;
	document.form.ethwan_DHCPClient[1].checked = !flag;
	// fix button color insist with others
	// inputCtrl change the style , change them back
	if (navigator.userAgent.indexOf('Opera')!=-1)
	{
		document.form.ethwan_DHCPClient[0].style = "";
		document.form.ethwan_DHCPClient[1].style = "";
	}
	inputCtrl(document.form.ethwan_ipaddr, !flag);
	inputCtrl(document.form.ethwan_netmask, !flag);
	inputCtrl(document.form.ethwan_gateway, !flag);

	if(flag == 0) //Static
	{
		enable_dynamic_dns(0);
		inputCtrl(document.form.ethwan_dnsenable_x[0], 0);
	}
	else
	{
		inputCtrl(document.form.ethwan_dnsenable_x[0], 1);
	}
	// fix button color insist with others
	// inputCtrl change the style , change them back
	if (navigator.userAgent.indexOf('Opera')!=-1)
	{
		document.form.ethwan_dnsenable_x[0].style = "";
		document.form.ethwan_dnsenable_x[1].style = "";
	}
}

function enable_wan_setting(type)
{
	if(type == "0")
	{
		hideEthernetWAN("none");
		inputCtrl(document.form.ethwan_heartbeat_x, 0);
		enable_ppp_account(0);
		inputCtrl(document.form.ethwan_pppoe_dial_on_demand[0], 0);
		inputCtrl(document.form.ethwan_pppoe_dial_on_demand[1], 0);
		enable_idle_timeout(0);
		inputCtrl(document.form.ethwan_pppoe_mtu, 0);
		//inputCtrl(document.form.ethwan_pppoe_mru, 0);
		inputCtrl(document.form.ethwan_proto, 0);
		inputCtrl(document.form.ethwan_ipaddr, 0);
		inputCtrl(document.form.ethwan_netmask, 0);
		inputCtrl(document.form.ethwan_gateway, 0);
		inputCtrl(document.form.ethwan_upnp_enable[0], 0);
		inputCtrl(document.form.ethwan_upnp_enable[1], 0);
		inputCtrl(document.form.use_failover_eth_wan[0], 0);
		inputCtrl(document.form.use_failover_eth_wan[1], 0);
		inputCtrl(document.form.ethwan_DHCPClient[0], 0);
		inputCtrl(document.form.ethwan_DHCPClient[1], 0);
		inputCtrl(document.form.ethwan_dns1_x, 0);
		inputCtrl(document.form.ethwan_dns2_x, 0);
		inputCtrl(document.form.ethwan_dnsenable_x[0], 0);
		inputCtrl(document.form.ethwan_dnsenable_x[1], 0);

		inputCtrl(document.form.ethwan_pppoe_service, 0);
		inputCtrl(document.form.ethwan_pppoe_ac, 0);
		inputCtrl(document.form.wan_pptp_options_x, 0);
		inputCtrl(document.form.ethwan_pppoe_options_x, 0);
		inputRCtrl1(document.form.ethwan_pppoe_relay_x, 0);

		inputCtrl(document.form.ethwan_hostname, 0);
		inputCtrl(document.form.ethwan_hwaddr_x, 0);
	}
	else
	{
		hideEthernetWAN("");
		inputCtrl(document.form.ethwan_upnp_enable[0], 1);
		inputCtrl(document.form.ethwan_upnp_enable[1], 1);
		inputCtrl(document.form.use_failover_eth_wan[0], 1);
		inputCtrl(document.form.use_failover_eth_wan[1], 1);
		inputCtrl(document.form.ethwan_ipaddr, 1);
		inputCtrl(document.form.ethwan_netmask, 1);
		inputCtrl(document.form.ethwan_gateway, 1);
		inputCtrl(document.form.ethwan_heartbeat_x, 1);

		inputRCtrl1(document.form.ethwan_pppoe_relay_x, 1);

		inputCtrl(document.form.ethwan_hostname, 1);
		inputCtrl(document.form.ethwan_hwaddr_x, 1);

		inputCtrl(document.form.ethwan_proto, 1);
		if(connection_type == "pptp")
		{
			change_wan_type('pptp');
			document.form.ethwan_proto.selectedIndex = 2;
		}
		else if(connection_type == "l2tp")
		{
			change_wan_type('l2tp');
			document.form.ethwan_proto.selectedIndex = 3;
		}
		else if(connection_type == "pppoe")
		{
			change_wan_type('pppoe');
			document.form.ethwan_proto.selectedIndex = 1;
		}
		else if(connection_type == "dhcp")
		{
			change_wan_type('dhcp');
			document.form.ethwan_proto.selectedIndex = 0;
		}
		else if(connection_type == "static")
		{
			change_wan_type('static');
			document.form.ethwan_proto.selectedIndex = 4;
		}
	}
	if (use_failover2=="0") {
		enable_failover_eth(0);
	}
	else {
		enable_failover_eth(1);
	}
	// fix button color insist with others
	// inputCtrl change the style , change them back
	if (navigator.userAgent.indexOf('Opera')!=-1)
	{
		document.form.ethwan_upnp_enable[0].style = "";
		document.form.ethwan_upnp_enable[1].style = "";
		document.form.ethwan_DHCPClient[0].style = "";
		document.form.ethwan_DHCPClient[1].style = "";
		document.form.ethwan_dnsenable_x[0].style = "";
		document.form.ethwan_dnsenable_x[1].style = "";
		document.form.use_failover_eth_wan[0].style = "";
		document.form.use_failover_eth_wan[1].style = "";
	}
}

function change_wan_type(wan_type)
{
	if(wan_type == "l2tp" || wan_type == "pptp"){
		inputCtrl(document.form.ethwan_DHCPClient[0], 1);
		inputCtrl(document.form.ethwan_DHCPClient[1], 1);
		inputCtrl(document.form.ethwan_dns1_x, 1);
		inputCtrl(document.form.ethwan_dns2_x, 1);
		inputCtrl(document.form.ethwan_dnsenable_x[0], 1);
		inputCtrl(document.form.ethwan_dnsenable_x[1], 1);
		enable_ppp_account(1);
		inputCtrl(document.form.ethwan_pppoe_dial_on_demand[0], 0);
		inputCtrl(document.form.ethwan_pppoe_dial_on_demand[1], 0);
		enable_idle_timeout(0);
		inputCtrl(document.form.ethwan_pppoe_mtu, 0);
		//inputCtrl(document.form.ethwan_pppoe_mru, 0);
		enable_dhcp_client(1);
		enable_dynamic_dns(1);
		inputCtrl(document.form.ethwan_pppoe_service, 0);
		inputCtrl(document.form.ethwan_pppoe_ac, 0);
		inputCtrl(document.form.ethwan_pppoe_options_x, 1);

		if(wan_type == "pptp")
		{
			inputCtrl(document.form.wan_pptp_options_x, 1);
			inputCtrl(document.form.ethwan_pppoe_dial_on_demand[0], 1);
			inputCtrl(document.form.ethwan_pppoe_dial_on_demand[1], 1);
		}
		else //l2tp
		{
			inputCtrl(document.form.wan_pptp_options_x, 0);
		}
	}
	else if(wan_type == "pppoe"){
		inputCtrl(document.form.ethwan_DHCPClient[0], 1);
		inputCtrl(document.form.ethwan_DHCPClient[1], 1);
		inputCtrl(document.form.ethwan_dns1_x, 1);
		inputCtrl(document.form.ethwan_dns2_x, 1);
		inputCtrl(document.form.ethwan_dnsenable_x[0], 1);
		inputCtrl(document.form.ethwan_dnsenable_x[1], 1);
		enable_ppp_account(1);
		inputCtrl(document.form.ethwan_pppoe_dial_on_demand[0], 1);
		inputCtrl(document.form.ethwan_pppoe_dial_on_demand[1], 1);
		enable_idle_timeout(0);
		inputCtrl(document.form.ethwan_pppoe_mtu, 1);
		//inputCtrl(document.form.ethwan_pppoe_mru, 1);
		enable_dhcp_client(1);
		enable_dynamic_dns(1);
		inputCtrl(document.form.ethwan_pppoe_service, 1);
		inputCtrl(document.form.ethwan_pppoe_ac, 1);
		inputCtrl(document.form.ethwan_pppoe_options_x, 1);
		inputCtrl(document.form.wan_pptp_options_x, 0);
	}
	else if(wan_type == "dhcp"){
		inputCtrl(document.form.ethwan_DHCPClient[0], 0);
		inputCtrl(document.form.ethwan_DHCPClient[1], 0);
		inputCtrl(document.form.ethwan_dns1_x, 1);
		inputCtrl(document.form.ethwan_dns2_x, 1);
		inputCtrl(document.form.ethwan_dnsenable_x[0], 1);
		inputCtrl(document.form.ethwan_dnsenable_x[1], 1);
		enable_ppp_account(0);
		inputCtrl(document.form.ethwan_pppoe_dial_on_demand[0], 0);
		inputCtrl(document.form.ethwan_pppoe_dial_on_demand[1], 0);
		enable_idle_timeout(0);
		inputCtrl(document.form.ethwan_pppoe_mtu, 0);
		//inputCtrl(document.form.ethwan_pppoe_mru, 0);
		enable_dhcp_client(1);
		enable_dynamic_dns(1);
		inputCtrl(document.form.ethwan_pppoe_service, 0);
		inputCtrl(document.form.ethwan_pppoe_ac, 0);
		inputCtrl(document.form.ethwan_pppoe_options_x, 0);
		inputCtrl(document.form.wan_pptp_options_x, 0);
	}
	else if(wan_type == "static"){
		inputCtrl(document.form.ethwan_DHCPClient[0], 0);
		inputCtrl(document.form.ethwan_DHCPClient[1], 1);
		inputCtrl(document.form.ethwan_dns1_x, 1);
		inputCtrl(document.form.ethwan_dns2_x, 1);
		inputCtrl(document.form.ethwan_dnsenable_x[0], 0);
		inputCtrl(document.form.ethwan_dnsenable_x[1], 1);
		enable_ppp_account(0);
		inputCtrl(document.form.ethwan_pppoe_dial_on_demand[0], 0);
		inputCtrl(document.form.ethwan_pppoe_dial_on_demand[1], 0);
		enable_idle_timeout(0);
		inputCtrl(document.form.ethwan_pppoe_mtu, 0);
		//inputCtrl(document.form.ethwan_pppoe_mru, 0);
		enable_dhcp_client(0);
		enable_dynamic_dns(0);
		inputCtrl(document.form.ethwan_pppoe_service, 0);
		inputCtrl(document.form.ethwan_pppoe_ac, 0);
		inputCtrl(document.form.ethwan_pppoe_options_x, 0);
		inputCtrl(document.form.wan_pptp_options_x, 0);
	}
	// fix button color insist with others
	// inputCtrl change the style , change them back
	if (navigator.userAgent.indexOf('Opera')!=-1)
	{
		document.form.ethwan_upnp_enable[0].style = "";
		document.form.ethwan_upnp_enable[1].style = "";
		document.form.ethwan_DHCPClient[0].style = "";
		document.form.ethwan_DHCPClient[1].style = "";
		document.form.ethwan_dnsenable_x[0].style = "";
		document.form.ethwan_dnsenable_x[1].style = "";
		document.form.ethwan_pppoe_dial_on_demand[0].style = "";
		document.form.ethwan_pppoe_dial_on_demand[1].style = "";
	}
}

function enable_dynamic_dns(type)
{
	document.form.ethwan_dnsenable_x[0].checked = type;
	document.form.ethwan_dnsenable_x[1].checked = !type;
	inputCtrl(document.form.ethwan_dns1_x, !type);
	inputCtrl(document.form.ethwan_dns2_x, !type);
	// fix button color insist with others
	// inputCtrl change the style , change them back
	if (navigator.userAgent.indexOf('Opera')!=-1)
	{
		document.form.ethwan_dnsenable_x[0].style = "";
		document.form.ethwan_dnsenable_x[1].style = "";
	}
}

function done_validating(action)
{
	refreshpage();
}

function enable_failover_eth(flag)
{
	if (flag == 0) {
		document.form.use_failover_eth_wan[0].checked = false;
		document.form.use_failover_eth_wan[1].checked = true;
	}
	else {
		document.form.use_failover_eth_wan[0].checked = true;
		document.form.use_failover_eth_wan[1].checked = false;
	}

	if (flag == 0) {
		document.getElementById("primary_wan_conn").innerHTML = "<font color='black'><#wl_securitylevel_0#></font>";
		document.getElementById("backup_wan_conn").innerHTML = "<font color='black'><#wl_securitylevel_0#></font>";
	}
	else {
		document.getElementById("primary_wan_conn").innerHTML = "<font color='black'><b><#menu5_dsl_wan#></b></font>";
		document.getElementById("backup_wan_conn").innerHTML = "<font color='black'><b><#menu5_eth_wan#></b></font>";
	}
}

function hideEthernetWAN(flag)
{
	document.getElementById("hideEthernetWAN1").style.display = flag;
	document.getElementById("hideEthernetWAN1a").style.display = flag;
	document.getElementById("hideEthernetWAN2").style.display = flag;
	document.getElementById("hideEthernetWAN3").style.display = flag;
	document.getElementById("hideEthernetWAN4").style.display = flag;
	document.getElementById("hideEthernetWAN5").style.display = flag;
	if (navigator.userAgent.indexOf('Opera')!=-1)
	{
		// fix button color insist with others
		// inputCtrl change the style , change them back
		document.form.ethwan_pppoe_dial_on_demand[0].style = "";
		document.form.ethwan_pppoe_dial_on_demand[1].style = "";
		document.form.ethwan_DHCPClient[0].style = "";
		document.form.ethwan_DHCPClient[1].style = "";
		document.form.ethwan_dnsenable_x[0].style = "";
		document.form.ethwan_dnsenable_x[1].style = "";
		document.form.ethwan_upnp_enable[0].style = "";
		document.form.ethwan_upnp_enable[1].style = "";
		document.form.use_failover_eth_wan[0].style = "";
		document.form.use_failover_eth_wan[1].style = "";
	}
}
</script>
</head>
<body onload="initial();" onunLoad="return unload_body();">

<div id="TopBanner"></div>

<div id="Loading" class="popup_bg"></div>

<iframe name="hidden_frame" id="hidden_frame" src="" width="0" height="0" frameborder="0"></iframe>
<form method="post" name="form" id="ruleForm" action="/start_apply.htm" target="hidden_frame">
<input type="hidden" name="productid" value="<#Web_Title#>">

<input type="hidden" name="current_page" value="Advanced_Eth_WAN_Content.asp">
<input type="hidden" name="next_page" value="">
<input type="hidden" name="next_host" value="">
<input type="hidden" name="sid_list" value="General;Layer3Forwarding;LANHostConfig;">
<input type="hidden" name="group_id" value="">
<input type="hidden" name="modified" value="0">
<input type="hidden" name="action_mode" value="">
<input type="hidden" name="first_time" value="">
<input type="hidden" name="action_script" value="">
<input type="hidden" name="preferred_lang" id="preferred_lang" value="<% nvram_get_x("LANGUAGE", "preferred_lang"); %>">
<input type="hidden" name="wl_ssid2" value="<% nvram_char_to_ascii("WLANConfig11b", "wl_ssid"); %>">
<input type="hidden" name="firmver" value="<% nvram_get_x("",  "firmver"); %>">
<input type="hidden" name="wan_stb_x" value="<% nvram_get_x("Layer3Forwarding", "wan_stb_x"); %>">
<input type="hidden" name="wan_proto" value="<% nvram_get_x("Layer3Forwarding",  "wan_proto"); %>">
<input type="hidden" name="hsdpa_enable" value="<% nvram_get_x("", "hsdpa_enable"); %>">
<input type="hidden" name="failover_3g_enable" value="<% nvram_get_x("", "failover_3g_enable"); %>">

<table class="content" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td width="23">&nbsp;</td>
    <td valign="top" width="202">
      <div  id="mainMenu"></div>
      <div  id="subMenu"></div>
    </td>
    <td valign="top">
      <div id="tabMenu" class="submenuBlock"></div><br />
      <!--===================================Beginning of Main Content===========================================-->
      <table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
	<td valign="top" align="left">
	<table width="92%" border="0" align="center" cellpadding="5" cellspacing="0" class="FormTitle">
	<thead>
	<tr>
	  <td><#menu5_3_1#> - <#menu5_eth_wan#></td>
	</tr>
	</thead>
	<tbody>
	<tr>
	  <td bgcolor="#FFFFFF"><#Ethernet_WAN_desc#> <#Ethernet_WAN_desc2#></td>
	</tr>
	<tr>
	  <td bgcolor="#FFFFFF"><table width="100%" border="1" align="center" cellpadding="4" cellspacing="0" bordercolor="#6b8fa3"  class="FormTable">
          <tr>
            <th width="30%"><a class="hintstyle" href="javascript:void(0);" onClick="window.parent.openHint(7,25);"><#Layer3Forwarding_x_EthWAN_itemname#></a></th>
	    <td align="left">
			<select name="use_eth_wan" class="input" onchange="enable_wan_setting(this.value);">
				<option value="0" <% nvram_match_x("Layer3Forwarding", "use_eth_wan", "0", "selected"); %>><#btn_Disabled#></option>
				<option value="1" <% nvram_match_x("Layer3Forwarding", "use_eth_wan", "1", "selected"); %>>LAN1</option>
				<option value="2" <% nvram_match_x("Layer3Forwarding", "use_eth_wan", "2", "selected"); %>>LAN2</option>
				<option value="3" <% nvram_match_x("Layer3Forwarding", "use_eth_wan", "3", "selected"); %>>LAN3</option>
				<option value="4" <% nvram_match_x("Layer3Forwarding", "use_eth_wan", "4", "selected"); %>>LAN4</option>
			</select>
<script>
	if(wan_mode != "0") //Ethernet WAN, show Route setting
	{
		document.writeln("<br><font color='red'><#dual_wan_config_route#></font>");
	}
</script>
	    </td>
          </tr>
          </table></td>
	</tr>

	<tr>
	  <td bgcolor="#FFFFFF">
<div id="hideEthernetWAN1" style="display='none'">
	  <table class="FormTable" align="center" border="1" bordercolor="#6b8fa3" cellpadding="4" cellspacing="0" width="100%">
	   <thead><tr><td colspan="2"><#WANCFG_connection#></td></tr></thead>
	   <tbody>
            <tr>
	     <th width="30%"><a class="hintstyle" href="javascript:void(0);" onClick="window.parent.openHint(7,16);"><#Connectiontype#></a></th>
	        <td align="left">
						<span id="protocolType">
						<select class="input" name="ethwan_proto" onchange="change_wan_type(this.value);">
							<option value="dhcp" <% nvram_match_x("Layer3Forwarding", "ethwan_proto", "dhcp", "selected"); %>>Automatic IP</option>
							<option value="pppoe" <% nvram_match_x("Layer3Forwarding", "ethwan_proto", "pppoe", "selected"); %>>PPPoE</option>
							<option value="pptp" <% nvram_match_x("Layer3Forwarding", "ethwan_proto", "pptp", "selected"); %>>PPTP</option>
							<option value="l2tp" <% nvram_match_x("Layer3Forwarding", "ethwan_proto", "l2tp", "selected"); %>>L2TP</option>
							<option value="static" <% nvram_match_x("Layer3Forwarding", "ethwan_proto", "static", "selected"); %>>Static IP</option>
						</select>
						</span>
						<br><#Eth_wan_dhcp_note_itemname#>
	        </td>
	    </tr>
	    <tr>
	     <th width="30%"><#BasicConfig_EnableMediaServer_itemname#></th>
		<td style="font-weight: normal;" align="left">
			<input type="radio" name="ethwan_upnp_enable"  class="content_input_fd" value="1" onclick="return change_common_radio(this, 'Layer3Forwarding', 'ethwan_upnp_enable', '1')" <% nvram_match_x("Layer3Forwarding","ethwan_upnp_enable", "1", "checked"); %>><#checkbox_Yes#>
			<input type="radio" name="ethwan_upnp_enable"  class="content_input_fd" value="0" onclick="return change_common_radio(this, 'Layer3Forwarding', 'ethwan_upnp_enable', '0')" <% nvram_match_x("Layer3Forwarding","ethwan_upnp_enable", "0", "checked"); %>><#checkbox_No#>
		</td>
	    </tr>
	   </tbody>
        </table>
</div>
	  </th></tr>


		<tr>
			<td bgcolor="#ffffff">
<div id="hideEthernetWAN2" style="display='none'">
		<table class="FormTable" align="center" border="1" bordercolor="#6b8fa3" cellpadding="4" cellspacing="0" width="100%">
				<thead>
				<tr>
					<td colspan="2"><#IPConnection_ExternalIPAddress_sectionname#></td>
				</tr>
				</thead>
				<tbody>

                <tr>
					<th width="30%"><a class="hintstyle" href="javascript:void(0);" onClick="window.parent.openHint(7,17);"><#Layer3Forwarding_x_DHCPClient_itemname#></a></th>
					<td align="left">
						<input style="background-color: rgb(192, 218, 228);" disabled="disabled" name="ethwan_DHCPClient" class="input" value="1" onclick="enable_dhcp_client(1);" checked="checked" type="radio" <% nvram_match_x("Layer3Forwarding", "ethwan_DHCPClient", "1", "checked"); %>><#checkbox_Yes#>
						<input style="background-color: rgb(192, 218, 228);" disabled="disabled" name="ethwan_DHCPClient" class="input" value="0" onclick="enable_dhcp_client(0);" type="radio" <% nvram_match_x("Layer3Forwarding", "ethwan_DHCPClient", "0", "checked"); %>><#checkbox_No#>
					</td>
				</tr>

				<tr>
					<th width="30%"><a class="hintstyle" href="javascript:void(0);" onClick="window.parent.openHint(7,1);"><#IPConnection_ExternalIPAddress_itemname#></a></th>
					<td align="left"><input style="background-color: rgb(204, 204, 204);" disabled="disabled" name="ethwan_ipaddr" value="<% nvram_get_x("Layer3Forwarding",  "ethwan_ipaddr"); %>" maxlength="15" class="input" size="15" onkeypress="return is_ipaddr(this);" onkeyup="change_ipaddr(this);" type="text"></td>
				</tr>
				<tr>
					<th><a class="hintstyle" href="javascript:void(0);" onClick="window.parent.openHint(7,2);"><#IPConnection_x_ExternalSubnetMask_itemname#></a></th>
					<td align="left"><input style="background-color: rgb(204, 204, 204);" disabled="disabled" name="ethwan_netmask" value="<% nvram_get_x("Layer3Forwarding",  "ethwan_netmask"); %>" maxlength="15" class="input" size="15" onkeypress="return is_ipaddr(this);" onkeyup="change_ipaddr(this);" type="text"></td>
				</tr>
				<tr>
					<th><a class="hintstyle" href="javascript:void(0);" onClick="window.parent.openHint(7,3);"><#IPConnection_x_ExternalGateway_itemname#></a></th>
					<td align="left"><input style="background-color: rgb(204, 204, 204);" disabled="disabled" name="ethwan_gateway" value="<% nvram_get_x("Layer3Forwarding",  "ethwan_gateway"); %>" maxlength="15" class="input" size="15" onkeypress="return is_ipaddr(this);" onkeyup="change_ipaddr(this);" type="text"></td>
				</tr>
				</tbody>
		</table>
</div>
			</td>
	  	</tr>

	  <tr>
	    <td bgcolor="#FFFFFF">
<div id="hideEthernetWAN3" style="display='none'">
		<table width="100%" border="1" align="center" cellpadding="4" cellspacing="0" bordercolor="#6b8fa3"  class="FormTable">
          <thead>
            <tr>
              <td colspan="2"><#IPConnection_x_DNSServerEnable_sectionname#></td>
            </tr>
          </thead>
          <tr>
            <th width="30%"><a class="hintstyle" href="javascript:void(0);" onClick="openHint(7,10);"><#IPConnection_x_DNSServerEnable_itemname#></a></th>
			<td>
			  <input class="content_input_fd" name="ethwan_dnsenable_x" value="1" onclick="enable_dynamic_dns(1);" type="radio" checked="checked"><#checkbox_Yes#>
			  <input class="content_input_fd" name="ethwan_dnsenable_x" value="0" onclick="enable_dynamic_dns(0);" type="radio"><#checkbox_No#>
			</td>
          </tr>
          <tr>
            <th><a class="hintstyle" href="javascript:void(0);" onClick="openHint(7,13);"><#IPConnection_x_DNSServer1_itemname#></a></th>
            <td><input type="text" maxlength="15" class="input" size="15" name="ethwan_dns1_x" value="<% nvram_get_x("Layer3Forwarding","ethwan_dns1_x"); %>" onkeypress="return is_ipaddr(this)" onkeyup="change_ipaddr(this)" /></td>
          </tr>
          <tr>
            <th><a class="hintstyle" href="javascript:void(0);" onClick="openHint(7,14);"><#IPConnection_x_DNSServer2_itemname#></a></th>
            <td><input type="text" maxlength="15" class="input" size="15" name="ethwan_dns2_x" value="<% nvram_get_x("Layer3Forwarding","ethwan_dns2_x"); %>" onkeypress="return is_ipaddr(this)" onkeyup="change_ipaddr(this)" /></td>
          </tr>
        </table>
</div>
        </td>
	  </tr>

	  <tr>
	    <td bgcolor="#ffffff">
<div id="hideEthernetWAN4" style="display='none'">
		  <table class="FormTable" align="center" border="1" bordercolor="#6b8fa3" cellpadding="4" cellspacing="0" width="100%">
            <thead>
            <tr>
              <td colspan="2"><#PPPConnection_UserName_sectionname#></td>
            </tr>
            </thead>
            <tbody><tr>
              <th width="30%"><a class="hintstyle" href="javascript:void(0);" onClick="window.parent.openHint(7,4);"><#PPPConnection_UserName_itemname#></a></th>
              <td align="left"><input style="background-color: rgb(204, 204, 204);" disabled="disabled" maxlength="64" class="input" size="32" name="ethwan_pppoe_username" value="<% nvram_get_x("Layer3Forwarding",  "ethwan_pppoe_username"); %>" onkeypress="return is_string(this)" type="text"></td>
            </tr>
            <tr>
              <th><a class="hintstyle" href="javascript:void(0);" onClick="window.parent.openHint(7,5);"><#PPPConnection_Password_itemname#></a></th>
              <td align="left"><input style="background-color: rgb(204, 204, 204);" disabled="disabled" maxlength="32" class="input" size="32" name="ethwan_pppoe_passwd" value="<% nvram_get_x("Layer3Forwarding",  "ethwan_pppoe_passwd"); %>" type="password"></td>
            </tr>
            <tr>
              <th><a class="hintstyle" href="javascript:void(0);" onClick="window.parent.openHint(7,6);"><#PPPConnection_dialondemand_itemname#></a></th>
              <td align="left">
						<input class="content_input_fd" name="ethwan_pppoe_dial_on_demand" value="1" onclick="enable_idle_timeout(1);" type="radio"><#checkbox_Yes#>
						<input class="content_input_fd" name="ethwan_pppoe_dial_on_demand" value="0" onclick="enable_idle_timeout(0);" type="radio" checked="checked"><#checkbox_No#>
              </td>
            </tr>
            <tr>
              <th><a class="hintstyle" href="javascript:void(0);" onClick="window.parent.openHint(7,6);"><#PPPConnection_IdleDisconnectTime_itemname#></a></th>
              <td align="left">
						<input style="background-color: rgb(204, 204, 204);" disabled="disabled" maxlength="10" class="input" size="15" name="ethwan_pppoe_idletime" value="<% nvram_get_x("Layer3Forwarding",  "ethwan_pppoe_idletime"); %>" onkeypress="return is_number(this)" type="text"> <b><span>0 - 4294927695</span></b>
              </td>
            </tr>
            <tr>
              <th><a class="hintstyle" href="javascript:void(0);" onClick="window.parent.openHint(7,7);"><#PPPConnection_x_PPPoEMTU_itemname#></a></th>
              <td align="left"><input style="background-color: rgb(204, 204, 204);" disabled="disabled" maxlength="4" size="5" name="ethwan_pppoe_mtu" value="<% nvram_get_x("Layer3Forwarding",  "ethwan_pppoe_mtu"); %>" class="input" value="1492" onkeypress="return is_number(this)" type="text"> <b><span>576 - 1492</span></b></td>
            </tr>
<!--
            <tr>
              <th><a class="hintstyle" href="javascript:void(0);" onClick="window.parent.openHint(7,8);"><#PPPConnection_x_PPPoEMRU_itemname#></a></th>
              <td align="left"><input style="background-color: rgb(204, 204, 204);" disabled="disabled" maxlength="4" size="5" name="ethwan_pppoe_mru" value="<% nvram_get_x("Layer3Forwarding",  "ethwan_pppoe_mru"); %>" class="input" value="1492" onkeypress="return is_number(this)" type="text"> <b><span>576 - 1492</span></b></td>
            </tr>
-->
            <tr>
              <th><a class="hintstyle" href="javascript:void(0);" onClick="window.parent.openHint(7,9);"><#PPPConnection_x_ServiceName_itemname#></a></th>
              <td align="left"><input style="background-color: rgb(204, 204, 204);" disabled="disabled" maxlength="32" class="input" size="32" name="ethwan_pppoe_service" value="<% nvram_get_x("Layer3Forwarding","ethwan_pppoe_service"); %>" onkeypress="return is_string(this)" type="text"></td>
            </tr>
            <tr>
              <th><a class="hintstyle" href="javascript:void(0);" onClick="window.parent.openHint(7,22);"><#PPPConnection_x_AccessConcentrator_itemname#></a></th>
              <td align="left"><input style="background-color: rgb(204, 204, 204);" disabled="disabled" maxlength="32" class="input" size="32" name="ethwan_pppoe_ac" value="<% nvram_get_x("Layer3Forwarding","ethwan_pppoe_ac"); %>" onkeypress="return is_string(this)" /></td>
            </tr>
	    <tr>
	      <th><a class="hintstyle" href="javascript:void(0);" onClick="openHint(7,23);"><#PPPConnection_x_PPTPOptions_itemname#></a></th>
	      <td><select name="wan_pptp_options_x" class="input">
		    <option value="-mppc" <% nvram_match_x("Layer3Forwarding","wan_pptp_options_x", "-mppc","selected"); %>>No Encryption</option>
		    <option value="+mppe-40" <% nvram_match_x("Layer3Forwarding","wan_pptp_options_x", "+mppe-40","selected"); %>>MPPE 40</option>
		    <!--option value="+mppe-56" <% nvram_match_x("Layer3Forwarding","wan_pptp_options_x", "+mppe-56","selected"); %>>MPPE 56</option-->
		    <option value="+mppe-128" <% nvram_match_x("Layer3Forwarding","wan_pptp_options_x", "+mppe-128","selected"); %>>MPPE 128</option>
		    <option value="" <% nvram_match_x("Layer3Forwarding","wan_pptp_options_x", "","selected"); %>>Auto</option>
		  </select></td>
	    </tr>
	    <tr>
	      <th><a class="hintstyle" href="javascript:void(0);" onClick="window.parent.openHint(7,18);"><#PPPConnection_x_AdditionalOptions_itemname#></a></th>
	      <td align="left"><input style="background-color: rgb(204, 204, 204);" disabled="disabled" class="input" maxlength="255" size="32" name="ethwan_pppoe_options_x" value="<% nvram_get_x("Layer3Forwarding","ethwan_pppoe_options_x"); %>" onkeypress="return is_string(this)" onblur="validate_string(this)" type="text"></td>
	    </tr>
            <tr>
              <th><a class="hintstyle" href="javascript:void(0);" onClick="openHint(7,11);"><#PPPConnection_x_PPPoERelay_itemname#></a></th>
              <td>
              	<input type="radio" value="1" name="ethwan_pppoe_relay_x" class="content_input_fd" onclick="return change_common_radio(this, 'Layer3Forwarding', 'ethwan_pppoe_relay_x', '1')" <% nvram_match_x("Layer3Forwarding","ethwan_pppoe_relay_x", "1", "checked"); %> /><#checkbox_Yes#>
                <input type="radio" value="0" name="ethwan_pppoe_relay_x" class="content_input_fd" onclick="return change_common_radio(this, 'Layer3Forwarding', 'ethwan_pppoe_relay_x', '0')" <% nvram_match_x("Layer3Forwarding","ethwan_pppoe_relay_x", "0", "checked"); %> /><#checkbox_No#>
              </td>
            </tr>
          </tbody>
          </table>
</div>
          </td>
	  </tr>

	<tr>
	<td bgcolor="#ffffff">
<div id="hideEthernetWAN5" style="display='none'">
	<table class="FormTable" align="center" border="1" bordercolor="#6b8fa3" cellpadding="4" cellspacing="0" width="100%">
	<thead>
	    <tr>
		<td colspan="2"><#PPPConnection_x_HostNameForISP_sectionname#></td>
	    </tr>
	</thead>
	<tr>
          <th width="30%"><a class="hintstyle" href="javascript:void(0);" onClick="openHint(7,19);"><#PPPConnection_x_HeartBeat_itemname#></a></th>
          <td align="left"><input style="background-color: rgb(204, 204, 204);" disabled="disabled" type="text" name="ethwan_heartbeat_x" class="input" maxlength="256" size="32" value="<% nvram_get_x("Layer3Forwarding","ethwan_heartbeat_x"); %>" onKeyPress="return is_string(this)"></td>
        </tr>
        <tr>
          <th width="30%"><a class="hintstyle" href="javascript:void(0);" onClick="openHint(7,15);"><#PPPConnection_x_HostNameForISP_itemname#></a></th>
          <td align="left"><input style="background-color: rgb(204, 204, 204);" disabled="disabled" type="text" name="ethwan_hostname" class="input" maxlength="32" size="32" value="<% nvram_get_x("Layer3Forwarding","ethwan_hostname"); %>" onkeypress="return is_string(this)"></td>
        </tr>
        <tr>
          <th width="30%"><a class="hintstyle" href="javascript:void(0);" onClick="openHint(7,24);"><#PPPConnection_x_MacAddressForISP_itemname#></a></th>
          <td align="left"><input style="background-color: rgb(204, 204, 204);" disabled="disabled" type="text" name="ethwan_hwaddr_x" class="input" maxlength="12" size="12" value="<% nvram_get_x("Layer3Forwarding","ethwan_hwaddr_x"); %>" onKeyPress="return is_hwaddr()"></td>
        </tr>
	</tbody>
	</table>
</div>
	</td>
	</tr>

	<tr>
	  <td bgcolor="#FFFFFF">
<div id="hideEthernetWAN1a" style="display='none'">
			<table class="FormTable" align="center" border="1" bordercolor="#6b8fa3" cellpadding="4" cellspacing="0" width="100%">
				<thead><tr><td colspan="2"><#fail_over_setting#></td></tr></thead>
				<tbody>
					<tr>
						<th width="30%"><a class="hintstyle" href="javascript:void(0);" onClick="openHint(7,29);"><#enable_fail_over#></a></th>
						<td align="left">
							<input type="radio" name="use_failover_eth_wan" class="input" value="1" onclick="enable_failover_eth(1);" <% nvram_match_x("Layer3Forwarding","use_failover_eth_wan", "1", "checked"); %>><#checkbox_Yes#>
							<input type="radio" name="use_failover_eth_wan" class="input" value="0" onclick="enable_failover_eth(0);" <% nvram_match_x("Layer3Forwarding","use_failover_eth_wan", "0", "checked"); %>><#checkbox_No#>
							<br><#eth_failover_IPTV_enable_msg#>
						</td>
					</tr>
					<tr>
						<th width="30%"><#primary_wan#></th>
						<td style="font-weight: normal;" align="left"><span id="primary_wan_conn"></span></td>
					</tr>
					<tr>
						<th width="30%"><#backup_wan#></th>
						<td style="font-weight: normal;" align="left"><span id="backup_wan_conn"></span></td>
					</tr>
</div>
				</tbody>
			</table>
</div>
	  </th></tr>


       	  <tr>
	  <td bgcolor="#ffffff">
	</tbody>
	<tr>
	  <td bgcolor="#FFFFFF">
	  <table width="100%" border="1" align="center" cellpadding="4" cellspacing="0" bordercolor="#6b8fa3"  class="FormTable">
            <tr>
              <td colspan="2"  align="right"><input name="button" type="button" class="button" onclick="applyRule()" value="<#CTL_apply#>"/></td>
            </tr>
		</table>
 	  </td>
	</tr>
</table>
</td>

</form>

	<td id="help_td" style="width:15px;" valign="top">

	  <div id="helpicon" onClick="openHint(0,0);" title="Click to open Help."><img src="images/help.gif" /></div>
	  <div id="hintofPM" style="display:none;">
<form name="hint_form"></form>
	    <table width="100%" cellpadding="0" cellspacing="1" class="Help" bgcolor="#999999">
		  <thead>
		  <tr>
			<td>
			  <div id="helpname" class="AiHintTitle"></div>
			  <a href="javascript:;" onclick="closeHint()" ><img src="images/button-close.gif" class="closebutton" /></a>
			</td>
		  </tr>
		  </thead>

		  <tr>
			<td valign="top" >
			  <div class="hint_body2" id="hint_body"></div>
			  <iframe id="statusframe" name="statusframe" class="statusframe" src="" frameborder="0"></iframe>
			</td>
		  </tr>
		</table>
	  </div><!--End of hintofPM-->
		  </td>
        </tr>
      </table>
		<!--===================================Ending of Main Content===========================================-->
	</td>

    <td width="10" align="center" valign="top">&nbsp;</td>
	</tr>
</table>

<div id="footer"></div>
<script>
var dial_on_demand = "<% nvram_get_x("Layer3Forwarding",  "ethwan_pppoe_dial_on_demand"); %>";
var idle_time ="<% nvram_get_x("Layer3Forwarding",  "ethwan_pppoe_idletime"); %>";
var enable_dhcp = "<% nvram_get_x("Layer3Forwarding",  "ethwan_DHCPClient"); %>";
var enable_dns = "<% nvram_get_x("Layer3Forwarding",  "ethwan_dnsenable_x"); %>";
var use_failover = "<% nvram_get_x("Layer3Forwarding",  "use_failover_eth_wan"); %>";


if(wan_mode != "0") //Ethernet WAN
{
	enable_wan_setting("1");

	if(idle_time == "")
		document.form.ethwan_pppoe_idletime.value = 0;

	if(connection_type == "pppoe" || connection_type == "pptp")
	{
		if(dial_on_demand == "1")
		{
			document.form.ethwan_pppoe_dial_on_demand[0].checked = 1;
			document.form.ethwan_pppoe_dial_on_demand[1].checked = 0;
			inputCtrl(document.form.ethwan_pppoe_idletime, 1);
		}
		else
		{
			document.form.ethwan_pppoe_dial_on_demand[0].checked = 0;
			document.form.ethwan_pppoe_dial_on_demand[1].checked = 1;
			inputCtrl(document.form.ethwan_pppoe_idletime, 0);
		}
		// fix insist radio button
		document.form.ethwan_pppoe_dial_on_demand[0].style = "";
		document.form.ethwan_pppoe_dial_on_demand[1].style = "";
	}

	if(connection_type != "dhcp") //no need to set enable_dhcp_client() for dhcp mode
	{
		if(enable_dhcp == "0")
			enable_dhcp_client(0);
		else
			enable_dhcp_client(1);
	}

	if(enable_dns == "0")
		enable_dynamic_dns(0);
	else
		enable_dynamic_dns(1);

	if (use_failover == "0")
		enable_failover_eth(0);
	else
		enable_failover_eth(1);

}
else //Ethernet WAN Disabled
	enable_wan_setting("0");

</script>
</body>
</html>
