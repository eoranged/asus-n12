﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<html xmlns:v>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta HTTP-EQUIV="Pragma" CONTENT="no-cache">
<meta HTTP-EQUIV="Expires" CONTENT="-1">
<title>ASUS <#Web_Title#> Wireless-N ADSL2/2+ Modem Router - <#menu5_3_1#> - <#menu5_3_3gwan#></title>
<link rel="stylesheet" type="text/css" href="index_style.css">
<link rel="stylesheet" type="text/css" href="form_style.css">

<script type="text/javascript" src="/state.js"></script>
<script type="text/javascript" src="/general.js"></script>
<script type="text/javascript" src="/popup.js"></script>
<script type="text/javascript" src="/help.js"></script>
<!-- <script type="text/javascript" src="/detect.js"></script> -->
<script type="text/javascript" src="/wcdma_list.js"></script>
<script type="text/javascript" src="/cdma2000_list.js"></script>
<script type="text/javascript" src="/td-scdma_list.js"></script>
<!-- <script type="text/javascript" src="/wimax_list.js"></script> -->
<script>
wan_route_x = '<% nvram_get_x("IPConnection", "wan_route_x"); %>';
wan_nat_x = '<% nvram_get_x("IPConnection", "wan_nat_x"); %>';
wan_proto = '<% nvram_get_x("Layer3Forwarding", "wan_proto"); %>';

var dsl_config = '<% nvram_get_x("", "dsl_config_num"); %>';

var DSLWANList = [<% get_DSL_WAN_list("DSLWANConfig", "DSLWANList"); %>];
var all_bridge = 1;
for(var i = 0; i < DSLWANList.length; i++){
	switch(DSLWANList[i][2])
	{
		case "pppoe":
		case "pppoa":
		case "mer":
		case "ipoa":
			all_bridge = 0;
			break;
	}
	if(all_bridge == 0)
		break;
}

<% login_state_hook(); %>
var wireless = [<% wl_auth_list(); %>]; // [[MAC, associated, authorized], ...]

var modem = '<% nvram_get_x("General", "Dev3G"); %>';
var country = '<% nvram_get_x("General", "hsdpa_country"); %>';
var isp = '<% nvram_get_x("General", "hsdpa_isp"); %>';

var apn = '<% nvram_get_x("General", "hsdpa_apn"); %>';
var dialnum = '<% nvram_get_x("General", "hsdpa_dialnum"); %>';
var user = '<% nvram_get_x("General", "hsdpa_user"); %>';
var pass = '<% nvram_get_x("General", "hsdpa_pass"); %>';
var enable3g = '<% nvram_get_x("General", "hsdpa_enable"); %>';
var chk3g = '<% nvram_get_x("", "chk3g"); %>';
var fo_3g_enable = '<% nvram_get_x("Layer3Forwarding", "failover_3g_enable"); %>';

var modemlist = new Array();
var countrylist = new Array();
var isplist = new Array();

var apnlist = new Array();
var daillist = new Array();
var userlist = new Array();
var passlist = new Array();

function initial(){
	show_banner(1);
	show_menu(5,3,3);
	show_footer();

	//hsdpa_enable remain origial value, even wan_proto is changed to others
	//so enable3g should be reset
	if(wan_proto != "3g" && fo_3g_enable != '1') {
		enable3g="0";
	}
	if(enable3g=="0")
		document.form.hsdpa_enable.options[0].selected = true;
	else if(enable3g=="1")
		document.form.hsdpa_enable.options[1].selected = true;
	else if(enable3g=="2")
		document.form.hsdpa_enable.options[2].selected = true;
	else if(enable3g=="3")
		document.form.hsdpa_enable.options[3].selected = true;

	//switch_modem_mode(document.form.hsdpa_enable.value);
	//gen_list(document.form.hsdpa_enable.value);
	switch_modem_mode(enable3g);
	gen_list(enable3g);

	show_ISP_list();
	show_APN_list();
	show_hide_failover();

	enable_auto_hint(21, 7);
}

function show_modem_list(mode){
	if(mode == "4")
		show_4G_modem_list();
	else
		show_3G_modem_list();
}

function show_3G_modem_list(){
	modemlist = new Array(
			"AUTO"
			, "ASUS-T500"
			, "BandLuxe-C120"
			, "Huawei-E1550"
			, "Huawei-E160G"
			, "Huawei-E161"
			, "Huawei-E169"
			, "Huawei-E176"
			, "Huawei-E220"
			, "Huawei-K3520"
			, "Huawei-ET128"
			, "Huawei-E1800"
			, "Huawei-K4505"
			, "Huawei-E172"
			, "Huawei-E372"
			, "Huawei-E122"
			, "Huawei-E160E"
			, "Huawei-E1552"
			, "Huawei-E173"
			, "Huawei-E1823"
			, "Huawei-E1762"
			, "Huawei-E1750C"
			, "Huawei-E1752Cu"
			, "Huawei-K3770"
			//, "MU-Q101"
			, "Alcatel-X200"
			, "AnyData-ADU-500A"
			, "ZTE-MF622"
			, "ZTE-MF626"
			, "Nokia-CS15"
			, "Nokia-CS17"
			, "Option-iCON-401"
			);

	free_options($("shown_modems"));
	for(var i = 0; i < modemlist.length; i++){
		$("shown_modems").options[i] = new Option(modemlist[i], modemlist[i]);
		if(modemlist[i] == modem)
			$("shown_modems").options[i].selected = "1";
	}
}

function show_4G_modem_list(){
	modemlist = new Array(
			"AUTO"
			, "Samsung U200"
			//, "Beceem BCMS250"
			);


	free_options($("shown_modems"));
	for(var i = 0; i < modemlist.length; i++){
		$("shown_modems").options[i] = new Option(modemlist[i], modemlist[i]);
		if(modemlist[i] == modem)
			$("shown_modems").options[i].selected = "1";
	}
}

function switch_modem_mode(mode){
	//if(mode != "0" && $("hsdpa_hint").style.display == "none")
	//	alert("<#HSDPA_LANtoWAN#>");

	show_modem_list(mode);

	if(mode == "1"){ // WCDMA
		document.form.Dev3G.disabled = false;
		document.form.hsdpa_country.disabled = false;
		document.form.hsdpa_isp.disabled = false;
		document.form.hsdpa_apn.disabled = false;
		document.form.wan_3g_pin.disabled = false;
		document.form.hsdpa_dialnum.disabled = false;
		document.form.hsdpa_user.disabled = false;
		document.form.hsdpa_pass.disabled = false;
		//document.form.modem_ttlsid.disabled = true;
		//$("hsdpa_hint").style.display = "";
	}
	else if(mode == "2"){ // CDMA2000
		document.form.Dev3G.disabled = false;
		document.form.hsdpa_country.disabled = false;
		document.form.hsdpa_isp.disabled = false;
		document.form.hsdpa_apn.disabled = false;
		document.form.wan_3g_pin.disabled = false;
		document.form.hsdpa_dialnum.disabled = false;
		document.form.hsdpa_user.disabled = false;
		document.form.hsdpa_pass.disabled = false;
		//document.form.modem_ttlsid.disabled = true;
		//$("hsdpa_hint").style.display = "";
	}
	else if(mode == "3"){ // TD-SCDMA
		document.form.Dev3G.disabled = false;
		document.form.hsdpa_country.disabled = false;
		document.form.hsdpa_isp.disabled = false;
		document.form.hsdpa_apn.disabled = false;
		document.form.wan_3g_pin.disabled = false;
		document.form.hsdpa_dialnum.disabled = false;
		document.form.hsdpa_user.disabled = false;
		document.form.hsdpa_pass.disabled = false;
		//document.form.modem_ttlsid.disabled = true;
		//$("hsdpa_hint").style.display = "";
	}
	else if(mode == "4"){
		document.form.Dev3G.disabled = false;
		document.form.hsdpa_country.disabled = false;
		document.form.hsdpa_isp.disabled = false;
		document.form.hsdpa_apn.disabled = true;
		document.form.wan_3g_pin.disabled = true;
		document.form.hsdpa_dialnum.disabled = true;
		document.form.hsdpa_user.disabled = false;
		document.form.hsdpa_pass.disabled = false;
		//document.form.modem_ttlsid.disabled = false;
		//$("hsdpa_hint").style.display = "";
	}
	else{
		document.form.Dev3G.disabled = true;
		document.form.hsdpa_country.disabled = true;
		document.form.hsdpa_isp.disabled = true;
		document.form.hsdpa_apn.disabled = true;
		document.form.wan_3g_pin.disabled = true;
		document.form.hsdpa_dialnum.disabled = true;
		document.form.hsdpa_user.disabled = true;
		document.form.hsdpa_pass.disabled = true;
		//document.form.modem_ttlsid.disabled = true;
		//$("hsdpa_hint").style.display = "none";
	}

	if(chk3g == "2") {
		document.form.wan_3g_puk.disabled = false;
		inputHideCtrl(document.form.wan_3g_puk, 1);
	} else {
		document.form.wan_3g_puk.disabled = true;
		inputHideCtrl(document.form.wan_3g_puk, 0);
	}

	if(mode == 0){
		inputHideCtrl(document.form.Dev3G, 0);
		inputHideCtrl(document.form.hsdpa_country, 0);
		inputHideCtrl(document.form.hsdpa_isp, 0);
		inputHideCtrl(document.form.hsdpa_apn, 0);
		inputHideCtrl(document.form.wan_3g_pin, 0);
		inputHideCtrl(document.form.hsdpa_dialnum, 0);
		inputHideCtrl(document.form.hsdpa_user, 0);
		inputHideCtrl(document.form.hsdpa_pass, 0);
		//inputHideCtrl(document.form.modem_ttlsid, 0);
	}
	else{
		inputHideCtrl(document.form.Dev3G, 1);
		inputHideCtrl(document.form.hsdpa_country, 1);
		inputHideCtrl(document.form.hsdpa_isp, 1);
		inputHideCtrl(document.form.hsdpa_apn, 1);
		inputHideCtrl(document.form.wan_3g_pin, 1);
		inputHideCtrl(document.form.hsdpa_dialnum, 1);
		inputHideCtrl(document.form.hsdpa_user, 1);
		inputHideCtrl(document.form.hsdpa_pass, 1);
		//inputHideCtrl(document.form.modem_ttlsid, 1);
	}

	gen_country_list(mode);


}

function inputHideCtrl(obj, flag){
	if(obj.type == "checkbox")
		return true;
	if(flag == 0)
		obj.parentNode.parentNode.style.display = "none";
	else
		obj.parentNode.parentNode.style.display = "";
	return true;
}

function gen_country_list(mode){
	if(mode == "1"){
		show_wcdma_country_list();
	}
	else if(mode == "2"){
		show_cdma2000_country_list();
	}
	else if(mode == "3"){
		show_tdscdma_country_list();
	}
	else if(mode == "4"){
		show_4G_country_list();
	}
}

function gen_list(mode){
	if(mode == "1"){
		gen_wcdma_list();
	}
	else if(mode == "2"){
		gen_cdma2000_list();
	}
	else if(mode == "3"){
		gen_tdscdma_list();
	}
	else if(mode == "4"){
		gen_4G_list();
	}
}

function show_ISP_list(){
	free_options($("hsdpa_isp"));
	$("hsdpa_isp").options.length = isplist.length;

	for(var i = 0; i < isplist.length; i++){
		$("hsdpa_isp").options[i] = new Option(isplist[i], isplist[i]);
		if(isplist[i] == isp)
			$("hsdpa_isp").options[i].selected = "1";
	}
}

function show_APN_list(){
	var ISPlist = $("hsdpa_isp").value;


	if(document.form.hsdpa_enable.value == "1"
			|| document.form.hsdpa_enable.value == "2"
			|| document.form.hsdpa_enable.value == "3"
			){
		if(ISPlist == isp
				&& (apn != "" || dialnum != "" || user != "" || pass != "")){
			$("hsdpa_apn").value = apn;
			$("hsdpa_dialnum").value = dialnum;
			$("hsdpa_user").value = user;
			$("hsdpa_pass").value = pass;
		}
		else{
			for(var i = 0; i < isplist.length; i++){
				if(isplist[i] == ISPlist){
					$("hsdpa_apn").value = apnlist[i];
					$("hsdpa_dialnum").value = daillist[i];
					$("hsdpa_user").value = userlist[i];
					$("hsdpa_pass").value = passlist[i];
				}
			}
		}
	}
	else if(document.form.hsdpa_enable.value == "4"){
		$("hsdpa_apn").value = "";
		$("hsdpa_dialnum").value = "";

		if(ISPlist == isp
				&& (user != "" || pass != "")){
			$("hsdpa_user").value = user;
			$("hsdpa_pass").value = pass;
		}
		else{
			for(var i = 0; i < isplist.length; i++){
				if(isplist[i] == ISPlist){
					$("hsdpa_user").value = userlist[i];
					$("hsdpa_pass").value = passlist[i];
				}
			}
		}
	}
}

function applyRule(){
	var mode = document.form.hsdpa_enable.value;

	if(document.form.failover_3g_enable[0].checked) {
		if (dsl_config == '0') {
			alert("<#no_PVC_warning_msg#>");
			location.href = "/Advanced_WAN_Content.asp";
			return;
		}
	}
	
	if(document.form.failover_3g_enable[0].checked) {
		if (all_bridge == 1) {
			alert("<#failover_no_supp_bridge#>");
			return;
		}
	}	
	
	if(document.form.wan_3g_pin.value != "") {
		if(document.form.wan_3g_pin.value.search(/^\d{4,8}$/)==-1) {
			alert("<#JS_InvalidPIN#>");
			return;
		}
	}

	if(document.form.hsdpa_enable.value != "0"){
		//if(confirm("<#HSDPA_enable_confirm#>")){
			if(document.form.hsdpa_enable.value == "1"){	// WCDMA
				document.form.EVDO_on.value = "0";
				document.form.wan_proto.value = "3g";
			}
			else if(document.form.hsdpa_enable.value == "2"){	// CDMA2000
				document.form.EVDO_on.value = "1";
				document.form.wan_proto.value = "3g";
			}
			else if(document.form.hsdpa_enable.value == "3"){	// TD-SCDMA
				document.form.EVDO_on.value = "2";
				document.form.wan_proto.value = "3g";
			}
			showLoading();
			document.form.action_mode.value = " Apply ";
			document.form.current_page.value = "/index.asp";
			document.form.next_page.value = "";

			document.form.use_eth_wan.value = "0";	// close ethernet wan
			document.form.use_failover_eth_wan.value = "0";	// close ethernet failover

			document.form.submit();
		//}
	}
	else{
		showLoading();
		document.form.wan_proto.value = "dhcp";
		document.form.action_mode.value = " Apply ";
		document.form.current_page.value = "/as.asp";
		document.form.next_page.value = "";
		document.form.submit();
	}
}

function done_validating(action){
	refreshpage();
}


function show_hide_failover(){
	if(document.form.failover_3g_enable[1].checked) //Off
		primary_wan_ctrl_3g('0');
	else
		primary_wan_ctrl_3g('1');

    if(document.form.hsdpa_enable.value == "0") {
        document.getElementById("failover_3g").style.display = "none";
    }
    else {
        document.getElementById("failover_3g").style.display = "";
    }
}

function primary_wan_ctrl_3g(flag){
	if(flag == "1") {
		document.getElementById("primary_wan_conn").innerHTML = "<font color='black'><b><#menu5_dsl_wan#></b></font>";
		document.getElementById("backup_wan_conn").innerHTML = "<font color='black'><b><#menu5_3_3gwan#></b></font>";
	}
	else {
		document.getElementById("primary_wan_conn").innerHTML = "<font color='black'><#wl_securitylevel_0#></font>";
		document.getElementById("backup_wan_conn").innerHTML = "<font color='black'><#wl_securitylevel_0#></font>";
	}
}
</script>
</head>

<body onload="initial();" onunLoad="disable_auto_hint(21, 7);return unload_body();">
<div id="TopBanner"></div>

<div id="Loading" class="popup_bg"></div>

<iframe name="hidden_frame" id="hidden_frame" src="" width="0" height="0" frameborder="0"></iframe>

<form method="post" name="form" id="ruleForm" action="/start_apply.htm" target="hidden_frame">
<input type="hidden" name="productid" value="<% nvram_get_f("general.log", "productid"); %>">
<input type="hidden" name="current_page" value="Advanced_modem_3g_Content.asp">
<input type="hidden" name="next_page" value="">
<input type="hidden" name="next_host" value="">
<input type="hidden" name="sid_list" value="General;Layer3Forwarding;">
<input type="hidden" name="group_id" value="">
<input type="hidden" name="modified" value="0">
<input type="hidden" name="action_mode" value="">
<input type="hidden" name="action_script" value="">
<input type="hidden" name="preferred_lang" id="preferred_lang" value="<% nvram_get_x("LANGUAGE", "preferred_lang"); %>">
<input type="hidden" name="wl_ssid2" value="<% nvram_char_to_ascii("WLANConfig11b", "wl_ssid"); %>">
<input type="hidden" name="firmver" value="<% nvram_get_x("", "firmver"); %>">
<input type="hidden" name="wan_proto" value="<% nvram_get_x("", "wan_proto"); %>">
<input type="hidden" name="EVDO_on" value="<% nvram_get_x("", "EVDO_on"); %>">
<input type="hidden" name="use_eth_wan" value="<% nvram_get_x("", "use_eth_wan"); %>">
<input type="hidden" name="use_failover_eth_wan" value="<% nvram_get_x("", "use_failover_eth_wan"); %>">
<table class="content" align="center" cellpadding="0" cellspacing="0">
<tr>
	<td width="23">&nbsp;</td>

	<!--=====Beginning of Main Menu=====-->
	<td valign="top" width="202">
		<div id="mainMenu"></div>
		<div id="subMenu"></div>
	</td>

	<td valign="top">
		<div id="tabMenu" class="submenuBlock"></div>
		<br />
		<!--===================================Beginning of Main Content===========================================-->
		<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
		<tr>
			<td align="left" valign="top" >
			<table width="90%" border="0" align="center" cellpadding="5" cellspacing="0" class="FormTitle">
				<thead>
					<tr>
						<td><#menu5_3_1#> - <#menu5_3_3gwan#></td>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td bgcolor="#FFFFFF"><#HSDPAConfig_hsdpa_mode_itemdesc#> <#HSDPAConfig_hsdpa_mode_itemdesc3#> <#HSDPAConfig_hsdpa_mode_itemdesc4#></td>
					</tr>
					<tr>
						<td bgcolor="#FFFFFF">
						<table width="100%" border="1" align="center" cellpadding="4" cellspacing="0" bordercolor="#6b8fa3"  class="FormTable">
							<tr>
								<th><a class="hintstyle" href="javascript:void(0)" onClick="openHint(21,1);"><#HSDPAConfig_hsdpa_enable_itemname#></a></th>
								<td>
									<select name="hsdpa_enable" class="input" onClick="openHint(21,1);" onchange="switch_modem_mode(this.value);gen_list(this.value);show_ISP_list();show_APN_list();show_hide_failover();">
										<!--<option value="0" <% nvram_match_x("General", "hsdpa_enable", "0", "selected"); %>><#WLANConfig11b_WirelessCtrl_buttonname#></option>
										<option value="1" <% nvram_match_x("General", "hsdpa_enable", "1", "selected"); %>>WCDMA (UMTS)</option>
										<option value="2" <% nvram_match_x("General", "hsdpa_enable", "2", "selected"); %>>CDMA2000 (EVDO)</option>
										<option value="3" <% nvram_match_x("General", "hsdpa_enable", "3", "selected"); %>>TD-SCDMA</option>
										<option value="4" <% nvram_match_x("General", "hsdpa_enable", "4", "selected"); %>>WiMAX</option> -->
										<option value="0"><#WLANConfig11b_WirelessCtrl_buttonname#></option>
										<option value="1">WCDMA (UMTS)</option>
										<option value="2">CDMA2000 (EVDO)</option>
										<option value="3">TD-SCDMA</option>
									</select>
									<br/><span id="hsdpa_hint" style="display:none;"><#HSDPAConfig_hsdpa_enable_hint1#></span>
								</td>
							</tr>
							<tr>
								<th><a class="hintstyle"  href="javascript:void(0);" onClick="openHint(21,9);"><#HSDPAConfig_Country_itemname#></a></th>
								<td>
									<select name="hsdpa_country" id="isp_countrys" class="input" onClick="openHint(21,9);" onchange="gen_list(document.form.hsdpa_enable.value);show_ISP_list();show_APN_list();"></select>
								</td>
							</tr>
							<tr>
								<th><a class="hintstyle"  href="javascript:void(0);" onClick="openHint(21,8);"><#HSDPAConfig_ISP_itemname#></a></th>
								<td>
									<select name="hsdpa_isp" id="hsdpa_isp" class="input" onClick="openHint(21,8);" onchange="show_APN_list()"></select>
								</td>
							</tr>
							<tr>
								<th><a class="hintstyle"  href="javascript:void(0);" onClick="openHint(21,13);"><#HSDPAConfig_USBAdapter_itemname#></a></th>
								<td>
									<select name="Dev3G" id="shown_modems" class="input" onClick="openHint(21,13);" disabled="disabled"></select>
								</td>
							</tr>
							<tr>
								<th><a class="hintstyle"  href="javascript:void(0);" onClick="openHint(21,3);"><#HSDPAConfig_private_apn_itemname#></a></th>
								<td>
									<input id="hsdpa_apn" name="hsdpa_apn" class="input" onClick="openHint(21,3);" type="text" value=""/>
								</td>
							</tr>
							<tr>
								<th><a class="hintstyle"  href="javascript:void(0);" onClick="openHint(21,2);"><#HSDPAConfig_PIN_itemname#></a></th>
								<td>
									<input id="wan_3g_pin" name="wan_3g_pin" class="input" onClick="openHint(21,2);" type="password" maxLength="8" value="<% nvram_get_x("", "wan_3g_pin"); %>"/>
								</td>
							</tr>
							<tr>
								<th><a class="hintstyle"  href="javascript:void(0);" onClick="openHint(21,14);"><#HSDPAConfig_PUK_itemname#></a></th>
								<td>
									<input id="wan_3g_puk" name="wan_3g_puk" class="input" onClick="openHint(21,14);" type="password" maxLength="8" value="<% nvram_get_x("", "wan_3g_puk"); %>"/>
								</td>
							</tr>
							<tr>
								<th><a class="hintstyle"  href="javascript:void(0);" onClick="openHint(21,10);"><#HSDPAConfig_DialNum_itemname#></a></th>
								<td>
									<input id="hsdpa_dialnum" name="hsdpa_dialnum" class="input" onClick="openHint(21,10);" type="text" value=""/>
								</td>
							</tr>
							<tr>
								<th><a class="hintstyle"  href="javascript:void(0);" onClick="openHint(21,11);"><#HSDPAConfig_Username_itemname#></a></th>
								<td>
									<input id="hsdpa_user" name="hsdpa_user" class="input" onClick="openHint(21,11);" type="text" value="<% nvram_get_x("", "hsdpa_user"); %>"/>
								</td>
							</tr>
							<tr>
								<th><a class="hintstyle"  href="javascript:void(0);" onClick="openHint(21,12);"><#AiDisk_Password#></a></th>
								<td>
									<input id="hsdpa_pass" name="hsdpa_pass" class="input" onClick="openHint(21,12);" type="password" value="<% nvram_get_x("", "hsdpa_pass"); %>"/>
								</td>
							</tr>
							<!--tr>
								<th>E-mail</th>
								<td>
									<input id="modem_ttlsid" name="modem_ttlsid" class="input" value="<% nvram_get_x("", "modem_ttlsid"); %>"/>
								</td>
							</tr-->
						</table>
                        <p>
                        <div id="failover_3g">
    						<table width="100%" border="1" align="center" cellpadding="4" cellspacing="0" bordercolor="#6b8fa3" class="FormTable">
            					<tr>
									<th width="40%"><a class="hintstyle"  href="javascript:void(0);" onClick="openHint(7,30);"><#enable_fail_over#></a></th>
            						<td align="left">
            							<input type="radio" name="failover_3g_enable" class="input" value="1" onclick="primary_wan_ctrl_3g('1');" <% nvram_match_x("Layer3Forwarding","failover_3g_enable", "1", "checked"); %>><#checkbox_Yes#>
            							<input type="radio" name="failover_3g_enable" class="input" value="0" onclick="primary_wan_ctrl_3g('0');" <% nvram_match_x("Layer3Forwarding","failover_3g_enable", "0", "checked"); %>><#checkbox_No#>
            						</td>
            					</tr>
            					<tr>
            						<th width="40%" align="left"><#primary_wan#></th>
            						<td style="font-weight: normal;" align="left"><span id="primary_wan_conn"></span></td>
            					</tr>
            					<tr>
            						<th width="40%" align="left"><#backup_wan#></th>
            						<td style="font-weight: normal;" align="left"><span id="backup_wan_conn"></span></td>
            					</tr>
    						</table>
                            <p>
                        </div>
						<table width="100%" border="1" align="center" cellpadding="4" cellspacing="0" bordercolor="#6b8fa3"  class="FormTable">
							<tr align="right">
								<td colspan="2">
									<input type="button" class="button" value="<#CTL_apply#>" onclick="applyRule();">
								</td>
							</tr>
						</table>
						</td>
					</tr>
				</tbody>
			</table>
			</td>
</form>
			<!--==============Beginning of hint content=============-->
			<td id="help_td" style="width:15px;" valign="top">
				<form name="hint_form"></form>
				<div id="helpicon" onClick="openHint(0,0);" title="<#Help_button_default_hint#>"><img src="images/help.gif" /></div>
				<div id="hintofPM" style="display:none;">
					<table width="100%" cellpadding="0" cellspacing="1" class="Help" bgcolor="#999999">
						<thead>
							<tr>
								<td>
									<div id="helpname" class="AiHintTitle"></div>
									<a href="javascript:closeHint();">
										<img src="images/button-close.gif" class="closebutton">
									</a>
								</td>
							</tr>
						</thead>

							<tr>
								<td valign="top">
									<div class="hint_body2" id="hint_body"></div>
									<iframe id="statusframe" name="statusframe" class="statusframe" src="" frameborder="0"></iframe>
								</td>
							</tr>

					</table>
				</div>
			</td>
			<!--==============Ending of hint content=============-->

		</tr>
		</table>
	</td>
	<td width="10" align="center" valign="top">&nbsp;</td>
</tr>
</table>

<div id="footer"></div>
</body>
</html>
