﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<html xmlns:v>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7"/>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta HTTP-EQUIV="Pragma" CONTENT="no-cache">
<meta HTTP-EQUIV="Expires" CONTENT="-1">
<title>ASUS <#Web_Title#> Wireless-N ADSL2/2+ Modem Router - <#menu5_3_3#></title>
<link rel="stylesheet" type="text/css" href="./index_style.css">
<link rel="stylesheet" type="text/css" href="./form_style.css">
<script language="JavaScript" type="text/javascript" src="./state.js"></script>
<script language="JavaScript" type="text/javascript" src="./general.js"></script>
<script language="JavaScript" type="text/javascript" src="./popup.js"></script>
<script type="text/javascript" language="JavaScript" src="./help.js"></script>
<script type="text/javascript" language="JavaScript" src="./detect.js"></script>
<script>
wan_route_x = '<% nvram_get_x("IPConnection", "wan_route_x"); %>';
wan_nat_x = '<% nvram_get_x("IPConnection", "wan_nat_x"); %>';
wan_proto = '<% nvram_get_x("Layer3Forwarding",  "wan_proto"); %>';

var wan_mode = '<% nvram_get_x("Layer3Forwarding",  "use_eth_wan"); %>';
var eth_failover = '<% nvram_get_x("Layer3Forwarding",  "use_failover_eth_wan"); %>';
	
function initial()
{
	show_banner(1);
	show_menu(5,3,4);
	show_footer();

	modify_iptv_list_setting(wan_mode);

	//enable_auto_hint(6, 6);
	//enable_auto_hint(7, 26);
}

function applyRule()
{
	if(wan_mode != '0' && eth_failover == '1') {
		if (document.form.wan_stb_x.value != "0") {
			alert("<#eth_failover_IPTV_enable_block_msg#>");
			return;
		}
	}

	if (!valid_udpxy())
		return;
		
	showLoading();

	document.form.action_mode.value = " Apply ";
	document.form.current_page.value = "/Advanced_Port_Mapping.asp";
	document.form.next_page.value = "";
	document.form.submit();
}

function done_validating(action)
{
	refreshpage();
}

function modify_iptv_list_setting(type)
{
	// dynamic generate iptv list
	var stb_port = '<% nvram_get_x("Layer3Forwarding", "wan_stb_x"); %>';

	free_options(document.form.wan_stb_x);
	add_option(document.form.wan_stb_x, "<#btn_Disabled#>", "0", (stb_port == "0" ? 1 : 0));
	if (type != "1")
		add_option(document.form.wan_stb_x, "LAN1", "1", (stb_port == "1" ? 1 : 0));
	if (type != "2")
		add_option(document.form.wan_stb_x, "LAN2", "2", (stb_port == "2" ? 1 : 0));
	if (type != "3")
		add_option(document.form.wan_stb_x, "LAN3", "3", (stb_port == "3" ? 1 : 0));
	if (type != "4")
		add_option(document.form.wan_stb_x, "LAN4", "4", (stb_port == "4" ? 1 : 0));
	if (type != "3" && type != "4")
		add_option(document.form.wan_stb_x, "LAN3 & LAN4", "5", (stb_port == "5" ? 1 : 0));
}

function valid_udpxy()
{
	return (document.form.udpxy_enable_x.value == 0 ||
		validate_range(document.form.udpxy_enable_x, 1024, 65535));
}

</script>
</head>
<body onload="initial();" onunLoad="return unload_body();">

<div id="TopBanner"></div>

<div id="Loading" class="popup_bg"></div>

<iframe name="hidden_frame" id="hidden_frame" src="" width="0" height="0" frameborder="0"></iframe>
<form method="post" name="form" id="ruleForm" action="/start_apply.htm" target="hidden_frame">
<input type="hidden" name="productid" value="<#Web_Title#>">

<input type="hidden" name="current_page" value="Advanced_Port_Mapping.asp">
<input type="hidden" name="next_page" value="">
<input type="hidden" name="next_host" value="">
<input type="hidden" name="sid_list" value="Layer3Forwarding;LANHostConfig;IPConnection;WLANConfig11b;RouterConfig;">
<input type="hidden" name="group_id" value="">
<input type="hidden" name="modified" value="0">
<input type="hidden" name="action_mode" value="">
<input type="hidden" name="first_time" value="">
<input type="hidden" name="action_script" value="">
<input type="hidden" name="preferred_lang" id="preferred_lang" value="<% nvram_get_x("LANGUAGE", "preferred_lang"); %>">
<input type="hidden" name="wl_ssid2" value="<% nvram_char_to_ascii("WLANConfig11b", "wl_ssid"); %>">
<input type="hidden" name="firmver" value="<% nvram_get_x("",  "firmver"); %>">

<table class="content" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td width="23">&nbsp;</td>
    <td valign="top" width="202">
      <div  id="mainMenu"></div>
      <div  id="subMenu"></div>
    </td>
    <td valign="top">
      <div id="tabMenu" class="submenuBlock"></div><br />
      <!--===================================Beginning of Main Content===========================================-->
      <table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
	<td valign="top" align="left">
	<table width="92%" border="0" align="center" cellpadding="5" cellspacing="0" class="FormTitle">
	<thead>
	<tr>
	  <td><#menu5_3_3#></td>
	</tr>
	</thead>
	<tr>
	  <td bgcolor="#FFFFFF"><#Port_Mapping_desc2#></td>
	</tr>
	<tr>
	  <td bgcolor="#FFFFFF"><table width="100%" border="1" align="center" cellpadding="4" cellspacing="0" bordercolor="#6b8fa3"  class="FormTable">
          <tr>
            <th width="50%"><a class="hintstyle" href="javascript:void(0);" onClick="openHint(7,21);"><#Layer3Forwarding_x_STB_itemname#></a></th>
	    <td align="left">
		<select name='wan_stb_x' class='input'>
                <option value="0"><#btn_Disabled#></option>
		</select>
	    </td>
          </tr>
          </table></td>
	</tr>



	<tr>


<td bgcolor="#FFFFFF">
<table width="100%" border="1" align="center" cellpadding="4" cellspacing="0" bordercolor="#6b8fa3"  class="FormTable">
<thead>
<tr>
<td colspan="2"><#IPTV_Special_desc1#></td>
</tr>
</thead>
<tr>
<td colspan="2" bgcolor="#FFFFFF"><#IPTV_Special_desc2#></td>
</tr>
	  <td bgcolor="#FFFFFF">
        <table width="100%" border="1" align="center" cellpadding="4" cellspacing="0" bordercolor="#6b8fa3"  class="FormTable">
          <tr>
            <th width="50%"><#RouterConfig_GWMulticastEnable_itemname#></th>
            <td>
          	<input type="radio" value="1" name="mr_enable_x" class="input" <% nvram_match_x("RouterConfig", "mr_enable_x", "1", "checked"); %>><#checkbox_Yes#>
          	<input type="radio" value="0" name="mr_enable_x" class="input" <% nvram_match_x("RouterConfig", "mr_enable_x", "0", "checked"); %>><#checkbox_No#>
            </td>
          </tr>
          <tr>
            <th width="50%"><a class="hintstyle" href="javascript:void(0);" onClick="openHint(7,27);"><#RouterConfig_IPTV_itemname#></a></th>
	    <td align="left">
          	<input id="udpxy_enable_x" type="text" maxlength="5" class="input" size="15" name="udpxy_enable_x" value="<% nvram_get_x("LANHostConfig", "udpxy_enable_x"); %>" onkeypress="return is_number(this);" onblur="valid_udpxy();"/>
	    </td>
          </tr>
          <tr>
	    <th width="50%"><a class="hintstyle" href="javascript:void(0);" onClick="openHint(7,26);"><#IPTV_RTSP_PassThrough_itemname#></a></th>
	    <td align="left">
          	<input type="radio" value="1" name="fw_pt_rtsp" class="content_input_fd" onClick="return change_common_radio(this, 'IPConnection', 'fw_pt_rtsp', '1')" <% nvram_match_x("IPConnection","fw_pt_rtsp", "1", "checked"); %>><#checkbox_Yes#>
          	<input type="radio" value="0" name="fw_pt_rtsp" class="content_input_fd" onClick="return change_common_radio(this, 'IPConnection', 'fw_pt_rtsp', '0')" <% nvram_match_x("IPConnection","fw_pt_rtsp", "0", "checked"); %>><#checkbox_No#>
	    </td>
          </tr>
          </table></td>
</table>		</td>
	</tr>
	<tr>
<td bgcolor="#FFFFFF">
<table width="100%" border="1" align="center" cellpadding="4" cellspacing="0" bordercolor="#6b8fa3"  class="FormTable">
<thead>
<tr>
<td colspan="2"><#IGMP_Proxy_itemname#></td>
</tr>
</thead>
<tr>
<td colspan="2" bgcolor="#FFFFFF"><#IGMP_Proxy_desc#></td>
</tr>
	  <td bgcolor="#FFFFFF">
        <table width="100%" border="1" align="center" cellpadding="4" cellspacing="0" bordercolor="#6b8fa3"  class="FormTable">
          <tr>
	    <th width="50%"><a class="hintstyle" href="javascript:void(0);" onClick="openHint(7,28);"><#IGMP_Proxy_itemname#></a></th>
	    <td align="left">
          	<input type="radio" value="1" name="IgmpSnEnable" class="content_input_fd" onClick="return change_common_radio(this, 'WLANConfig11b', 'IgmpSnEnable', '1')" <% nvram_match_x("WLANConfig11b","IgmpSnEnable", "1", "checked"); %>><#checkbox_Yes#>
          	<input type="radio" value="0" name="IgmpSnEnable" class="content_input_fd" onClick="return change_common_radio(this, 'WLANConfig11b', 'IgmpSnEnable', '0')" <% nvram_match_x("WLANConfig11b","IgmpSnEnable", "0", "checked"); %>><#checkbox_No#>
	    </td>
          </tr>
          </table></td>
</table>		</td>
	</tr>	
	<tr>
	  <td bgcolor="#FFFFFF">
	  <table width="100%" border="1" align="center" cellpadding="4" cellspacing="0" bordercolor="#6b8fa3"  class="FormTable">
            <tr>
              <td colspan="2"  align="right"><input name="button" type="button" class="button" onclick="applyRule()" value="<#CTL_apply#>"/></td>
            </tr>
		</table>
 	  </td>
	</tr>
</table>
</td>

</form>

	<td id="help_td" style="width:15px;" valign="top">

	  <div id="helpicon" onClick="openHint(0,0);" title="Click to open Help."><img src="images/help.gif" /></div>
	  <div id="hintofPM" style="display:none;">
<form name="hint_form"></form>
	    <table width="100%" cellpadding="0" cellspacing="1" class="Help" bgcolor="#999999">
		  <thead>
		  <tr>
			<td>
			  <div id="helpname" class="AiHintTitle"></div>
			  <a href="javascript:;" onclick="closeHint()" ><img src="images/button-close.gif" class="closebutton" /></a>
			</td>
		  </tr>
		  </thead>

		  <tr>
			<td valign="top" >
			  <div class="hint_body2" id="hint_body"></div>
			  <iframe id="statusframe" name="statusframe" class="statusframe" src="" frameborder="0"></iframe>
			</td>
		  </tr>
		</table>
	  </div><!--End of hintofPM-->
		  </td>
        </tr>
      </table>
		<!--===================================Ending of Main Content===========================================-->
	</td>

    <td width="10" align="center" valign="top">&nbsp;</td>
	</tr>
</table>

<div id="footer"></div>
</body>
</html>
