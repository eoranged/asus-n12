﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta HTTP-EQUIV="Pragma" CONTENT="no-cache">
<meta HTTP-EQUIV="Expires" CONTENT="-1">
<title>ASUS <#Web_Title#> Wireless-N ADSL2/2+ Modem Router - <#menu5_3_1#> - <#menu5_dsl_wan#></title>
<link rel="stylesheet" type="text/css" href="../form_style.css">
<link rel="stylesheet" type="text/css" href="../content.css">
<script type="text/javascript" src="/state.js"></script>
<script type="text/javascript" src="/general.js"></script>
<script type="text/javascript" src="/popup.js"></script>
<script type="text/javascript" src="/help.js"></script>
<script>
wan_route_x = '<% nvram_get_x("IPConnection", "wan_route_x"); %>';
wan_nat_x = '<% nvram_get_x("IPConnection", "wan_nat_x"); %>';
wan_proto = '<% nvram_get_x("Layer3Forwarding",  "wan_proto"); %>';
use_failover_eth = "<% nvram_get_x("Layer3Forwarding",  "use_failover_eth_wan"); %>";
use_failover_3g = "<% nvram_get_x("Layer3Forwarding",  "failover_3g_enable"); %>";

// get WAN setting list from hook.
// Index, VPI, VCI, Protocol
var DSLWANList = [<% get_DSL_WAN_list("DSLWANConfig", "DSLWANList"); %>];

var ticked = 0;
var internetPvc = 0;

for(var i = 0; i < DSLWANList.length; i++){
	switch(DSLWANList[i][2])
	{
		case "pppoe":
		case "pppoa":
		case "mer":
		case "ipoa":
			internetPvc = 1;
			break;
		default:
			internetPvc = 0;
			break;
	}
	if(internetPvc == 1)
		break;
}

var all_bridge = 1;
for(var i = 0; i < DSLWANList.length; i++){
	switch(DSLWANList[i][2])
	{
		case "pppoe":
		case "pppoa":
		case "mer":
		case "ipoa":
			all_bridge = 0;
			break;
	}
	if(all_bridge == 0)
		break;
}



function initial(){
	parent.hideLoading();
	showDSLWANList();
	parent.window.scrollTo(0,0);
}

function rebootClick() {
	if(use_failover_eth == '1') {
		// failover on
		if(DSLWANList.length == 0){
			alert("<#failover_disable_chg_to_eth_wan#>");
		}
		else if(all_bridge == 1) {
			alert("<#bridge_fo_disable_chg_to_eth_wan#>");
		}
	}

	if(use_failover_3g == '1') {
		// failover on
		if(DSLWANList.length == 0){
			alert("<#failover_disable_chg_to_3g_wan#>");
		}
		else if (all_bridge == 1) {
			alert("<#bridge_fo_disable_chg_to_3g_wan#>");
		}
	}

	parent.showLoading();

	document.form.action_mode.value = " Restart ";
	document.form.current_page.value = "DSL_WAN_Content.asp";
	document.form.next_page.value = "/";

	document.form.submit();
}

function addClick(){
	if (DSLWANList.length == 8)
	{
		alert("<#pvccfg_max#>");
		return;
	}
	parent.document.getElementById("ruleForm").redir.value = "INIT";
	location.href = "Advanced_DSL_WAN_Config.asp";
}

function removeClick() {
	document.form.group_id.value = "DSLWANList";
	document.form.action_mode.value = " Del ";
	return true;
}

function showDSLWANList(){
	var addRow;
	var cell = new Array(10);
	if(DSLWANList.length == 0){
		addRow = document.getElementById('DSL_WAN_table').insertRow(2);
		cell[0] = addRow.insertCell(0);
		cell[0].colSpan = "10";
		cell[0].innerHTML = "<#Nodata#>";
		cell[0].style.color = "#C00";
	}
	else{
		for(var i = 0; i < DSLWANList.length; i++){
			addRow = document.getElementById('DSL_WAN_table').insertRow(i+2);

			switch(DSLWANList[i][2])
			{
				case "pppoe":
				case "pppoa":
				case "mer":
				case "ipoa":
					cell[0] = addRow.insertCell(0);
					cell[0].innerHTML = "&nbsp;";
					break;
				default:
					if(internetPvc == 0 && i == 0)
					{
	          cell[0] = addRow.insertCell(0);
	          cell[0].innerHTML = "&nbsp;";
					}else {
            cell[0] = addRow.insertCell(0);
            cell[0].innerHTML = "<center><img src=images/checked.gif border=0></center>";
					}
					break;
			}

			switch(DSLWANList[i][2])
			{
				case "pppoe":
				case "pppoa":
				case "mer":
				case "ipoa":
					cell[0] = addRow.insertCell(0);
					cell[0].innerHTML = "<center><img src=images/checked.gif border=0></center>";
					break;
				default:
					if(internetPvc == 0 && i == 0)
					{
						cell[0] = addRow.insertCell(0);
						cell[0].innerHTML = "<center><img src=images/checked.gif border=0></center>";
					}else {
						cell[0] = addRow.insertCell(0);
						cell[0].innerHTML = "&nbsp;";
					}
					break;
			}

			cell[1] = addRow.insertCell(0);

			switch(DSLWANList[i][2])
			{
				case "pppoe":
				case "pppoa":
				case "mer":
				case "ipoa":
					cell[1].innerHTML = "<center><a href=Edit_Advanced_DSL_WAN_Config.asp?pvcindex="+ i +"&internetpvc=1><img src=images/edit.png border=0></a></center>";
					break;
				default:
					cell[1].innerHTML = "<center><a href=Edit_Advanced_DSL_WAN_Config.asp?pvcindex="+ i +"&internetpvc=0><img src=images/edit.png border=0></a></center>";
					break;
			}

			cell[2] = addRow.insertCell(0);
			cell[2].innerHTML = i+1;


			for(var j=1; j<=(DSLWANList[i].length); j++){
				cell[j] = addRow.insertCell(j);
				cell[j].innerHTML = DSLWANList[i][j-1];
			}
			cell[j] = addRow.insertCell(j);
			cell[j].innerHTML = "<center><input type='checkbox' name='DSLWANList_s' value=\'"+ i +"\'></center>";
		}
	}
}

function done_validating(action){
	refreshpage();
}
</script>
</head>
<form method="post" name="form" action="/start_apply.htm" target="hidden_frame" >
<input type="hidden" name="flag" value="">
<input type="hidden" name="current_page" value="">
<input type="hidden" name="next_page" value="">
<input type="hidden" name="sid_list" value="DSLWANConfig;">
<input type="hidden" name="group_id" value="DSLWANList">
<input type="hidden" name="action_mode" value="">
<body onload="initial();" BGCOLOR="#A7D2E2"><p id="m">
<table width="98%" border="0" align="center" cellpadding="4" cellspacing="0" class="FormTitle">
	<thead>
		<tr>
			<td><#menu5_3_1#> - <#menu5_dsl_wan#></td>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td bgcolor="#FFFFFF"><#WANCFG_annote1#><br/>
			<font color="#FF0000">
			<#WANCFG_annote2#>
			</font>
<script>
	var ethwan = "<% nvram_get_x("Layer3Forwarding", "use_eth_wan"); %>";
	var hsdpa = "<% nvram_get_x("General", "hsdpa_enable"); %>";
	var failover_eth = "<% nvram_get_x("Layer3Forwarding", "use_failover_eth_wan"); %>";
	var failover_3g = "<% nvram_get_x("Layer3Forwarding", "failover_3g_enable"); %>";
	if(ethwan != "0")
	{
		if (failover_eth == "0")
		{
			document.writeln("<br><br><b><font color='#FF0000'>Note: </font></b><#note_eth_wan#>");
		}
	}
	else if(hsdpa != "0")
	{
		if (failover_3g == "0")
		{
			document.writeln("<br><br><b><font color='#FF0000'>Note: </font></b><#note_3g_wan#>");
		}
	}
	else
	{
		if(DSLWANList.length >1)
		{
			var port0 = "<% nvram_match_x("Layer3Forwarding", "wan_stb_x", "0", "selected"); %>";
			var port1 = "<% nvram_match_x("Layer3Forwarding", "wan_stb_x", "1", "selected"); %>";
			var port2 = "<% nvram_match_x("Layer3Forwarding", "wan_stb_x", "2", "selected"); %>";
			var port3 = "<% nvram_match_x("Layer3Forwarding", "wan_stb_x", "3", "selected"); %>";
			var port4 = "<% nvram_match_x("Layer3Forwarding", "wan_stb_x", "4", "selected"); %>";
			var port34= "<% nvram_match_x("Layer3Forwarding", "wan_stb_x", "5", "selected"); %>";
			var IPTVport = "";

			if(port0=="selected")
					IPTVport = "None";
			else if(port1=="selected")
					IPTVport = "1";
			else if(port2=="selected")
					IPTVport = "2";
			else if(port3=="selected")
					IPTVport = "3";
			else if(port4=="selected")
					IPTVport = "4";
			else if(port34=="selected")
					IPTVport = "3 & 4";

			if(IPTVport == "None")
				document.writeln("<br><font color='#FF0000'>Current IPTV STB Port is set to None.</font>");
			else
				document.writeln("<br><font color='#FF0000'>Current IPTV STB Port is LAN Port "+ IPTVport + ".</font>");
		}
	}
</script>
			</td>
		</tr>
		<tr>
			<td bgcolor="#FFFFFF">
				<table width="100%" border="1" align="center" cellspacing="0"  class="list" id="DSL_WAN_table">
				<thead>
					<tr>
						<td height="25" colspan="11" style="background-color:#C0DAE4; text-align:right;">
							<input type="button" class="button" onClick='addClick()' value="<#CTL_add#>" style="font-weight:normal;"/>
							<!--t type="submit" class="button" onClick='removeClick(this.form.rml)' name="DSLWANList" value="<#CTL_del#>" style="font-weight:normal;"> -->
							<input type="submit" class="button" onClick='removeClick()' name="DSLWANList" value="<#CTL_del#>" style="font-weight:normal;">
						</td>
					</tr>
					<tr>
						<th><#Index#></th>
						<th>VPI</th>
						<th>VCI</th>
						<th><#IPConnection_autofwInProto_itemname#></th>
						<th><#t2IF#></th>
						<th width="10%"><#CTL_del#></th>
						<th width="8%"></th>
						<th width="10%"><#Internet#></th>
						<th width="10%"><#menu5_3_3#></th>
					</tr>
				</thead>
				<tbody>
					<tr>
					</tr>
				</tbody>
				<tfoot>
					<tr>
						<td height="25" colspan="11" align="right" style="background-color:#C0DAE4; text-align:right;">
							<input type="button" class="button" onClick='rebootClick()' value="<#SReboot_btn#>"/>
						</td>
					</tr>
				</tfoot>
				</table>
			</td>
		</tr>
	</tbody>
</table>
</form>
</body>
</html>
