﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns:v="" xmlns="http://www.w3.org/1999/xhtml"><head>
<html xmlns="http://www.w3.org/1999/xhtml">
<html xmlns:v>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta HTTP-EQUIV="Pragma" CONTENT="no-cache">
<meta HTTP-EQUIV="Expires" CONTENT="-1">
<title>ASUS <#Web_Title#> Wireless-N ADSL2/2+ Modem Router - Internet Connection</title>
<link rel="stylesheet" type="text/css" href="./form_style.css">
<!--link rel="stylesheet" type="text/css" href="/index_style.css"-->
<style media="screen" type="text/css">
.hintstyle{
	color: #FFFF99;
	text-decoration:none;
	border-bottom:1px dashed #069;
	cursor:help;
}
.hintstyle:link{
	text-decoration:none;
	cursor:help;
	border-bottom:1px dashed #069;
}
.hintstyle:hover{
	color: #FFF;
	border-bottom:1px dashed #069;
	text-decoration:none;
	cursor:help;
}
</style>
<script type="text/javascript" src="./state.js"></script>
<script type="text/javascript" src="./general.js"></script>
<script type="text/javascript" src="./popup.js"></script>
<script type="text/javascript" src="./help.js"></script>
<script>
wan_route_x = '<% nvram_get_x("IPConnection", "wan_route_x"); %>';
wan_nat_x = '<% nvram_get_x("IPConnection", "wan_nat_x"); %>';
wan_proto = '<% nvram_get_x("Layer3Forwarding",  "wan_proto"); %>';

var DSLWANList = [<%get_DSL_WAN_list("DSLWANConfig", "DSLWANList");%>];
var InternetPVCfound = 0;

for(var i = 0; i < DSLWANList.length; i++)
{
		switch(DSLWANList[i][2])
		{
			case "pppoe":
			case "pppoa":
			case "mer":
			case "ipoa":
				InternetPVCfound = 1;
				break;
			default:
				InternetPVCfound = 0;
				break;
		}
		if(InternetPVCfound == 1)
			break;
}

function initial(){
	var redir = parent.document.getElementById("ruleForm").redir.value;
	parent.document.getElementById("ruleForm").redir.value = "INIT";
	if (redir == "SUBMIT") location.href="DSL_WAN_Content.asp";
	enable_auto_hint(7, 19);
	document.form.dsl_vpi_0.value = 0;
	document.form.dsl_vci_0.value = 32;

	if(InternetPVCfound == 1)
		document.getElementById("protocolType").innerHTML = "<select class='input' name='dsl_proto_0' onchange='change_wan_type(this.value);'><option value='bridge'>Bridge</option></select>";

	document.form.dsl_proto_0.options[0].selected = 1;
	change_wan_type(document.form.dsl_proto_0.value);
	document.form.dsl_svc_cat_0.options[0].selected = 1;
	change_qos_type(document.form.dsl_svc_cat_0.value);
	if (navigator.userAgent.indexOf('Opera')!=-1)
	{
		// fix button color insist with others
		// inputCtrl change the style , change them back
		document.form.dsl_pppoe_dial_on_demand_0[0].style = "";
		document.form.dsl_pppoe_dial_on_demand_0[1].style = "";
		document.form.dsl_DHCPClient_0[0].style = "";
		document.form.dsl_DHCPClient_0[1].style = "";
		document.form.dsl_dnsenable_0[0].style = "";
		document.form.dsl_dnsenable_0[1].style = "";
		document.form.dsl_upnp_enable_0[0].style = "";
		document.form.dsl_upnp_enable_0[1].style = "";
	}
}

function disable_pppoe_relay()
{
	inputCtrl(document.form.dsl_pppoe_relay_0[0], 0);
	inputCtrl(document.form.dsl_pppoe_relay_0[1], 0);
	document.form.dsl_pppoe_relay_0[0].checked = false;
	document.form.dsl_pppoe_relay_0[1].checked = true;
	if (navigator.userAgent.indexOf('Opera')!=-1)
	{
		// fix button color insist with others
		// inputCtrl change the style , change them back
		document.form.dsl_pppoe_relay_0[0].style = "";
		document.form.dsl_pppoe_relay_0[1].style = "";
	}
}

function enable_pppoe_relay()
{
	inputCtrl(document.form.dsl_pppoe_relay_0[0], 1);
	inputCtrl(document.form.dsl_pppoe_relay_0[1], 1);
	document.form.dsl_pppoe_relay_0[0].checked = false;
	document.form.dsl_pppoe_relay_0[1].checked = true;
	if (navigator.userAgent.indexOf('Opera')!=-1)
	{
		// fix button color insist with others
		// inputCtrl change the style , change them back
		document.form.dsl_pppoe_relay_0[0].style = "";
		document.form.dsl_pppoe_relay_0[1].style = "";
	}
}



function applyRule(){
	if(validForm()){
		inputCtrl(document.form.dsl_pcr_0, true);
		inputCtrl(document.form.dsl_scr_0, true);
		inputCtrl(document.form.dsl_mbs_0, true);

		document.form.next_page.value = "Advanced_WAN_Content.asp";
		inputCtrl(document.form.dsl_DHCPClient_0[0], 1);
		inputCtrl(document.form.dsl_DHCPClient_0[1], 1);
		if(!document.form.dsl_DHCPClient_0[0].checked){
			inputCtrl(document.form.dsl_ipaddr_0, 1);
			inputCtrl(document.form.dsl_netmask_0, 1);
			inputCtrl(document.form.dsl_gateway_0, 1);
		}
		inputCtrl(document.form.dsl_dnsenable_0[0], 1);
		inputCtrl(document.form.dsl_dnsenable_0[1], 1);
		if(!document.form.dsl_dnsenable_0[0].checked){
			inputCtrl(document.form.dsl_dns1_0, 1);
			inputCtrl(document.form.dsl_dns2_0, 1);
		}
		parent.document.getElementById("ruleForm").redir.value = "SUBMIT";
		document.form.action_mode.value = " Add ";
		document.form.submit();
	}
	if (navigator.userAgent.indexOf('Opera')!=-1)
	{
		// fix button color insist with others
		// inputCtrl change the style , change them back
		document.form.dsl_DHCPClient_0[0].style = "";
		document.form.dsl_DHCPClient_0[1].style = "";
		document.form.dsl_dnsenable_0[0].style = "";
		document.form.dsl_dnsenable_0[1].style = "";
	}
}

function dsl_wan_netmask_check(o) {
	ip =intoa(document.form.dsl_ipaddr_0.value);
	gw =intoa(document.form.dsl_gateway_0.value);
	nm =intoa(document.form.dsl_netmask_0.value);
	lip=intoa(document.form.lan_ipaddr.value);
	lnm=intoa(document.form.lan_netmask.value);
	rem=1;

	if (document.form.dsl_ipaddr_0.value != '0.0.0.0' && (ip&lnm)==(lip&lnm)) {
		alert(o.value + ' <#JS_validip#>');
		document.form.dsl_ipaddr_0.value="10.1.1.1";
		document.form.dsl_netmask_0.value="255.0.0.0";
		o.focus();
		o.select();
		return false;
	}
	if (gw==0 || gw==0xffffffff || (ip&nm)==(gw&nm))
		return true;
	else
	{
		if(document.form.dsl_netmask_0.value == "255.255.255.248")
			return true;
		else
		{
			alert(o.value + ' <#JS_validip#>');
			o.focus();
			o.select();
			return false;
		}
	}
}

function validForm(){
	if(document.form.dsl_vpi_0.value == ""){
		alert("<#JS_fieldblank#>");
		document.form.dsl_vpi_0.focus();
		return;
	}
	else if ( isNaN(document.form.dsl_vpi_0.value) == true ) {
		alert('VPI "' + document.form.dsl_vpi_0.value + '" <#Manual_Setting_JS_invalid#>');
		document.form.dsl_vpi_0.focus();
		return;
	}

	if(document.form.dsl_vci_0.value == ""){
		alert("<#JS_fieldblank#>");
		document.form.dsl_vci_0.focus();
		return;
	}
	else if ( isNaN(document.form.dsl_vci_0.value) == true ) {
		alert('VCI "' + document.form.dsl_vci_0.value + '" <#Manual_Setting_JS_invalid#>');
		document.form.dsl_vci_0.focus();
		return;
	}

	var vpi = parseInt(document.form.dsl_vpi_0.value);
	if ( vpi < 0 || vpi > 255 ) {
		alert('VPI "' + document.form.dsl_vpi_0.value + '" <#Manual_Setting_JS_outrange#> [0-255].');
		document.form.dsl_vpi_0.focus();
		document.form.dsl_vpi_0.select();
		return;
	}

	var vci = parseInt(document.form.dsl_vci_0.value);
	if ( vci < 0 || vci > 65535 ) {
		alert('VCI "' + document.form.dsl_vci_0.value + '" <#Manual_Setting_JS_outrange#> [0-65535].');
		document.form.dsl_vci_0.focus();
		document.form.dsl_vci_0.select();
		return;
	}



	if(document.form.dsl_ifc_name_0.value == ""){
		alert("<#JS_fieldblank#>");
		document.form.dsl_ifc_name_0.focus();
		return;
	}

	for(var i = 0; i < DSLWANList.length; i++){
		if((document.form.dsl_vpi_0.value==DSLWANList[i][0])&&
		   (document.form.dsl_vci_0.value==DSLWANList[i][1])) {
			alert("VPI/VCI pair already exist!");
			return false;
		}
	}

	qos = parseInt(document.form.dsl_svc_cat_0.value);
	if (qos > 0)
	{
		if(document.form.dsl_pcr_0.value == ""){
			alert("<#JS_fieldblank#>");
			document.form.dsl_pcr_0.focus();
			return;
		}
		else if ( isNaN(document.form.dsl_pcr_0.value) == true ) {
			alert('pcr "' + document.form.dsl_pcr_0.value + '" <#Manual_Setting_JS_invalid#>');
			document.form.dsl_pcr_0.focus();
			return;
		}
		pcr = parseInt(document.form.dsl_pcr_0.value);
		if ( pcr < 1 || pcr > 1887 ) {
			alert('pcr "' + document.form.dsl_pcr_0.value + '" <#Manual_Setting_JS_outrange#> [1-1887].');
			document.form.dsl_pcr_0.focus();
			document.form.dsl_pcr_0.select();
			return;
		}
	}
	if (qos > 2)
	{
		if(document.form.dsl_scr_0.value == ""){
			alert("<#JS_fieldblank#>");
			document.form.dsl_scr_0.focus();
			return;
		}
		else if ( isNaN(document.form.dsl_scr_0.value) == true ) {
			alert('scr "' + document.form.dsl_scr_0.value + '" <#Manual_Setting_JS_invalid#>');
			document.form.dsl_scr_0.focus();
			return;
		}
		scr = parseInt(document.form.dsl_scr_0.value);
		if ( scr < 1 || scr > 1887 ) {
			alert('scr "' + document.form.dsl_scr_0.value + '" <#Manual_Setting_JS_outrange#> [1-1887].');
			document.form.dsl_scr_0.focus();
			document.form.dsl_scr_0.select();
			return;
		}
		if(document.form.dsl_mbs_0.value == ""){
			alert("<#JS_fieldblank#>");
			document.form.dsl_mbs_0.focus();
			return;
		}
		else if ( isNaN(document.form.dsl_mbs_0.value) == true ) {
			alert('mbs "' + document.form.dsl_mbs_0.value + '" <#Manual_Setting_JS_invalid#>');
			document.form.dsl_mbs_0.focus();
			return;
		}
		mbs = parseInt(document.form.dsl_mbs_0.value);
		if ( mbs < 1 || mbs > 300 ) {
			alert('mbs "' + document.form.dsl_mbs_0.value + '" <#Manual_Setting_JS_outrange#> [1-300].');
			document.form.dsl_mbs_0.focus();
			document.form.dsl_mbs_0.select();
			return;
		}
		if ( scr >= pcr) {
			alert('scr should be less than pcr');
			document.form.dsl_scr_0.focus();
			document.form.dsl_scr_0.select();
			return;
		}
		if ( mbs >= scr) {
			alert('mbs should be less than scr');
			document.form.dsl_mbs_0.focus();
			document.form.dsl_mbs_0.select();
			return;
		}
	}

	if(!document.form.dsl_DHCPClient_0[0].checked){
		if(!validate_ipaddr_final(document.form.dsl_ipaddr_0, 'dsl_ipaddr')
				|| !validate_ipaddr_final(document.form.dsl_netmask_0, 'dsl_netmask')
				|| !validate_ipaddr_final(document.form.dsl_gateway_0, 'dsl_gateway')
				)
			return false;

		if(document.form.dsl_gateway_0.value == document.form.dsl_ipaddr_0.value){
			alert("<#IPConnection_warning_WANIPEQUALGatewayIP#>");
			document.form.dsl_gateway_0.select();
			document.form.dsl_gateway_0.focus();
			return false;
		}

		if(!dsl_wan_netmask_check(document.form.dsl_netmask_0))
			return false;
	}

	if(!document.form.dsl_dnsenable_0[0].checked){
		if(!validate_ipaddr_final(document.form.dsl_dns1_0, 'dsl_dns1')){
			document.form.dsl_dns1_0.select();
			document.form.dsl_dns1_0.focus();

			return false;
		}
		if(!validate_ipaddr_final(document.form.dsl_dns2_0, 'dsl_dns2')){
			document.form.dsl_dns2_0.select();
			document.form.dsl_dns2_0.focus();

			return false;
		}
	}

	if(document.form.dsl_proto_0.value == "pppoe"
	|| document.form.dsl_proto_0.value == "pppoa"
	){
		if(!validate_string(document.form.dsl_pppoe_username_0)
		|| !validate_string(document.form.dsl_pppoe_passwd_0)
		)
			return false;

		if(document.form.dsl_pppoe_username_0.value == "" || document.form.dsl_pppoe_passwd_0.value == "")
		{
			alert("User Name/Password cannot be empty.");
			return false;
		}

		if(document.form.dsl_pppoe_dial_on_demand_0[0].checked){
			if(document.form.dsl_pppoe_idletime_0.value == ""){
				alert("<#JS_fieldblank#>");
				document.form.dsl_pppoe_idletime_0.focus();
				return;
			}
			else if ( isNaN(document.form.dsl_pppoe_idletime_0.value) == true ) {
				alert( '"' + document.form.dsl_pppoe_idletime_0.value + '" <#Manual_Setting_JS_invalid#>');
				document.form.dsl_pppoe_idletime_0.focus();
				return;
			}

			var idle_time = parseInt(document.form.dsl_pppoe_idletime_0.value);
			if ( idle_time < 0 || idle_time > 4294927695 ) {
				alert('"' + document.form.dsl_pppoe_idletime_0.value + '" <#Manual_Setting_JS_outrange#> [0-4294927695].');
				document.form.dsl_pppoe_idletime_0.focus();
				return;
			}
		}
	}

	if(document.form.dsl_proto_0.value == "pppoe"
	|| document.form.dsl_proto_0.value == "pppoa"
	)
	{
		if(!validate_range(document.form.dsl_pppoe_mtu_0, 576, 1492))
				//|| !validate_range(document.form.dsl_pppoe_mru_0, 576, 1492))
			return false;

		//if(!validate_string(document.form.dsl_pppoe_service_0)
		//		|| !validate_string(document.form.dsl_pppoe_ac_0))
		//	return false;
		if(!validate_string(document.form.dsl_pppoe_service_0))
			return false;
	}

	if(document.form.dsl_proto_0.value == "pppoa")
		document.form.dsl_mode_0.value = 1;
	else if(document.form.dsl_proto_0.value == "ipoa")
		document.form.dsl_mode_0.value = 2;
	else
		document.form.dsl_mode_0.value = 0;

	if(document.form.dsl_proto_0.value == "pppoe"
	|| document.form.dsl_proto_0.value == "pppoa"
	)
	{
		if(document.form.dsl_pppoe_dial_on_demand_0[1].checked)
		{
			inputCtrl(document.form.dsl_pppoe_idletime_0, 1);
			document.form.dsl_pppoe_idletime_0.value = "0";
		}
		if (document.form.dsl_proto_0.value == "pppoa")
		{
			disable_pppoe_relay();
		}
	}

	return true;
}

function done_validating(action){
	refreshpage();
}

function enable_ppp_account(type) {
	inputCtrl(document.form.dsl_pppoe_username_0, type);
	inputCtrl(document.form.dsl_pppoe_passwd_0, type);
	inputCtrl(document.form.dsl_pppoe_mtu_0, type);
	//inputCtrl(document.form.dsl_pppoe_mru_0, type);
	inputCtrl(document.form.dsl_pppoe_service_0, type);
	//inputCtrl(document.form.dsl_pppoe_ac_0, type);
	inputCtrl(document.form.dsl_pppoe_options_x_0, type);
	//inputCtrl(document.form.dsl_pppoe_dial_on_demand_0[0],type);
	//inputCtrl(document.form.dsl_pppoe_dial_on_demand_0[1],type);
}

function enable_upnp(type) {
	inputCtrl(document.form.dsl_upnp_enable_0[0], type);
	inputCtrl(document.form.dsl_upnp_enable_0[1], type);
	if (navigator.userAgent.indexOf('Opera')!=-1)
	{
		// fix button color insist with others
		// inputCtrl change the style , change them back
		document.form.dsl_upnp_enable_0[0].style = "";
		document.form.dsl_upnp_enable_0[1].style = "";
	}
}

function enable_dhcp_client(type){
	document.form.dsl_DHCPClient_0[0].checked = type;
	document.form.dsl_DHCPClient_0[1].checked = !type;
	inputCtrl(document.form.dsl_ipaddr_0, !type);
	inputCtrl(document.form.dsl_netmask_0, !type);
	inputCtrl(document.form.dsl_gateway_0, !type);

	if(type == 0)
	{
		document.form.dsl_dnsenable_0[0].checked = 0;
		document.form.dsl_dnsenable_0[1].checked = 1;
		document.form.dsl_dnsenable_0[0].disabled = true;
		inputCtrl(document.form.dsl_dns1_0, 1);
		inputCtrl(document.form.dsl_dns2_0, 1);
	}
	else //1
	{
		document.form.dsl_dnsenable_0[0].checked = 1;
		document.form.dsl_dnsenable_0[1].checked = 0;
		document.form.dsl_dnsenable_0[0].disabled = false;
		inputCtrl(document.form.dsl_dns1_0, 0);
		inputCtrl(document.form.dsl_dns2_0, 0);
	}

	if (navigator.userAgent.indexOf('Opera')!=-1)
	{
		// fix button color insist with others
		// inputCtrl change the style , change them back
		document.form.dsl_DHCPClient_0[0].style = "";
		document.form.dsl_DHCPClient_0[1].style = "";
		document.form.dsl_dnsenable_0[0].style = "";
		document.form.dsl_dnsenable_0[1].style = "";
	}
}

function enable_dynamic_dns(type) {
	document.form.dsl_dnsenable_0[0].checked = type;
	document.form.dsl_dnsenable_0[1].checked = !type;
	inputCtrl(document.form.dsl_dns1_0, !type);
	inputCtrl(document.form.dsl_dns2_0, !type);
	if (navigator.userAgent.indexOf('Opera')!=-1)
	{
		// fix button color insist with others
		// inputCtrl change the style , change them back
		document.form.dsl_dnsenable_0[0].style = "";
		document.form.dsl_dnsenable_0[1].style = "";
	}
}

function enable_idle_timeout(type) {
	document.form.dsl_pppoe_dial_on_demand_0[0].checked = type;
	document.form.dsl_pppoe_dial_on_demand_0[1].checked = !type;
	inputCtrl(document.form.dsl_pppoe_idletime_0, type);
}

function change_dsl_interface(){
	var interface_name = document.form.dsl_proto_0.value+"_"+document.form.dsl_vpi_0.value+"_"+document.form.dsl_vci_0.value;
	document.form.dsl_ifc_name_0.value = interface_name;
}

function change_wan_type(wan_type){
	change_dsl_interface();
	if(wan_type == "mer"){
		enable_upnp(1);
		inputCtrl(document.form.dsl_DHCPClient_0[0], 1);
		inputCtrl(document.form.dsl_DHCPClient_0[1], 1);
		enable_dhcp_client(1);
		inputCtrl(document.form.dsl_dnsenable_0[0], 1);
		inputCtrl(document.form.dsl_dnsenable_0[1], 1);
		enable_dynamic_dns(1);
		enable_ppp_account(0);
		inputCtrl(document.form.dsl_pppoe_dial_on_demand_0[0], 0);
		inputCtrl(document.form.dsl_pppoe_dial_on_demand_0[1], 0);
		enable_idle_timeout(0);
		disable_pppoe_relay();
	}
	else if(wan_type == "pppoe"||wan_type == "pppoa"){
		enable_upnp(1);
		inputCtrl(document.form.dsl_DHCPClient_0[0], 1);
		inputCtrl(document.form.dsl_DHCPClient_0[1], 1);
		enable_dhcp_client(1);
		inputCtrl(document.form.dsl_dnsenable_0[0], 1);
		inputCtrl(document.form.dsl_dnsenable_0[1], 1);
		enable_dynamic_dns(1);
		enable_ppp_account(1);
		inputCtrl(document.form.dsl_pppoe_dial_on_demand_0[0], 1);
		inputCtrl(document.form.dsl_pppoe_dial_on_demand_0[1], 1);
		enable_idle_timeout(0);
		if (wan_type == "pppoa") disable_pppoe_relay();
		else enable_pppoe_relay();
	}
	else if(wan_type == "ipoa"){
		enable_upnp(1);
		inputCtrl(document.form.dsl_DHCPClient_0[0], 0);
		inputCtrl(document.form.dsl_DHCPClient_0[1], 0);
		enable_dhcp_client(0);
		inputCtrl(document.form.dsl_dnsenable_0[0], 0);
		inputCtrl(document.form.dsl_dnsenable_0[1], 0);
		enable_dynamic_dns(0);
		enable_ppp_account(0);
		inputCtrl(document.form.dsl_pppoe_dial_on_demand_0[0], 0);
		inputCtrl(document.form.dsl_pppoe_dial_on_demand_0[1], 0);
		enable_idle_timeout(0);
		disable_pppoe_relay();
	}
	else{	// Bridge
		enable_upnp(0);
		inputCtrl(document.form.dsl_DHCPClient_0[0], 0);
		inputCtrl(document.form.dsl_DHCPClient_0[1], 0);
		enable_dhcp_client(1);
		inputCtrl(document.form.dsl_dnsenable_0[0], 0);
		inputCtrl(document.form.dsl_dnsenable_0[1], 0);
		enable_dynamic_dns(1);
		enable_ppp_account(0);
		inputCtrl(document.form.dsl_pppoe_dial_on_demand_0[0], 0);
		inputCtrl(document.form.dsl_pppoe_dial_on_demand_0[1], 0);
		enable_idle_timeout(0);
		disable_pppoe_relay();
	}

	if (navigator.userAgent.indexOf('Opera')!=-1)
	{
		// fix button color insist with others
		// inputCtrl change the style , change them back
		document.form.dsl_pppoe_dial_on_demand_0[0].style = "";
		document.form.dsl_pppoe_dial_on_demand_0[1].style = "";
		document.form.dsl_DHCPClient_0[0].style = "";
		document.form.dsl_DHCPClient_0[1].style = "";
		document.form.dsl_dnsenable_0[0].style = "";
		document.form.dsl_dnsenable_0[1].style = "";
		document.form.dsl_upnp_enable_0[0].style = "";
		document.form.dsl_upnp_enable_0[1].style = "";
	}
}


function change_qos_type(type){
	if(type == "0"){
		document.form.dsl_pcr_0.value = "0";
		document.form.dsl_scr_0.value = "0";
		document.form.dsl_mbs_0.value = "0";
		inputCtrl(document.form.dsl_pcr_0, false);
		inputCtrl(document.form.dsl_scr_0, false);
		inputCtrl(document.form.dsl_mbs_0, false);
	}
	else if(type == "1" || type == "2"){
		document.form.dsl_pcr_0.value = "300";
		document.form.dsl_scr_0.value = "0";
		document.form.dsl_mbs_0.value = "0";
		inputCtrl(document.form.dsl_pcr_0, true);
		inputCtrl(document.form.dsl_scr_0, false);
		inputCtrl(document.form.dsl_mbs_0, false);
	}
	else{
		document.form.dsl_pcr_0.value = "300";
		document.form.dsl_scr_0.value = "299";
		document.form.dsl_mbs_0.value = "32";
		inputCtrl(document.form.dsl_pcr_0, true);
		inputCtrl(document.form.dsl_scr_0, true);
		inputCtrl(document.form.dsl_mbs_0, true);
	}
}

function goBack(){
	parent.location.href = "/Advanced_WAN_Content.asp"
}
//onunload="disable_auto_hint(7, 19);
</script>
</head>
<body onload="initial();" onunLoad="return unload_body();" BGCOLOR="#A7D2E2">
 <div id="TopBanner"></div>
 <div id="Loading" class="popup_bg"></div>
 <iframe name="hidden_frame" id="hidden_frame" src="" width="0" height="0" frameborder="0"></iframe>
 <form method="post" name="form" id="ruleForm" action="/start_apply3.htm" target="hidden_frame">
<input name="productid" value="<#Web_Title#>" type="hidden">
<input name="support_cdma" value="" type="hidden">
<input name="current_page" value="Wan_Config.asp" type="hidden">
<input name="next_page" value="" type="hidden">
<input name="next_host" value="" type="hidden">
<input name="sid_list" value="DSLWANConfig;" type="hidden">
<input name="group_id" value="DSLWANList" type="hidden">
<input name="modified" value="0" type="hidden">
<input name="action_mode" value="" type="hidden">
<input name="first_time" value="" type="hidden">
<input name="action_script" value="" type="hidden">
<input name="preferred_lang" id="preferred_lang" value="EN" type="hidden">
<input type="hidden" name="wl_ssid2" value="<% nvram_char_to_ascii("WLANConfig11b", "wl_ssid"); %>">
<input name="firmver" value="1.0.2.0" type="hidden">
 <input type="hidden" name="lan_ipaddr" value="<% nvram_get_x("LANHostConfig", "lan_ipaddr"); %>" />
 <input type="hidden" name="lan_netmask" value="<% nvram_get_x("LANHostConfig", "lan_netmask"); %>" />
<input name="dsl_mode_0" value="" type="hidden">
<input name="dsl_pppoe_mru_0" value="1492" type="hidden">
<table border="0" class="content" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <!--===================================Beginning of Main Content===========================================-->
    <table align="center" border="0" cellpadding="0" cellspacing="0" width="98%">
     <tbody>
     <tr>
      <td align="left" valign="top">
       <table class="FormTitle" align="center" border="0" cellpadding="5" cellspacing="0" width="98%">
	<thead><tr><td><#menu5_3_1#> - <#menu5_dsl_wan#></td></tr></thead>
	<tbody>
	<tr>
		<td bgcolor="#ffffff"><#WANCFG_annote0#></td>
	</tr>
	</tbody>
	<tbody>
	 <tr>
	  <th bgcolor="#ffffff">
	   <table class="FormTable" align="center" border="1" bordercolor="#6b8fa3" cellpadding="4" cellspacing="0" width="100%">
	    <thead><tr><td colspan="2"><#pvccfg_PVC_Setting#></td></tr></thead>
	    <tbody>
             <tr>
	      <th width="30%">VPI</th>
          	<td align="left"><input type="text" size="4" value="" maxlength="3" name="dsl_vpi_0" class="input" onchange="change_dsl_interface();"> <span>0 - 255</span></td>
             </tr>
             <tr>
	      <th>VCI</th>
        	<td align="left"><input type="text" size="4" value="" maxlength="5" name="dsl_vci_0" class="input" onchange="change_dsl_interface();"> <span>0 - 65535</span></td>
             </tr>
	     <tr>
	      <th><#prtcl_JS_encmode#></th>
        	<td align="left">
  	          <select class="input" name="dsl_encap_0" value="0">
	           <option value="0">LLC</option>
	           <option value="1">VC</option>
	          </select>
		</td>
             </tr>
	     <tr>
	      <th><#pvccfg_service#></th>
        	<td align="left">
  	          <select class="input" name="dsl_svc_cat_0" onchange="change_qos_type(this.value);">
	           <option value="0">UBR without PCR</option>
	           <option value="1">UBR with PCR</option>
				<option value="2">CBR</option>
				<option value="3">VBR</option>
				<option value="4">GFR</option>
				<option value="5">NRT_VBR</option>
	          </select>
			</td>
             </tr>
	      <th>PCR</th>
        	<td align="left"><input type="text" size="4" value="0" maxlength="4" name="dsl_pcr_0" class="input"> <span>1 - 1887</span></td>
             </tr>

	      <th>SCR</th>
        	<td align="left"><input type="text" size="4" value="0" maxlength="4" name="dsl_scr_0" class="input"> <span>1 - 1887</span></td>
             </tr>

	      <th>MBS</th>
        	<td align="left"><input type="text" size="4" value="0" maxlength="3" name="dsl_mbs_0" class="input"> <span>1 - 300</span></td>
             </tr>

	    </tbody>
	   </table>
	  </td>
	 </tr>
	 </tbody>
	 <tbody><tr>
	  <th bgcolor="#ffffff">
	  <table class="FormTable" align="center" border="1" bordercolor="#6b8fa3" cellpadding="4" cellspacing="0" width="100%">
	   <thead><tr><td colspan="2"><#WANCFG_connection#></td></tr></thead>
	   <tbody>
            <tr>
	     <th width="30%"><a class="hintstyle" href="javascript:void(0);" onClick="window.parent.openHint(7,16);"><#Connectiontype#></a></th>
	        <td align="left">
						<span id="protocolType">
						<select class="input" name="dsl_proto_0" onchange="change_wan_type(this.value);">
							<option value="pppoe">PPPoE</option>
							<option value="pppoa">PPPoA</option>
							<option value="mer">MER</option>
							<option value="ipoa">IPOA</option>
							<option value="bridge">Bridge</option>
						</select>
						</span>
	        </td>
	    </tr>
	    <tr>
	     <th width="200"><#BasicConfig_EnableMediaServer_itemname#></th>
		<td style="font-weight: normal;" align="left">
			<input type="radio" name="dsl_upnp_enable_0" class="input" value="1" checked="checked"><#checkbox_Yes#>
			<input type="radio" name="dsl_upnp_enable_0" class="input" value="0"><#checkbox_No#>
		</td>
	    </tr>
	    <tr>
              <th width="30%"><#DSL_CFG_item1#></th>
              <td align="left"><input maxlength="32" class="input" size="32" name="dsl_ifc_name_0" onkeypress="return is_string(this)" type="text" readonly="1"></td>
            </tr>
	   </tbody>
          </table>
	  </th></tr>
		<tr>
			<td bgcolor="#ffffff">
			<table class="FormTable" align="center" border="1" bordercolor="#6b8fa3" cellpadding="4" cellspacing="0" width="100%">
				<thead>
				<tr>
					<td colspan="2"><#IPConnection_ExternalIPAddress_sectionname#></td>
				</tr>
				</thead>
				<tbody><tr>
					<th width="30%"><a class="hintstyle" href="javascript:void(0);" onClick="window.parent.openHint(7,17);"><#Layer3Forwarding_x_DHCPClient_itemname#></a></th>
					<td align="left">
						<input style="background-color: rgb(192, 218, 228);" disabled="disabled" name="dsl_DHCPClient_0" class="input" value="1" onclick="enable_dhcp_client(1);" checked="checked" type="radio"><#checkbox_Yes#>
						<input style="background-color: rgb(192, 218, 228);" disabled="disabled" name="dsl_DHCPClient_0" class="input" value="0" onclick="enable_dhcp_client(0);" type="radio"><#checkbox_No#>
					</td>
				</tr>
				<tr>
					<th width="30%"><a class="hintstyle" href="javascript:void(0);" onClick="window.parent.openHint(7,1);"><#IPConnection_ExternalIPAddress_itemname#></a></th>
					<td align="left"><input style="background-color: rgb(204, 204, 204);" disabled="disabled" name="dsl_ipaddr_0" maxlength="15" class="input" size="15" onkeypress="return is_ipaddr(this);" onkeyup="change_ipaddr(this);" type="text"></td>
				</tr>
				<tr>
					<th><a class="hintstyle" href="javascript:void(0);" onClick="window.parent.openHint(7,2);"><#IPConnection_x_ExternalSubnetMask_itemname#></a></th>
					<td align="left"><input style="background-color: rgb(204, 204, 204);" disabled="disabled" name="dsl_netmask_0" maxlength="15" class="input" size="15" onkeypress="return is_ipaddr(this);" onkeyup="change_ipaddr(this);" type="text"></td>
				</tr>
				<tr>
					<th><a class="hintstyle" href="javascript:void(0);" onClick="window.parent.openHint(7,3);"><#IPConnection_x_ExternalGateway_itemname#></a></th>
					<td align="left"><input style="background-color: rgb(204, 204, 204);" disabled="disabled" name="dsl_gateway_0" maxlength="15" class="input" size="15" onkeypress="return is_ipaddr(this);" onkeyup="change_ipaddr(this);" type="text"></td>
				</tr>
				</tbody>
			</table>
			</td>
	  	</tr>
	  <tr>
	    <td bgcolor="#ffffff">
		<table class="FormTable" align="center" border="1" bordercolor="#6b8fa3" cellpadding="4" cellspacing="0" width="100%">
          <thead>
            <tr>
              <td colspan="2"><#IPConnection_x_DNSServerEnable_sectionname#></td>
            </tr>
          </thead>
          <tbody><tr>
            <th width="30%"><a class="hintstyle" href="javascript:void(0);" onClick="window.parent.openHint(7,10);"><#IPConnection_x_DNSServerEnable_itemname#></a></th>
			<td align="left">
			  <input style="background-color: rgb(192, 218, 228);" name="dsl_dnsenable_0" value="1" onclick="enable_dynamic_dns(1);" type="radio" checked="checked"><#checkbox_Yes#>
			  <input style="background-color: rgb(192, 218, 228);" name="dsl_dnsenable_0" value="0" onclick="enable_dynamic_dns(0);" type="radio"><#checkbox_No#>
			</td>
          </tr>
          <tr>
            <th><a class="hintstyle" href="javascript:void(0);" onClick="window.parent.openHint(7,13);"><#IPConnection_x_DNSServer1_itemname#></a></th>
            <td align="left"><input style="background-color: rgb(204, 204, 204);" disabled="disabled" maxlength="15" class="input" size="15" name="dsl_dns1_0" onkeypress="return is_ipaddr(this)" onkeyup="change_ipaddr(this)" type="text"></td>
          </tr>
          <tr>
            <th><a class="hintstyle" href="javascript:void(0);" onClick="window.parent.openHint(7,14);"><#IPConnection_x_DNSServer2_itemname#></a></th>
            <td align="left"><input style="background-color: rgb(204, 204, 204);" disabled="disabled" maxlength="15" class="input" size="15" name="dsl_dns2_0" onkeypress="return is_ipaddr(this)" onkeyup="change_ipaddr(this)" type="text"></td>
          </tr>
        </tbody></table>
        </td>
	  </tr>

	  <tr>
	    <td bgcolor="#ffffff">
		  <table class="FormTable" align="center" border="1" bordercolor="#6b8fa3" cellpadding="4" cellspacing="0" width="100%">
            <thead>
            <tr>
              <td colspan="2"><#PPPConnection_UserName_sectionname#></td>
            </tr>
            </thead>
            <tbody><tr>
              <th width="30%"><a class="hintstyle" href="javascript:void(0);" onClick="window.parent.openHint(7,4);"><#PPPConnection_UserName_itemname#></a></th>
              <td align="left"><input style="background-color: rgb(204, 204, 204);" disabled="disabled" maxlength="64" class="input" size="32" name="dsl_pppoe_username_0" onkeypress="return is_string(this)" type="text"></td>
            </tr>
            <tr>
              <th><a class="hintstyle" href="javascript:void(0);" onClick="window.parent.openHint(7,5);"><#PPPConnection_Password_itemname#></a></th>
              <td align="left"><input style="background-color: rgb(204, 204, 204);" disabled="disabled" maxlength="32" class="input" size="32" name="dsl_pppoe_passwd_0" value="" type="password"></td>
            </tr>
            <tr>
              <th><a class="hintstyle" href="javascript:void(0);" onClick="window.parent.openHint(7,6);"><#PPPConnection_dialondemand_itemname#></a></th>
              <td align="left">
							  <input style="background-color: rgb(192, 218, 228);" name="dsl_pppoe_dial_on_demand_0" value="1" onclick="enable_idle_timeout(1);" type="radio"><#checkbox_Yes#>
							  <input style="background-color: rgb(192, 218, 228);" name="dsl_pppoe_dial_on_demand_0" value="0" onclick="enable_idle_timeout(0);" type="radio" checked="checked"><#checkbox_No#>
              </td>
            </tr>
            <tr>
              <th><a class="hintstyle" href="javascript:void(0);" onClick="window.parent.openHint(7,6);"><#PPPConnection_IdleDisconnectTime_itemname#></a></th>
              <td align="left">
							  <input style="background-color: rgb(204, 204, 204);" disabled="disabled" maxlength="10" class="input" size="15" name="dsl_pppoe_idletime_0" value="0" onkeypress="return is_number(this)" type="text"> <b><span>0 - 4294927695 sec</span></b>
              </td>
            </tr>
            <!--<tr>
              <th>Tx Only</th>
              <td align="left">
                <input disabled="disabled" style="background-color: rgb(192, 218, 228);" name="dsl_pppoe_idletime_check_0" value="" onclick="return change_common_radio(this, 'PPPConnection', 'dsl_pppoe_idletime', '1')" type="checkbox">Tx Only
              </td>
            </tr>-->
            <tr>
              <th><a class="hintstyle" href="javascript:void(0);" onClick="window.parent.openHint(7,7);"><#PPPConnection_x_PPPoEMTU_itemname#></a></th>
              <td align="left"><input style="background-color: rgb(204, 204, 204);" disabled="disabled" maxlength="4" size="5" name="dsl_pppoe_mtu_0" class="input" value="1492" onkeypress="return is_number(this)" type="text"> <b><span>576 - 1492</span></b></td>
            </tr>
<!--
            <tr>
              <th><a class="hintstyle" href="javascript:void(0);" onClick="window.parent.openHint(7,8);"><#PPPConnection_x_PPPoEMRU_itemname#></a></th>
              <td align="left"><input style="background-color: rgb(204, 204, 204);" disabled="disabled" maxlength="4" size="5" name="dsl_pppoe_mru_0" class="input" value="1492" onkeypress="return is_number(this)" type="text"> <b><span>576 - 1492</span></b></td>
            </tr>
-->
            <tr>
              <th><a class="hintstyle" href="javascript:void(0);" onClick="window.parent.openHint(7,9);"><#PPPConnection_x_ServiceName_itemname#></a></th>
              <td align="left"><input style="background-color: rgb(204, 204, 204);" disabled="disabled" maxlength="32" class="input" size="32" name="dsl_pppoe_service_0" onkeypress="return is_string(this)" type="text"></td>
            </tr>
            <!--<tr>
              <th><#PPPConnection_x_AccessConcentrator_itemname#></th>
              <td align="left"><input style="background-color: rgb(204, 204, 204);" disabled="disabled" maxlength="32" class="input" size="32" name="dsl_pppoe_ac_0" onkeypress="return is_string(this)" type="text"></td>
            </tr>-->
	    <tr>
		<th><a class="hintstyle" href="javascript:void(0);" onClick="window.parent.openHint(7,18);"><#PPPConnection_x_AdditionalOptions_itemname#></a></th>
		<td align="left">
			<input style="background-color: rgb(204, 204, 204);" disabled="disabled" name="dsl_pppoe_options_x_0" class="input" maxlength="255" size="32" onkeypress="return is_string(this)" onblur="validate_string(this)" type="text"></td>
	    </tr>

	    <tr>
		<th><a class="hintstyle" href="javascript:void(0);" onClick="window.parent.openHint(7,11);">PPPoE Relay</a></th>
		<td align="left">
							  <input style="background-color: rgb(192, 218, 228);" name="dsl_pppoe_relay_0" value="1" type="radio"><#checkbox_Yes#>
							  <input style="background-color: rgb(192, 218, 228);" name="dsl_pppoe_relay_0" value="0" type="radio" checked="checked"><#checkbox_No#>
		</td>
		</tr>


          </tbody></table>
          </td>
	  </tr>
       	  <tr>
	  <td bgcolor="#ffffff">

		<table class="FormTable" align="center" border="1" bordercolor="#6b8fa3" cellpadding="4" cellspacing="0" width="100%">
			<tbody><tr align="right">
				<td align="right"><input class="button" onclick="goBack();" value="<#qis_back#>" type="button">&nbsp;<input class="button" onclick="applyRule();" value="<#CTL_apply#>" type="button"></td>
			</tr>
		</tbody></table>
		</td>
	</tr>
</tbody></table>
</td>

				</tr>
			</tbody></table>
		</td>
    <!--===================================Ending of Main Content===========================================-->
  </tr>
 </table>
</form>
</body>
</html>
