﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<html xmlns:v>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7"/>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta HTTP-EQUIV="Pragma" CONTENT="no-cache">
<meta HTTP-EQUIV="Expires" CONTENT="-1">
<title>ASUS <#Web_Title#> Wireless-N ADSL2/2+ Modem Router - <#menu5_2_1#></title>
<link rel="stylesheet" type="text/css" href="index_style.css">
<link rel="stylesheet" type="text/css" href="form_style.css">

<script type="text/javascript" src="/state.js"></script>
<script type="text/javascript" src="/general.js"></script>
<script type="text/javascript" src="/popup.js"></script>
<script type="text/javascript" src="/help.js"></script>
<script type="text/javascript" src="/detect.js"></script>
<script>
wan_route_x = '<% nvram_get_x("IPConnection", "wan_route_x"); %>';
wan_nat_x = '<% nvram_get_x("IPConnection", "wan_nat_x"); %>';
wan_proto = '<% nvram_get_x("Layer3Forwarding",  "wan_proto"); %>';


<% login_state_hook(); %>
var wireless = [<% wl_auth_list(); %>];	// [[MAC, associated, authorized], ...]

function initial(){
	final_flag = 1;	// for the function in general.js

	show_banner(1);
	show_menu(5,2,1);
	show_footer();

	var dsl_nat_x = "<% nvram_get_x("DSLWANConfig", "dsl_nat"); %>";

	if(dsl_nat_x == "0")
		document.form.dsl_nat[1].checked = true;
	else
		document.form.dsl_nat[0].checked = true;

	enable_auto_hint(4, 2);
}

function checkIP(){
	var re=/^(\d+)\.(\d+)\.(\d+)\.(\d+)$/g;
	var strIP = $('lan_ipaddr').value;

	if (document.form.dsl_nat[1].checked)
	{	
			applyRule();
			re.test(strIP);
			return;
	}

	if(re.test(strIP)){
		if( RegExp.$1 == 192 && RegExp.$2 == 168 && RegExp.$3 < 256 && RegExp.$4 < 256){
			applyRule();
			re.test(strIP);
		}
		else if( RegExp.$1 == 172 && RegExp.$2 > 15 && RegExp.$2 < 32 && RegExp.$3 < 256 && RegExp.$4 < 256){
			applyRule();
			re.test(strIP);
		}
		else if( RegExp.$1 == 10 && RegExp.$2 < 256 && RegExp.$3 < 256 && RegExp.$4 < 256){
			applyRule();
			re.test(strIP);
		}
		else{
			alert('"'+strIP+'"'+" <#BM_alert_IP2#>");
			re.test(strIP);
		}
	}
	else alert('"'+strIP+'"'+" <#JS_validip#>");
}

function applyRule(){
	if(validForm()){
		document.form.action_mode.value = " Apply ";
		document.form.current_page.value = "/Advanced_LAN_Content.asp";
		document.form.next_page.value = "";

		document.form.submit();
	}
}

function validForm(){
	if(!validate_ipaddr_final(document.form.lan_ipaddr, 'lan_ipaddr') ||
			!validate_ipaddr_final(document.form.lan_netmask, 'lan_netmask'))
		return false;

	changed_hint();
	checkSubnet();

	return true;
}

function done_validating(action){
	refreshpage();
}

var old_lan_ipaddr = "<% nvram_get_x("LANHostConfig","lan_ipaddr"); %>";
function changed_hint(){
	if(document.form.lan_ipaddr.value != old_lan_ipaddr){
		alert("<#LANHostConfig_lanipaddr_changed_hint#>");
	}
	return true;
}
</script>
</head>

<body onload="initial();" onunLoad="disable_auto_hint(4, 2);return unload_body();">
<div id="TopBanner"></div>

<div id="Loading" class="popup_bg"></div>

<iframe name="hidden_frame" id="hidden_frame" src="" width="0" height="0" frameborder="0"></iframe>

<form method="post" name="form" id="ruleForm" action="/start_apply.htm" target="hidden_frame">
<input type="hidden" name="productid" value="<% nvram_get_f("general.log","productid"); %>">

<input type="hidden" name="current_page" value="Advanced_LAN_Content.asp">
<input type="hidden" name="next_page" value="">
<input type="hidden" name="next_host" value="">
<input type="hidden" name="sid_list" value="LANHostConfig;DSLWANConfig;">
<input type="hidden" name="group_id" value="">
<input type="hidden" name="modified" value="0">
<input type="hidden" name="action_mode" value="">
<input type="hidden" name="action_script" value="">
<input type="hidden" name="preferred_lang" id="preferred_lang" value="<% nvram_get_x("LANGUAGE", "preferred_lang"); %>">
<input type="hidden" name="wl_ssid2" value="<% nvram_char_to_ascii("WLANConfig11b", "wl_ssid"); %>">
<input type="hidden" name="firmver" value="<% nvram_get_x("",  "firmver"); %>">
<input type="hidden" name="wan_ipaddr" value="<% nvram_get_x("IPConnection", "wan_ipaddr"); %>">
<input type="hidden" name="wan_netmask" value="<% nvram_get_x("IPConnection", "wan_netmask"); %>" >
<input type="hidden" name="wan_gateway" value="<% nvram_get_x("IPConnection", "wan_gateway"); %>">
<input type="hidden" name="wan_proto" value="<% nvram_get_x("IPConnection", "wan_proto"); %>">
<table class="content" align="center" cellpadding="0" cellspacing="0">
  <tr>
	<td width="23">&nbsp;</td>

	<!--=====Beginning of Main Menu=====-->
	<td valign="top" width="202">
	  <div id="mainMenu"></div>
	  <div id="subMenu"></div>
	</td>

    <td valign="top">
	<div id="tabMenu" class="submenuBlock"></div>
		<br>
		<!--===================================Beginning of Main Content===========================================-->
<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
	<tr>
		<td align="left" valign="top" >

  <table width="98%" border="0" align="center" cellpadding="5" cellspacing="0" class="FormTitle" table>
	<thead>
	<tr>
		<td><#menu5_2#> - <#menu5_2_1#></td>
	</tr>
	</thead>
	<tbody>
	  <tr>
	    <td bgcolor="#FFFFFF"><#LANHostConfig_display1_sectiondesc#></td>
	  </tr>
	</tbody>

	<tr>
	  <td bgcolor="#FFFFFF">
		<table width="100%" border="1" align="center" cellpadding="4" cellspacing="0" bordercolor="#6b8fa3"  class="FormTable">

		<tr>
		<th width="30%">
			<a class="hintstyle" href="javascript:void(0);" onClick="openHint(4,3);"><#DSL_Mode#></a>
		</th>
		<td>
			  <input type="radio" value="1" name="dsl_nat" onClick="openHint(4,3);" class="content_input_fd"><#IP_Sharing#>
			  <input type="radio" value="0" name="dsl_nat" onClick="openHint(4,3);" class="content_input_fd"><#Router_mode#>
		</td>
		</tr>

		  <tr>
			<th width="30%">
			  <a class="hintstyle" href="javascript:void(0);" onClick="openHint(4,1);"><#LANHostConfig_IPRouters_itemname#></a>
			</th>

			<td>
			  <input type="text" maxlength="15" class="input" size="15" id="lan_ipaddr" name="lan_ipaddr" value="<% nvram_get_x("LANHostConfig","lan_ipaddr"); %>" onKeyPress="return is_ipaddr(this);" onKeyUp="change_ipaddr(this);">
			</td>
		  </tr>

		  <tr>
			<th>
			  <a class="hintstyle"  href="javascript:void(0);" onClick="openHint(4,2);"><#LANHostConfig_SubnetMask_itemname#></a>
			</th>

			<td>
              <input type="text" maxlength="15" class="input" size="15" name="lan_netmask" value="<% nvram_get_x("LANHostConfig","lan_netmask"); %>" onkeypress="return is_ipaddr(this);" onkeyup="change_ipaddr(this);" />
			  <input type="hidden" name="dhcp_start" value="<% nvram_get_x("LANHostConfig", "dhcp_start"); %>">
			  <input type="hidden" name="dhcp_end" value="<% nvram_get_x("LANHostConfig", "dhcp_end"); %>">
			</td>
		  </tr>

		  <tr align="right">
				<td colspan="2"><input class="button" onclick="checkIP()" type="button" value="<#CTL_apply#>"/></td>
		  </tr>
		</table>
	  </td>
	</tr>
  </table>

		</td>
</form>

					<!--==============Beginning of hint content=============-->
					<td id="help_td" style="width:15px;" valign="top">
						<form name="hint_form"></form>
						<div id="helpicon" onClick="openHint(0,0);" title="<#Help_button_default_hint#>"><img src="images/help.gif" /></div>
						<div id="hintofPM" style="display:none;">
							<table width="100%" cellpadding="0" cellspacing="1" class="Help" bgcolor="#999999">
								<thead>
								<tr>
									<td>
										<div id="helpname" class="AiHintTitle"></div>
										<a href="javascript:closeHint();">
											<img src="images/button-close.gif" class="closebutton">
										</a>
									</td>
								</tr>
								</thead>

								<tr>
									<td valign="top">
										<div class="hint_body2" id="hint_body"></div>
										<iframe id="statusframe" name="statusframe" class="statusframe" src="" frameborder="0"></iframe>
									</td>
								</tr>
							</table>
						</div>
					</td>
					<!--==============Ending of hint content=============-->

				</tr>
			</table>
		</td>

    <td width="10" align="center" valign="top">&nbsp;</td>
	</tr>
</table>

<div id="footer"></div>
</body>
</html>
