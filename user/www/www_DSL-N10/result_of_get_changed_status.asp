﻿<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta HTTP-EQUIV="Pragma" CONTENT="no-cache">
<meta HTTP-EQUIV="Expires" CONTENT="-1">
<script>
<% get_changed_status(); %>
<% get_printer_info(); %>
<% wanlink(); %>

var flag = '<% get_parameter("flag"); %>';

var manually_stop_wan = '<% nvram_get_x("", "manually_disconnect_wan"); %>';
var ifWANConnect = <% detect_if_wan(); %>;
var wan_status_log = "<% get_wan_status_log(); %>";
var detect_dhcp_pppoe = "<% detect_dhcp_pppoe(); %>";

var wireless = [<% wl_auth_list(); %>];	// [[MAC, associated, authorized], ...]

var qos_global_enable = '<% nvram_get_x("PrinterStatus", "qos_global_enable"); %>';
var qos_userdef_enable = '<% nvram_get_x("PrinterStatus", "qos_userdef_enable"); %>';
var qos_ubw = '<% nvram_get_x("PrinterStatus", "qos_ubw"); %>';
var qos_manual_ubw = '<% nvram_get_x("PrinterStatus", "qos_manual_ubw"); %>';
var qos_ready = 1;
if((qos_global_enable == '1' || qos_userdef_enable == '1')
		&& qos_ubw == '0'
		&& (qos_manual_ubw == '0' || qos_manual_ubw == ''))
	qos_ready = 0;

var current_wan_proto = '<% nvram_get_x("Layer3Forwarding",  "wan_proto"); %>';
var current_wanlink_statusstr = "";

function initial(){
	if(current_wan_proto == "pptp" || current_wan_proto == "l2tp")
	{
		if(wan_status_log.indexOf("Failed to authenticate ourselves to peer") >= 0)
			current_wanlink_statusstr = "Disconnected";
		else if(wan_status_log.indexOf("No response from ISP") >= 0)
			current_wanlink_statusstr = "Disconnected"; 
		else
			current_wanlink_statusstr = wanlink_statusstr(); 		
	}
	else
	{ 
		current_wanlink_statusstr = wanlink_statusstr();
	}
	
	//alert("initial(), flag: " + flag + " ifWANConnect:[" + ifWANConnect + " ]\n" + " wanlink_statusstr():[" + wanlink_statusstr() + " ]\n" + " detect_dhcp_pppoe:[" + detect_dhcp_pppoe + " ]\n" + " wan_status_log:[" + wan_status_log + " ]\n" + " qos_ready:[" + qos_ready + " ]\n");
	if(flag == "initial")
	{
		parent.initial_change_status(manually_stop_wan,
                                 ifWANConnect,
                                 current_wanlink_statusstr,
                                 detect_dhcp_pppoe,
                                 wan_status_log,
                                 get_disk_status_changed(),
                                 get_mount_status_changed(),
                                 printer_models()[0],
                                 wireless,
                                 qos_ready
                                 );
	}
	else
	{
		parent.set_changed_status(manually_stop_wan,
                              ifWANConnect,
                              current_wanlink_statusstr,
                              detect_dhcp_pppoe,
                              wan_status_log,
                              get_disk_status_changed(),
                              get_mount_status_changed(),
                              printer_models()[0],
                              wireless,
							  current_wan_proto
                              );
	}

	parent.check_changed_status(flag);
}
</script>
</head>

<body onload="initial();">
</body>
</html>
