﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7"/>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta HTTP-EQUIV="Pragma" CONTENT="no-cache">
<meta HTTP-EQUIV="Expires" CONTENT="-1">
<title>ASUS <#Web_Title#> Wireless-N ADSL2/2+ Modem Router - <#menu5_7_2#></title>
<link rel="stylesheet" type="text/css" href="index_style.css">
<link rel="stylesheet" type="text/css" href="form_style.css">

<script language="JavaScript" type="text/javascript" src="/state.js"></script>
<script language="JavaScript" type="text/javascript" src="/general.js"></script>
<script language="JavaScript" type="text/javascript" src="/popup.js"></script>
<script language="JavaScript" type="text/javascript" src="/help.js"></script>
<script>
wan_route_x = '<% nvram_get_x("IPConnection", "wan_route_x"); %>';
wan_nat_x = '<% nvram_get_x("IPConnection", "wan_nat_x"); %>';
wan_proto = '<% nvram_get_x("Layer3Forwarding",  "wan_proto"); %>';

function initial(){
	replace_Content("Modulation : 0", "Modulation : T1.413");
	replace_Content("Modulation : 1", "Modulation : G.lite");
	replace_Content("Modulation : 2", "Modulation : G.Dmt");
	replace_Content("Modulation : 3", "Modulation : ADSL2");
	replace_Content("Modulation : 4", "Modulation : ADSL2+");
	replace_Content("Modulation : 5", "Modulation : Multiple Mode");

	replace_Content("Line State : 0", "Line State : down");
	replace_Content("Line State : 1", "Line State : wait for init");
	replace_Content("Line State : 2", "Line State : init");
	replace_Content("Line State : 3", "Line State : up");

	replace_ContentAnnex("Annex Mode : Annex A ", "Annex Mode : Annex A/L");
}

function clearLog(){
	document.form.action_mode2.value = " ClearDSL ";
	document.form.submit();
}

function refreshLog(){
	document.form.action_mode.value = " Refresh ";
	document.form.submit();
}

function replace_Content(oldMod, newMod){
	var tb = document.form2.textarea1;
	if(tb.value.indexOf(oldMod)!= -1)
	{
		var startString = tb.value.substr(0, tb.value.indexOf(oldMod));
		var endString = tb.value.substring(tb.value.indexOf(oldMod)+14);
		tb.value = startString+newMod+endString;
	}
}

function replace_ContentAnnex(oldMod, newMod){
	var tb = document.form2.textarea1;
	if(tb.value.indexOf(oldMod)!= -1)
	{
		var startString = tb.value.substr(0, tb.value.indexOf(oldMod));
		var endString = tb.value.substring(tb.value.indexOf(oldMod)+21);
		tb.value = startString+newMod+endString;
	}
}
</script>
</head>

<body onload="show_banner(1); show_menu(5,7,6); show_footer(); load_body(); initial();" onunLoad="return unload_body();">
<div id="TopBanner"></div>

<div id="Loading" class="popup_bg"></div>
<iframe name="hidden_frame" id="hidden_frame" src="" width="0" height="0" frameborder="0"></iframe>
<form method="post" name="form" action="apply.cgi" >
<input type="hidden" name="current_page" value="Main_AdslStatus_Content.asp">
<input type="hidden" name="next_page" value="Main_AdslStatus_Content.asp">
<input type="hidden" name="next_host" value="">
<input type="hidden" name="sid_list" value="FirewallConfig;">
<input type="hidden" name="group_id" value="">
<input type="hidden" name="modified" value="0">
<input type="hidden" name="action_mode2" value="">
<input type="hidden" name="action_mode" value="">
<input type="hidden" name="first_time" value="">
<input type="hidden" name="action_script" value="">
<input type="hidden" name="preferred_lang" id="preferred_lang" value="<% nvram_get_x("LANGUAGE", "preferred_lang"); %>">
<input type="hidden" name="wl_ssid2" value="<% nvram_char_to_ascii("WLANConfig11b", "wl_ssid"); %>">
<input type="hidden" name="firmver" value="<% nvram_get_x("",  "firmver"); %>">
</form>
<table class="content" align="center" cellpadding="0" cellspacing="0">
	<tr>
		<td width="23">&nbsp;</td>
    	<td valign="top" width="202">
			<div id="mainMenu"></div>
			<div id="subMenu"></div>
		</td>

		<td valign="top">
			<div id="tabMenu" class="submenuBlock"></div><br/>

			<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
				<tr>
					<td align="left" valign="top" >

					<table width="95%" border="0" align="center" cellpadding="5" cellspacing="0" bordercolor="#6b8fa3"  class="FormTitle">
						<thead>
						<tr>
							<td><#menu5_7#> - <#menu5_7_7#></td>
						</tr>
						</thead>
						<tbody>
						<tr>
							<td bgcolor="#FFFFFF">
								<table width="100%" border="1" align="center" cellpadding="4" cellspacing="0" bordercolor="#6b8fa3" class="FormTable">
									<tr>
										<th width="20%"><#adsl_fw_ver_itemname#></th>
										<td>
											<% nvram_dump("adsl/tc_fw_ver.txt",""); %>
										</td>
									</tr>
									<tr>
										<th><#adsl_link_sts_itemname#></th>
										<td>
											<% nvram_dump("adsl/adsllinksts.log",""); %>
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td align="center"  bgcolor="#FFFFFF">
<form method="post" name="form2" action="adsllog.cgi">
								<textarea name="textarea1" cols="63" rows="14" wrap=off readonly=readonly style="width:98%; font-family:'Courier New', Courier, mono; font-size:11px;" ><% nvram_dump("adsl/adsllog.log",""); %></textarea>
							</td>
						</tr>
						</tbody>
					</table>
			<table width="300" border="0" align="right" cellpadding="2" cellspacing="0">
				<tr align="center">
					<td>
						<input type="button" onClick="clearLog();" value="<#CTL_clear#>" class="button">
					</td>
					<td>
<input type="hidden" name="next_host" value="">
<input type="submit" onClick="document.form2.next_host.value = location.host; onSubmitCtrl(this, ' Save ')" value="<#CTL_onlysave#>" class="button">
</form>
					</td>

					<td>
						<input type="button" onClick="refreshLog();" value="<#CTL_refresh#>" class="button">
					</td>
				</tr>
			</table>
		</td>
		<td id="help_td" style="width:15px;" valign="top">
			<form name="hint_form"></form>
			<div id="helpicon" onClick="openHint(0, 0);" title="<#Help_button_default_hint#>">
			<img src="images/help.gif">
		</div>

		<div id="hintofPM" style="display:none;">
		<table width="100%" cellpadding="0" cellspacing="1" class="Help" bgcolor="#999999">
		  <thead>
		  <tr>
			<td>
			  <div id="helpname" class="AiHintTitle"></div>
			  <a href="javascript:closeHint();"><img src="images/button-close.gif" class="closebutton" /></a>
			</td>
		  </tr>
		  </thead>

		  <tbody>
		  <tr>
			<td valign="top">
			  <div id="hint_body" class="hint_body2"></div>
			  <iframe id="statusframe" name="statusframe" class="statusframe" src="" frameborder="0"></iframe>
			</td>
		  </tr>
		  </tbody>
		</table>
		</div>
		</td>
	</tr>
</table>
	<td width="10" align="center" valign="top"></td>
  </tr>
</table>
<div id="footer"></div>
</body>
</html>
