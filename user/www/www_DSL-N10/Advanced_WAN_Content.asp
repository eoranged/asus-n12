﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns:v="" xmlns="http://www.w3.org/1999/xhtml"><head>
<html xmlns="http://www.w3.org/1999/xhtml">
<html xmlns:v>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta HTTP-EQUIV="Pragma" CONTENT="no-cache">
<meta HTTP-EQUIV="Expires" CONTENT="-1">
<title>ASUS <#Web_Title#> Wireless-N ADSL2/2+ Modem Router - <#menu5_3_1#> - <#menu5_dsl_wan#></title>
<link rel="stylesheet" type="text/css" href="index_style.css">
<link rel="stylesheet" type="text/css" href="form_style.css">

<script type="text/javascript" src="/state.js"></script>
<script type="text/javascript" src="/general.js"></script>
<script type="text/javascript" src="/popup.js"></script>
<script type="text/javascript" src="/help.js"></script>
<script>
wan_route_x = '<% nvram_get_x("IPConnection", "wan_route_x"); %>';
wan_nat_x = '<% nvram_get_x("IPConnection", "wan_nat_x"); %>';
wan_proto = '<% nvram_get_x("Layer3Forwarding",  "wan_proto"); %>';

wan_exist_prtcl_x = '<% nvram_get_x("ATMConnection",  "wan_prtcl"); %>';

var pcrMax = 255000;   // Assuming nitro: 255000 * 49 * 8 = 100Mbps  // SASHA: VDSL
wan_enca_mode = '0';
var original_wan_type = wan_exist_prtcl_x;
var original_wan_dhcpenable = parseInt('');
var original_dnsenable = parseInt('');
var proto = '<% ejGetOther(sysInfo, noBrPrtcl); %>';
var firewall = '<% ejGetOther(sysInfo, enblFirewall); %>';
var nat = '<% nvram_get_x("ATMConnection", "wan_enbl_nat"); %>';
var firewall = '<% nvram_get_x("ATMConnection", "wan_enbl_firewall"); %>';

function initial(){
	show_banner(1);
	show_menu(5,3,1);
	show_footer();
	enable_auto_hint(7, 19);
}

function done_validating(action){
	refreshpage();
}

</script>
</head>
<body onload="initial();" onunload="disable_auto_hint(7, 19);return unload_body();">
 <div id="TopBanner"></div>
 <div id="Loading" class="popup_bg"></div>
 <iframe name="hidden_frame" id="hidden_frame" src="" width="0" height="0" frameborder="0"></iframe>
 <form method="post" name="form" id="ruleForm" action="/start_apply.htm" target="hidden_frame">
 <input type="hidden" name="productid" value="<% nvram_get_f("general.log", "productid"); %>">
 <input type="hidden" name="support_cdma" value="<% nvram_get_x("IPConnection", "support_cdma"); %>">
 <input type="hidden" name="current_page" value="Advanced_WAN_Content.asp">
 <input type="hidden" name="next_page" value="">
 <input type="hidden" name="next_host" value="">
 <input type="hidden" name="sid_list" value="Layer3Forwarding;LANHostConfig;IPConnection;PPPConnection;ATMConnection;">
 <input type="hidden" name="group_id" value="wancfg">
 <input type="hidden" name="modified" value="0">
 <input type="hidden" name="action_mode" value="">
 <input type="hidden" name="first_time" value="">
 <input type="hidden" name="action_script" value="">
 <input type="hidden" name="preferred_lang" id="preferred_lang" value="<% nvram_get_x("LANGUAGE", "preferred_lang"); %>">
 <input type="hidden" name="wl_ssid2" value="<% nvram_char_to_ascii("WLANConfig11b", "wl_ssid"); %>">
 <input type="hidden" name="firmver" value="<% nvram_get_x("",  "firmver"); %>">
 <input type="hidden" name="lan_ipaddr" value="<% nvram_get_x("LANHostConfig", "lan_ipaddr"); %>" />
 <input type="hidden" name="lan_netmask" value="<% nvram_get_x("LANHostConfig", "lan_netmask"); %>" />
 <input type="hidden" name="redir" value="INIT">

 <table border="0" class="content" align="center" cellpadding="0" cellspacing="0">
  <tr>
   <td width="23">&nbsp;</td>
   <!--=====Beginning of Main Menu=====-->
   <td valign="top" width="202">
    <div id="mainMenu"></div>
    <div id="subMenu"></div>
   </td>
   <td height="450" valign="top">
    <div id="tabMenu" class="submenuBlock"></div><br />
    <!--===================================Beginning of Main Content===========================================-->
    <table align="center" border="0" cellpadding="0" cellspacing="0" width="98%">
     <tbody>
     <tr>
      <td valign="top">
      	<iframe id="sub_frame" src="DSL_WAN_Content.asp" width="98%" height="1300" frameborder="0" scrolling="no" style="background-color:transparent;"></iframe>
      </td>
      </form>
      <td id="help_td" style="width: 15px;" valign="top">
       <form name="hint_form"></form><img src="images/space.gif" border=0 height=15 width=1><br>
 	<div id="helpicon" onclick="openHint(0,0);" title="Click to open AiHelp">
         <img src="images/help.gif">
	</div>
	<div id="hintofPM" style="display: none;">
	 <table class="Help" bgcolor="#999999" cellpadding="0" cellspacing="1" width="100%">
	  <thead>
	   <tr>
	    <td>
	     <div id="helpname" class="AiHintTitle">Help</div>
	     <a href="javascript:void(0);" onclick="closeHint()">
	      <img src="images/button-close.gif" class="closebutton">
	     </a>
	    </td>
	   </tr>
	  </thead>
	  <tbody>
	   <tr>
	    <td valign="top">
	     <div class="hint_body2" id="hint_body">Help provides you with guidelines and information about using the router's functions.  Click the <a class="hintstyle" style="background-color: rgb(122, 163, 189);">hyperlinked words with yellow and blue underline.</a> to get help.</div>
	     <iframe id="statusframe" name="statusframe" class="statusframe" src="" frameborder="0"></iframe>
	    </td>
	   </tr>
	  </tbody>
	 </table>
	</div>
       </td>
      </tr>
    </tbody>
   </table>
  </td>

  <!--===================================Ending of Main Content===========================================-->
  <td align="center" valign="top" width="10">&nbsp;</td>
 </tr>
</tbody>
</table>

<div id="footer"><div class="bottom-image" align="center"></div>
<div class="copyright" align="center">2008 ASUSTek Computer Inc. All rights reserved.</div>
</div>
</body></html>
