/* This is a generated file, don't edit */

#define NUM_APPLETS 59

const char applet_names[] ALIGN1 = ""
"[" "\0"
"[[" "\0"
"arp" "\0"
"ash" "\0"
"basename" "\0"
"brctl" "\0"
"cat" "\0"
"chmod" "\0"
"chpasswd" "\0"
"cp" "\0"
"date" "\0"
"dd" "\0"
"dmesg" "\0"
"echo" "\0"
"expr" "\0"
"free" "\0"
"ftpget" "\0"
"ftpput" "\0"
"grep" "\0"
"hostname" "\0"
"ifconfig" "\0"
"insmod" "\0"
"kill" "\0"
"killall" "\0"
"klogd" "\0"
"ln" "\0"
"logger" "\0"
"login" "\0"
"logread" "\0"
"ls" "\0"
"lsmod" "\0"
"mdev" "\0"
"mkdir" "\0"
"mknod" "\0"
"mount" "\0"
"mv" "\0"
"netstat" "\0"
"nslookup" "\0"
"ping" "\0"
"ps" "\0"
"pwd" "\0"
"rm" "\0"
"rmmod" "\0"
"route" "\0"
"sed" "\0"
"sh" "\0"
"sleep" "\0"
"syslogd" "\0"
"telnetd" "\0"
"test" "\0"
"tftp" "\0"
"top" "\0"
"touch" "\0"
"traceroute" "\0"
"umount" "\0"
"vconfig" "\0"
"vi" "\0"
"wc" "\0"
"zcip" "\0"
;

int (*const applet_main[])(int argc, char **argv) = {
test_main,
test_main,
arp_main,
ash_main,
basename_main,
brctl_main,
cat_main,
chmod_main,
chpasswd_main,
cp_main,
date_main,
dd_main,
dmesg_main,
echo_main,
expr_main,
free_main,
ftpgetput_main,
ftpgetput_main,
grep_main,
hostname_main,
ifconfig_main,
insmod_main,
kill_main,
kill_main,
klogd_main,
ln_main,
logger_main,
login_main,
logread_main,
ls_main,
lsmod_main,
mdev_main,
mkdir_main,
mknod_main,
mount_main,
mv_main,
netstat_main,
nslookup_main,
ping_main,
ps_main,
pwd_main,
rm_main,
rmmod_main,
route_main,
sed_main,
ash_main,
sleep_main,
syslogd_main,
telnetd_main,
test_main,
tftp_main,
top_main,
touch_main,
traceroute_main,
umount_main,
vconfig_main,
vi_main,
wc_main,
zcip_main,
};
const uint16_t applet_nameofs[] ALIGN2 = {
0x0000,
0x0002,
0x0005,
0x0009,
0x000d,
0x0016,
0x001c,
0x0020,
0x0026,
0x002f,
0x0032,
0x0037,
0x003a,
0x0040,
0x0045,
0x004a,
0x004f,
0x0056,
0x005d,
0x0062,
0x006b,
0x0074,
0x007b,
0x0080,
0x0088,
0x008e,
0x0091,
0x8098,
0x009e,
0x00a6,
0x00a9,
0x00af,
0x00b4,
0x00ba,
0x00c0,
0x00c6,
0x00c9,
0x00d1,
0x40da,
0x00df,
0x00e2,
0x00e6,
0x00e9,
0x00ef,
0x00f5,
0x00f9,
0x00fc,
0x0102,
0x010a,
0x0112,
0x0117,
0x011c,
0x0120,
0x4126,
0x0131,
0x0138,
0x0140,
0x0143,
0x0146,
};
#define MAX_APPLET_NAME_LEN 10
