/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 */
#ifndef RA3052H
#define RA3052H

#define GPIO_CONTROL(led,flag)   ra_gpio_write_spec(led, flag)

#define LED_POWER       AP13M_PWRLED_GPIO_IRQ
#define BTN_RESET       AP13M_RESETDF_GPIO_IRQ

#define BTN_WLAN_ACTIVE		8
#define AP13M_PWRLED_ALARM      7	// red led
#define AP13M_PWRLED_GPIO_IRQ   11	// make one led only (same w/ WPS)
#define AP13M_RESETDF_GPIO_IRQ  10
#define AP13M_EZSETUP_GPIO_IRQ  0	// wps
#define AP13M_3G_ON             9	// blue
#define AP13M_3G_RED            13	// red 
#define AP13M_WPSLED            11
#define AP13M_USBPWR            12
#define AP13M_PSOURCE           14	// when 3g plugged, chk this if high(ok), else light red led and stop 3g process 
//#define AP13M_LOWV              8	// low voltage indicate, 0->indicate lower than 3.5V
#define AP13M_BAT_GPWR		17	// 0->valid input source
#define AP13M_BAT_LED		18	// (charg led) 1->full charged, 1/0->charging
#define AP13M_BAT_CHARG		19	// (charg indicate) 1->CHARG done, 0->charging

#define LED_ON  0       // low active (all 5xx series)
#define LED_OFF 1

#define PULL_HIGH	1
#define PULL_LOW	0

#define TASK_HTTPD       0
#define TASK_UDHCPD      1
#define TASK_LLD2D       2
#define TASK_WANDUCK     3
#define TASK_UDHCPC      4
#define TASK_NETWORKMAP  5
#define TASK_DPROXY      6
#define TASK_NTP         7
#define TASK_U2EC        8
#define TASK_OTS         9
#define TASK_LPD         10
#define TASK_UPNPD       11
#define TASK_WATCHDOG    12
#define TASK_INFOSVR     13
#define TASK_SYSLOGD     14
#define TASK_KLOGD       15
#define TASK_PPPD        16
#define TASK_PPPOE_RELAY 17
#define TASK_IGMP	 18

unsigned long task_mask;

int switch_init(void);

void switch_fini(void);

int ra3052_reg_read(int offset, int *value);

int ra3052_reg_write(int offset, int value);

int config_3052(int type);

int restore_3052();

void ra_gpio_write_spec(bit_idx, flag);

int check_all_tasks();

int ra_gpio_set_dir(int dir);

int ra_gpio_write_int(int value);

int ra_gpio_read_int(int *value);

int ra_gpio_write_bit(int idx, int value);

#endif
