/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 *
 * Copyright 2004, ASUSTek Inc.
 * All Rights Reserved.
 * 
 * THIS SOFTWARE IS OFFERED "AS IS", AND ASUS GRANTS NO WARRANTIES OF ANY
 * KIND, EXPRESS OR IMPLIED, BY STATUTE, COMMUNICATION OR OTHERWISE. BROADCOM
 * SPECIFICALLY DISCLAIMS ANY IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A SPECIFIC PURPOSE OR NONINFRINGEMENT CONCERNING THIS SOFTWARE.
 *
 * $Id: watchdog.c,v 1.1.1.1 2007/01/25 12:52:21 jiahao_jhou Exp $
 */

 
#include <stdio.h>
#include <signal.h>
#include <time.h>
#include <sys/time.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <shutils.h>
#include <rc.h>
#include <stdarg.h>
#include <ra3052.h>

typedef unsigned char   bool;   // 1204 ham

#include <wlioctl.h>
#include <syslog.h>
#include <nvram/bcmnvram.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <math.h>
#include <string.h>
#include <sys/wait.h>
#include <nvram/bcmutils.h>

#include <sys/ioctl.h>
#include "ralink_gpio.h"

#define GPIO_DEV	"/dev/gpio"



int ra_gpio_write_bit(int idx, int value)
{
	int fd, req;

	fd = open(GPIO_DEV, O_RDONLY);
	if (fd < 0) {
		perror(GPIO_DEV);
		return -1;
	}
	value &= 1;
	if (0L <= idx && idx < RALINK_GPIO_DATA_LEN)
		req = RALINK_GPIO_WRITE_BIT | (idx << RALINK_GPIO_DATA_LEN);
	else {
		close(fd);
		printf("gpio_write_bit: index %d out of range\n", idx);
		return -1;
	}
	if (ioctl(fd, req, value) < 0) {
		perror("ioctl");
		close(fd);
		return -1;
	}
	close(fd);

	return 0;
}

void ra_gpio_write_spec(bit_idx, flag)
{
	//set gpio direction to output
	//ra_gpio_set_dir(RALINK_GPIO_DIR_ALLOUT);

	//ra_gpio_write_int(RALINK_GPIO_DATA_MASK);

	ra_gpio_write_bit(bit_idx, flag);
}

void 
ra_gpio_init()
{
	//set gpio direction to output
	//ra_gpio_set_dir(0x80);	// bit 7 as power led
	//ra_gpio_set_dir(0x5080);	// setup bits 7,12,14

	/* set JTAG_GPIO_MODE */
	system("reg s 0xb0000000");
	system("reg w 60 2DD");

	//ra_gpio_set_dir(0x47a80);	/* set GPIO_DIR out 18,14,13,12,11,9,7, others in */
	ra_gpio_set_dir(0x800);        // bit 11 as power led

	//turn off all gpio (set all to high)
	//ra_gpio_write_int(RALINK_GPIO_DATA_MASK);

	//ra_gpio_write_bit(AP13M_PWRLED_GPIO_IRQ, LED_OFF);
	//ra_gpio_write_bit(AP13M_RESETDF_GPIO_IRQ, LED_OFF);
	//ra_gpio_write_bit(AP13M_EZSETUP_GPIO_IRQ, LED_OFF);
}

