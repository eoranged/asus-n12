#if 0

#define MAX_IPC_MSG_BUF 20

typedef struct
{
    long mtype;
    char mtext[MAX_IPC_MSG_BUF];
} msgbuf;

// linux msg mtype MUST be positive number
enum {
    IPC_CLIENT_MSG_Q_ID = 1,
    IPC_START_AUTO_DET,
    IPC_LINK_STATE,
    IPC_ADD_PVC,
//    IPC_DEL_PVC,
    IPC_DEL_ALL_PVC,
    IPC_STOP_AUTO_DET,
// ATE    
    IPC_ATE_ADSL_SHOW,
    IPC_ATE_SET_ADSL_MODE,    
    IPC_ATE_ADSL_DISP_PVC
};


#ifndef WIN32
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <sys/time.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <signal.h>
#include <errno.h>
#else
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#define msgsnd(...) 0
#define msgrcv(...) 0
#define EINTR 0
#endif

enum {
    EnumAdslModeT1,
    EnumAdslModeGlite,
    EnumAdslModeGdmt,
    EnumAdslModeAdsl2,
    EnumAdslModeAdsl2plus,
    EnumAdslModeMultimode,
};

enum {
    EnumAdslTypeA,
    EnumAdslTypeL,
    EnumAdslTypeA_L,
    EnumAdslTypeM,
    EnumAdslTypeA_I_J_L_M,    
};



static int m_msqid_to_d=0;
static int m_msqid_from_d=0;

void delete_msg_q(void)
{
    // delete msg queue from kernel
#ifndef WIN32    
    struct msqid_ds stMsgInfo;
    msgctl(m_msqid_from_d, IPC_RMID, &stMsgInfo);
#endif    
}

void tp_test(int argc)
{
    char buf[32];
    msgbuf send_buf;    
    msgbuf receive_buf;        
    int infolen;
    FILE* fp;
    unsigned short* pWord;
    int* pInt;    
    
    fp = fopen("/tmp/tc_resp_to_d.txt","r");
    if (fp == NULL) return;
    fgets(buf,sizeof(buf),fp);
    m_msqid_to_d=atoi(buf);
    fclose(fp);      
    
/*    
    fp = fopen("/tmp/tc_resp_from_d.txt","r");
    if (fp == NULL) return -1;
    fgets(buf,sizeof(buf),fp);
    m_msqid_from_d=atoi(buf);
    fclose(fp);      
*/    
#ifndef WIN32
    if ((m_msqid_from_d=msgget(IPC_PRIVATE,0700))<0)
    {
        printf("msgget err\n");
        return -1;
    }
    else
    {
        printf("msgget ok\n");
    }    
#endif    
    
    printf("MSQ_to_daemon : %d\n",m_msqid_to_d);
    printf("MSQ_from_daemon : %d\n",m_msqid_from_d);    
    
    send_buf.mtype=IPC_CLIENT_MSG_Q_ID;
    pInt = (int*)send_buf.mtext;      
    *pInt = m_msqid_from_d;
    if(msgsnd(m_msqid_to_d,&send_buf,MAX_IPC_MSG_BUF,0)<0)
    {
        printf("msgsnd fail (IPC_CLIENT_MSG_Q_ID)\n");
        goto delete_msgq_and_quit;
    }
    else
    {
        printf("msgsnd ok (IPC_CLIENT_MSG_Q_ID)\n");
    }    
    
    
    if (argc == 1)
    {
        printf("IPC_STOP_AUTO_DET\n");    
        send_buf.mtype=IPC_STOP_AUTO_DET;
        strcpy(send_buf.mtext,"stopdet");
        printf("msg :%s\n",send_buf.mtext);
    }
    if (argc == 2)
    {
        printf("IPC_LINK_STATE\n");    
        send_buf.mtype=IPC_LINK_STATE;
        strcpy(send_buf.mtext,"linksts");
        printf("msg :%s\n",send_buf.mtext);
    }
    if (argc == 3)
    {
        printf("IPC_START_AUTO_DET\n");    
        send_buf.mtype=IPC_START_AUTO_DET;
        strcpy(send_buf.mtext,"startdet");
        printf("msg :%s\n",send_buf.mtext);
    }        
    if (argc == 4)
    {
        printf("IPC_ADD_PVC\n");    
        send_buf.mtype=IPC_ADD_PVC;
        pWord = (unsigned short*)send_buf.mtext;
        *pWord++ = 1; // idx (0-7)
        *pWord++ = 2; // always equal to idx_1
        *pWord++ = 0; // vpi
        *pWord++ = 35; // vci
        *pWord++ = 0; // encap
        *pWord++ = 0; // mode
        printf("msg :%s\n","addpvc");
    }  
    if (argc == 5)
    {
        printf("IPC_DEL_ALL_PVC\n");    
        send_buf.mtype=IPC_DEL_ALL_PVC;
        strcpy(send_buf.mtext,"delall");
        printf("msg :%s\n",send_buf.mtext);
    }           
///////////////////    
    if(msgsnd(m_msqid_to_d,&send_buf,MAX_IPC_MSG_BUF,0)<0)
    {
        printf("msgsnd fail\n");
        goto delete_msgq_and_quit;
    }
    else
    {
        printf("msgsnd ok\n");
        
        while (1)
        {
            if((infolen=msgrcv(m_msqid_from_d,&receive_buf,MAX_IPC_MSG_BUF,0,0))<0)
            {
            // WIN32: uninit warning is ok
                if (errno == EINTR)
                {
                  continue;
                }
                else
                {
                  printf("msgrcv err %d\n",errno);
                  break;
                }                    
            }
            else
            {
                //printf("%d",receive_buf.mtype);
                printf(receive_buf.mtext);
                printf("\n");
                break;

            }
        }
    }
///////////////////////////    

delete_msgq_and_quit:
    delete_msg_q();
}


#ifndef WIN32
int main(int argc, char* argv[])
#else
int no_call_to_main(int argc, char* argv[])
#endif
{
    tp_test(argc);
    return 0;
}


#endif