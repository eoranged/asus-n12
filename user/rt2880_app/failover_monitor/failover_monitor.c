#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <sys/time.h>
#include <signal.h>
#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <dirent.h>

#include <nvram/typedefs.h>
#include <nvram/bcmnvram.h>
#include <nvparse.h>




static int m_Exit = 0;


void mysigterm()
{
	//printf("I caught the SIGTERM signal!\n");
	m_Exit = 1;
	return;    
}


int main(int argc, char* argv[])
{
	int dsl_num;
	int failover_wait_counter = 0;	

	dsl_num = atoi(nvram_safe_get("dsl_config_num"));	 
	if (dsl_num == 0)
	{
		goto quit_failover_monitor;
	}	


	if (nvram_match("use_eth_wan", "0") || nvram_match("use_eth_wan", ""))
	{
		goto quit_failover_monitor;	
	}

	if (nvram_invmatch("use_failover_eth_wan", "1"))
	{
		goto quit_failover_monitor;
	}

    if (signal(SIGTERM, mysigterm) == SIG_ERR)
    {
		printf("Cannot handle SIGTERM!\n");
	}

	// it will not do failover in first 2 min
	sleep(120);
	printf("start failover monitor...\n");


    while (m_Exit == 0)
    {
        int failback = 0;
		//
		sleep(3);

		// if user press pause button, disable failover
		if (nvram_match("manually_disconnect_wan", "1"))
		{
			continue;
		}

		if (failover_wait_counter > 0)
		{
			failover_wait_counter--;
			continue;
		}
		
		if (nvram_match("eth_wan_failover_status", "idle"))
		{
			// now is dsl
			FILE* fp;
			char buf[32];
			fp = fopen("/tmp/adsl/adsllinksts.log","rb");
			if (fp == NULL) continue;
			fgets(buf,sizeof(buf),fp);	  
			buf[2] = 0;
			fclose(fp); 
			if (strcmp(buf,"up") != 0)
			{
				// ethernet link is down
				system("chg_to_ethwan");
				nvram_set("eth_wan_failover_status", "done");
				printf("switched to eth\n");
				// It could not use a small value
				// 30~60 seconds
				if( nvram_match("ethwan_proto", "pppoe") )
					failover_wait_counter = 20;
				else
					failover_wait_counter = 10;
			}
			continue;
		}
		
		if (nvram_match("eth_wan_failover_status", "done"))
		{
			// now is failover
			if( nvram_match("ethwan_proto", "pppoe") ) {
				FILE* FpTest = fopen("/tmp/ppp/link.ppp0","r");
				if(FpTest)
					fclose(FpTest);
				else
					failback = 1;
			}
			else if( nvram_match("ethwan_proto", "dhcp") ) {
				if( nvram_match("wan_ipaddr_t", "") )
					failback = 1;
			}
			if(nvram_invmatch("ethwansts", "up"))	// ethernet link is down
				failback = 1;
			if(failback) {
				system("chg_to_dslwan");
				nvram_set("eth_wan_failover_status", "idle");
				printf("switched to dsl\n");
				// one minute
				failover_wait_counter = 20;
				failback = 0;
			}
			continue;
		}
    }

quit_failover_monitor:	  
    
	printf("failover_monitor exit\n");    

	return 0;
    
}

