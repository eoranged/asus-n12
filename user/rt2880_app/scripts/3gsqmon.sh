get_modem_node=`get_modem_node`
signode=`expr $get_modem_node + 1`
while true
do
	if [ -c "/dev/ttyUSB$signode" ]; then
		sig=`comgt -d /dev/ttyUSB$signode sig`
		sig=`expr "$sig" : '[$Signal Quality:]*\([^,]*\)'`
		#nvram set sig="$sig"
		if [ `expr length $sig` -gt 3 ]; then
			echo $sig
		else
			if [ $sig -gt 0 ]; then
				if [ `nvram get run_sh` == "off" -a `nvram get wan0_ipaddr` == "" ]; then
					echo -n
					#3g.sh &
					#break
				fi
			fi
			dbm=`expr -113 + $sig + $sig`
			echo $dbm "dbm"
			#nvram set dbm="$dbm"
		fi
	else
		break
	fi
	sleep 15
done
echo "3gsqmon.sh end."
