#!/bin/sh

# ledoff
run_sh=`nvram get run_sh`
if [ "$run_sh" == "on" ]; then
	exit 0
fi
nvram set run_sh=on

# debug info
nvram set simerr=100
nvram set apnerr=100
nvram set connerr=100
nvram set pinerr=100
nvram set g3err=100
nvram set g3state_pin=0
nvram set g3state_rd=0
nvram set g3state_z=0
nvram set g3state_dial=0
nvram set g3state_conn=0
nvram set dev_g3node=

HEADWORD="3G_Scripts"
logmessage "$HEADWORD" "$1 $2 $3 $4"
if [ "$1" != "" ]; then
        dev=$1
else
        dev=`nvram get d3g`
fi
if [ "$2" != "" ]; then
        dev_vid=$2
else
        dev_vid=`nvram get dev_vid`
fi
if [ "$3" != "" ]; then
        dev_pid=$3
else
        dev_pid=`nvram get dev_pid`
fi
if [ "$4" != "" ]; then
        dev_pin=$4
else
        dev_pin=`nvram get wan_3g_pin`
fi

stop_3g_script() {
	scrierr=`nvram get g3err`
	chk3g=`nvram get chk3g`
	if [ "$scrierr" == "1" ]; then
			simerr=`nvram get simerr`
			apnerr=`nvram get apnerr`
			pinerr=`nvram get pinerr`
			connerr=`nvram get connerr`
		if [ "$simerr" = "1" ]; then
			logmessage "$HEADWORD" "Check SIM is inserted"
		elif [ "$apnerr" = "1" ]; then
			logmessage "$HEADWORD" "APN/Dial_num error"
		elif [ "$apnerr" = "2" ]; then
			logmessage "$HEADWORD" "APN/Dial_num timeout"
		elif [ "$pinerr" = "1" ]; then
			logmessage "$HEADWORD" "PIN code error"
		elif [ "$pinerr" = "2" ]; then
			logmessage "$HEADWORD" "device not respond to PIN"
		elif [ "$pinerr" = "3" ]; then
			logmessage "$HEADWORD" "The COMGTPIN env variable is not set"
		elif [ "$connerr" = "1" ]; then
			logmessage "$HEADWORD" "connect error"
		elif [ "$connerr" = "2" ]; then
			logmessage "$HEADWORD" "connect timeout"
		else
			logmessage "$HEADWORD" "other script error"
		fi
	fi
	stop3g
	nvram set run_sh=off
	if [ "$chk3g" == "0" -a "$scrierr" == "0" ]; then
		hotplug
	fi
	exit 1
}

#hso_connect.sh down
#rmmod hso
rmmod option
rmmod usbserial
rmmod cdc-acm
rmmod sr_mod
rmmod cdrom

dev_name=`nvram get Dev3G`
dev_vid=`get_device_id vid`
dev_pid=`get_device_id pid`
logmessage "$HEADWORD" "get device $dev_vid / $dev_pid"
sleep 2

#change 3G dongle state to modem
if [ -c "/dev/ttyUSB0" ]; then
	echo "cdrom has been ejected"
else
	i=0
	sgmode=0
	while [ $i -lt "10" ]; do
		i=`expr $i + 1`
		if [ -b "/dev/sr0" ] || [ -b "/dev/sg0" ] || [ -c "/dev/sg0" ]
		then
			echo "got sr0/sg0"
			sgmode=1
			break
		else
			echo "wait $i for sr0/sg0"
		fi
		sleep 1
	done
fi

# some case
if [ "$dev_vid" = "0x1bbb" ] || [ "$dev_vid" = "0x12d1" ]; then
	sgmode=0
elif [ "$dev_vid" = "0x1c9e" ] || [ "$dev_vid" = "0xf000" ]; then
	sgmode=0
elif [ "$dev_vid" = "0x230d" ] && [ "$dev_pid" = "0x0001" ]; then	# BSNL TARACOM LW272
	sgmode=0
elif [ "$dev_vid" = "0x201e" ] && [ "$dev_pid" = "0x2009" ]; then
	sgmode=0
elif [ "$dev_vid" = "0x19d2" ] && [ "$dev_pid" = "0xfff5" ]; then	# ZTE AC2736
	sgmode=0
elif [ "$dev_vid" = "0x0af0" ]; then
	sgmode=0
elif [ "$dev_vid" = "0x1da5" -a "$dev_pid" = "0xf000" ]; then		#tmp test, QISDA H21
	sgmode=0
fi

if [ $sgmode == "0" ]; then
	echo "start modeswitch"
	usb_modeswitch -c /etc_ro/usb/g3.conf
	logmessage "$HEADWORD" "switch mode (general)"
	sleep 3
elif [ "$dev_vid" = "0x0408" -a "$dev_pid" = "0xf000" ] || [ "$dev_vid" = "0x07d1" -a "$dev_pid" = "0xa800" ]; then	# Royaltek Q110, D-Link DWM-156
	mount /dev/sda /tmp/harddisk
	touch /tmp/harddisk/wcdma.cfg
	umount /dev/sda
	sleep 3
else
	if [ "$dev_vid" = "0x12d1" -a "$dev_pid" = "0x1001" ]; then # blocked case: E169
		echo "block_cd"
	elif [ "$dev_vid" = "0x230d" -a "$dev_pid" = "0x0001" ]; then	# BSNL TARACOM LW272
		echo "block cd"
	else
		insmod cdrom
		insmod sr_mod
		sleep 1
	fi
	sdparm --command=eject /dev/sr0
	sdparm --command=eject /dev/sg0
	logmessage "$HEADWORD" "eject CDROM device"
fi

#wait for state change and re-enable state
i=0
while [ $i -lt "10" ]; do
	i=`expr $i + 1`
	usb_state=`nvram get usb3g`
	umode=`nvram get umode`
	if [ "$usb_state" = "re" ] || [ "$umode" = "done" ]; then
		echo "ready to re-insmod usbserial/hso"
		nvram set usb3g=on
		break
	else
		echo "wait $i seconds for state change"
	fi
	sleep 1
done

# check vid/pid now
i=0
while [ $i -lt "10" ]; do
	i=`expr $i + 1`
	vend_id=`get_device_id vid`
	if [ "$vend_id" != "" ]; then
		break
	else
		echo "wait $i seconds to get vid"
		sleep 1
	fi
done

if [ "$vend_id" != "" ]; then
	prod_id=`get_device_id pid`
	echo "usbserial using vid $vend_id pid $prod_id"
else
	echo "! invalid vid/pid"
	logmessage "$HEADWORD" "invalid vid/pid"
	stop_3g_script
fi

# if [ "$vend_id" = "0x0b05" ] && [ "$prod_id" = "0x0302" ]; then         # ex.case ASUS T-500
	# get_modem_node="0"
if [ "$vend_id" = "0x0af0" ] && [ "$prod_id" = "0x7401" ]; then         # iCon405 test
	get_modem_node="9"
elif [ "$vend_id" = "0x19d2" ] && [ "$prod_id" = "0x0145" ]; then         # zte ac30 test
	get_modem_node="2"
elif [ "$dev_name" = "Huawei-EC122" ] ; then         # tmp test
	get_modem_node="1"
elif [ "$dev_name" = "Huawei-EC306" ] ; then         # tmp test
	get_modem_node="1"
else
	get_modem_node=`get_modem_node`
fi
logmessage "$HEADWORD" "get node: $get_modem_node"

# if [ "$dev" = "OPTION-ICON225" ] || [ "$dev" = "Option-GlobeTrotter-HSUPA-Modem" ] || [ "$dev" = "Option-iCON-401" ] || [ "$dev" = "Vodafone-K3760" ] || [ "$dev" = "ATT-USBConnect-Quicksilver" ]; then
	# echo "[3g.sh] Using hso"
	# insmod hso
# elif [ "$get_modem_node" -eq "16" ]; then
if [ "$get_modem_node" -eq "16" ]; then
	echo "[3g.sh] Using ACM."
	insmod cdc-acm
elif [ "$get_modem_node" -eq "32" ]; then
	echo "[3g.sh] Using CDC ETHER."
	insmod usbnet
	insmod cdc_ether
else 
	echo "[3g.sh] Using usbserial"
	insmod usbserial
	insmod option vendor=$vend_id product=$prod_id
fi
sleep 3

i=0
while [ $i -lt "10" ]; do
	i=`expr $i + 1`
	if [ "$get_modem_node" -eq "16" ]; then
		if [ ! -c "/dev/ttyACM0" ] && [ ! -b "/dev/ttyACM0" ]
		then
			echo "wait $i for "/dev/ttyACM0""
		elif [ "$vend_id" = "0x0b05" -a "$prod_id" = "0x0302" ]; then	# ASUS T500
			modem_f=ttyACM1
			echo "ready to call pppd"
			break
		else
			modem_f=ttyACM0
			echo "ready to call pppd"
			break
		fi
	elif [ "$get_modem_node" -eq "32" ]; then	#test Huawei E353
		ifconfig eth0 up
		udhcpc -i eth0 -p /var/run/udhcpc_usb.pid -s /sbin/udhcpc.sh
		iptables --table nat --append POSTROUTING --out-interface eth0 -j MASQUERADE
		iptables --append FORWARD --in-interface br0 -j ACCEPT
		nvram set run_sh=off
		exit 0
	else
		if [ ! -c "/dev/ttyUSB$get_modem_node" ]
		then
			echo "wait $i for "/dev/ttyUSB$get_modem_node""
		else
			modem_f=ttyUSB$get_modem_node
			echo "ready to call pppd"
			break
		fi
	fi
	sleep 1
done

if [ $i = 10 ]; then
	logmessage "$HEADWORD" "Get USB node failed!."
	stop_3g_script
fi

nvram set modf=$modem_f

echo "[3g.sh] Check 3G Parameter"
user_3g=`nvram get hsdpa_user`
pass_3g=`nvram get hsdpa_pass`
apn_3g=`nvram get hsdpa_apn`
pin_3g=`nvram get wan_3g_pin`
disable_pppd=`nvram get disable_pppd`
baud_3g=`nvram get hsdpa_baud`
EVDO_on=`nvram get EVDO_on`
sleep 1

if [ "$user_3g" != "" ]; then
	op_user="-u $user_3g"
	if [ "$pass_3g" != "" ]; then
		op_pass="-p $pass_3g"
	fi
else
	op_user=""
	op_pass=""
fi

# check PIN Code, skip CDMA2000(EVDO), skip cdc_ether
if [ $EVDO_on -ne "1" ] && [ $get_modem_node -ne "32" ]; then
	comgt -d /dev/$modem_f -s /etc_ro/ppp/3g/chk_param
	res="$?"
	if [ $res -ne "0" ]; then
		if [ $res -eq "1" ]; then
			echo "PIN ERROR"
			nvram set chk3g=1
		elif [ $res -eq "2" ]; then
			echo "PUK ERROR"
			nvram set chk3g=2
		elif [ $res -eq "3" ]; then
			echo "TIMEOUT ERROR"
			nvram set chk3g=3
		else
			echo "UNKNOWN ERROR"
			nvram set chk3g=99
		fi
		logmessage "$HEADWORD" "check PIN code failed"
		stop_3g_script
	else
		nvram set chk3g=0
	fi
fi

if [ "$EVDO_on" -eq "1" ]; then		#CDMA2000
	if [ "$vend_id" = "0x201e" ] && [ "$prod_id" = "0x2009" ]; then
		config-3g-ppp.sh $op_user $op_pass -m $modem_f $op_baud -c EVDO_ce100.scr -d EVDO_disconn.scr
	elif [ "$vend_id" = "0x19d2" ] && [ "$prod_id" = "0x1181" ]; then	#K3772-Z
		config-3g-ppp.sh $op_user $op_pass -m $modem_f $op_baud -c EVDO_k3772z.scr -d EVDO_disconn.scr
	else
		config-3g-ppp.sh $op_user $op_pass -m $modem_f $op_baud -c EVDO_conn.scr -d EVDO_disconn.scr
	fi
elif [ "$EVDO_on" -eq "2" ]; then	#TD-SCDMA
	config-3g-ppp.sh $op_user $op_pass -m $modem_f $op_baud -c td.scr -d Generic_disconn.scr
	echo "[3g.sh] config-3g-ppp.sh $op_pass $op_user -m $modem_f -c td_conn.scr -d Generic_disconn.scr"
else	#WCDMA
	if [ "$vend_id" = "0x0b05" ] && [ "$prod_id" = "0x0302" ]; then		# ex.case ASUS T-500
		config-3g-ppp.sh $op_user $op_pass -m $modem_f $op_baud -c t500_conn.scr -d Generic_disconn.scr
		echo "[3g.sh] config-3g-ppp.sh $op_pass $op_user -m $modem_f -c t500_conn.scr -d Generic_disconn.scr"
	elif [ "$vend_id" = "0x0421" ] && [ "$prod_id" = "0x0612" ]; then	# ex.case.test CS-15 
		config-3g-ppp.sh $op_user $op_pass -m $modem_f $op_baud -c t500_conn.scr -d Generic_disconn.scr
	elif [ "$vend_id" = "0x106c" ] && [ "$prod_id" = "0x3716" ]; then
		config-3g-ppp.sh $op_user $op_pass -m $modem_f $op_baud -c verizon_conn.scr -d Generic_disconn.scr
	elif [ "$vend_id" = "0x1410" ] && [ "$prod_id" = "0x4400" ]; then
		config-3g-ppp.sh $op_user $op_pass -m $modem_f $op_baud -c rogers_conn.scr -d Generic_disconn.scr
	elif [ "$vend_id" = "0x230d" ] && [ "$prod_id" = "0x0001" ]; then
		config-3g-ppp.sh $op_user $op_pass -m $modem_f $op_baud -c bnsl_lw272.scr -d Generic_disconn.scr
	elif [ $get_modem_node -eq "32" ]; then	# test Huawei E353 for cdc_ether
		config-3g-ppp.sh $op_user $op_pass -m $modem_f $op_baud -c test_conn.scr -d Generic_disconn.scr
	else
		config-3g-ppp.sh $op_user $op_pass -m $modem_f $op_baud -c Generic_conn.scr -d Generic_disconn.scr
		echo "[3g.sh] config-3g-ppp.sh $op_pass $op_user -m $modem_f -c Generic_conn.scr -d Generic_disconn.scr"
	fi
fi

if [ "$disable_pppd" = "1" ]
then
	echo "[3g.sh] do not run pppd for debug test"
else
	sleep 1
	pppd call 3g
fi

# ledon

echo -n "wait for 3g connecting..."
i=0
while [ $i -lt "150" ]; do
	ip=`nvram get wan_ipaddr_t`
	if [ "$ip" != "" ]; then
		logmessage "$HEADWORD" "3G connected."
		#nvram set wan_dns_t="`nvram get wan0_dns`"
		break
	elif [ ! -c "/dev/$modem_f" ]; then
		break
	else
		echo -n "."
		sleep 1
	fi
	i=`expr $i + 1`
done

if [ "$ip" = "" ]; then
	logmessage "$HEADWORD" "3G connection timeout."
	stop_3g_script
fi

nvram set run_sh=off
