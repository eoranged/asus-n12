#!/bin/sh

PPP_3G_FILE=/tmp/ppp/peers/3g
PPP_ISP=`nvram get hsdpa_isp`
PPP_APN=`nvram get hsdpa_apn`
EVDO_on=`nvram get EVDO_on`

usage () {
  echo "usage: config-3g-ppp.sh [option]..."
  echo "options:"
  echo "  -h              : print this help"
  echo "  -p password     : set password"
  echo "  -u username     : set username"
  echo "  -b baud         : Set baudrate"
  echo "  -m dev 	  : set modem device"
  echo "  -c conn         : set connect AT script"
  echo "  -d disconn	  : set disconnect AT script"
  exit
}


for arg in $*
  do
    if [ "$1" != "" ] 
    then
      case "$1" in
        "-p")
          PASSWORD="password $2" 
    	  shift ;;
        "-u")
          USERNAME="user $2" 
    	  shift ;;
        "-b")
          BAUD="$2"
	  shift ;;
        "-m")
          MODEM="/dev/$2" 
	  shift ;;
        "-c")
          CONN="$2" 
	  shift ;;
        "-d")
          DISCONN="$2" 
	  shift ;;
        "-h")
	  usage ;;
        *) 
	  echo "illegal option -- $2" 
	  usage ;;
      esac
      shift
  fi
  done



echo $MODEM > $PPP_3G_FILE
echo $BAUD >> $PPP_3G_FILE
echo $USERNAME >> $PPP_3G_FILE
echo $PASSWORD >> $PPP_3G_FILE
if [ "$PPP_ISP" == 'Virgin' ]; then
	echo "refuse-chap" >> $PPP_3G_FILE
	echo "refuse-mschap" >> $PPP_3G_FILE
	echo "refuse-mschap-v2" >> $PPP_3G_FILE
fi
#if [ "$PPP_APN" == 'TATA' ] && [ "$USERNAME" != '' ]; then
#	echo "remotename tata" >> $PPP_3G_FILE
#	echo "ipparam tata" >> $PPP_3G_FILE
#fi
if [ "$PPP_APN" == 'bsnlnet' ]; then
	echo "refuse-chap" >> $PPP_3G_FILE
	echo "refuse-mschap" >> $PPP_3G_FILE
	echo "refuse-mschap-v2" >> $PPP_3G_FILE
	echo "refuse-eap" >> $PPP_3G_FILE
	echo "-ccp" >> $PPP_3G_FILE
	echo "-vj" >> $PPP_3G_FILE
fi
echo "modem" >> $PPP_3G_FILE
echo "crtscts" >> $PPP_3G_FILE
echo "noauth" >> $PPP_3G_FILE
echo "defaultroute" >> $PPP_3G_FILE
echo "noipdefault" >> $PPP_3G_FILE
#echo "nopcomp" >> $PPP_3G_FILE
#echo "noaccomp" >> $PPP_3G_FILE
echo "novj" >> $PPP_3G_FILE
echo "nobsdcomp" >> $PPP_3G_FILE
echo "holdoff 10" >> $PPP_3G_FILE	# tmp test
echo "usepeerdns" >> $PPP_3G_FILE
#if [ $PPPOE_OPMODE == "KeepAlive" ]; then
	echo "persist" >> $PPP_3G_FILE
#	echo "holdoff $PPPOE_OPTIME" >> $PPP_3G_FILE
#elif [ $PPPOE_OPMODE == "OnDemand" ]; then
#	PPPOE_OPTIME=`expr $PPPOE_OPTIME \* 60`
#	echo "demand" >> $PPP_3G_FILE
#	echo "idle $PPPOE_OPTIME" >> $PPP_3G_FILE
#fi
echo "nodeflate" >> $PPP_3G_FILE 
echo "debug" >> $PPP_3G_FILE 
echo "kdebug 7" >> $PPP_3G_FILE 
echo "lcp-echo-interval 15" >> $PPP_3G_FILE 
echo "lcp-echo-failure 10" >> $PPP_3G_FILE 
echo "maxfail 0" >> $PPP_3G_FILE 
echo "connect \"/bin/comgt -d $MODEM -s /etc_ro/ppp/3g/$CONN\"" >> $PPP_3G_FILE 
echo "disconnect \"/bin/comgt -d $MODEM -s /etc_ro/ppp/3g/$DISCONN\"" >> $PPP_3G_FILE 


