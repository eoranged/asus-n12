
#ifndef WIN32

#include <linux/if_ether.h>
#include <linux/if_packet.h>
#include <linux/socket.h>
#include <linux/net.h>
#include <linux/if.h>
#include <linux/ioctl.h>
#include "ra_ioctl.h"
#include "ra_reg_rw.h"

// do not know why socket.h has no this enum define
enum sock_type
{
    SOCK_STREAM     = 1,
    SOCK_DGRAM      = 2,
    SOCK_RAW        = 3,
    SOCK_RDM        = 4,
    SOCK_SEQPACKET  = 5,
    SOCK_DCCP       = 6,
    SOCK_PACKET     = 10,
};

static int esw_fd;
/*
int ra3052_reg_writ(eint offset, int value)
{
    struct ifreq ifr;
    esw_reg reg;

    reg.off = offset;
    reg.val = value;
    strncpy(ifr.ifr_name, "eth2", 5);
    ifr.ifr_data = &reg;
    if (-1 == ioctl(esw_fd, RAETH_ESW_REG_WRITE, &ifr))
    {
        perror("ioctl");
        close(esw_fd);
    }
    return 0;
}
*/



int switch_rcv_okt(unsigned char* prcv_buf, unsigned short max_rcv_buf_size, unsigned short* prcv_buf_len)
{
    struct ifreq ifr;
    unsigned char ioctl_read_buf[MAX_TC_RESP_BUF_LEN+2];
    strncpy(ifr.ifr_name, "eth2", 5);
    ifr.ifr_data = ioctl_read_buf;
    if (-1 == ioctl(esw_fd, RAETH_GET_TC_RESP, &ifr))
    {
        perror("ioctl");
        return -1;
    }
							
    unsigned short* pWord;
    pWord=(unsigned short*)(ioctl_read_buf);
    unsigned short pkt_len;
    pkt_len = *pWord++;
    *prcv_buf_len = 0;    
    
    if (pkt_len > 0)
    {
        if (max_rcv_buf_size >= pkt_len)
        {
            memcpy(prcv_buf, pWord,pkt_len);
            *prcv_buf_len = pkt_len;
        }
    }
    
    return 0;    							
}							

int
switch_init(void)
{
    esw_fd = socket(AF_INET, SOCK_DGRAM, 0);
    if (esw_fd < 0)
    {
        perror("socket");
        return -1;
    }
    return 0;
}

void
switch_fini(void)
{
    close(esw_fd);
}


#endif
