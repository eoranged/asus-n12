#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <sys/time.h>
#include <signal.h>
#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <dirent.h>
#include "ra_reg_rw_led.h"

#include <nvram/typedefs.h>
#include <nvram/bcmnvram.h>
#include <nvparse.h>

extern void InternetLedOn(void);
extern void InternetLedOff(void);
extern void PowerLedOn(void);
extern void PowerLedOff(void);
extern void ra_gpio_init();
extern int ra3052_reg_read(int offset, int *value);



// 0xff = uninit
int g_LedSts = 0xff;

void SetInternetLedOn(void)
{
    if (g_LedSts == 0xff || g_LedSts==0)
    {
        InternetLedOn();
    }
    g_LedSts = 1;        
}

void SetInternetLedOff(void)
{
    if (g_LedSts == 0xff || g_LedSts==1)
    {
        InternetLedOff();
    }
    g_LedSts = 0;        
}

int ethwan_sts(void)
{
	// ethernet WAN
	int val;
	int ret;
	FILE* fp2;
	// POA: Port Ability (offset: 0x80)
	// 29:25 , Port 4 ~ port 0 Link
	// 1=up, 0=down
	if(ra3052_reg_read(0x80, &val) != 0)
			return 0;
	
	//printf ("phy = %x,",val);
	
	if (nvram_match("use_eth_wan", "1"))								
	{
		val >>= 28;
		val &= 1;
		//printf ("LAN 1 phy = %x \n",val);
	}
	else if (nvram_match("use_eth_wan", "2"))								
	{
		val >>= 27;
		val &= 1;
		//printf ("LAN 2 phy = %x \n",val); 		
	}
	else if (nvram_match("use_eth_wan", "3"))								
	{
		val >>= 26;
		val &= 1;
		//printf ("LAN 3 phy = %x \n",val); 		
	}
	else if (nvram_match("use_eth_wan", "4"))								
	{
		val >>= 25;
		val &= 1;
		//printf ("LAN 4 phy = %x \n",val); 		
	}		
	
	// use on wan_duck
	fp2 = fopen("/tmp/adsl/ethlinksts.log","wb");
	if (fp2 == NULL) return;
	if (val == 1)
	{
		fputs("up",fp2);
		nvram_set("ethwansts","up");
	}
	else
	{
		fputs("down",fp2);
		nvram_set("ethwansts","down");
	}
	fclose(fp2);
	
	return val; 
}

int adslwan_sts(void)
{
	// ADSL
	FILE* fp;
	char buf[32];
	fp = fopen("/tmp/adsl/adsllinksts.log","rb");
	if (fp == NULL) return 0;
	//memset(buf,0,sizeof(buf));
	fgets(buf,sizeof(buf),fp);	  
	buf[2] = 0;
	fclose(fp); 
	if (strcmp(buf,"up") == 0)
	{
		return 1;
	}
	return 0;
}

int is_phyconnected()
{
	if (nvram_match("hsdpa_enable", "0"))
	{
		// 3g disabled
		// ethernet wan failover case
		if (nvram_invmatch("use_eth_wan", "0") && nvram_match("use_failover_eth_wan", "1"))
		{
			if (nvram_match("eth_wan_failover_status", "idle"))
			{
				// internet_led used for update ethernet status
				ethwan_sts();
				return adslwan_sts();
			}
			else
			{
				return ethwan_sts();
			}
		}
		// ethernet wan without failover
		if (nvram_invmatch("use_eth_wan", "0") && nvram_match("use_failover_eth_wan", "0")) 							
		{
			return ethwan_sts();
		}
		return adslwan_sts();		
	}
	else
	{
		// 3g enabled
		// fail-over case
		if (nvram_invmatch("failover_3g_enable", "0"))
		{
			if (nvram_match("failover_3g_status", "idle"))
			{
				return adslwan_sts();
			}
			else
			{
				if(nvram_invmatch("usb_path1", "") && nvram_invmatch("wan0_ipaddr", ""))
					return 1;
				else
					return 0;
			}
		}

		// 3g wan without failover
		if(nvram_invmatch("usb_path1", "") && nvram_invmatch("wan0_ipaddr", ""))
			return 1;
		else
			return 0;		
	}


}


static int m_ChkInternetCnt = 0;
static int m_PrevInternetSts = 0;
static int m_ChkPpp = 0;
//static int m_ChkPppIdleTimeout = 0;
static int m_PktRecv = 0;
static int m_Bridge = 0;


/* Find process name by pid from /proc directory */

#if 0
char *find_name_by_proc(int pid){
	FILE *fp;
	char line[254];
	char filename[80];
	static char name[80];
	
	snprintf(filename, sizeof(filename), "/proc/%d/status", pid);
	
	if((fp = fopen(filename, "r")) != NULL){
		fgets(line, sizeof(line), fp);
		/* Buffer should contain a string like "Name:   binary_name" */
		sscanf(line, "%*s %s", name);
		fclose(fp);
		return name;
	}
	
	return "";
}


int get_ppp_pid(char *conntype){
	int pid = -1;
	char tmp[80], tmp1[80];
	
	snprintf(tmp, sizeof(tmp), "/var/run/%s.pid", conntype);
	file_to_buf(tmp, tmp1, sizeof(tmp1));
	pid = atoi(tmp1);
	
	return pid;
}


int file_to_buf(char *path, char *buf, int len){
	FILE *fp;
	memset(buf, 0 , len);
	
	if((fp = fopen(path, "r")) != NULL){
		fgets(buf, len, fp);
		fclose(fp);
		
		return 1;
	}
	
	return 0;
}

#endif


#define wan_prefix(unit, prefix)	snprintf(prefix, sizeof(prefix), "wan%d_", unit)

//
// pppd is upgrade to new new version, this dirty patch is no longer required
//
#if 0
static int m_PppdTerminated = 0;

void chk_linksts_and_handle_ppp(void)
{
		//
		// workaround fix for pppd crash
		// if wan down , disable eth interface and stop pppd
		// if wan re-up, enable eth and start pppd
		//
		// this patch is very ineffecient , we should modify PPP source code to fix PPP disappearing
		//
		//
		if (m_ChkPpp)
		{
			if (m_ChkPppIdleTimeout == 0)
			{
				FILE* FpChkPppEver;
				FpChkPppEver = fopen("/tmp/ppp_ever_created","r");
				if (FpChkPppEver != NULL)
				{
					fclose(FpChkPppEver);
					if (is_phyconnected() == 1)
					{
						if (m_PppdTerminated == 1)
						{
							m_PppdTerminated = 0;
							if (nvram_invmatch("use_eth_wan", "0"))								
							{
								system("ifconfig eth2.3 up"); 										
							}
							else
							{
								system("ifconfig eth2.2.1 up"); 					
							}
							system("/usr/sbin/pppd file /tmp/ppp/options.wan0");
						}
					}
					else
					{
						if (m_PppdTerminated == 0)		
						{
							m_PppdTerminated = 1;			
							if (nvram_invmatch("use_eth_wan", "0"))								
							{
								system("ifconfig eth2.3 down");		
							}
							else
							{
								system("ifconfig eth2.2.1 down");		
							}
							system("killall pppd");
						}
					}
				}
			}
		}	
}
#endif

int GetInternetSts()
{
#if 0
	char type[32], ip[32], netmask[32], gateway[32], dns[128], statusstr[32];
	char tmp[100], prefix[] = "wanXXXXXXXXXX_";
	struct ifreq ifr;
	struct sockaddr_in *our_ip;
	struct in_addr in;
	char *pwanip = NULL, *ppp_addr, *usb_device;
	
#endif	
	FILE *fp;
	int status = 0, unit, s;
	char filename[80], conntype[10];	
	char tmp[100], prefix[] = "wanXXXXXXXXXX_";
	int RetVal=0;	
	
#if 0	
	/* current unit */
	if((unit = atoi(nvram_safe_get("wan_unit"))) < 0)
		unit = 0;
	wan_prefix(unit, prefix);
#endif	

	if(!is_phyconnected()){
		m_PrevInternetSts = 0;
		return 0;
	}	

	if (m_ChkInternetCnt < 6)
	{
		m_ChkInternetCnt++;
		return m_PrevInternetSts;
	}

	m_ChkInternetCnt = 0;
	
		if(m_ChkPpp)
		{
			FILE* FpTest;
			DIR *ppp_dir;	// 2008.01 James.
			struct dirent *entry;	// 2008.01 James.
			char *name;
			char* pos = NULL;
			char* cpos = NULL;
			

//
// pppoe/pppoa time out
//
			/*
			FpTest = fopen("/tmp/adsl/idletimeout","r");
			if (FpTest != NULL)
			{
				fclose(FpTest);
				RetVal = 0;
				goto GetInternetSts_Exit;
			}
			*/

//
// check ppp link actived ?
//

			FpTest = fopen("/tmp/ppp/link.ppp0","r");
			if (FpTest != NULL)
			{
				fclose(FpTest);
				RetVal = 1;
			}
			else
			{
				RetVal = 0;
			}			
			
#if 0			
			memset(filename, 0, 80);
			
			ppp_dir = opendir("/tmp/ppp");
			if(ppp_dir == NULL){
				RetVal = 0;
			}
			
			while((entry = readdir(ppp_dir)) != NULL){
				if(!strcmp(entry->d_name, ".") || !strcmp(entry->d_name, ".."))
					continue;
				
				if((pos = strstr(entry->d_name, "link")) != NULL){
					sprintf(filename, "/tmp/ppp/%s", entry->d_name);
					strcpy(conntype, pos+5);					
					break;
				}
			}
			closedir(ppp_dir);
			
				if(strlen(filename) > 0 && (fp = fopen(filename, "r")) != NULL){
						int pid = -1;
						fclose(fp);
				
//				if(nvram_match(strcat_r(prefix, "proto", tmp), "heartbeat")){
//					char buf[20];
					
//					file_to_buf(filename, buf, sizeof(buf));
//					pid = atoi(buf);
//				}
//				else
					pid = get_ppp_pid(conntype);
																						 
				name = find_name_by_proc(pid);	
				
				if(!strncmp(name, "pppoecd", 7) ||	// for PPPoE
					!strncmp(name, "pppd", 4)	// for PPTP
					/*!strncmp(name, "bpalogin", 8) // for HearBeat*/
				){
					if(!strcmp(nvram_safe_get("manually_disconnect_wan"), "1"))
					{
						RetVal = 0;
					}
					else
					{
						RetVal = 1;
					}
				}
				else{
					RetVal = 0;				
					// For some reason, the pppoed had been died, by link file still exist.
					unlink(filename);					
				}
			}
			else{
				RetVal = 0;
			}			
#endif			
			
		}	
		else
		{
			if (m_Bridge) 
			{
				RetVal = 0;
			}
			else
			{
				// ipoa/mer
				if (m_PktRecv) RetVal = 1;
				else RetVal = 0;
			}
		}
#if 0
		else { 
			/* Open socket to kernel */
			if((s = socket(AF_INET, SOCK_DGRAM, 0)) < 0){
				RetVal = 0;
			}
			else{
				/* Check for valid IP address */
				strncpy(ifr.ifr_name, nvram_safe_get(strcat_r(prefix, "ifname", tmp)), IFNAMSIZ);
				
				if(!ioctl(s, SIOCGIFADDR, &ifr)){
					our_ip = (struct sockaddr_in *) &ifr.ifr_addr;
					in.s_addr = our_ip->sin_addr.s_addr;
					pwanip = inet_ntoa(in);
					
					if(!strcmp(pwanip, "") || pwanip == NULL){
						RetVal = 0;
					}
					else if(!strcmp(nvram_safe_get("manually_disconnect_wan"), "1")){
						RetVal = 0;
					}
					else{
						RetVal = 1;
					}
				}
				else{
					RetVal = 0;
				}
				
				close(s);
			}
		}
#endif

GetInternetSts_Exit:

	// internet_ready.txt is required by ntp client for a dirty fix.
	// the ntp issue is that ntp client will not send ntp packet after internet connection up
	// ntp client will snoop the existence of this file and then send the ntp packet
	
	// PPPoE/PPPoA : internet_ready.txt = ppp
	// MER/IPoA : internet_ready.txt = internet_led
	// bridge : router have not internet connectivity
	if (RetVal)
	{
		FILE* fp;
		fp = fopen("/tmp/internet_ready.txt","r");
		if (fp != NULL)
		{
			fclose(fp);
		}
		else
		{
			fp = fopen("/tmp/internet_ready.txt","w");
			fputs("Internet is up\n",fp);
			fclose(fp);
		}
	}
	else
	{
		FILE* fp;
		fp = fopen("/tmp/internet_ready.txt","r");
		if (fp != NULL)
		{
			fclose(fp);
			remove("/tmp/internet_ready.txt");			
		}	
	}

	m_PrevInternetSts = RetVal;
		

	return RetVal;
		

}

void SleepMs(int ms)
{
    usleep(ms*1000); //convert to microseconds
}

void RstBtnFormatNvram()
{
	int i;
	int btn;
	int not_pressed = 0;
	for (i=0; i<10; i++)
	{
		btn = GetRstBtnSts();
		if (btn == 1)
		{
			// not pressed , exit
			not_pressed = 1;
			break;
		}
	}
	if (not_pressed == 0)
	{
		fprintf(stderr, "\n## Format NVRAM flash block ##\n");
		system("erase /dev/mtd2");
		// wait 4 seconds
		for (i=0; i<80; i++)		
		{
			if ((i%2) == 0)
			{
				PowerLedOn();
			}
			else
			{
				PowerLedOff();
			}
			SleepMs(50);
		}
		// user need to 
		while (1)
		{
			for (i=0; ; i++)		
			{
				if ((i%2) == 0)
				{
					PowerLedOn();
					InternetLedOff();
				}
				else
				{
					PowerLedOff();
					InternetLedOn();					
				}
				fprintf(stderr, "\n## Turn off power and turn on ##\n");				
				sleep(1);
			}
		}
	}


	
}

static int m_ChkLinkStsAndHandlePppCnt = 0;

static int m_Exit = 0;


void mysigterm()
{
	//printf("I caught the SIGTERM signal!\n");
	m_Exit = 1;
	return;    
}


int main(int argc, char* argv[])
{
	int proto_type = 0;
	int dsl_num;
    if (argc == 2)
    {
        printf("test on\n");
        SetInternetLedOn();
        return;
    }
    if (argc == 3)
    {
        printf("test off\n");
        SetInternetLedOff();
        return;
    }
    
    // test blinking
    if (argc == 4)
    {
        printf("test blinking.\n");    
        while (1)
        {
                SetInternetLedOff();
                SleepMs(atoi(argv[1]));
                SetInternetLedOn();
                SleepMs(atoi(argv[1]));    
        }
    }

	// NVRAM erase to avoid always reboot issue
    if (argc == 5)
    {
    	// this is from RC init before watchdog()
    	// we need init GPIO
		ra_gpio_init();
	    RstBtnFormatNvram();
	    return;
    }
	
	if(nvram_match("wan_proto_t", "PPPoE") || 
		nvram_match("wan_proto_t", "PPPoA") ||
		nvram_match("wan_proto_t", "PPTP") ||
		nvram_match("wan_proto_t", "L2TP") ||
		nvram_match("wan_proto_t", "3G") )
	{
		m_ChkPpp = 1;
	}

	if(nvram_match("wan_proto_t", "bridge"))
	{
		m_Bridge = 1;
	}
    
    
    switch_init();    
    InternetLedOff();

	// if 3g mode , we need not to care about PVC
	if (nvram_match("hsdpa_enable", "0"))
	{
		// 3g disabled
		if (nvram_match("use_eth_wan", "0") && nvram_match("use_failover_eth_wan", "0"))
		{

			{
				// if ADSL mode and no PVC created
				dsl_num = atoi(nvram_safe_get("dsl_config_num"));    
				if (dsl_num == 0)
				{
					goto quit_internet_led;
				}
			}		
		}
		
		if (nvram_invmatch("use_eth_wan", "0") && nvram_invmatch("use_failover_eth_wan", "0"))
		{
			// if ethernet-failover mode and no PVC created
			// not feasible, quit internet led
			dsl_num = atoi(nvram_safe_get("dsl_config_num"));    
			if (dsl_num == 0)
			{
				goto quit_internet_led;
			}	
		}
		
		if (nvram_invmatch("use_eth_wan", "0") && nvram_invmatch("use_failover_eth_wan", "0"))
		{
			// if ethernet-failover mode
			// ethernet WAN interface's VLAN is different with ADSL port
			// we need to tell driver to watch different VLAN ID
			if (nvram_match("eth_wan_failover_status", "done"))
			{
				switch_ethernet_wan_mode();	
			}
			else
			{
				switch_dsl_wan_mode();			
			}
		}			
	}
	else
	{
		// 3g enabled, ethernet wan always disabled
		if (nvram_invmatch("failover_3g_enable", "0"))
		{
			// if 3g-failover mode and no PVC created		
			// not feasible, quit internet led
			dsl_num = atoi(nvram_safe_get("dsl_config_num"));    
			if (dsl_num == 0)
			{
				goto quit_internet_led;
			}			
		}
	}
	


/*	
	if (nvram_invmatch("hsdpa_enable", "0") && nvram_invmatch("3g", "0"))
	{
		// if 3g-failover mode and no PVC created
		dsl_num = atoi(nvram_safe_get("dsl_config_num"));    
		if (dsl_num == 0)
		{
			goto quit_internet_led;
		}	
	}	
*/	

/*	
	if (nvram_invmatch("hsdpa_enable", "0") && nvram_invmatch("3g", "0"))
	{
		// if 3g-failover mode
		// ethernet WAN interface's VLAN is different with ADSL port
		// we need to tell driver to watch different VLAN ID
		if (nvram_match("eth_wan_failover_status", "done"))
		{
			switch_ethernet_wan_mode();	
		}
		else
		{
			switch_dsl_wan_mode();			
		}
	}
*/	

	

    if (signal(SIGTERM, mysigterm) == SIG_ERR)
    {
		printf("Cannot handle SIGTERM!\n");
	}

	

    //SetPollingTimerLed();        
    unsigned int prev_cnt = 0xffffffff;
    unsigned int cnt;    
	time_t start_time = 0;
	time_t curr_time; 
	int dif;
	int auto_fw_set = 0;

    
	    while (m_Exit == 0)
	    {
	        int ret;

	        // if boot after 30 seconds, add autofw iptables
	        // autofw rules MUST add after 30 seconds
	        // if not, the autofw will not write to iptables
	        
	        //
	        // if onnect the telphone line after 30 seconds
	        // the patch will be failure
			// when autofw.txt generated, we apply the patch
	        //
	        
	        //
	        // this is a dirty fix
	        //
			if (auto_fw_set == 0)
			{
				FILE* fp;
				fp = fopen("/tmp/autofw.txt","rb");
			
		        if (fp != NULL)
				{
					if (start_time == 0)
					{
					  start_time = time (NULL);
				  }
					curr_time = time (NULL);
					dif = curr_time - start_time;
					
                    if (dif > 30)
                    {
												char BufAutofw[256];
												auto_fw_set = 1;
												printf("start iptables autofw...");
                    	
                        while(1)
                        {
                            char* ret;
                            ret = fgets(BufAutofw,sizeof(BufAutofw),fp);
                            // eof or something wrong
                            if (ret == NULL) break;
                            system(BufAutofw);
                        }
                    }					
					fclose(fp);                           					
				}
			}

/*			
			m_ChkLinkStsAndHandlePppCnt++;
			if ((m_ChkLinkStsAndHandlePppCnt) % 200 == 0)
			{
				// 2 seconds
				chk_linksts_and_handle_ppp();
			}
*/			
			
			//
			SleepMs(10);
			
	        ret = switch_rcv_internet_pkt_cnt(&cnt);    
	        //printf("%d..\n",ret);
	        if (ret == 0)
	        {
			
	            //printf("%d %d\n",prev_cnt,cnt);
	            if (cnt == prev_cnt)
	            {
	                if (GetInternetSts())
	                {
	                    SetInternetLedOn();            
	                }
	                else
	                {
	                    SetInternetLedOff();            
	                }
	                SleepMs(50);
	            }
	            else
	            {
					prev_cnt = cnt;
	            	if (cnt > 0)
	            	{
						if (GetInternetSts())
						{
		                    SetInternetLedOff();
			                SleepMs(50);
		    	            SetInternetLedOn();
		        	        SleepMs(50);
	                    }
						m_PktRecv = 1;	                    
	                }
	            }
	        }
	        else
	        {
	            break;
	        }

	    }

quit_internet_led:	  
    
    switch_fini();

	printf("internet_led exit\n");    

	return 0;
    
}

