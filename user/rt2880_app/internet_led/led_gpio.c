#ifndef WIN32

#include <stdio.h>
#include <signal.h>
#include <time.h>
#include <sys/time.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <shutils.h>
#include <../../../user/rc/ra3052.h>
#include <stdarg.h>

typedef unsigned char   bool;   // 1204 ham

#include <fcntl.h>
#include <sys/ioctl.h>
//from linux-2.6.21.x/drivers/char/ralink_gpio.h

/*
 * ioctl commands
 */
#define RALINK_GPIO_SET_DIR             0x01
#define RALINK_GPIO_SET_DIR_IN          0x11
#define RALINK_GPIO_SET_DIR_OUT         0x12
#define RALINK_GPIO_READ                0x02
#define RALINK_GPIO_WRITE               0x03
#define RALINK_GPIO_SET                 0x21
#define RALINK_GPIO_CLEAR               0x31
#define RALINK_GPIO_READ_BIT            0x04
#define RALINK_GPIO_WRITE_BIT           0x05
#define RALINK_GPIO_READ_BYTE           0x06
#define RALINK_GPIO_WRITE_BYTE          0x07
#define RALINK_GPIO_READ_INT            0x02 //same as read
#define RALINK_GPIO_WRITE_INT           0x03 //same as write
#define RALINK_GPIO_SET_INT             0x21 //same as set
#define RALINK_GPIO_CLEAR_INT           0x31 //same as clear
#define RALINK_GPIO_ENABLE_INTP         0x08
#define RALINK_GPIO_DISABLE_INTP        0x09
#define RALINK_GPIO_REG_IRQ             0x0A
#define RALINK_GPIO_LED_SET             0x41
  
#define RALINK_GPIO_NUMBER              24
#define RALINK_GPIO_DATA_MASK           0x00FFFFFF
#define RALINK_GPIO_DATA_LEN            24
#define RALINK_GPIO_DIR_IN              0
#define RALINK_GPIO_DIR_OUT             1
#define RALINK_GPIO_DIR_ALLIN           0
#define RALINK_GPIO_DIR_ALLOUT          0x00FFFFFF


#define RALINK_GPIO_0                   0x00000001
#define RALINK_GPIO_1                   0x00000002
#define RALINK_GPIO_2                   0x00000004
#define RALINK_GPIO_3                   0x00000008
#define RALINK_GPIO_4                   0x00000010
#define RALINK_GPIO_5                   0x00000020
#define RALINK_GPIO_6                   0x00000040
#define RALINK_GPIO_7                   0x00000080
#define RALINK_GPIO_8                   0x00000100
#define RALINK_GPIO_9                   0x00000200
#define RALINK_GPIO_10                  0x00000400
#define RALINK_GPIO_11                  0x00000800
#define RALINK_GPIO_12                  0x00001000
#define RALINK_GPIO_13                  0x00002000
#define RALINK_GPIO_14                  0x00004000
#define RALINK_GPIO_15                  0x00008000
#define RALINK_GPIO_16                  0x00010000
#define RALINK_GPIO_17                  0x00020000
#define RALINK_GPIO_18                  0x00040000
#define RALINK_GPIO_19                  0x00080000
#define RALINK_GPIO_20                  0x00100000
#define RALINK_GPIO_21                  0x00200000
#define RALINK_GPIO_22                  0x00400000
#define RALINK_GPIO_23                  0x00800000


#define GPIO_DEV        "/dev/gpio"

int ra_gpio_write_bit(int idx, int value)
{
    int fd, req;

    fd = open(GPIO_DEV, O_RDONLY);
    if (fd < 0) {
        perror(GPIO_DEV);
        return -1;
    }
    value &= 1;
    if (0L <= idx && idx < RALINK_GPIO_DATA_LEN)
        req = RALINK_GPIO_WRITE_BIT | (idx << RALINK_GPIO_DATA_LEN);
    else {
        close(fd);
        printf("gpio_write_bit: index %d out of range\n", idx);
        return -1;
    }
    if (ioctl(fd, req, value) < 0) {
        perror("ioctl");
        close(fd);
        return -1;
    }
    close(fd);

    return 0;
}


int
ra_gpio_read(int idx)
{
        int fd, req, value;

        value = 0;
        fd = open(GPIO_DEV, O_RDONLY);
        
        if (fd < 0) {
                perror(GPIO_DEV);
                return -1;
        }
        
        if (0L <= idx && idx < RALINK_GPIO_DATA_LEN)
                req = RALINK_GPIO_READ_BIT | (idx << RALINK_GPIO_DATA_LEN);
        else {
                close(fd);
                printf("gpio_read_bit: index %d out of range\n", idx);
                return -1;
        }

        if (ioctl(fd, req, &value) < 0) {
                perror("ioctl");
                close(fd);
                return -1;
        }
        
        close(fd);

        return value;
}



void ra_gpio_write_spec(bit_idx, flag)
{
    ra_gpio_write_bit(bit_idx, flag);
}

/* dir:  base:0xB0000600, offset: 0x24*/
/* data: base:0xB0000600, offset: 0x20*/
int ra_gpio_set_dir(int dir)
{
	int fd;

	printf("[ra setdir]: %x\n", dir);	// tmp test
	fd = open(GPIO_DEV, O_RDONLY);
	if (fd < 0) {
		perror(GPIO_DEV);
		return -1;
	}
	if (ioctl(fd, RALINK_GPIO_SET_DIR, dir) < 0) {
		perror("ioctl");
		close(fd);
		return -1;
	}
	close(fd);

	return 0;
}


void ra_gpio_init()
{
	ra_gpio_set_dir(0x880); 	   // bit 11 as power led + internet
}


int GetRstBtnSts()
{
	// BTN_RESET = 10
	return ra_gpio_read(10);
}



void InternetLedOn(void)
{
	// it use ra_gpio_write_spec
    GPIO_CONTROL(7, LED_ON);
}

void InternetLedOff(void)
{
	// it use ra_gpio_write_spec
    GPIO_CONTROL(7, LED_OFF);
}

void PowerLedOn(void)
{
	// it use ra_gpio_write_spec
    GPIO_CONTROL(11, LED_ON);
}

void PowerLedOff(void)
{
	// it use ra_gpio_write_spec
    GPIO_CONTROL(11, LED_OFF);
}




#endif
