#include <stdio.h>
#include <string.h>
#include <sys/time.h>
#include <signal.h>


extern void chk_internet();

void UpdateInternetConnectivitySts()
{
    char fn_buf[80];
	char OneLine[80];    
    FILE* fp;
    int i;
    int Connectivity = 0;
    for (i=0; i<3; i++)
    {
        sprintf(fn_buf, "/tmp/chk_int%d.txt", i);
        remove(fn_buf);
    }

    chk_internet();
    
    for (i=0; i<3; i++)
    {
        sprintf(fn_buf, "/tmp/chk_int%d.txt", i);
	    fp = fopen(fn_buf,"r");
	    if (fp != NULL) 
	    {
	    	char* ret;
		    ret = fgets(OneLine,sizeof(OneLine),fp);			
		    if (ret != NULL)
		    {
				ret = fgets(OneLine,sizeof(OneLine),fp);			
				if (ret != NULL)
				{
				    if (strncmp(OneLine, "64 bytes from", 13) == 0)
				    {
					    Connectivity = 1;
					    break;
				    }
			    }
		    }
		    fclose(fp);				
	    }
    }
    
    fp = fopen("/tmp/adsl/internet_conn.txt","wb");
    if (fp == NULL) return;
    if (Connectivity)
    {
        fputs("up",fp);        
    }
    else
    {
        fputs("down",fp);        
    }
    fclose(fp);        
}

void polling_internet_sts(int signo) 
{ 
    UpdateInternetConnectivitySts();
} 


void init_sigaction(void) 
{ 
    struct sigaction act;
    act.sa_handler=polling_internet_sts; 
    act.sa_flags=0; 
    sigemptyset(&act.sa_mask); 
    sigaction(SIGALRM,&act,NULL); 
    //printf("process id is %d\n", getpid());
}


void init_time() 
{ 
    struct itimerval value; 
    value.it_value.tv_sec=5; 
    value.it_value.tv_usec=0; 
    value.it_interval=value.it_value; 
    setitimer(ITIMER_REAL,&value,NULL); 
}


void SetPollingTimer()
{
    init_sigaction(); 
    init_time(); 
}


int main(int argc, char* argv[])
{
	int WaitAdslCnt;
	int ExitCnt;

	for (WaitAdslCnt = 0; WaitAdslCnt<120; WaitAdslCnt++)
	{
		FILE* fp;
		fp = fopen("/tmp/adsl/tcbootup.txt","r");
		if (fp != NULL)
		{
			fclose(fp);
			break;
		}
		usleep(1000*1000*1);
	}

	SetPollingTimer();

	for (ExitCnt = 0; ExitCnt<120; ExitCnt++)
	{
		// 100 seconds
		usleep(1000*1000*100);
	}
}


