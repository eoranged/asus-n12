#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

void chk_internet()
{
    int outcopy;
    int outcopy2;    
    int fd; /*file descriptor to the file we will redirect ls's output*/    
    outcopy = dup(STDOUT_FILENO);
    outcopy2 = dup(STDERR_FILENO);

    if((fd = open("/tmp/chk_int0.txt", O_RDWR | O_CREAT))==-1){ /*open the file */
        perror("open");
        return 1;
    }
    dup2(fd,STDOUT_FILENO); /*copy the file descriptor fd into standard output*/
    dup2(fd,STDERR_FILENO); /* same, for the standard error */
    close(fd); /* close the file descriptor as we don't need it more  */
    remove("/tmp/cmdret2.txt");		        
    system("ping www.google.com -c 1;adslate makeretfile2;adslate waitcmdret2");
    
    if((fd = open("/tmp/chk_int1.txt", O_RDWR | O_CREAT))==-1){ /*open the file */
        perror("open");
        return 1;
    }
    dup2(fd,STDOUT_FILENO); /*copy the file descriptor fd into standard output*/
    dup2(fd,STDERR_FILENO); /* same, for the standard error */
    close(fd); /* close the file descriptor as we don't need it more  */
    remove("/tmp/cmdret2.txt");		        
    system("ping www.yahoo.com -c 1;adslate makeretfile2;adslate waitcmdret2");    
    
    if((fd = open("/tmp/chk_int2.txt", O_RDWR | O_CREAT))==-1){ /*open the file */
        perror("open");
        return 1;
    }
    dup2(fd,STDOUT_FILENO); /*copy the file descriptor fd into standard output*/
    dup2(fd,STDERR_FILENO); /* same, for the standard error */
    close(fd); /* close the file descriptor as we don't need it more  */
    remove("/tmp/cmdret2.txt");		        
    system("ping www.asus.com -c 1;adslate makeretfile2;adslate waitcmdret2");        
    
    
    close(STDOUT_FILENO);
    close(STDERR_FILENO);
    
    dup2(outcopy, STDOUT_FILENO);
    dup2(outcopy2, STDERR_FILENO);    
}                    



///////////////////////////////



#if 0

#include <nvram/typedefs.h>
#include <nvram/bcmnvram.h>
#include <nvparse.h>
#include <stdio.h>
#include <memory.h>
#include <string.h>

int file_to_buf(char *path, char *buf, int len){
	FILE *fp;
	memset(buf, 0 , len);
	
	if((fp = fopen(path, "r")) != NULL){
		fgets(buf, len, fp);
		fclose(fp);
		
		return 1;
	}
	
	return 0;
}

int get_ppp_pid(char *conntype){
	int pid = -1;
	char tmp[80], tmp1[80];
	
	snprintf(tmp, sizeof(tmp), "/var/run/%s.pid", conntype);
	file_to_buf(tmp, tmp1, sizeof(tmp1));
	pid = atoi(tmp1);
	
	return pid;
}

/* Find process name by pid from /proc directory */
char *find_name_by_proc(int pid){
	FILE *fp;
	char line[254];
	char filename[80];
	static char name[80];
	
	snprintf(filename, sizeof(filename), "/proc/%d/status", pid);
	
	if((fp = fopen(filename, "r")) != NULL){
		fgets(line, sizeof(line), fp);
		/* Buffer should contain a string like "Name:   binary_name" */
		sscanf(line, "%*s %s", name);
		fclose(fp);
		return name;
	}
	
	return "";
}
         

int ReadyForInternet()
{
	FILE *fp;
	char type[32], ip[32], netmask[32], gateway[32], dns[128], statusstr[32];
	int status = 0, unit, s;
	char tmp[100], prefix[] = "wanXXXXXXXXXX_";
	char filename[80], conntype[10];
	struct ifreq ifr;
	struct sockaddr_in *our_ip;
	struct in_addr in;
	char *pwanip = NULL, *ppp_addr, *usb_device;
	
	/* current unit */
	if((unit = atoi(nvram_safe_get("wan_unit"))) < 0)
		unit = 0;
	wan_prefix(unit, prefix);

	if(!is_phyconnected())
	{
        return 0;
	}
    
	if( nvram_match("wan_proto_t", "PPPoE")	|| nvram_match("wan_proto_t", "PPPoA"))
	{
		DIR *ppp_dir;	// 2008.01 James.
		struct dirent *entry;	// 2008.01 James.
		char *name;
		char* pos = NULL;
		char* cpos = NULL;
		
		memset(filename, 0, 80);
		
		ppp_dir = opendir("/tmp/ppp");
		if(ppp_dir == NULL) {
            return 0;
		}
		
		while((entry = readdir(ppp_dir)) != NULL){
			if(!strcmp(entry->d_name, ".") || !strcmp(entry->d_name, ".."))
				continue;
			
			if((pos = strstr(entry->d_name, "link")) != NULL){
				sprintf(filename, "/tmp/ppp/%s", entry->d_name);
				strcpy(conntype, pos+5);
				break;
			}
		}
		closedir(ppp_dir);
		
    		if(strlen(filename) > 0 && (fp = fopen(filename, "r")) != NULL){
            		int pid = -1;
            		fclose(fp);
			
			if(nvram_match(strcat_r(prefix, "proto", tmp), "heartbeat")){
				char buf[20];
				
				file_to_buf(filename, buf, sizeof(buf));
				pid = atoi(buf);
			}
			else
				pid = get_ppp_pid(conntype);
			                                                                         
			name = find_name_by_proc(pid);  
			
			if(!strncmp(name, "pppoecd", 7)){
				if(!strcmp(nvram_safe_get("manually_disconnect_wan"), "1"))
				{
                    return 0;
				}
				else
				{
                    return 1;				
				}
			}
			else{
				// For some reason, the pppoed had been died, by link file still exist.
				unlink(filename);
                return 0;			
			}
		}
		else{
            return 0;			
		}
	}
	else { 
		/* Open socket to kernel */
		if((s = socket(AF_INET, SOCK_DGRAM, 0)) < 0){
            return 0;			
		}
		else{
			/* Check for valid IP address */
			strncpy(ifr.ifr_name, nvram_safe_get(strcat_r(prefix, "ifname", tmp)), IFNAMSIZ);
			
			if(!ioctl(s, SIOCGIFADDR, &ifr)){
				our_ip = (struct sockaddr_in *) &ifr.ifr_addr;
				in.s_addr = our_ip->sin_addr.s_addr;
				pwanip = inet_ntoa(in);
			    close(s);				
				
				if(!strcmp(pwanip, "") || pwanip == NULL){
                    return 0;			
				}
				else if(!strcmp(nvram_safe_get("manually_disconnect_wan"), "1")){
                    return 0;			
				}
				else{
                    return 1;			
				}
			}
			else{
                return 0;			
			}
			
		}
	}
}

#endif

