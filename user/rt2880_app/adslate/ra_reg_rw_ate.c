
#ifndef WIN32

#include <linux/if_ether.h>
#include <linux/if_packet.h>
#include <linux/socket.h>
#include <linux/net.h>
#include <linux/if.h>
#include <linux/ioctl.h>
#include "ra_ioctl.h"
#include "ra_reg_rw_ate.h"

// do not know why socket.h has no this enum define
enum sock_type
{
    SOCK_STREAM     = 1,
    SOCK_DGRAM      = 2,
    SOCK_RAW        = 3,
    SOCK_RDM        = 4,
    SOCK_SEQPACKET  = 5,
    SOCK_DCCP       = 6,
    SOCK_PACKET     = 10,
};

static int esw_fd = 0;
/*
int ra3052_reg_writ(eint offset, int value)
{
    struct ifreq ifr;
    esw_reg reg;

    reg.off = offset;
    reg.val = value;
    strncpy(ifr.ifr_name, "eth2", 5);
    ifr.ifr_data = &reg;
    if (-1 == ioctl(esw_fd, RAETH_ESW_REG_WRITE, &ifr))
    {
        perror("ioctl");
        close(esw_fd);
    }
    return 0;
}
*/



int switch_bridge_mode()
{
    char ioctl_read_buf[1];
    struct ifreq ifr;
    strncpy(ifr.ifr_name, "eth2", 5);
    ifr.ifr_data = ioctl_read_buf;
    if (-1 == ioctl(esw_fd, RAETH_START_BRIDGE_MODE, &ifr))
    {
        perror("ioctl");
    }
							
    return 0;    							
}							

int
switch_init(void)
{
    esw_fd = socket(AF_INET, SOCK_DGRAM, 0);
    if (esw_fd < 0)
    {
        perror("socket");
        return -1;
    }
    return 0;
}

void
switch_fini(void)
{
    if (esw_fd !=0)
    {
        close(esw_fd);
    }        
}


#endif
