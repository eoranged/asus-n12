#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <sys/time.h>
#include <signal.h>
#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <dirent.h>

#include <nvram/typedefs.h>
#include <nvram/bcmnvram.h>
#include <nvparse.h>

#define STARTUP_DELAY	10	//unit: sec
#define DOUBLE_CHECK_INTERVAL	20	//unit: 3sec
#define SWITCH_3G_DELAY		60	//unit: 3sec
#define SWITCH_DSL_DELAY	20	//unit: 3sec

static int quit = 0;

void beClosed()
{
	quit = 1;
	return;
}

int main(int argc, char* argv[])
{
	int dsl_num;
	int failover_wait_counter = 0;
	int dbl_chk = 2;

	//if failover is not set
	if(nvram_match("failover_3g_enable", "0")) {
		goto quit_failover_3g;
	}
	
	if(nvram_match("hsdpa_enable", "0")) {
		goto quit_failover_3g;
	}
	

	// if ADSL mode and no PVC created
	dsl_num = atoi(nvram_safe_get("dsl_config_num"));
	if (dsl_num == 0)
	{
		goto quit_failover_3g;
	}

	if (signal(SIGTERM, beClosed) == SIG_ERR) {
		perror("[failover_3g] Cannot handle SIGTERM!\n");
	}

	//if router shutdown while 3G failover running
	//should recover to dsl
	if (nvram_match("failover_3g_status", "done")) {
		nvram_set("wan_proto",nvram_safe_get("fo_backup_wan_proto"));
		nvram_set("wan_proto_t",nvram_safe_get("fo_backup_wan_proto_t"));
	}

	if (!nvram_match("failover_3g_status", "idle")) {
		nvram_set("failover_3g_status", "idle");
	}


    //startup delay
	sleep(STARTUP_DELAY);
	printf("Start 3G Failover...\n");

	while (quit == 0) {
		sleep(3);
		
		// if user press pause button, disable failover
		if (nvram_match("manually_disconnect_wan", "1"))
		{
			continue;
		}
		

		if (failover_wait_counter > 0)
		{
			failover_wait_counter--;
			continue;
		}

		if(nvram_match("failover_3g_status", "idle")) {
			FILE* fp;
			char buf[32];
			fp = fopen("/tmp/adsl/adsllinksts.log","rb");
			if (fp == NULL) continue;
			fgets(buf,sizeof(buf),fp);
			buf[2] = 0;
			fclose(fp);
			dbl_chk--;
			if (strcmp(buf,"up") != 0) {
				if(dbl_chk) {	//double check, avoid tmp disconnect
					printf("[failover_3g] dsl is down, 1st check\n");
					failover_wait_counter = DOUBLE_CHECK_INTERVAL;
					continue;
				}
				printf("[failover_3g] switched to 3g wan\n");
				system("switch_to_3gwan");
				nvram_set("failover_3g_status", "done");

				dbl_chk = 2;
				//big enough to wait 3g process end
				failover_wait_counter = SWITCH_3G_DELAY;
				continue;
			}
			else
				dbl_chk = 2;
		}
		else {	//failover_3g_status = done
			FILE* FpTest = fopen("/tmp/ppp/link.ppp0","r");
			if (FpTest != NULL) {
				fclose(FpTest);
			}
			else {
				printf("[failover_3g] switched to dsl wan\n");
				system("switch_to_dslwan");
				nvram_set("failover_3g_status", "idle");				

				failover_wait_counter = SWITCH_DSL_DELAY;
				continue;
			}
		}
	}

quit_failover_3g:
	printf("Stop 3G Failover\n");
	return 0;

}

