
#define TC_DSL_MAGIC_NUMBER "DSL"

// it MUST match the number that asus_tc.bin created
#define TC_DSL_FW_VER "004"
#define TC_DSL_FW_VER_FROM_MODEM "3.16.18.0"
#define TC_DSL_RAS_VER_FROM_MODEM "ASUS_ANNEXAIJLM_20120710"