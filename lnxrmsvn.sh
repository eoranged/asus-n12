#!/bin/bash
rm -f www_ver/*
rm -rf dict/*
cd user
rm -rf rc/.svn
rm -f rc/*.c
rm -f rc/*.h
rm -rf httpd/.svn
rm -f httpd/*.c 
rm -f httpd/*.h
rm -f httpd/Makefile
rm -rf shared/.svn
rm -f shared/defaults.c
rm -f shared/flash.default
rm -rf www/www_DSL-N10/*
rm -f rt2880_app/internet_led/*.c
rm -f rt2880_app/internet_led/*.h
rm -f rt2880_app/wanduck/*.c
rm -f rt2880_app/wanduck/*.h
echo REMOVE_SVN
cd ..


