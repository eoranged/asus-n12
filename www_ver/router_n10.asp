﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7"/>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta HTTP-EQUIV="Pragma" CONTENT="no-cache">
<meta HTTP-EQUIV="Expires" CONTENT="-1">
<link rel="shortcut icon" href="images/favicon.png">
<link rel="icon" href="images/favicon.png">

<title>ASUS <#Web_Title#> Wireless-N ADSL2/2+ Modem Router - <#menu1#></title>
<link href="/NM_style.css" rel="stylesheet" type="text/css" />
<link href="/form_style.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" src="/general.js"></script>
<script type="text/javascript" src="/state.js"></script>
<script type="text/javascript" src="formcontrol.js"></script>
<script type="text/javascript" src="/ajax.js"></script>
<script type="text/javascript" src="/mssid.js"></script>
<script>
var had_wrong_wep_key = false;

function initial(){
	flash_button();
	loadXML();

	if(document.form.mbss1_ssid2.value == "ASUS_MSSID_1")
		free_mssid = 1;
	else if(document.form.mbss2_ssid2.value == "ASUS_MSSID_2")
		free_mssid = 2;
	else if(document.form.mbss3_ssid2.value == "ASUS_MSSID_3")
		free_mssid = 3;
	else
		free_mssid = 0;

	if(free_mssid){
		$("Add-SSID-btn").style.display = "";
		if(free_mssid == 1)
		{
			$("mssid_arrow").style.display = "none";
			$("ssid_help").style.display = "none";
		}
		else
		{
			$("mssid_arrow").style.display = "";
			$("ssid_help").style.display = "";
		}
	}
	else
	{
		$("Add-SSID-btn").style.display = "none";
		$("mssid_arrow").style.display = "";
		$("ssid_help").style.display = "";
	}

	document.form.wl_ssid.value = decodeURIComponent(document.form.wl_ssid2.value);
	document.form.wl_wpa_psk.value = decodeURIComponent(document.form.wl_wpa_psk_org.value);
	document.form.wl_key1.value = decodeURIComponent(document.form.wl_key1_org.value);
	document.form.wl_key2.value = decodeURIComponent(document.form.wl_key2_org.value);
	document.form.wl_key3.value = decodeURIComponent(document.form.wl_key3_org.value);
	document.form.wl_key4.value = decodeURIComponent(document.form.wl_key4_org.value);

	if(document.form.wl_wpa_psk.value.length <= 0)
		document.form.wl_wpa_psk.value = "Please type Password";

	mssidlist[0][2] = decodeURIComponent(document.form.wl_ssid2.value);
	mssidlist[1][2] = decodeURIComponent(document.form.mbss1_ssid2.value);
	mssidlist[2][2] = decodeURIComponent(document.form.mbss2_ssid2.value);
	mssidlist[3][2] = decodeURIComponent(document.form.mbss3_ssid2.value);

	wl_auth_mode_change2(1);
	show_LAN_info();
}

function wl_auth_mode_change2(isload){
	var mode = document.form.wl_auth_mode.value;
	var opts = document.form.wl_auth_mode.options;
	var new_array;
	var cur_crypto;
	var cur_key_index, cur_key_obj;

	//if(mode == "open" || mode == "shared" || mode == "radius"){
	if(mode == "open" || mode == "shared"){
		blocking("all_related_wep", 1);
		change_wep_type2(mode);
	}
	else{
		blocking("all_related_wep", 0);
	}

	/* enable/disable crypto algorithm */
	if(mode == "wpa" || mode == "wpa2" || mode == "psk")
		blocking("wl_crypto", 1);
	else
		blocking("wl_crypto", 0);

	/* enable/disable psk passphrase */
	if(mode == "psk")
		blocking("wl_wpa_psk", 1);
	else
		blocking("wl_wpa_psk", 0);

	/* update wl_crypto */
	for(var i = 0; i < document.form.wl_crypto.length; ++i)
		if(document.form.wl_crypto[i].selected){
			cur_crypto = document.form.wl_crypto[i].value;
			break;
		}

	/* Reconstruct algorithm array from new crypto algorithms */
	if(mode == "psk"){
		/* Save current crypto algorithm */
		if(isModel() == "SnapAP" || isBand() == 'b')
			new_array = new Array("TKIP");
		else{
			if(opts[opts.selectedIndex].text == "WPA-Personal")
				new_array = new Array("TKIP");
			else if(opts[opts.selectedIndex].text == "WPA2-Personal")
				new_array = new Array("AES");
			else
				new_array = new Array("TKIP", "AES", "TKIP+AES");
		}

		free_options(document.form.wl_crypto);
		for(var i in new_array){
			document.form.wl_crypto[i] = new Option(new_array[i], new_array[i].toLowerCase());
			document.form.wl_crypto[i].value = new_array[i].toLowerCase();
			if(new_array[i].toLowerCase() == cur_crypto)
				document.form.wl_crypto[i].selected = true;
		}
	}
	else if(mode == "wpa"){
		if(opts[opts.selectedIndex].text == "WPA-Enterprise")
			new_array = new Array("TKIP");
		else
			new_array = new Array("TKIP", "AES", "TKIP+AES");

		free_options(document.form.wl_crypto);
		for(var i in new_array){
			document.form.wl_crypto[i] = new Option(new_array[i], new_array[i].toLowerCase());
			document.form.wl_crypto[i].value = new_array[i].toLowerCase();
			if(new_array[i].toLowerCase() == cur_crypto)
				document.form.wl_crypto[i].selected = true;
		}
	}
	else if(mode == "wpa2"){
		new_array = new Array("AES");

		free_options(document.form.wl_crypto);
		for(var i in new_array){
			document.form.wl_crypto[i] = new Option(new_array[i], new_array[i].toLowerCase());
			document.form.wl_crypto[i].value = new_array[i].toLowerCase();
			if(new_array[i].toLowerCase() == cur_crypto)
				document.form.wl_crypto[i].selected = true;
		}
	}

	/* Save current network key index */
	for(var i = 0; i < document.form.wl_key.length; ++i)
		if(document.form.wl_key[i].selected){
			cur_key_index = document.form.wl_key[i].value;
			break;
		}

	/* Define new network key indices */
	//if(mode == "psk" || mode == "wpa" || mode == "wpa2" || mode == "radius")
	if(mode == "psk" || mode == "wpa" || mode == "wpa2")
		new_array = new Array("2", "3");
	else{
		new_array = new Array("1", "2", "3", "4");

		if(isload == 0 || isload == 2)
			cur_key_index = "1";
	}

	/* Reconstruct network key indices array from new network key indices */
	free_options(document.form.wl_key);
	for(var i in new_array){
		document.form.wl_key[i] = new Option(new_array[i], new_array[i]);
		document.form.wl_key[i].value = new_array[i];
		if(new_array[i] == cur_key_index)
			document.form.wl_key[i].selected = true;
	}

	wl_wep_change2();

	if(isload == 2)
	{
		if(document.form.wl_gmode.value == "3"){
			if(document.form.wl_auth_mode.selectedIndex == 4){
				if(document.form.wl_crypto.selectedIndex == 0 || document.form.wl_crypto.selectedIndex == 2)
				{
					alert("<#WLANConfig11n_nmode_limition_hint#>");
					document.form.wl_auth_mode.selectedIndex = 3;
					document.form.wl_wpa_mode.value = 2;
					wl_auth_mode_change2(1);
				}
			}
		}
	}
}

function change_wep_type2(mode){

	var cur_wep = document.form.wl_wep_x.value;
	var wep_type_array;
	var value_array;

	free_options(document.form.wl_wep_x);

	//if(mode == "shared" || mode == "radius"){
	if(mode == "shared"){
		wep_type_array = new Array("WEP-64bits", "WEP-128bits");
		value_array = new Array("1", "2");
	}
	else{
		wep_type_array = new Array("None", "WEP-64bits", "WEP-128bits");
		value_array = new Array("0", "1", "2");
	}

	add_options_x2(document.form.wl_wep_x, wep_type_array, value_array, cur_wep);

	if(mode == "open"){
		document.form.wl_wep_x.selectedIndex = mssidlist[0][5];
	}

	if(mode == "psk" || mode == "wpa" || mode == "wpa2")
	//if(mode == "psk" || mode == "wpa" || mode == "wpa2" || mode == "radius")
		document.form.wl_wep_x.value = "0";

	change_wlweptype2(document.form.wl_wep_x);
}

function change_wlweptype2(wep_type_obj){
	var mode = document.form.wl_auth_mode.value;

	//if(wep_type_obj.value == "0" || mode == "radius")
	if(wep_type_obj.value == "0")
		blocking("all_wep_key", 0);
	else{
		if(document.form.wl_gmode.value == 3 && document.form.wl_wep_x.value != 0){
			nmode_limitation2();
		}
		blocking("all_wep_key", 1);
	}

	wl_wep_change2();
}

function wl_wep_change2(){
	var mode = document.form.wl_auth_mode.value;
	var wep = document.form.wl_wep_x.value;

	if(mode == "psk" || mode == "wpa" || mode == "wpa2"){
		if(mode == "psk"){
			blocking("wl_crypto", 1);
			blocking("wl_wpa_psk", 1);
		}
		blocking("all_related_wep", 0);
	}
	else{
		blocking("wl_crypto", 0);
		blocking("wl_wpa_psk", 0);
		//if(mode == "radius")
		//	blocking("all_related_wep", 0);
		//else
		//	blocking("all_related_wep", 1);

		if(wep == "0" || mode == "radius")
			blocking("all_wep_key", 0);
		else{
			blocking("all_wep_key", 1);
			show_key2();
		}
		}
	change_key_des2();
	}

function change_key_des2(){
	var objs = getElementsByName_iefix("span", "key_des");
	var wep_type = document.form.wl_wep_x.value;
	var str = "";

	if(wep_type == "1")
		str = " (<#WLANConfig11b_WEPKey_itemtype1#>)";
	else if(wep_type == "2")
		str = " (<#WLANConfig11b_WEPKey_itemtype2#>)";

	str += ":";

	for(var i = 0; i < objs.length; ++i)
		showtext(objs[i], str);
}

function change_auth_mode2(auth_mode_obj){
	wl_auth_mode_change2(0);
	if(auth_mode_obj.value == "psk" || auth_mode_obj.value == "wpa"){
		var opts = document.form.wl_auth_mode.options;

		if(opts[opts.selectedIndex].text == "WPA-Personal")
			document.form.wl_wpa_mode.value = "1";
		else if(opts[opts.selectedIndex].text == "WPA2-Personal")
			document.form.wl_wpa_mode.value="2";
		else if(opts[opts.selectedIndex].text == "WPA-Auto-Personal")
			document.form.wl_wpa_mode.value="0";
		else if(opts[opts.selectedIndex].text == "WPA-Enterprise")
			document.form.wl_wpa_mode.value="3";
		else if(opts[opts.selectedIndex].text == "WPA-Auto-Enterprise")
			document.form.wl_wpa_mode.value = "4";

		//if(auth_mode_obj.value == "psk"){
		//	document.form.wl_wpa_psk.focus();
		//	document.form.wl_wpa_psk.select();
		//}
	}
	else if(auth_mode_obj.value == "shared")
		show_key2();

	nmode_limitation2();
}

function show_key2(){
	var wep_type = document.form.wl_wep_x.value;
	var matrix_key = parseInt(document.form.wl_key.value)+6;
	var cur_key_obj = mssidlist[ssid_order][matrix_key];
	var cur_key_length = cur_key_obj.length;

	if(wep_type == 1){
		if(cur_key_length == 5 || cur_key_length == 10)
			document.form.wl_asuskey1.value = mssidlist[ssid_order][matrix_key];
		else
			document.form.wl_asuskey1.value = "0000000000";
	}
	else if(wep_type == 2){
		if(cur_key_length == 13 || cur_key_length == 26)
			document.form.wl_asuskey1.value = mssidlist[ssid_order][matrix_key];
		else
			document.form.wl_asuskey1.value = "00000000000000000000000000";
	}
	else
		document.form.wl_asuskey1.value = "";

	document.form.wl_asuskey1.focus();
	document.form.wl_asuskey1.select();
}

function show_LAN_info(){
	var lan_ipaddr_t = '<% nvram_get_x("LANHostConfig", "lan_ipaddr_t"); %>';
	if(lan_ipaddr_t != '')
		showtext($("LANIP"), '<% nvram_get_x("LANHostConfig", "lan_ipaddr_t"); %>');
	else
		showtext($("LANIP"), '<% nvram_get_x("LANHostConfig", "lan_ipaddr"); %>');
	showtext($("PINCode"), '<% nvram_get_x("", "secret_code"); %>');
	showtext($("MAC"), '<% nvram_get_x("", "il0macaddr"); %>');
}

function show_wepkey_help(){
	if(document.form.wl_wep_x.value == 1)
		parent.showHelpofDrSurf(0, 12);
	else if(document.form.wl_wep_x.value == 2)
		parent.showHelpofDrSurf(0, 13);
}

var secs;
var timerID = null;
var timerRunning = false;
var timeout = 1000;
var delay = 500;
var stopFlag=0;

function resetTimer()
{
	if (stopFlag==1)
	{
		stopFlag=0;
		InitializeTimer();
	}
}

function InitializeTimer()
{
	if(document.form.wl_auth_mode.value == "shared"
		|| document.form.wl_auth_mode.value == "wpa"
		|| document.form.wl_auth_mode.value == "wpa2"
		|| document.form.wl_auth_mode.value == "radius")
		return;

	msecs = timeout;
	StopTheClock();
	StartTheTimer();
}

function StopTheClock()
{
	if(timerRunning)
		clearTimeout(timerID);
	timerRunning = false;
}

function StartTheTimer(){
	if(msecs == 0){
		StopTheClock();

		if(stopFlag == 1)
			return;

		updateWPS();
		msecs = timeout;
		StartTheTimer();
	}
	else{
		msecs = msecs-500;
		timerRunning = true;
		timerID = setTimeout("StartTheTimer();", delay);
	}
}

function updateWPS()
{
	var ie = window.ActiveXObject;

	if (ie)
		makeRequest_ie('/WPS_info.asp');
	else
		makeRequest('/WPS_info.asp');
}

function loadXML()
{
	updateWPS();
	InitializeTimer();
}

function refresh_wpsinfo(xmldoc){
	var wpss = xmldoc.getElementsByTagName("wps");

	if(wpss==null || wpss[0]==null){
		stopFlag=1;
		return;
	}

	var wps_infos = wpss[0].getElementsByTagName("wps_info");

	if(wps_infos[0].firstChild.nodeValue == "Start WPS Process"){
		$("WPS_PBCbutton_hint_span").innerHTML = "<#WPS_PBCbutton_hint_waiting#>";
		if($("wpsPBC_button").src != "/images/EZSetup_button_s.gif")
			$("wpsPBC_button").src = "/images/EZSetup_button_s.gif";

		PBC_shining();
	}
	else{
		$("WPS_PBCbutton_hint_span").innerHTML = "<#WPS_PBCbutton_hint_timeout#>";
		PBC_normal();
	}
}

function PBC_shining(){
	$("wpsPBC_button").src = "/images/EZSetup_button_s.gif";
	$("wpsPBC_button").onmousedown = function(){};
}

function PBC_normal(){
	$("wpsPBC_button").src = "/images/EZSetup_button.gif";
	$("wpsPBC_button").onmousedown = function(){
			$("wpsPBC_button").src = "/images/EZSetup_button_0.gif";
		};
}

function startPBCmethod(){
	stopFlag = 1;

	document.WPSForm.action_script.value = "WPS_push_button";
	document.WPSForm.current_page.value = "/";
	document.WPSForm.submit();
}

function wpsPBC(obj){
	obj.src = "/images/EZSetup_button_0.gif";

	startPBCmethod();
}

function nmode_limitation2(){
	if(document.form.wl_gmode.value == "3"){
		if(document.form.wl_auth_mode.selectedIndex == 0 && (document.form.wl_wep_x.selectedIndex == "1" || document.form.wl_wep_x.selectedIndex == "2")){
			alert("<#WLANConfig11n_nmode_limition_hint#>");
			document.form.wl_auth_mode.selectedIndex = 3;
			document.form.wl_wpa_mode.value = 2;
		}
		else if(document.form.wl_auth_mode.selectedIndex == 1){
			alert("<#WLANConfig11n_nmode_limition_hint#>");
			document.form.wl_auth_mode.selectedIndex = 3;
			document.form.wl_wpa_mode.value = 2;
		}
		else if(document.form.wl_auth_mode.selectedIndex == 2){
			alert("<#WLANConfig11n_nmode_limition_hint#>");
			document.form.wl_auth_mode.selectedIndex = 3;
			document.form.wl_wpa_mode.value = 2;
		}
		else if(document.form.wl_auth_mode.selectedIndex == 5){
			alert("<#WLANConfig11n_nmode_limition_hint#>");
			document.form.wl_auth_mode.selectedIndex = 6;
		}

		wl_auth_mode_change2(1);
	}
	//document.form.wl_wpa_psk.focus();
	//document.form.wl_wpa_psk.select();
}

document.onclick = function(event)
{
	var obj;
	if(window.event)
	{
	  obj = window.event.srcElement;
	}
	else
	{
	  obj = event.target;
	}
	if(obj.id == "mssid_arrow" || obj.id == "ssid_check0" || obj.id == "ssid_check1" || obj.id == "ssid_check2" || obj.id == "ssid_check3")
		return;
	var topElement = "HTML";
	while(obj.tagName!=topElement)
	{
		obj = obj.parentNode;
		if(obj.id == "mssid_arrow"){
			return;
		}
	}
	if(obj.id != "MSSIDList_Block")
	{
		hideMSSID_Block();
	}
}

function verify_ssidkey(){
	var tmpKeyStr = document.form.wl_asuskey1.value;
	for(i=0; i<tmpKeyStr.length; i++)
	{
		if(tmpKeyStr.charAt(i) == "/" || tmpKeyStr.charAt(i) == "&" || tmpKeyStr.charAt(i) == "\"" || tmpKeyStr.charAt(i) == "<")
		{
			alert("<#WEPKey#> can't contain / or & or \" or <");
			document.form.wl_asuskey1.focus();
			return false;
		}
	}

	newString = document.form.wl_asuskey1.value.replace(/[/\\/]/g,"  #");
	var re = new RegExp('  #');
	if(re.test(newString)){
		alert("<#WEPKey#> can't contain '\\'");
		document.form.wl_asuskey1.focus()
		return false;
	}

	var tmpWpaKeyStr = document.form.wl_wpa_psk.value;
	for(i=0; i<tmpWpaKeyStr.length; i++)
	{
		if(tmpWpaKeyStr.charAt(i) == "/" || tmpWpaKeyStr.charAt(i) == "&" || tmpWpaKeyStr.charAt(i) == "\"" || tmpWpaKeyStr.charAt(i) == "<")
		{
			alert("<#WPA-PSKKey#> can't contain / or & or \" or <");
			document.form.wl_wpa_psk.focus();
			return false;
		}
	}

	newString = document.form.wl_wpa_psk.value.replace(/[/\\/]/g,"  #");
	var re = new RegExp('  #');
	if(re.test(newString)){
		alert("<#WPA-PSKKey#> can't contain '\\'");
		document.form.wl_wpa_psk.focus()
		return false;
	}

	return true;
}

function applyForm(){
	if(verify_ssidkey())
	{
		var mbss = new Array(3);
		var mbss_idx = ["ssid", "enabled", "auth_mode", "wep_x", "key", "key1", "key2", "key3", "key4", "crypto", "wpa_psk", "wpa_mode", "nolan", "nowan", "priority", "closed", "time"];

		var auth_mode = document.form.wl_auth_mode.value;
		var wl_wep_x = document.form.wl_wep_x.value;

		if(!validate_string_ssid(document.form.wl_ssid))
			return false;

		if(!mssid_duplicate(document.form.wl_ssid))
			return false;

		if(auth_mode == "psk" || auth_mode == "wpa" || auth_mode == "wpa2"){
			if(!validate_psk(document.form.wl_wpa_psk))
				return false;
		}
		else{
			if(ssid_order != 0){
				var key_type = "mbss"+ssid_order+"_key_type";
				if(auth_mode != "radius" && !validate_wlkey_mbss(document.form.wl_asuskey1, key_type))
					return false;
			}
			else{
				if(auth_mode != "radius" && !validate_wlkey(document.form.wl_asuskey1))
					return false;
			}
		}

		if(ssid_order != 0){

			save_temp_ssid_setting(ssid_order);
			var wl_info = "";
			for(var i=0; i<mssidlist[0].length; i++){
				wl_info += "\n wl_" + mbss_idx[i];
				wl_info += ": " + mssidlist[0][i+2];
			}
			document.form.wl_ssid.value = mssidlist[0][2];

			document.form.wl_auth_mode.name = "x_auth_mode";
			document.form.wl_wep_x.name = "x_wep_x";
			document.form.wl_key.name = "x_auth_mode";
			document.form.wl_crypto.name = "x_crypto";

			var wl_auth_mode_obj = document.createElement("input");
			var wl_wep_x_obj = document.createElement("input");
			var wl_key_obj = document.createElement("input");
			var wl_crypto_obj = document.createElement("input");

			wl_auth_mode_obj.type = "hidden";
			wl_auth_mode_obj.name = "wl_auth_mode";
			wl_auth_mode_obj.value= mssidlist[0][4];
			$("form").appendChild(wl_auth_mode_obj);

			wl_wep_x_obj.type = "hidden";
			wl_wep_x_obj.name = "wl_wep_x";
			wl_wep_x_obj.value= mssidlist[0][5];
			$("form").appendChild(wl_wep_x_obj);

			wl_key_obj.type = "hidden";
			wl_key_obj.name = "wl_key";
			wl_key_obj.value= mssidlist[0][6];
			$("form").appendChild(wl_key_obj);

			wl_crypto_obj.type = "hidden";
			wl_crypto_obj.name = "wl_crypto";
			wl_crypto_obj.value= mssidlist[0][11];
			$("form").appendChild(wl_crypto_obj);

			document.form.wl_key1.value = mssidlist[0][7];
			document.form.wl_key2.value = mssidlist[0][8];
			document.form.wl_key3.value = mssidlist[0][9];
			document.form.wl_key4.value = mssidlist[0][10];
			//$("key"+mssidlist[0][6]).value = mssidlist[0][6+parseInt(mssidlist[0][6])];

			document.form.wl_wpa_psk.value = mssidlist[0][12];
			document.form.wl_wpa_mode.value = mssidlist[0][13];

		}
		else{
			var wep11 = eval('document.form.wl_key'+document.form.wl_key.value);
			wep11.value = document.form.wl_asuskey1.value;
		}

		mbss[0] = new Array(mbss_idx.length);
		mbss[1] = new Array(mbss_idx.length);
		mbss[2] = new Array(mbss_idx.length);
		if(free_mssid == 0)
			free_mssid = 4;
		for(var i=1; i<free_mssid; i++){
			for(var j=0; j<mbss_idx.length; j++){
				mbss[i-1][j] = document.createElement("input");
				mbss[i-1][j].type = "hidden";
				mbss[i-1][j].name = "mbss"+i+"_"+mbss_idx[j];
				mbss[i-1][j].value= mssidlist[i][j+2];
				$("form").appendChild(mbss[i-1][j]);
			}
		}
		stopFlag = 1;

		if((auth_mode == "shared" || auth_mode == "wpa" || auth_mode == "wpa2" || auth_mode == "radius")
				&& document.form.wps_enable.value == "1"){
			document.form.wps_enable.value = "0";
			document.form.action_script.value = "WPS_apply";
		}
		document.form.wsc_config_state.value = "1";

		if(auth_mode == "wpa" || auth_mode == "wpa2" || auth_mode == "radius"){
			document.form.target = "";
			document.form.next_page.value = "/Advanced_WSecurity_Content.asp";
		}

		document.form.flag.value = "mssid";
		document.form.action_mode.value = " Apply ";
		document.form.submit();
	}
}

function clearField()
{
	if(document.form.wl_wpa_psk.value == "Please type Password")
		document.form.wl_wpa_psk.value = "";
}
</script>
</head>

<body class="statusbody" BGCOLOR="#A7D2E2" onload="initial();">
<iframe name="hidden_frame" id="hidden_frame" width="0" height="0" frameborder="0"></iframe>
<form method="post" name="form" id="form" action="/start_apply2.htm">
<input type="hidden" name="current_page" value="">
<input type="hidden" name="next_page" value="/">
<input type="hidden" name="sid_list" value="WLANConfig11b;">
<input type="hidden" name="group_id" value="">
<input type="hidden" name="action_mode" value="">
<input type="hidden" name="action_script" value="">
<input type="hidden" name="productid" value="<% nvram_get_x("",  "productid"); %>">
<input type="hidden" name="flag" value="">

<input type="hidden" name="wps_enable" value="<% nvram_get_x("WLANConfig11b", "wps_enable"); %>">
<input type="hidden" name="wsc_config_state" value="<% nvram_get_x("WLANConfig11b", "wsc_config_state"); %>">
<input type="hidden" name="wl_wpa_mode" value="<% nvram_get_x("WLANConfig11b", "wl_wpa_mode"); %>">
<input type="hidden" name="wl_key1" id="key1" value="">
<input type="hidden" name="wl_key2" id="key2" value="">
<input type="hidden" name="wl_key3" id="key3" value="">
<input type="hidden" name="wl_key4" id="key4" value="">

<input type="hidden" id="ssid2_mbss1" name="mbss1_ssid2" value="<% nvram_char_to_ascii("WLANConfig11b", "mbss1_ssid"); %>">
<input type="hidden" id="ssid2_mbss2" name="mbss2_ssid2" value="<% nvram_char_to_ascii("WLANConfig11b", "mbss2_ssid"); %>">
<input type="hidden" id="ssid2_mbss3" name="mbss3_ssid2" value="<% nvram_char_to_ascii("WLANConfig11b", "mbss3_ssid"); %>">

<input type="hidden" name="mbss1_key_type" value="<% nvram_get_x("WLANConfig11b","mbss1_key_type"); %>">
<input type="hidden" name="mbss2_key_type" value="<% nvram_get_x("WLANConfig11b","mbss2_key_type"); %>">
<input type="hidden" name="mbss3_key_type" value="<% nvram_get_x("WLANConfig11b","mbss3_key_type"); %>">


<input type="hidden" name="wl_ssid2" value="<% nvram_char_to_ascii("WLANConfig11b", "wl_ssid"); %>">
<input type="hidden" name="wl_wpa_psk_org" value="<% nvram_char_to_ascii("WLANConfig11b", "wl_wpa_psk"); %>">
<input type="hidden" name="wl_key_type" value="<% nvram_get_x("WLANConfig11b","wl_key_type"); %>">
<input type="hidden" name="wl_key1_org" value="<% nvram_char_to_ascii("WLANConfig11b", "wl_key1"); %>">
<input type="hidden" name="wl_key2_org" value="<% nvram_char_to_ascii("WLANConfig11b", "wl_key2"); %>">
<input type="hidden" name="wl_key3_org" value="<% nvram_char_to_ascii("WLANConfig11b", "wl_key3"); %>">
<input type="hidden" name="wl_key4_org" value="<% nvram_char_to_ascii("WLANConfig11b", "wl_key4"); %>">
<input type="hidden" name="nolan" id="nolan" value="0">
<input type="hidden" name="nowan" id="nowan" value="0">
<input type="hidden" name="wl_gmode" value="<% nvram_get_x("WLANConfig11b","wl_gmode"); %>">
<input type="hidden" name="matrix_index" id="matrix_index" value="0">


<table width="350" border="1" align="center" cellpadding="4" cellspacing="0" bordercolor="#6b8fa3" class="table1px">
  <tr>
    <th id="toolbar-th">
    	SSID <input id="Add-SSID-btn" type="button" class="sbtn" value="<#CTL_add#>" onclick="add_ssid_event();" style="display:none;padding:0 2px; margin:0px;">
    </th>
    <td id="toolbar-td">
    	<div id="mssid_arrow" onclick="pullMSSIDList(this);" title="<#CTL_MultiSSID#>"></div>
		  <div id="MSSIDList_Block" class="MSSIDList_Block">
				<div id="other_ssid1" title="1"></div>
				<div id="other_ssid2" title="2"></div>
				<div id="other_ssid3" title="3"></div>
		  </div>
		  <div id="ssid_input" class="input" abbr="0" style="padding:1px; width:158px; height:20px;">
      	<input type="text" id="ssid" name="wl_ssid" value="<% nvram_get_x("WLANConfig11b", "wl_ssid"); %>" maxlength="32" size="22" autocomplete="off"  onClick="hideMSSID_Block();" class="nocssinput" onblur="if(!mssid_duplicate(this)){return false;};" onfocus="parent.showHelpofDrSurf(0, 16);" />
      	<input type="checkbox" name="ssid_check" id="ssid_check0" style="float:right;" onclick="" disabled="disabled" checked>
      </div>
			<div id="ssid_help" class="redArrow" style="display:none;">
			<#CTL_redArrow_desc#>
			</div>
    </td>
  </tr>
  <tr>
    <th width="150"><#WLANConfig11b_AuthenticationMethod_itemname#></th>
    <td width="200">
      <select id="auth_mode" name="wl_auth_mode" class="input" onfocus="parent.showHelpofDrSurf(0, 5);" onchange="change_auth_mode2(this);">
		<option value="open" <% nvram_match_x("WLANConfig11b","wl_auth_mode", "open","selected"); %>>Open System</option>
		<option value="shared" <% nvram_match_x("WLANConfig11b","wl_auth_mode", "shared","selected"); %>>Shared Key</option>
		<option value="psk" <% nvram_double_match_x("WLANConfig11b", "wl_auth_mode", "psk", "WLANConfig11b", "wl_wpa_mode", "1", "selected"); %>>WPA-Personal</option>
		<option value="psk" <% nvram_double_match_x("WLANConfig11b", "wl_auth_mode", "psk", "WLANConfig11b", "wl_wpa_mode", "2", "selected"); %>>WPA2-Personal</option>
		<option value="psk" <% nvram_double_match_x("WLANConfig11b", "wl_auth_mode", "psk", "WLANConfig11b", "wl_wpa_mode", "0", "selected"); %>>WPA-Auto-Personal</option>
		<option value="wpa" <% nvram_double_match_x("WLANConfig11b", "wl_auth_mode", "wpa", "WLANConfig11b", "wl_wpa_mode", "3", "selected"); %>>WPA-Enterprise</option>
		<option value="wpa2" <% nvram_match_x("WLANConfig11b", "wl_auth_mode", "wpa2", "selected"); %>>WPA2-Enterprise</option>
		<!--<option value="wpa" <% nvram_double_match_x("WLANConfig11b", "wl_auth_mode", "wpa", "WLANConfig11b", "wl_wpa_mode", "4", "selected"); %>>WPA-Auto-Enterprise</option>-->
		<option value="radius" <% nvram_match_x("WLANConfig11b","wl_auth_mode", "radius","selected"); %>>Radius with 802.1x</option>
	  </select>
    </td>
  </tr>
</table>

<div id='all_related_wep' style='display:none;'>
<table width="350" border="1" align="center" cellpadding="4" cellspacing="0" bordercolor="#6b8fa3" class="table1px">
  <tr>
	<th width="147"><#WLANConfig11b_WEPType_itemname#></th>
	<td width="203">
	  <select name="wl_wep_x" id="wl_wep_x" class="input" onfocus="parent.showHelpofDrSurf(0, 9);" onchange="change_wlweptype2(this);">
		<option value="0" <% nvram_match_x("WLANConfig11b", "wl_wep_x", "0", "selected"); %>>None</option>
		<option value="1" <% nvram_match_x("WLANConfig11b", "wl_wep_x", "1", "selected"); %>>WEP-64bits</option>
		<option value="2" <% nvram_match_x("WLANConfig11b", "wl_wep_x", "2", "selected"); %>>WEP-128bits</option>
	  </select>
	</td>
  </tr>
</table>

<div id='all_wep_key' style='display:none;'>
<table width="350" border="1" align="center" cellpadding="4" cellspacing="0" bordercolor="#6b8fa3" class="table1px">
  <tr>
    <th width="152"><#WLANConfig11b_WEPDefaultKey_itemname#></th>
    <td width="198">
      <select name="wl_key" id="key" class="input" onfocus="parent.showHelpofDrSurf(0, 10);" onchange="change_wlweptype2(document.form.wl_wep_x);">
        <option value="1" <% nvram_match_x("WLANConfig11b", "wl_key", "1", "selected"); %>>Key1</option>
        <option value="2" <% nvram_match_x("WLANConfig11b", "wl_key", "2", "selected"); %>>Key2</option>
        <option value="3" <% nvram_match_x("WLANConfig11b", "wl_key", "3", "selected"); %>>Key3</option>
        <option value="4" <% nvram_match_x("WLANConfig11b", "wl_key", "4", "selected"); %>>Key4</option>
      </select>
    </td>
  </tr>
  <tr>
    <th><#WLANConfig11b_WEPKey_itemname#></th>
    <td>
      <input type="text" id="sta_asuskey1" name="wl_asuskey1" onfocus="show_wepkey_help();" onKeyUp="return change_wlkey(this, 'WLANConfig11b');" value="" size="22" class="input"/>
    </td>
  </tr>
</table>
</div>
</div>

<div id='wl_crypto' style='display:none;'>
<table width="350" border="1" align="center" cellpadding="4" cellspacing="0" bordercolor="#6b8fa3" class="table1px">
  <tr>
	<th width="139.5"><#WLANConfig11b_WPAType_itemname#></th>
	<td>
	  <select name="wl_crypto" class="input" onfocus="parent.showHelpofDrSurf(0, 6);" onchange="wl_auth_mode_change2(2);">
		<option value="tkip" <% nvram_match_x("WLANConfig11b", "wl_crypto", "tkip", "selected"); %>>TKIP</option>
		<option value="aes" <% nvram_match_x("WLANConfig11b", "wl_crypto", "aes", "selected"); %>>AES</option>
		<option value="tkip+aes" <% nvram_match_x("WLANConfig11b", "wl_crypto", "tkip+aes", "selected"); %>>TKIP+AES</option>
	  </select>
	</td>
  </tr>
</table>
</div>

<div id='wl_wpa_psk' style='display:none'>
<table width="350" border="1" align="center" cellpadding="4" cellspacing="0" bordercolor="#6b8fa3" class="table1px">
  <tr>
    <th width="139.5"><#WPA-PSKKey#></th>
    <td>
      <input type="text" id="sta_wpa_psk" name="wl_wpa_psk" onfocus="parent.showHelpofDrSurf(0, 7);clearField();" value="" size="22" maxlength="64" class="input"/>
    </td>
  </tr>
</table>
</div>

<table width="350" border="1" align="center" cellpadding="4" cellspacing="0" bordercolor="#6b8fa3" class="table1px">
  <tr id="wl_radio_tr">
    <th width="139.5"><#Wireless_Radio#></th>
		<td>
	  	<input type="radio" name="wl_radio_x" value="1" <% nvram_match_x("WLANConfig11b", "wl_radio_x", "1", "checked"); %>>on
	  	<input type="radio" name="wl_radio_x" value="0" <% nvram_match_x("WLANConfig11b", "wl_radio_x", "0", "checked"); %>>off
		</td>
  </tr>
  <tr id="mssid_network_tr" style="display:none;">
    <th><#mssid_network_access#></th>
		<td>
	  	<select name="mssid_network" class="input" onchange="set_mssid_network(this);">
				<option value="0">Both</option>
				<option value="1">LAN Only</option>
				<option value="2">Internet Only</option>
	  	</select>
		</td>
  </tr>
  <tr id="mssid_qos_tr" style="display:none;">
    <th><#mssid_qos_priority#></th>
		<td>
	  	<select id="priority" name="priority" class="input">
				<option value="1"><#Priority_Level_2#></option>
				<option value="0"><#Priority_Level_3#></option>
	  	</select>
		</td>
  </tr>
  <tr id="mssid_closed_tr" style="display:none;">
    <th><#WLANConfig11b_x_BlockBCSSID_itemname#></th>
		<td>
			<input type="radio" value="1" id="mbss_closed1" name="closed"><#checkbox_Yes#>
			<input type="radio" value="0" id="mbss_closed0" name="closed" checked><#checkbox_No#>
		</td>
  </tr>
  <tr id="apply_tr">
    <td colspan="2">
    <input id="applySecurity" type="button" class="button" value="<#CTL_onlysave#>" onclick="applyForm();" style="margin-left:110px;">
    </td>
  <tr>
    <th><#LAN_IP#></th>
    <td id="LANIP"></td>
  </tr>
  <tr>
    <th><#PIN_code#></th>
    <td id="PINCode"></td>
  </tr>
  <tr>
    <th><#MAC_Address#></th>
    <td id="MAC"></td>
  </tr>
  <tr id="wps_pbc_tr">
    <th>WPS</th>
    <td>
	<img src="/images/EZSetup_button.gif" style="cursor:pointer;" align="absmiddle" title="<#WPS_PBCbutton_hint#>" id="wpsPBC_button" onClick="wpsPBC(this);">
	<span id="WPS_PBCbutton_hint_span"></span>
	</td>
	</tr>
</table>
</form>

<select id="Router_domore" class="domore" onchange="domore_link(this);">
	<option value="../index.asp"><#MoreConfig#>...</option>
	<option value="../Advanced_Wireless_Content.asp"><#menu5_1#>-<#menu5_1_1#></option>
	<option value="../Advanced_WWPS_Content.asp"><#menu5_1_2#></option>
	<option value="../Advanced_LAN_Content.asp"><#menu5_2_1#></option>
	<option value="../Advanced_DHCP_Content.asp"><#menu5_2_2#></option>
	<!--<option value="../Advanced_GWStaticRoute_Content.asp"><#menu5_2_3#></option>-->
	<option value="../Main_LogStatus_Content.asp"><#menu5_7_2#></option>
</select>

<form method="post" name="WPSForm" id="WPSForm" action="/start_apply.htm">
	<input type="hidden" name="current_page" value="">
	<input type="hidden" name="next_page" value="">
	<input type="hidden" name="sid_list" value="WLANConfig11b;">
	<input type="hidden" name="group_id" value="">
	<input type="hidden" name="action_mode" value="">
	<input type="hidden" name="action_script" value="">
	<input type="hidden" name="wps_method" value="<% nvram_get_x("WLANConfig11b", "wps_method"); %>">
	<input type="hidden" name="wps_pbc_force" value="<% nvram_get_x("WLANConfig11b", "wps_pbc_force"); %>">
	<input type="hidden" name="pbc_overlap" value="<% nvram_get_x("WLANConfig11b", "pbc_overlap"); %>">
</form>

<form method="post" name="stopPINForm" id="stopPINForm" action="/start_apply.htm" target="hidden_frame">
	<input type="hidden" name="current_page" value="">
	<input type="hidden" name="sid_list" value="WLANConfig11b;">
	<input type="hidden" name="group_id" value="">
	<input type="hidden" name="action_mode" value="">
	<input type="hidden" name="wps_config_command" value="<% nvram_get_x("WLANConfig11b", "wps_config_command"); %>">
</form>
</body>
</html>