echo -------------------------------------------------
echo are you sure want to delete svn files?
choice /C:YN 

IF ERRORLEVEL 2 goto normal_end

cd user
del /q rc\*.c
del /q rc\*.h
del /q httpd\*.c 
del /q httpd\*.h
del /q shared\defaults.c
del /q shared\flash.default
del /q www_ver\*
del /q dict\*
del /s /q www\www_DSL-N10
echo REMOVE_SVN
cd ..


:normal_end