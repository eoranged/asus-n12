sh ./rmbak.sh
cp -f CfgCfg/.config_n12u config/.config
cp -f LnxCfg/.config_n12u linux-2.6.21.x/.config
cp -f ver/wsc_n12.h linux-2.6.21.x/drivers/net/wireless/rt2860v2/include/wsc.h
cp -f ver/rt2860v2_ap.o.n12 user/extra_bin/rt2860v2_ap.o
cp -f ver/rt2860v2_sta.o.n12 user/extra_bin/rt2860v2_sta.o
cp -f ver/n12.txt vendors/Ralink/RT3052/version.conf
cp -f ver/n12m.txt vendors/Ralink/RT3052/Makefile
cp -f ver/Makefile_rc_n12 user/rc/Makefile
cp -f ver/mydef_n12.h mydef.h
rm -f user/www/www_DSL-N10/*.dict
cp -f ModelConv/N12U/*.dict user/www/www_DSL-N10
cp -f www_ver/index_n12.asp user/www/www_DSL-N10/index.asp
cp -f www_ver/router_n12.asp user/www/www_DSL-N10/device-map/router.asp
cp -f www_ver/Advanced_WAdvanced_Content_n12.asp user/www/www_DSL-N10/Advanced_WAdvanced_Content.asp
cp -f www_ver/map-iconRouter_n12.gif user/www/www_DSL-N10/images/map-iconRouter.gif
cp -f www_ver/map-iconRouter_d_n12.gif user/www/www_DSL-N10/images/map-iconRouter_d.gif
cp -f www_ver/iframe-iconRouter_n12.gif user/www/www_DSL-N10/images/iframe-iconRouter.gif
cp -f www_ver/old_map-iconRouter_n12.gif user/www/www_DSL-N10/images/old_map-iconRouter.gif
cp -f www_ver/old_map-iconRouter_d_n12.gif user/www/www_DSL-N10/images/old_map-iconRouter_d.gif
cp -f www_ver/old_iframe-iconRouter_n12.gif user/www/www_DSL-N10/images/old_iframe-iconRouter.gif
cp -f www_ver/upnp_xml_n12.sh user/linux-igd.asus/upnp_xml.sh
cp -f www_ver/upnp_xml_n12.sh user/linux-igd/upnp_xml.sh
cp -f www_ver/gatedesc_n12.xml user/linux-igd.asus/etc/gatedesc.xml
cp -f www_ver/gatedesc_n12.xml user/linux-igd/etc/gatedesc.xml
cp -f www_ver/test_n12u.zip user/www/www_DSL-N10
rm -rf romfs
make oldconfig_linux
make
echo N12____N12
